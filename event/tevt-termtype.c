/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glib.h>
#include <ttyutils.h>
#include <sqlite3.h>
#include "tevt.h"

static int
t_event_termtype_query (const char *no, int *port)
{
  char type[2] = {0,}, printer[2] = {0,}, magcard[2] = {0,};
  char keypad[2]={0,}, lp[2] = {0,}, ic[2] = {0,};
  char oth1[2] = {0,}, oth2[2] = {0,};
  char no2[16] = {0,};
  int ret;

  strncpy(no2, no, 9);
  no2[9] = no[10];
  no2[10] = no[12];
  no2[11] = no[13];
  no2[12] = no[15];
  no2[13] = no[16];
  no2[14] = no[17];

  ret = DCC_OpenDB();
  if (ret != 0)
    {
      g_warning ("不能连接到外设配置数据库: %d\n", ret);
      return -1;
    }
  ret = DCC_GetTermInfoByLogicNo(no2, type, printer,
		  magcard, keypad, lp, ic, oth1, oth2);
  if (ret != 0)
    {
      g_warning ("查询'%s'的外设配置信息出错: %d\n", no, ret);
      DCC_CloseDB();
      return -1;
    }
  DCC_CloseDB();

  *port = 2;

  switch (type[0])
    {
    case '1':
    case '4':
      return 1;
    case '2':
    case '3':
      return 3;
    default:
      g_warning ("终端类型不支持，将使用默认的实达!");
      break;
    }
  return -1;
}


void
t_event_termtype(const char *tty)
{
  gchar no[32] = {0,};
  sqlite3 *db;
  int rc, termtype, port;
  char *sql, *errmsg = NULL;

  t_rpc_call_snap_getxy	(7, 35, 18, no);

  termtype = t_event_termtype_query (no, &port);
  if (termtype == -1)
    {
      g_warning ("不能获取终端'%s'的配置信息", no);
      termtype = 1; /* 默认使用实达 */
      port = 2;
    }

  rc = sqlite3_open (DBNAME, &db);
  if (rc != SQLITE_OK)
    {
      g_warning ("打开数据库 %s 失败: %s\n", DBNAME, sqlite3_errmsg(db));
      sqlite3_close(db);
      return;
    }
  sqlite3_busy_timeout (db, 1000 * 10);

  sql = sqlite3_mprintf ("delete from Ttyinfo where ttyname=%Q or logicno=%Q;",
		  	tty, no);
  sqlite3_exec (db, sql, NULL, 0, &errmsg);

  sql = sqlite3_mprintf ("insert into Ttyinfo values(NULL, "
		  	 "%Q, %Q, %d, %d, NULL, NULL, 0);",
		         tty, no, termtype, port);
  rc = sqlite3_exec (db, sql, NULL, 0, &errmsg);
  sqlite3_free (sql);
  if (rc)
    {
      g_warning ("插入数据库失败: %s\n", errmsg);
      sqlite3_free(errmsg);
    }
  sqlite3_close(db);
}
