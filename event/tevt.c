/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <ttyutils.h>
#include <syslog.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "tevt.h"

static gboolean		opt_version = FALSE;
static gchar		*opt_event = 0;
static gchar		*opt_tty = NULL;
static gchar		*opt_logfile = NULL;
static FILE		*t_logfile = NULL;

static GOptionEntry option_entries[] = 
{
    { "version", 'v', 0, G_OPTION_ARG_NONE, &opt_version,
      N_("Output version information and exit"), NULL },

    { "event", 'e', 0, G_OPTION_ARG_STRING, &opt_event,
      N_("Specify event type number"), NULL },

    { "tty", 'p', 0, G_OPTION_ARG_STRING, &opt_tty,
      N_("Specify tty"), NULL },

    { "logfile", 'g', 0, G_OPTION_ARG_STRING, &opt_logfile,
      N_("Log message to file"), "file" },

    { NULL, }
};

static void
t_event_close_logfile (void)
{
  if (t_logfile)
    {
      fclose (t_logfile);
      t_logfile = NULL;
    }
}

static void
t_event_print_handler (const gchar *string)
{
  if (t_logfile)
    {
      fprintf (t_logfile, "%s", string);
      fflush (t_logfile);
    }
}

int
main (int argc, char *argv[])
{
  GOptionContext *opt_ctx;
  GError *error = NULL;
  TRpcTarget target;

  t_set_locale ();
  g_set_prgname (argv[0]);
  g_set_application_name ("ttyevt");
  t_init();

  opt_ctx = g_option_context_new (NULL);
  g_option_context_add_main_entries (opt_ctx, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (opt_ctx, &argc, &argv, &error))
    {
      if (error)
	{
	  g_warning (_("parse command line error: %s"), error->message);
	  g_error_free (error);
	}
      else
	{
	  g_warning (_("An unknown error occurred when parse command line."));
	}
      g_option_context_free (opt_ctx);
      return 1;
    }
  g_option_context_free (opt_ctx);
  opt_ctx = NULL;

  if (opt_logfile)
    {
      t_logfile = fopen (opt_logfile, "a+");
      if (!t_logfile)
	{
	  g_warning (_("open %s error: %s"), opt_logfile, g_strerror (errno));
	  return 1;
	}
      g_free (opt_logfile);
      opt_logfile = NULL;

      g_log_set_default_handler (t_debug_log_handler, t_logfile);
      g_atexit (t_event_close_logfile);

      g_set_print_handler (t_event_print_handler);
      g_set_printerr_handler (t_event_print_handler);
    }
  else
    {
      openlog (g_get_prgname (), LOG_PID, LOG_LOCAL3);
      g_log_set_default_handler (t_debug_log_handler, NULL);

      g_set_print_handler (t_debug_print_handler);
      g_set_printerr_handler (t_debug_print_handler);
    }

  if (!opt_tty)
    {
      g_warning ("missing --tty option.");
      return 1;
    }
  target.rt_timeout = 5;
  target.rt_type = RPC_TARGET_TTY;
  target.rt_tty = opt_tty;

  if (!t_rpc_call_set_target (&target))
    {
      g_warning ("connect target with tty '%s' failed.", opt_tty);
      return 1;
    }
  if (strcasecmp (opt_event, "termtype") == 0)
    t_event_termtype(opt_tty);
  else if (strcasecmp (opt_event, "login") == 0)
    t_event_login(opt_tty);
  else if (strcasecmp (opt_event, "reset") == 0)
    t_event_reset();
  else if (strcasecmp (opt_event, "eval") == 0)
    t_event_eval(opt_tty);
  else
    g_warning ("event %s unsupported.", opt_event);

  t_uninit ();
  return 0;
}
