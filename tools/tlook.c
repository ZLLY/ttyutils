/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <ttyutils.h>
#ifdef DMALLOC
#include <dmalloc.h>
#endif

#ifdef  G_LOG_DOMAIN
# undef G_LOG_DOMAIN
#endif
#define G_LOG_DOMAIN	"Ttylook"


#define	TERM_CHAR	0x1C		/* ctrl + '\' */
#define RECORD_CHAR	0x19		/* ctrl + 'y' */

typedef struct termios		termios_t;

typedef enum _TExitReason
{
  BY_UNKNOWN,
  BY_USER,
  BY_EOF,
  BY_STDIO,
  BY_SERVER,
} TExitReason;

/* save terminal originality state.
 */
static termios_t	 terminal_state;
static GMainLoop	*t_main_loop = NULL;
static TShmemEntry 	*t_target_entry = NULL;
static TUnixSocket	*t_unix_socket = NULL;
static GIOChannel 	*t_socket_channel = NULL;
static GIOChannel 	*t_stdin_channel = NULL;
static GIOChannel	*t_stdout_channel = NULL;
static TExitReason  	 t_exit_reason = BY_UNKNOWN;

static gboolean     opt_version = FALSE;
static gchar       *opt_tty = NULL;
static gchar       *opt_pts = NULL;
static gint         opt_pid = -1;
static gchar       *opt_unixpath = NULL;
static gboolean     opt_writable = FALSE;

static GOptionEntry option_entries[] = 
{
    { "version", 'v', 0, G_OPTION_ARG_NONE, &opt_version,
      N_("Output version information and exit"), NULL },

    { "tty", 't', 0, G_OPTION_ARG_STRING, &opt_tty,
      N_("Which terminal to connect"), "ttyname" },

    { "pts", 'p', 0, G_OPTION_ARG_STRING, &opt_pts,
      N_("Which pseudo terminal to connect"), "ptsname" },

    { "pid", 'd', 0, G_OPTION_ARG_INT, &opt_pid,
      N_("Which process to connect"), "pid" },

    { "unixpath", 'u', 0, G_OPTION_ARG_STRING, &opt_unixpath,
      N_("Which unix socket path to connect, rare"), "path" },

    { "writable", 'w', 0, G_OPTION_ARG_NONE, &opt_writable,
      N_("Writable mode, default is readonly"), NULL },

    { NULL, }
};

/** 
 * t_reset_terminal:
 */
static void
t_reset_terminal (void)
{
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &terminal_state);
}

/** 
 * t_setup_terminal:
 * 
 * @Returns: TRUE if success.
 */
gboolean
t_setup_terminal (void)
{
  termios_t state;

  if (!isatty (STDIN_FILENO))
    {
      g_warning (_("STDIN_FILENO does not refer a terminal"));
      return FALSE;
    }

  /* save default terminal attributes.
   */
  tcgetattr (STDIN_FILENO, &state);

  terminal_state = state;

  /* change terminal attribute.
   */
  state.c_lflag &= ~(ICANON | IEXTEN | ISIG | ECHO);
  //state.c_iflag &= ~(ICRNL | INLCR | INPCK | ISTRIP | IXON | BRKINT);
  state.c_iflag &= ~(ICRNL | INLCR | ISTRIP | IXON | BRKINT);
  state.c_oflag &= ~(OPOST | ONLCR);
#ifdef OXTABS
  state.c_oflag &= ~OXTABS;
#endif
  state.c_cflag |= CS8;

  state.c_cc[VMIN]  = 1;
  state.c_cc[VTIME] = 0;

  tcsetattr (STDIN_FILENO, TCSAFLUSH, &state);

  g_atexit (t_reset_terminal);

  tcflush (STDIN_FILENO, TCIFLUSH);

  return TRUE;
}

/** 
 * t_write_channel:
 * @channel 
 * @buff 
 * @length 
 */
static void
t_write_channel (GIOChannel  *channel,
		 const gchar *buff,
		 gsize        length)
{
  gsize nwritten;
  const gchar *p = buff;
  GIOFlags flags;

  flags = g_io_channel_get_flags (channel);

  while (length > 0 && flags & G_IO_FLAG_IS_WRITEABLE)
    {
      g_io_channel_write_chars (channel, p, length, &nwritten, NULL);

      length -= nwritten;
      p += nwritten;
    }
}

/** 
 * t_watch_socket_io_read:
 * @channel 
 * @cond 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_watch_socket_io_read (GIOChannel  *channel,
			GIOCondition cond,
			gpointer     data)
{
  gchar buf[1024];
  gsize nread;
  GIOStatus status;
  GError *error = NULL;

  g_return_val_if_fail (channel, FALSE);

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      while (1)
	{
	  status = g_io_channel_read_chars (channel, buf, sizeof (buf),
					    &nread, &error);
	  switch (status)
	    {
	    case G_IO_STATUS_NORMAL:
	    case G_IO_STATUS_AGAIN:
	      break;

	    case G_IO_STATUS_ERROR:
	      if (error)
		{
		  g_warning (_("read socket error: %s"), error->message);
		  g_error_free (error);
		}
	      return FALSE;

	    case G_IO_STATUS_EOF:
	      t_exit_reason = BY_SERVER;
	      return FALSE;
	    }

	  /* write receive data to stdout.
	   */
	  t_write_channel (t_stdout_channel, buf, nread);

	  if (nread < sizeof (buf))
	    break;
	}
    }

  if (cond & G_IO_ERR || cond & G_IO_HUP)
    {
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_watch_socket_hangup:
 * @data 
 */
static void
t_watch_hangup (gpointer data)
{
  TExitReason reason = GPOINTER_TO_UINT (data);

  if (t_exit_reason == BY_UNKNOWN)
    {
      t_exit_reason = reason;
    }
  g_main_loop_quit (t_main_loop);
}

/** 
 * t_find_server:
 * @entry 
 * 
 * @Returns: 
 */
static gboolean
t_find_server (TShmemEntry *entry,
	       gpointer     data)
{
  gchar *ttyn;

  if (opt_tty)
    {
      ttyn = t_guess_ttyname (opt_tty);
      if (ttyn && strcmp (ttyn, entry->tty) == 0)
	{
	  g_free (ttyn);
	  return TRUE;
	}
      g_free (ttyn);
    }
  if (opt_pts)
    {
      ttyn = t_guess_ttyname (opt_pts);
      if (ttyn && strcmp (ttyn, entry->pts) == 0)
	{
	  g_free (ttyn);
	  return TRUE;
	}
      g_free (ttyn);
    }
  if (opt_unixpath && strcmp (opt_unixpath, entry->lookpath) == 0)
    {
	return TRUE;
    }
  if (opt_pid > 0 && opt_pid == entry->pid)
    {
      return TRUE;
    }

  return FALSE;
}

/** 
 * t_connect_server:
 * 
 * @Returns: TRUE on success
 */
static gboolean
t_connect_server (void)
{
  if (!t_shmem_open (FALSE))
    return FALSE;

  /* find server socket address.
   */
  t_target_entry = t_shmem_lookup_entry (t_find_server, NULL);
  t_shmem_close ();

  if (!t_target_entry)
    {
      g_warning (_("cannot decide server socket address."));
      return FALSE;
    }
  if (strlen (t_target_entry->lookpath) == 0)
    {
      g_warning (_("server has not activating look services."));
      g_free (t_target_entry);
      t_target_entry = NULL;
      return FALSE;
    }

  /* prevent connect to self.
   */
  if (strcmp (ttyname (STDOUT_FILENO), t_target_entry->pts) == 0)
    {
      g_warning (_("cannot connect to %s from this terminal(infinite loop)."),
		 t_target_entry->pts);
      g_free (t_target_entry);
      t_target_entry = NULL;
      return FALSE;
    }

  /* connect to server.
   */
  t_unix_socket = t_unix_socket_new_abstract (t_target_entry->lookpath);
  if (!t_unix_socket)
    {
      g_warning (_("create socket failed."));
      return FALSE;
    }

  t_socket_channel = t_unix_socket_get_io_channel (t_unix_socket);
  if (!t_socket_channel)
    {
      g_warning (_("no io channel for socket."));
      t_unix_socket_delete (t_unix_socket);
      return FALSE;
    }
  g_io_channel_set_buffered (t_socket_channel, TRUE);
  g_io_channel_set_encoding (t_socket_channel, NULL, NULL);
  g_io_channel_set_buffered (t_socket_channel, FALSE);
  g_io_channel_set_flags (t_socket_channel, G_IO_FLAG_NONBLOCK, NULL);

  g_io_add_watch_full (t_socket_channel, G_PRIORITY_DEFAULT,
		       G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP,
		       t_watch_socket_io_read,
		       GUINT_TO_POINTER (BY_SERVER),
		       t_watch_hangup);

  return TRUE;
}

/** 
 * t_watch_stdin_io_read:
 * @channel 
 * @cond 
 * @data 
 * 
 * @Returns: TRUE to continue, FALSE to disconnect channel.
 */
static gboolean
t_watch_stdin_io_read (GIOChannel  *channel,
		       GIOCondition cond,
		       gpointer     data)
{
  gchar buf[512];
  gsize nread;
  GIOStatus status;
  GError *error = NULL;

  g_return_val_if_fail (channel, FALSE);

  if (cond & G_IO_IN)
    {
      while (1)
	{
	  status = g_io_channel_read_chars (channel, buf, sizeof (buf),
					    &nread, &error);
	  switch (status)
	    {
	    case G_IO_STATUS_NORMAL:
	    case G_IO_STATUS_AGAIN:
	      break;

	    case G_IO_STATUS_ERROR:
	      if (error)
		{
		  g_warning (_("read stdin error: %s"), error->message);
		  g_error_free (error);
		}
	      return FALSE;

	    case G_IO_STATUS_EOF:
	      t_exit_reason = BY_EOF;
	      return FALSE;
	    }

	  /* interrupt character.
	   */
	  if (nread == 1 && buf[0] == TERM_CHAR)
	    {
	      t_exit_reason = BY_USER;
	      return FALSE;
	    }

	  /* record to file 'ttylook.rec' if user press ^w twice.
	   */
	  if (nread == 1 && buf[0] == RECORD_CHAR)
	    {
	      // TODO
	    }

	  /* if writable mode open, write to server.
	   */
	  if (opt_writable && t_socket_channel)
	    {
	      t_write_channel (t_socket_channel, buf, nread);
	    }

	  if (nread < sizeof (buf))
	    break;
	}
    }

  if (cond & G_IO_ERR)
    {
      g_warning (_("stdin io error."));
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_watch_stdio_channel:
 * 
 * @Returns: TRUE on success
 */
static gboolean
t_watch_stdio_channel (void)
{
  t_stdin_channel = g_io_channel_unix_new (STDIN_FILENO);
  if (!t_stdin_channel)
    {
      g_warning (_("create channel from stdin failed."));
      return FALSE;
    }
  g_io_channel_set_encoding (t_stdin_channel, NULL, NULL);
  g_io_channel_set_buffered (t_stdin_channel, FALSE);
  g_io_channel_set_flags (t_stdin_channel, G_IO_FLAG_NONBLOCK, NULL);

  t_stdout_channel = g_io_channel_unix_new (STDIN_FILENO);
  if (!t_stdout_channel)
    {
      g_warning (_("create channel from stdout failed."));
      return FALSE;
    }
  g_io_channel_set_encoding (t_stdout_channel, NULL, NULL);
  g_io_channel_set_buffered (t_stdout_channel, FALSE);
  g_io_channel_set_flags (t_stdout_channel, G_IO_FLAG_NONBLOCK, NULL);

  g_io_add_watch_full (t_stdin_channel, G_PRIORITY_DEFAULT,
		       G_IO_IN | G_IO_ERR,
		       t_watch_stdin_io_read,
		       GUINT_TO_POINTER (BY_STDIO),
		       t_watch_hangup);
  return TRUE;
}

/** 
 * main:
 * @argc: argument number
 * @argv: arguments
 * 
 * @Returns: 0 on success
 */
gint
main (gint argc, gchar *argv[])
{
  GOptionContext *opt_ctx;
  GError *error = NULL;
  GTimer *timer;
  struct winsize size1, size2;
  gint ttyfd;

  t_set_locale ();

  g_set_prgname (argv[0]);
  g_set_application_name ("ttylook");

  /* parse command line options
   */
  opt_ctx = g_option_context_new ("[ptsname]");
#if GLIB_MINOR_VERSION >= 12
  g_option_context_set_summary (opt_ctx, _("Watch other user login terminal"));
#endif
  g_option_context_add_main_entries (opt_ctx, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (opt_ctx, &argc, &argv, &error))
    {
      if (error)
	{
	  g_warning (_("parse command line error: %s"), error->message);
	  g_error_free (error);
	}
      else
	{
	  g_warning (_("An unknown error occurred when parse command line."));
	}
      g_option_context_free (opt_ctx);
      return 1;
    }

  g_option_context_free (opt_ctx);
  opt_ctx = NULL;

  /* print version info and exit.
   */
  if (opt_version)
    {
      t_print_version ();
      return 0;
    }

  if (!t_init ())
    return 2;

  timer = g_timer_new ();

  /* must has connect type.
   */
  if (!opt_tty && !opt_pts && (opt_pid <= 0) && argc <= 1)
    {
      if (isatty (STDOUT_FILENO))
	{
	  g_print ("\n");
	  g_print (_("unable to arresting server! please run `%s --help`."),
		   g_get_application_name ());
	  g_print ("\n\n");
	}
      return 3;
    }

  /* if not specify connect type and there has a command argument,
   * use it as pts name.
   */
  if (!opt_pts && argc > 1)
    {
      opt_pts = g_strdup (argv[1]);
    }

  /* connect to server.
   */
  if (!t_connect_server ())
    {
      g_warning (_("connect to server failed."));
      return 1;
    }

  g_free (opt_tty);
  g_free (opt_pts);
  g_free (opt_unixpath);

  if (!g_file_test (t_target_entry->pts, G_FILE_TEST_EXISTS) ||
      !g_file_test (t_target_entry->tty, G_FILE_TEST_EXISTS))
    {
      g_warning (_("either %s or %s not found."),
		 t_target_entry->pts, t_target_entry->tty);
      return 1;
    }

  /* check target terminal size.
   */
  ttyfd = open (t_target_entry->tty, O_RDONLY);
  if (ttyfd > 0)
    {
      if (ioctl (STDOUT_FILENO, TIOCGWINSZ, &size1) != -1)
	{
	  if (ioctl (ttyfd, TIOCGWINSZ, &size2) != -1)
	    {
	      if (size1.ws_row != size2.ws_row || size1.ws_col != size2.ws_col)
		{
		  fprintf (stdout, _("Target terminal size %dx%d %s our %dx%d\n"),
			   size2.ws_col, size2.ws_row,
			   (size2.ws_row > size1.ws_row || size2.ws_col > size1.ws_col) ?
			   _("greater than") : _("less than"),
			   size1.ws_col, size1.ws_row);
		  fflush (stdout);

		  /* prompt user select if operate on a terminal.
		   */
		  if (isatty (STDIN_FILENO))
		    {
		      char ch;

		      fprintf (stdout, _("Press `q' to quit, other key to continue..."));
		      fflush (stdout);

		      read (STDIN_FILENO, &ch, 1);
		      if (ch == 'q')
			{
			  close (ttyfd);
			  g_free (t_target_entry);
			  return 0;
			}
		    }
		}
	    }
	}
      close (ttyfd);
    }
  else
    {
      g_warning (_("cannot open %s: %s"), t_target_entry->pts,
		 g_strerror (errno));
      g_message (_("ignore target terminal size."));
    }

  /* print prompt.
   */
  if (isatty (STDIN_FILENO))
    {
      time_t tmn;
      struct tm *tmp;

      tmn = time (NULL);
      tmp = localtime (&tmn);

      g_print (_("Connected at %02d:%02d:%02d (%s), press 'Ctrl \\' to exit.\n"),
	       tmp->tm_hour, tmp->tm_min, tmp->tm_sec,
	       opt_writable ? _("Writable") : _("ReadOnly"));

      /* change terminal state.
       */
      t_setup_terminal ();
    }

  /* add stdin channel for watch.
   */
  if (!t_watch_stdio_channel ())
    {
      g_warning (_("setup stdio channel failed."));
      return 1;
    }

  /* enter main loop.
   */
  t_main_loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (t_main_loop);

  if (t_unix_socket)
    {
      t_unix_socket_delete (t_unix_socket);
      t_unix_socket = NULL;
    }
  if (t_stdin_channel)
    {
      g_io_channel_unref (t_stdin_channel);
      t_stdin_channel = NULL;
    }
  if (t_stdout_channel)
    {
      g_io_channel_unref (t_stdout_channel);
      t_stdout_channel = NULL;
    }

  t_reset_terminal ();

  if (isatty (STDOUT_FILENO))
    {
      gdouble elapsed;
      gchar reason[128];

      switch (t_exit_reason)
	{
	case BY_USER:
	  strcpy (reason, _("user desire"));
	  break;
	case BY_EOF:
	  strcpy (reason, _("end of file"));
	  break;
	case BY_STDIO:
	  strcpy (reason, _("stdio error"));
	  break;
	case BY_SERVER:
	  strcpy (reason, _("disconnect from server"));
	  break;
	case BY_UNKNOWN:
	default:
	  strcpy (reason, _("unknown reason"));
	  break;
	}

      elapsed = g_timer_elapsed (timer, NULL);

      g_print (_("%s interrupt by %s! elapsed %f second.\n"),
	       g_get_application_name (), reason, elapsed);
    }

  g_timer_destroy (timer);
  g_free (t_target_entry);

  return 0;
}
