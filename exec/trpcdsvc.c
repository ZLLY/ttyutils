/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <string.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tapp.h"
#include "tterm.h"
#include "tevent.h"
#include "tevtable.h"
#include "trpcdsvc.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef gint (*TrpcdSvcF)	(const gchar *buffer,
				 gsize        size,
				 gchar      **rbuffer,
				 gsize       *rsize);

typedef struct _TRpcdSvc	TRpcdSvc;
struct _TRpcdSvc
{
  guint16   code;
  TrpcdSvcF func;
};

gint	t_rpcdsvc_snap 			(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_feed 			(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_get_events		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_insert_event		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_remove_event		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_remove_event_group	(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_stdin_suspend		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_stdin_resume		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_stdout_suspend	(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_stdout_resume		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_trigger_event		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_get_cursor_pos	(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_refresh		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);
gint	t_rpcdsvc_set_env		(const gchar *buffer,
					 gsize        size,
					 gchar      **rbuffer,
					 gsize       *rsize);


static TRpcdSvc		t_rpcd_svcs[] =
{
    { RPC_CODE_SNAP,			t_rpcdsvc_snap },
    { RPC_CODE_FEED,			t_rpcdsvc_feed },
    { RPC_CODE_GET_EVENTS,		t_rpcdsvc_get_events },
    { RPC_CODE_INSERT_EVENT,		t_rpcdsvc_insert_event },
    { RPC_CODE_REMOVE_EVENT,		t_rpcdsvc_remove_event },
    { RPC_CODE_REMOVE_EVENT_GROUP,	t_rpcdsvc_remove_event_group },
    { RPC_CODE_STDIN_SUSPEND,		t_rpcdsvc_stdin_suspend },
    { RPC_CODE_STDIN_RESUME,		t_rpcdsvc_stdin_resume },
    { RPC_CODE_STDOUT_SUSPEND,		t_rpcdsvc_stdout_suspend },
    { RPC_CODE_STDOUT_RESUME,		t_rpcdsvc_stdout_resume },
    { RPC_CODE_TRIGGER_EVENT,		t_rpcdsvc_trigger_event },
    { RPC_CODE_CURSOR_XY,		t_rpcdsvc_get_cursor_pos },
    { RPC_CODE_REFRESH,			t_rpcdsvc_refresh },
    { RPC_CODE_SET_ENV,			t_rpcdsvc_set_env },

    { 0, NULL },
};

/** 
 * t_rpcdsvc_call:
 * @request 
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_call (TRpcRequest *request,
		const gchar *buffer,
		gsize        size,
		gchar      **rbuffer,
		gsize       *rsize)
{
  guint16 i, code;

  g_return_val_if_fail (request, RPC_ERR_SYSTEM);

  code = g_ntohs (request->code);

  for (i = 0; t_rpcd_svcs[i].func; i++)
    {
      if (t_rpcd_svcs[i].code == code)
	{
	  return t_rpcd_svcs[i].func (buffer, size, rbuffer, rsize);
	}
    }

  g_warning (_("bad request code: %d"), code);

  return RPC_ERR_BADCODE;
}

/** 
 * t_rpcdsvc_snap:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_snap (const gchar *buffer,
		gsize        size,
		gchar      **rbuffer,
		gsize       *rsize)
{
  TApp *app = t_app_get ();
  gchar **ret, *data;
  guint16 rows, cols, i;
  TRpcSnapOut *snapout;

  g_return_val_if_fail (rbuffer, RPC_ERR_SYSTEM);
  g_return_val_if_fail (rsize, RPC_ERR_SYSTEM);

  ret = t_screen_snapshot (app->term->screen, &rows, &cols);
  if (!ret)
    {
      return RPC_ERR_SYSTEM;
    }
  t_println ("take %d rows, %d cols, real %d rows", rows, cols,
	     g_strv_length (ret));

  *rsize = sizeof (TRpcSnapOut) + rows * cols;
  snapout = g_malloc0 (*rsize);
  *rbuffer = (gchar*) snapout;

  snapout->rows = g_htons (rows);
  snapout->cols = g_htons (cols);

  data = snapout->data;

  for (i = 0; i < rows; i++)
    {
      g_strlcpy (data, ret[i], cols);
      data += cols;
    }
  g_strfreev (ret);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_feed:
 * @buffer: in buffer
 * @size: @buffer size
 * @rbuffer: return buffer
 * @rsize: return buffer size
 *
 * @Returns: RPC_ERR_OK on success.
 */
gint
t_rpcdsvc_feed (const gchar *buffer,
		gsize        size,
		gchar      **rbuffer,
		gsize       *rsize)
{
  TRpcFeedIn *feedin;

  g_return_val_if_fail (buffer, RPC_ERR_SYSTEM);

  feedin = (TRpcFeedIn*) buffer;

  if (g_ntohl (feedin->length) > 0)
    {
      TApp *app = t_app_get ();

      t_term_feed_child (app->term, feedin->string,
			 g_ntohl (feedin->length));
    }

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvr_add_event:
 * @data 
 * @user_data 
 */
static void
t_rpcdsvc_add_event (gpointer key, gpointer value, gpointer user_data)
{
  TApp *app = t_app_get ();
  TEvent *event = T_EVENT (value);
  guint *cnt;
  GString *string;
  TEventEntry *buffer = (TEventEntry*) user_data;
  const gchar *p;
  GSList *matcher, *node;

  cnt = g_object_get_data (G_OBJECT (app), "event_number");
  g_assert (cnt);

  if ((p = t_event_get_name (event)))
    {
      g_strlcpy (buffer[*cnt].name, p, sizeof (buffer[*cnt].name));
    }
  if ((p = t_event_get_group (event)))
    {
      g_strlcpy (buffer[*cnt].group, p, sizeof (buffer[*cnt].group));
    }
  if ((p = t_event_get_program (event)))
    {
      g_strlcpy (buffer[*cnt].program, p, sizeof (buffer[*cnt].program));
    }

  matcher = t_event_get_matcher (event);
  string = g_string_new ("");

  for (node = matcher; node; node = node->next)
    {
      gchar *keyname;
      TEventCondition *cond = (TEventCondition*) node->data;

      g_assert (cond);

      if (node != matcher)
	{
	  g_string_append (string, ";;");
	}
      g_string_append_printf (string, "%d,", cond->ec_type);

      switch (cond->ec_type)
	{
	case EVENT_TYPE_CURSOR:
	  g_string_append_printf (string, "%d,%d",
				  cond->ec_cursor_row,
				  cond->ec_cursor_col);
	  break;

	case EVENT_TYPE_CONTENT:
	  g_string_append_printf (string, "%d,%d:%d,%s",
				  cond->ec_content_row,
				  cond->ec_content_col,
				  cond->ec_content_length,
				  cond->ec_content_regexpstr);
	  break;

	case EVENT_TYPE_HOTKEY:
	  keyname = t_hotkey_to_string (cond->ec_hotkey_key);
	  g_string_append_printf (string, "%s,%s",
				  keyname ? keyname : "",
				  cond->ec_hotkey_omit ? "TRUE" : "FALSE");
	  g_free (keyname);
	  break;

	default:
	  g_assert_not_reached ();
	}
    }

  g_strlcpy (buffer[*cnt].matcher, string->str,
	     sizeof (buffer[*cnt].matcher));
  g_string_free (string, TRUE);

  *cnt = *cnt + 1;
  g_object_set_data (G_OBJECT (app), "event_number", cnt);
}

/** 
 * t_rpcdsvc_get_events:
 * @buffer:
 * @size:
 * @rbuffer:
 * @rsize:
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_get_events (const gchar *buffer,
		      gsize        size,
		      gchar      **rbuffer,
		      gsize       *rsize)
{
  TApp *app = t_app_get ();
  guint len, cnt = 0;
  TEventEntry *events;

  g_return_val_if_fail (rbuffer, RPC_ERR_SYSTEM);
  g_return_val_if_fail (rsize, RPC_ERR_SYSTEM);

  if ((len = t_event_table_size (app->etable)) == 0)
    {
      t_println (_("no events"));
      return RPC_ERR_OK;
    }

  events = g_new0 (TEventEntry, len);

  g_object_set_data (G_OBJECT (app), "event_number", &cnt);

  t_event_table_foreach (app->etable, t_rpcdsvc_add_event, events);

  g_object_set_data (G_OBJECT (app), "event_number", NULL);

  *rbuffer = (gchar*) events;
  *rsize = sizeof (TEventEntry) * len;

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_insert_event:
 * @buffer 
 * @size 
 * @rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_insert_event (const gchar *buffer,
			gsize        size,
			gchar      **rbuffer,
			gsize       *rsize)
{
  TApp *app = t_app_get ();
  TEventEntry *entry;
  TEvent *event;

  g_return_val_if_fail (buffer, RPC_ERR_SYSTEM);

  if (size != sizeof (TEventEntry))
    {
      g_warning (_("data length %d for insert event is invalid."), size);
      return RPC_ERR_BADSIZE;
    }

  entry = (TEventEntry*) buffer;

  g_strstrip (entry->name);
  g_strstrip (entry->group);
  g_strstrip (entry->matcher);
  g_strstrip (entry->program);

  if (strlen (entry->name) == 0 || strlen (entry->matcher) == 0 ||
      strlen (entry->program) == 0)
    {
      g_warning (_("event name, matcher or program cannot be empty!"));
      return RPC_ERR_FORMAT;
    }

  event = t_event_new (entry->name, entry->group, entry->program);

  if (!t_event_parse_matcher (event, entry->matcher))
    {
      g_object_unref (event);
      return RPC_ERR_FORMAT;
    }

  if (!t_event_table_insert (app->etable, event))
    {
      g_object_unref (event);
      return RPC_ERR_SYSTEM;
    }

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_remove_event:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_remove_event (const gchar *buffer,
			gsize        size,
			gchar      **rbuffer,
			gsize       *rsize)
{
  TApp *app = t_app_get ();
  TEventEntry *entry;

  g_assert (size == sizeof (TEventEntry));

  entry = (TEventEntry*) buffer;

  if (!t_event_table_remove (app->etable, entry->name))
    {
      return RPC_ERR_SYSTEM;
    }
  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_remove_event_group:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_remove_event_group (const gchar *buffer,
			      gsize        size,
			      gchar      **rbuffer,
			      gsize       *rsize)
{
  TApp *app = t_app_get ();
  TEventEntry *entry;

  g_assert (size == sizeof (TEventEntry));

  entry = (TEventEntry*) buffer;

  if (!t_event_table_remove_group (app->etable, entry->group))
    {
      return RPC_ERR_SYSTEM;
    }
  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_stdin_suspend:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_stdin_suspend (const gchar *buffer,
			 gsize        size,
			 gchar      **rbuffer,
			 gsize       *rsize)
{
  TApp *app = t_app_get ();

  if (!app->stdio)
    {
      return RPC_ERR_SYSTEM;
    }
  t_stdio_suspend_stdin (app->stdio);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_stdin_resume:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_stdin_resume (const gchar *buffer,
			gsize        size,
			gchar      **rbuffer,
			gsize       *rsize)
{
  TApp *app = t_app_get ();

  if (!app->stdio)
    {
      return RPC_ERR_SYSTEM;
    }
  t_stdio_resume_stdin (app->stdio);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_stdout_suspend:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_stdout_suspend (const gchar *buffer,
			  gsize        size,
			  gchar      **rbuffer,
			  gsize       *rsize)
{
  TApp *app = t_app_get ();

  if (!app->stdio)
    {
      return RPC_ERR_SYSTEM;
    }
  t_stdio_suspend_stdout (app->stdio);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_stdout_resume:
 * @buffer 
 * @size 
 * @*rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_stdout_resume (const gchar *buffer,
			 gsize        size,
			 gchar      **rbuffer,
			 gsize       *rsize)
{
  TApp *app = t_app_get ();

  if (!app->stdio)
    {
      return RPC_ERR_SYSTEM;
    }
  t_stdio_resume_stdout (app->stdio);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_trigger_event:
 * @buffer:
 * @size:
 * @*rbuffer:
 * @rsize:
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_trigger_event (const gchar *buffer,
			 gsize        size,
			 gchar      **rbuffer,
			 gsize       *rsize)
{
  TApp *app = t_app_get ();
  TEventEntry *entry;
  TEvent *event;

  g_assert (size == sizeof (TEventEntry));

  entry = (TEventEntry*) buffer;

  event = t_event_table_lookup (app->etable, entry->name);
  if (!event)
    {
      g_warning (_("try to trigger a unexists event %s, give up."), entry->name);
      return RPC_ERR_SYSTEM;
    }
  if (!t_event_activate (event))
    {
      return RPC_ERR_SYSTEM;
    }
  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_get_cursor_pos:
 * @buffer:
 * @size:
 * @rbuffer:
 * @rsize:
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_get_cursor_pos (const gchar *buffer,
			  gsize        size,
			  gchar      **rbuffer,
			  gsize       *rsize)
{
  TApp *app = t_app_get ();
  TScreen *screen;
  TRpcCurPosOut *curpos;

  screen = app->term->screen;

  curpos = g_new0 (TRpcCurPosOut, 1);
  curpos->row = g_htons (screen->crow);
  curpos->col = g_htons (screen->ccol);

  *rbuffer = (gchar*) curpos;
  *rsize = sizeof (TRpcCurPosOut);

  return RPC_ERR_OK;
}

/**
 * t_rpcdsvc_refresh:
 * @buffer 
 * @size 
 * @rbuffer 
 * @rsize 
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_refresh (const gchar *buffer,
		   gsize        size,
		   gchar      **rbuffer,
		   gsize       *rsize)
{
  TApp *app = t_app_get ();

  t_stdio_refresh (app->stdio);

  return RPC_ERR_OK;
}

/** 
 * t_rpcdsvc_set_env:
 * @buffer:
 * @size:
 * @rbuffer:
 * @rsize:
 * 
 * @Returns: 
 */
gint
t_rpcdsvc_set_env (const gchar *buffer,
		   gsize        size,
		   gchar      **rbuffer,
		   gsize       *rsize)
{
  gchar **p = NULL;

  if (strlen (buffer) > 0)
    {
      p = g_strsplit (buffer, "=", 2);
      if (!p)
	{
	  g_warning (_("arugments for t_rpc_call_set_env () invalid."));
	  return RPC_ERR_FORMAT;
	}
      if (g_setenv (p[0], p[1], 1))
	{
	  g_strfreev (p);
	  return RPC_ERR_OK;
	}
      g_strfreev (p);
    }
  return RPC_ERR_SYSTEM;
}
