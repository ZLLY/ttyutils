/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_UNIX_SOCK_H__
#define __T_UNIX_SOCK_H__

#ifdef HAVE_SYS_SOCKIO_H
# include <sys/sockio.h>
#endif
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in_systm.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>		/* Need for TOS */
#include <arpa/inet.h>

#include <glib.h>

G_BEGIN_DECLS

typedef struct _TUnixSocket	TUnixSocket;

TUnixSocket*	t_unix_socket_new			(const gchar *path);
TUnixSocket*	t_unix_socket_new_abstract		(const gchar *path);

void 	     	t_unix_socket_delete			(TUnixSocket *socket);

void 	     	t_unix_socket_ref			(TUnixSocket *socket);
void 	     	t_unix_socket_unref			(TUnixSocket *socket);

GIOChannel*  	t_unix_socket_get_io_channel		(TUnixSocket *socket);

gchar*       	t_unix_socket_get_path			(const TUnixSocket *socket);

TUnixSocket* 	t_unix_socket_server_new		(const gchar *path);
TUnixSocket* 	t_unix_socket_server_new_abstract 	(const gchar *path);

TUnixSocket* 	t_unix_socket_server_accept		(const TUnixSocket *socket);
TUnixSocket* 	t_unix_socket_server_accept_nonblock 	(const TUnixSocket* socket);

G_END_DECLS

#endif /* __T_UNIX_SOCK_H__ */
