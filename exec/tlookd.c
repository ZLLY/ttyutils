/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <uuid/uuid.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tapp.h"
#include "tterm.h"
#include "tlookd.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

struct _TLookd
{
  gchar       *unixpath;
  TUnixSocket *unixsock;
  GSList      *clients;
};

static TLookd	*t_lookd = NULL;


/** 
 * t_lookd_io_read:
 * @channel 
 * @cond 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_lookd_io_read (GIOChannel  *channel,
		 GIOCondition cond,
		 gpointer     data)
{
  gchar buff[1024];
  gsize nread;
  GIOStatus status;
  GError *error = NULL;
  TApp *app = t_app_get ();

  g_return_val_if_fail (channel, FALSE);

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      while (1)
	{
	  status = g_io_channel_read_chars (channel, buff, sizeof (buff),
					    &nread, &error);
	  switch (status)
	    {
	    case G_IO_STATUS_NORMAL:
	    case G_IO_STATUS_AGAIN:
	      break;

	    case G_IO_STATUS_ERROR:
	    case G_IO_STATUS_EOF:
	      if (error)
		{
		  g_warning (_("lookd read error: %s"), error->message);
		  g_error_free (error);
		}
	      return FALSE;
	    }

	  /* feed to pseudo terminal.
	   */
	  t_term_feed_child (app->term, buff, nread);

	  if (nread < sizeof (buff))
	    break;
	}
    }

  if (cond & G_IO_HUP)
    {
      t_println (_("disconnect from client."));
      return FALSE;
    }
  if (cond & G_IO_ERR)
    {
      g_warning (_("read client socket channel error."));
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_lookd_disconnect:
 * 
 * @data 
 */
static void
t_lookd_disconnect (gpointer data)
{
  TUnixSocket *clisock = (TUnixSocket*) data;

  g_assert (t_lookd->clients);
  t_lookd->clients = g_slist_remove (t_lookd->clients, clisock);
  t_unix_socket_delete (clisock);

  t_shmem_open (TRUE);
  t_shmem_looknum_decrease ();
  t_shmem_close ();

  t_println ("client disconnect, remained %d clients.",
	     t_lookd_get_nclients ());
}

/** 
 * t_lookd_accept:
 * @channel 
 * @cond 
 * @data 
 * 
 * @Returns: TRUE on success
 */
static gboolean
t_lookd_accept (GIOChannel  *channel,
		GIOCondition cond,
		gpointer     data)
{
  TLookd *server = (TLookd*) data;
  TUnixSocket *clisock = NULL;
  GIOChannel *iochannel = NULL;

  if (cond & G_IO_IN)
    {
      clisock = t_unix_socket_server_accept (server->unixsock);
      if (!clisock)
	{
	  g_warning (_("accept client connect failed."));
	  return TRUE;
	}

      iochannel = t_unix_socket_get_io_channel (clisock);
      if (!iochannel)
	{
	  g_warning (_("create client channel failed."));
	  return TRUE;
	}

      server->clients = g_slist_append (server->clients, clisock);

      g_io_channel_set_buffered (iochannel, TRUE);
      g_io_channel_set_encoding (iochannel, NULL, NULL);
      g_io_channel_set_buffered (iochannel, FALSE);
      g_io_channel_set_flags (iochannel, G_IO_FLAG_NONBLOCK, NULL);

      /* add new channel to watch.
       */
      g_io_add_watch_full (iochannel, G_PRIORITY_DEFAULT,
			   G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP,
			   t_lookd_io_read, clisock,
			   t_lookd_disconnect);

      t_shmem_open (TRUE);
      t_shmem_looknum_increase ();
      t_shmem_close ();

      t_println ("new client connected, total %d clients",
		 t_lookd_get_nclients ());
    }

  if (cond & G_IO_ERR)
    {
      g_warning (_("Socket accept error."));
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_lookd_hangup:
 * @data 
 */
static void
t_lookd_hangup (gpointer data)
{
  g_warning (_("lookd hangup!"));

  t_lookd_shut ();
}

/** 
 * t_lookd_boot:
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_lookd_boot (void)
{
  uuid_t uuid;
  gchar buff[128];
  gchar *path;
  GIOChannel *channel;
  GError *error = NULL;

  if (t_lookd)
    return TRUE;

  t_lookd = g_new0 (TLookd, 1);

  uuid_generate (uuid);
  uuid_unparse (uuid, buff);
  uuid_clear (uuid);

  /* create unix socket.
   */
  path = g_build_filename ("/", "tmp", buff, NULL);
  t_lookd->unixsock = t_unix_socket_server_new_abstract (path);
  t_lookd->unixpath = path;

  if (!t_lookd->unixsock)
    {
      g_free (t_lookd);
      t_lookd = NULL;
      return FALSE;
    }

  /* set channel attributes.
   */
  channel = t_unix_socket_get_io_channel (t_lookd->unixsock);

  g_io_channel_set_buffered (channel, TRUE);
  if (g_io_channel_set_encoding (channel, NULL, &error) != G_IO_STATUS_NORMAL)
    {
      g_warning (_("change channel encoding error: %s"), error->message);
      g_error_free (error);
    }
  g_io_channel_set_buffered (channel, FALSE);

  g_io_add_watch_full (channel, G_PRIORITY_DEFAULT,
		       G_IO_IN | G_IO_ERR, 
		       t_lookd_accept, t_lookd,
		       t_lookd_hangup);

  return TRUE;
}

/** 
 * t_lookd_shut:
 */
void
t_lookd_shut (void)
{
  if (!t_lookd)
    return;

  if (t_lookd->unixsock)
    {
      t_unix_socket_delete (t_lookd->unixsock);
    }
  g_unlink (t_lookd->unixpath);
  g_free (t_lookd->unixpath);

  g_free (t_lookd);
  t_lookd = NULL;
}

/** 
 * t_lookd_get_unixpath:
 * 
 * @Returns: unix socket path name
 */
const gchar*
t_lookd_get_unixpath (void)
{
  g_return_val_if_fail (t_lookd, NULL);

  return t_lookd->unixpath;
}

/** 
 * t_lookd_get_nclients:
 * 
 * @Returns: 
 */
gint
t_lookd_get_nclients (void)
{
  g_return_val_if_fail (t_lookd, 0);

  if (t_lookd->clients)
    return g_slist_length (t_lookd->clients);

  return 0;
}

/** 
 * t_lookd_send:
 * @data 
 * @length 
 */
void
t_lookd_send (const gchar *data,
	      gssize      length)
{
  GSList *node;

  g_return_if_fail (data);

  if (!t_lookd)
    return;

  if (length < 0)
    length = strlen (data);

  /* send data to all connected clients.
   */
  for (node = t_lookd->clients; node; node = node->next)
    {
      TUnixSocket *clisock = (TUnixSocket*) node->data;
      GIOChannel *channel;
      const gchar *p = data;
      gsize nwritten, len = length;
      GIOFlags flags;

      channel = t_unix_socket_get_io_channel (clisock);
      if (!channel)
	{
	  g_critical (_("no io channel for client socket"));
	  return;
	}

      /* writable, rare.
       */
      flags = g_io_channel_get_flags (channel);
      if (!(flags & G_IO_FLAG_IS_WRITEABLE))
	continue;

      /* write to client.
       */
      while (len > 0)
	{
	  g_io_channel_write_chars (channel, p, len, &nwritten, NULL);
	  len -= nwritten;
	  p += nwritten;
	}
    }
}
