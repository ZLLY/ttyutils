/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <ttyutils.h>

#include "tstdio.h"
#include "tringbuffer.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

#define STDOUT_REFRESH_CACHE_SIZE       8192

enum
{
  INCOMING,
  SUSPEND_STDIN,
  RESUME_STDIN,
  SUSPEND_STDOUT,
  RESUME_STDOUT,
  LAST_SIGNAL
};


struct _TStdioPrivate
{
  GIOChannel  *stdin_channel;
  guint        stdin_source;
  gchar       *stdin_cache;
  gsize        stdin_cache_size;

  GIOChannel  *stdout_channel;

  gboolean     susp_in;
  gboolean     susp_out;
  TRingBuffer *stdout_cache;
};

#define T_STDIO_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), T_TYPE_STDIO, TStdioPrivate))

static guint	stdio_signals[LAST_SIGNAL] = { 0 };
static gboolean	stdio_sigttou = FALSE;

G_DEFINE_TYPE (TStdio, t_stdio, G_TYPE_OBJECT);


/** 
 * t_stdio_setup_channel:
 * @fd 
 * @Returns: 
 */
GIOChannel*
t_stdio_setup_channel (gint fd)
{
  GIOChannel *channel;

  g_return_val_if_fail (fd >= 0, NULL);

  channel = g_io_channel_unix_new (fd);
  if (!channel)
    {
      g_warning (_("cannot create channel from %s."), ttyname (fd));
      return FALSE;
    }

  g_io_channel_set_encoding (channel, NULL, NULL);
  g_io_channel_set_buffered (channel, FALSE);
  g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);

  return channel;
}

/** 
 * t_sigttou_handler:
 * @signo:
 */
static void
t_sigttou_handler (gint signo)
{
  g_warning ("sigttou trigger");
  stdio_sigttou = TRUE;
}

/** 
 * t_stdio_init:
 * @object 
 */
static void
t_stdio_init (TStdio *stdio)
{
  stdio->priv = T_STDIO_PRIVATE (stdio);

  stdio->priv->stdin_channel = t_stdio_setup_channel (STDIN_FILENO);
  stdio->priv->stdin_source = -1;
  stdio->priv->stdin_cache = g_malloc0 (512);
  stdio->priv->stdin_cache_size = 512;

  stdio->priv->stdout_channel = t_stdio_setup_channel (STDOUT_FILENO);

  stdio->priv->susp_in = FALSE;
  stdio->priv->susp_out = FALSE;
  stdio->priv->stdout_cache = t_ring_buffer_new ();

  signal (SIGTTOU, t_sigttou_handler);
}

/** 
 * t_stdio_finalize:
 * @object 
 */
static void
t_stdio_finalize (GObject *object)
{
  TStdio *stdio = T_STDIO (object);

  if (stdio->priv->stdin_cache)
    {
      g_free (stdio->priv->stdin_cache);
      stdio->priv->stdin_cache = NULL;
      stdio->priv->stdin_cache_size = 0;
    }

  if (stdio->priv->stdin_channel)
    {
      g_io_channel_unref (stdio->priv->stdin_channel);
      stdio->priv->stdin_channel = NULL;
    }
  if (stdio->priv->stdout_channel)
    {
      g_io_channel_unref (stdio->priv->stdout_channel);
      stdio->priv->stdout_channel = NULL;
    }
  if (stdio->priv->stdout_cache)
    {
      t_ring_buffer_free (stdio->priv->stdout_cache);
      stdio->priv->stdout_cache = NULL;
    }

  G_OBJECT_CLASS (t_stdio_parent_class)->finalize (object);
}

/** 
 * t_stdio_incoming_real:
 * @stdio 
 */
static void
t_stdio_incoming_real (TStdio      *stdio,
		       const gchar *incoming,
		       guint        size)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  t_println ("stdio incoming %d bytes has been processed.", size);
}

/** 
 * t_stdio_suspend_stdin_real:
 * @stdio:
 */
static void
t_stdio_suspend_stdin_real (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  t_println ("suspension stdin...");
}

/** 
 * t_stdio_resume_stdin_real:
 * @stdio 
 */
static void
t_stdio_resume_stdin_real (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  t_println ("resume stdin ...");
}

/** 
 * t_stdio_suspend_stdout_real:
 * @stdio 
 */
static void
t_stdio_suspend_stdout_real (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  t_println ("suspension stdout...");
}

/** 
 * t_stdio_resume_stdout_real:
 * @stdio 
 */
static void
t_stdio_resume_stdout_real (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  t_println ("resume stdout ...");
}

/** 
 * t_stdio_class_init:
 * @klass 
 */
static void
t_stdio_class_init (TStdioClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = t_stdio_finalize;

  g_type_class_add_private (klass, sizeof (TStdioPrivate));

  klass->incoming = t_stdio_incoming_real;
  klass->suspend_stdin = t_stdio_suspend_stdin_real;
  klass->resume_stdin = t_stdio_resume_stdin_real;
  klass->suspend_stdout = t_stdio_suspend_stdout_real;
  klass->resume_stdout = t_stdio_resume_stdout_real;

  stdio_signals[INCOMING] = g_signal_new ("incoming",
					  G_OBJECT_CLASS_TYPE (klass),
					  G_SIGNAL_RUN_LAST,
					  G_STRUCT_OFFSET (TStdioClass, incoming),
					  NULL, NULL,
					  t_marshal_VOID__POINTER_UINT,
					  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_UINT);
  stdio_signals[SUSPEND_STDIN] = g_signal_new ("suspend_stdin",
					       G_OBJECT_CLASS_TYPE (klass),
					       G_SIGNAL_RUN_FIRST,
					       G_STRUCT_OFFSET (TStdioClass, suspend_stdin),
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);
  stdio_signals[RESUME_STDIN] = g_signal_new ("resume_stdin",
					      G_OBJECT_CLASS_TYPE (klass),
					      G_SIGNAL_RUN_FIRST,
					      G_STRUCT_OFFSET (TStdioClass, resume_stdin),
					      NULL, NULL,
					      g_cclosure_marshal_VOID__VOID,
					      G_TYPE_NONE, 0);
  stdio_signals[SUSPEND_STDOUT] = g_signal_new ("suspend_stdout",
						G_OBJECT_CLASS_TYPE (klass),
						G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET (TStdioClass, suspend_stdout),
						NULL, NULL,
						g_cclosure_marshal_VOID__VOID,
						G_TYPE_NONE, 0);
  stdio_signals[RESUME_STDOUT] = g_signal_new ("resume_stdout",
					       G_OBJECT_CLASS_TYPE (klass),
					       G_SIGNAL_RUN_FIRST,
					       G_STRUCT_OFFSET (TStdioClass, resume_stdout),
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);
}

/** 
 * t_stdio_stdin_read:
 * @channel:
 * @cond:
 * @data:
 * 
 * @Returns: 
 */
gboolean
t_stdio_stdin_read (GIOChannel  *channel,
		    GIOCondition cond,
		    gpointer     data)
{
  TStdio *stdio = T_STDIO (data);
  GIOStatus status;
  GError *error = NULL;
  gsize nread;
#ifdef T_DEBUG
  gint i, j, k;
#endif

  g_return_val_if_fail (T_IS_STDIO (stdio), FALSE);

  if (cond & G_IO_IN)
    {
      status = g_io_channel_read_chars (channel,
					stdio->priv->stdin_cache,
					stdio->priv->stdin_cache_size,
					&nread, &error);
      switch (status)
	{
	case G_IO_STATUS_NORMAL:
	  break;

	case G_IO_STATUS_ERROR:
	  if (error)
	    {
	      g_warning (_("read stdin error: %s"), error->message);
	      g_error_free (error);
	    }
	  return FALSE;

	case G_IO_STATUS_EOF:
	  t_println ("read stdin get eof flag.");
	  return FALSE;

	case G_IO_STATUS_AGAIN:
	  return TRUE;
	}

      stdio->priv->stdin_cache[nread] = '\0';

#ifdef T_DEBUG
      t_println ("stdio incoming %d bytes, content was(first 48 bytes):",
		 nread);

      for (i = 0, j = 0; i < 4 && j < nread; i++, j += 12)
	{
	  g_print ("%03d  ", i + 1);

	  for (k = 0; j + k < nread && k < 12; k++)
	    {
	      g_print ("%c", g_ascii_iscntrl (stdio->priv->stdin_cache[j + k]) ?
		             '.' : stdio->priv->stdin_cache[j + k]);
	    }
	  for (; k < 12; k++)
	    {
	      g_print (" ");
	    }
	  g_print (" ");

	  for (k = 0; j + k < nread && k < 12; k++)
	    {
	      g_print (" 0x%02x", stdio->priv->stdin_cache[j + k]);
	    }
	  g_print ("\n");
	}
#endif

      if (stdio->priv->susp_in)
	{
	  t_println ("discard %d bytes beacuse stdin been suspend.", nread);
	}
      else
	{
	  g_signal_emit (stdio, stdio_signals[INCOMING], 0,
			 stdio->priv->stdin_cache, nread, NULL);
	}
    }

  if (cond & G_IO_ERR)
    {
      g_warning ("t_stdin_channel_read get condition G_IO_ERR.");
      return FALSE;
    }
  if (cond & G_IO_HUP)
    {
      g_warning ("t_stdin_channel_read get condition G_IO_HUP.");
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_stdin_disconnected:
 * @data:
 */
static void
t_stdin_disconnected (gpointer data)
{
  TStdio *stdio = T_STDIO (data);

  t_println ("stdin disconnect.");

  stdio->priv->stdin_source = -1;
}

/** 
 * t_stdio_connect_stdin:
 * @stdio 
 */
void
t_stdio_connect_stdin (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (!stdio->priv->stdin_channel)
    {
      stdio->priv->stdin_channel = t_stdio_setup_channel (STDIN_FILENO);
    }
  if (stdio->priv->stdin_source == -1)
    {
      stdio->priv->stdin_source = g_io_add_watch_full (stdio->priv->stdin_channel,
						       G_PRIORITY_DEFAULT_IDLE,
						       G_IO_IN | G_IO_ERR | G_IO_HUP,
						       t_stdio_stdin_read, stdio,
						       t_stdin_disconnected);
    }
}

/** 
 * t_stdio_disconnect_stdin:
 * @stdio:
 */
void
t_stdio_disconnect_stdin (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (stdio->priv->stdin_source != -1)
    {
      g_source_remove (stdio->priv->stdin_source);
      stdio->priv->stdin_source = -1;
    }
}

/** 
 * t_stdio_new:
 *
 * @Returns: a new #TStdio object, use g_object_unref() to free it.
 */
TStdio*
t_stdio_new (void)
{
  TStdio *stdio;

  stdio = g_object_new (T_TYPE_STDIO, NULL);

  return stdio;
}

/** 
 * t_stdio_feed_stdout:
 * @stdio 
 * @data 
 * @length 
 */
void
t_stdio_feed_stdout (TStdio      *stdio,
		     const gchar *data,
		     gssize       length,
		     gboolean     to_auxport)
{
  GIOStatus status;
  GError *error = NULL;
  gsize nwritten = 0;
  gssize length2, length3;
  const gchar *p = data;

  g_return_if_fail (T_IS_STDIO (stdio));

  if (stdio_sigttou)
    return;

  if (length == 0)
    length = strlen (data);

  length2 = length3 = length;

  t_println ("feed stdout %d bytes.", length);

  while (length3 > 0)
    {
      length = (length3 > 256) ? 256 : length3;
      length3 -= length;

      while (length > 0)
	{
	  status = g_io_channel_write_chars (stdio->priv->stdout_channel,
					     p, length, &nwritten, &error);
	  switch (status)
	    {
	    case G_IO_STATUS_NORMAL:
	    case G_IO_STATUS_AGAIN:
	      break;

	    case G_IO_STATUS_EOF:
	    case G_IO_STATUS_ERROR:
	      if (error)
		{
		  g_warning (_("write to tty error: %s"), error->message);
		  g_error_free (error);
		}
	      return;
	    }
	  length -= nwritten;
	  p += nwritten;
	}
      g_io_channel_flush (stdio->priv->stdout_channel, NULL);
    }

  /* cache
  */
  if (to_auxport)
    {
      nwritten = t_ring_buffer_size (stdio->priv->stdout_cache);
      if (nwritten + length2 > STDOUT_REFRESH_CACHE_SIZE)
	{
	  gint discard_size = nwritten + length2 - STDOUT_REFRESH_CACHE_SIZE;
	  t_ring_buffer_discard_to (stdio->priv->stdout_cache, discard_size,
				    discard_size + 100, "\033", 1);
	}
      t_ring_buffer_write (stdio->priv->stdout_cache, data, length2);
    }
}

/** 
 * t_stdio_suspend_stdin:
 * @stdio 
 *
 * suspension stdin, so user cannot input again.
 */
void
t_stdio_suspend_stdin (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (!stdio->priv->susp_in)
    {
      stdio->priv->susp_in = TRUE;
      g_signal_emit (stdio, stdio_signals[SUSPEND_STDIN], 0, NULL);
    }
}

/** 
 * t_stdio_resume_stdin:
 * @stdio 
 */
void
t_stdio_resume_stdin (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (stdio->priv->susp_in)
    {
      stdio->priv->susp_in = FALSE;
      g_signal_emit (stdio, stdio_signals[RESUME_STDIN], 0, NULL);
    }
}

/** 
 * t_stdio_suspend_stdout:
 * @stdio 
 *
 * suspension stdout, so user cannot output again.
 */
void
t_stdio_suspend_stdout (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (!stdio->priv->susp_out)
    {
      stdio->priv->susp_out = TRUE;
      g_signal_emit (stdio, stdio_signals[SUSPEND_STDOUT], 0, NULL);
    }
}

/** 
 * t_stdio_resume_stdout:
 * @stdio: #TStdio object.
 */
void
t_stdio_resume_stdout (TStdio *stdio)
{
  g_return_if_fail (T_IS_STDIO (stdio));

  if (stdio->priv->susp_out)
    {
      stdio->priv->susp_out = FALSE;
      g_signal_emit (stdio, stdio_signals[RESUME_STDOUT], 0, NULL);
    }
}

/** 
 * t_stdio_is_suspend:
 * @stdio: #TStdio object.
 * @in: if TRUE, test stdin, else test stdout.
 * 
 * @Returns: TRUE if suspend.
 */
gboolean
t_stdio_is_suspend (TStdio      *stdio,
		    gboolean     in)
{
  g_return_val_if_fail (T_IS_STDIO (stdio), FALSE);

  return in ? stdio->priv->susp_in : stdio->priv->susp_out;
}

/**
 * t_stdio_refresh:
 */
void
t_stdio_refresh (TStdio *stdio)
{
  gint size;

  t_println ("refresh ... %d", t_ring_buffer_size (stdio->priv->stdout_cache));

  tcdrain (g_io_channel_unix_get_fd (stdio->priv->stdout_channel));

  if (t_ring_buffer_size (stdio->priv->stdout_cache) <= 0)
    return;

  /* clear screen
   */
  fprintf (stdout, "\033[H\033[2J");
  fflush (stdout);

  size = t_ring_buffer_size (stdio->priv->stdout_cache);
  while (size > 0)
    {
      gchar buff[2048];
      gint nread, toread;
      GIOStatus status;
      GError *error = NULL;

      toread = MIN (size, sizeof (buff));
      nread = t_ring_buffer_read (stdio->priv->stdout_cache, buff, toread);
      if (nread < 0)
	break;

      /* append to buffer end for next read
       */
      t_ring_buffer_write (stdio->priv->stdout_cache, buff, nread);

      status = g_io_channel_write_chars (stdio->priv->stdout_channel, buff, nread,
					 NULL, &error);
      if (status == G_IO_STATUS_ERROR)
	{
	  if (error)
	    {
	      g_warning (_("write stdout channel error: %s"), error->message);
	      g_error_free (error);
	    }
	  return;
	}
      else if (status == G_IO_STATUS_AGAIN)
	{
	  g_warning (_("refresh error beacuse stdout channel busy"));
	  return;
	}

      size -= nread;

      /* wait for transfer finish.
       */
      tcdrain (g_io_channel_unix_get_fd (stdio->priv->stdout_channel));
    }
}
