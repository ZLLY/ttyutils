/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_EVENT_H__
#define __T_EVENT_H__

#include <regex.h>
#include <glib-object.h>
#include <ttyutils.h>

G_BEGIN_DECLS

#define T_TYPE_EVENT             (t_event_get_type ())
#define T_EVENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), T_TYPE_EVENT, TEvent))
#define T_EVENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), T_TYPE_EVENT, TEventClass))
#define T_IS_EVENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), T_TYPE_EVENT))
#define T_IS_EVENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), T_TYPE_EVENT))
#define T_EVENT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), T_TYPE_EVENT, TEventClass))

typedef struct _TEvent		TEvent;
typedef struct _TEventClass 	TEventClass;
typedef struct _TEventPrivate 	TEventPrivate;
typedef struct _TEventCursor	TEventCursor;
typedef struct _TEventContent	TEventContent;
typedef struct _TEventHotkey	TEventHotkey;
typedef struct _TEventCondition	TEventCondition;

struct _TEvent
{
  GObject  object;

  TEventPrivate *priv;
};

struct _TEventClass
{
  GObjectClass parent_class;

  void	(*start)	(TEvent  *event);
  void	(*stop)		(TEvent  *event);
};

struct _TEventCursor
{
  guint16 row;
  guint16 col;
};

struct _TEventContent
{
  guint16      row;
  guint16      col;
  guint16      length;
  regex_t      regexp;
  gchar       *regexpstr;
};

struct _TEventHotkey
{
  gint key;
  gboolean omit;
};

struct _TEventCondition
{
  TEventType   ec_type;
  union
    {
      TEventCursor  m_cursor;
      TEventContent m_content;
      TEventHotkey  m_hotkey;
    } ec_cond;
};

/* macro for member of m_cursur and m_content.
 */
#define ec_cursor		ec_cond.m_cursor
#define ec_content		ec_cond.m_content
#define ec_cursor_row		ec_cond.m_cursor.row
#define ec_cursor_col		ec_cond.m_cursor.col
#define ec_content_row		ec_cond.m_content.row
#define ec_content_col		ec_cond.m_content.col
#define ec_content_length	ec_cond.m_content.length
#define ec_content_regexp	ec_cond.m_content.regexp
#define ec_content_regexpstr	ec_cond.m_content.regexpstr
#define ec_hotkey_key		ec_cond.m_hotkey.key
#define ec_hotkey_omit		ec_cond.m_hotkey.omit


GType		t_event_get_type		(void) G_GNUC_CONST;

TEvent*		t_event_new			(const gchar    *name,
						 const gchar    *group,
						 const gchar    *program);

void		t_event_set_name		(TEvent          *event,
						 const gchar     *name);
const gchar*	t_event_get_name		(TEvent          *event);
void		t_event_set_group		(TEvent          *event,
						 const gchar     *group);
const gchar*	t_event_get_group		(TEvent          *event);
void		t_event_set_program		(TEvent          *event,
						 const gchar     *program);
const gchar*	t_event_get_program		(TEvent          *event);
GPid		t_event_get_child_pid		(TEvent          *event);

GSList*		t_event_get_matcher		(TEvent          *event);

gboolean	t_event_activate		(TEvent          *event);
gboolean	t_event_activate_with_args	(TEvent          *event,
						 gchar     	**args,
						 gint             nargs);

#ifdef T_DEBUG
void		t_event_print			(TEvent          *event,
						 gint            *no);
#endif

gboolean	t_event_parse_matcher		(TEvent          *event,
						 const gchar     *string);

G_END_DECLS

#endif /* __T_EVENT_H__ */
