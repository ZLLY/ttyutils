/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_RPC_H__
#define __T_RPC_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _TRpc	TRpc;

TRpc*		t_rpc_new_from_pts	(const gchar  *pts);
TRpc*		t_rpc_new_from_tty	(const gchar  *tty);
TRpc*		t_rpc_new_from_pid	(GPid          pid);
TRpc*		t_rpc_new_from_path	(const gchar  *path);
void		t_rpc_free		(TRpc         *rpc);

void		t_rpc_set_timeout	(TRpc         *rpc,
					 guint         timeout);

gboolean	t_rpc_send		(TRpc         *rpc,
					 gconstpointer data,
					 gssize        size);
gpointer	t_rpc_recv		(TRpc         *rpc,
					 gsize         size);

const gchar*	t_rpc_strerror		(guint         code);

G_END_DECLS

#endif /* __T_RPC_H__ */
