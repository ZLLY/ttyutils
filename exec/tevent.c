/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <sys/types.h>
#include <stdlib.h>
#include <pwd.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <lua.h>
#include <lauxlib.h>
#include <ttyutils.h>

#include "tapp.h"
#include "tevent.h"
#include "treaper.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

enum
{
  START,
  STOP,
  LAST_SIGNAL
};

struct _TEventPrivate
{
  gchar     *name;
  gchar     *group;
  GSList    *matcher;
  gchar     *program;
  gboolean   has_stdio;
  GPid       child_pid;
};

#define T_EVENT_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), T_TYPE_EVENT, TEventPrivate))

static guint	 event_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (TEvent, t_event, G_TYPE_OBJECT);

/** 
 * t_event_init:
 * @object:
 */
static void
t_event_init (TEvent *event)
{
  event->priv = T_EVENT_PRIVATE (event);

  event->priv->name = NULL;
  event->priv->group = NULL;
  event->priv->matcher = NULL;
  event->priv->program = NULL;
  event->priv->has_stdio = TRUE;
  event->priv->child_pid = -1;
}

/** 
 * t_event_matcher_free:
 * @data 
 * @user_data 
 */
static void
t_event_matcher_free (gpointer data,
		      gpointer user_data)
{
  TEventCondition *cond = (TEventCondition*) data;

  if (cond->ec_type == EVENT_TYPE_CONTENT)
    {
      g_free (cond->ec_content_regexpstr);
      regfree (&cond->ec_content_regexp);
    }
  g_free (cond);
}

/** 
 * t_event_finalize:
 * @object 
 */
static void
t_event_finalize (GObject *object)
{
  TEvent *event = T_EVENT (object);

  if (event->priv->name)
    {
      g_free (event->priv->name);
      event->priv->name = NULL;
    }
  if (event->priv->group)
    {
      g_free (event->priv->group);
      event->priv->group = NULL;
    }
  if (event->priv->program)
    {
      g_free (event->priv->program);
      event->priv->program = NULL;
    }

  if (event->priv->matcher)
    {
      g_slist_foreach (event->priv->matcher, t_event_matcher_free, NULL);
      g_slist_free (event->priv->matcher);
      event->priv->matcher = NULL;
    }

  G_OBJECT_CLASS (t_event_parent_class)->finalize (object);
}

/** 
 * t_event_start:
 * @event 
 */
static void
t_event_start (TEvent *event)
{
  g_return_if_fail (T_IS_EVENT (event));

  t_println ("program %s started.", event->priv->program);
}

/** 
 * t_event_stop:
 * @event 
 */
static void
t_event_stop (TEvent *event)
{
  g_return_if_fail (T_IS_EVENT (event));

  t_println ("program %s pid %d stoped.", event->priv->program,
	     event->priv->child_pid);

  event->priv->child_pid = -1;
}

/** 
 * t_event_class_init:
 * @klass 
 */
static void
t_event_class_init (TEventClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TEventPrivate));

  object_class->finalize = t_event_finalize;

  klass->start = t_event_start;
  klass->stop = t_event_stop;

  event_signals[START] = g_signal_new ("start",
					G_OBJECT_CLASS_TYPE (klass),
					G_SIGNAL_RUN_LAST,
					G_STRUCT_OFFSET (TEventClass, start),
					NULL, NULL,
					g_cclosure_marshal_VOID__VOID,
					G_TYPE_NONE, 0);
  event_signals[STOP] = g_signal_new ("stop",
				      G_OBJECT_CLASS_TYPE (klass),
				      G_SIGNAL_RUN_FIRST,
				      G_STRUCT_OFFSET (TEventClass, stop),
				      NULL, NULL,
				      g_cclosure_marshal_VOID__VOID,
				      G_TYPE_NONE, 0);
}


/** 
 * t_event_variable_repalce:
 * @input 
 * 
 * @Returns: 
 */
static gchar*
t_event_variable_repalce (TEvent *event, const gchar *input)
{
  TApp *app = t_app_get ();
  gchar *vars[] =
    {
      "@TTYEXEC_TTY@",
      "@ttyexec_tty@",
      "@TTYEXEC_PTS@",
      "@ttyexec_pts@",
      "@TTYEXEC_PID@",
      "@ttyexec_pid@",
      "@TTYEXEC_EVENT_NAME@",
      "@ttyexec_event_name@",
      "@TTYEXEC_EVENT_GROUP@",
      "@ttyexec_event_group@",
      NULL
    };
  gint i;
  gchar *p, *p2;
  GString *string;

  g_return_val_if_fail (input, NULL);

  p = g_strdup (input);

again:
  for (i = 0; vars[i]; i++)
    {
      p2 = strstr (p, vars[i]);
      if (p2)
	{
	  string = g_string_new_len (p, p2 - p);

	  switch (i)
	    {
	    case 0:		/* tty */
	    case 1:
	      g_string_append_printf (string, "%s", ttyname (STDIN_FILENO));
	      break;
	    case 2:		/* pts */
	    case 3:
	      g_string_append_printf (string, "%s", t_term_ptsname (app->term));
	      break;
	    case 4:		/* pid */
	    case 5:
	      g_string_append_printf (string, "%d", getpid ());
	      break;
	    case 6:		/* event name */
	    case 7:
	      g_string_append_printf (string, "%s", event->priv->name);
	      break;
	    case 8:		/* event group */
	    case 9:
	      g_string_append_printf (string, "%s", event->priv->group ?
				                    event->priv->group : "");
	      break;
	    default:
	      g_assert_not_reached ();
	      break;
	    }
	  g_string_append (string, p2 + strlen (vars[i]));
	  g_free (p);
	  p = g_string_free (string, FALSE);
	  goto again;
	}
    }
  return p;
}

/** 
 * t_event_new:
 * @Returns: 
 */
TEvent*
t_event_new (const gchar *name,
	     const gchar *group,
	     const gchar *program)
{
  TEvent *event;

  g_return_val_if_fail (name, NULL);
  g_return_val_if_fail (program, NULL);

  event = g_object_new (T_TYPE_EVENT, NULL);

  event->priv->name = g_strdup (name);
  g_strstrip (event->priv->name);
  event->priv->program = t_event_variable_repalce (event, program);
  g_strstrip (event->priv->program);

  /* foreground or background job.
   */
  if (event->priv->program [strlen (event->priv->program) - 1] == '&')
    {
      event->priv->has_stdio = FALSE;
      event->priv->program [strlen (event->priv->program) - 1] = '\0';
    }
  if (group)
    {
      event->priv->group = g_strdup (group);
      g_strstrip (event->priv->group);
    }
  return event;
}

/** 
 * t_event_set_name:
 * @event 
 * @name 
 */
void
t_event_set_name (TEvent      *event,
		  const gchar *name)
{
  gchar *newname;

  g_return_if_fail (T_IS_EVENT (event));
  g_return_if_fail (name);

  newname = g_strdup (name);

  if (event->priv->name)
    {
      g_free (event->priv->name);
    }
  event->priv->name = newname;
}

/** 
 * t_event_get_name:
 * @event 
 * 
 * @Returns: 
 */
const gchar*
t_event_get_name (TEvent *event)
{
  g_return_val_if_fail (T_IS_EVENT (event), NULL);

  return event->priv->name;
}

/** 
 * t_event_set_group:
 * @event 
 * @group 
 */
void
t_event_set_group (TEvent      *event,
		   const gchar *group)
{
  gchar *newgroup;

  g_return_if_fail (T_IS_EVENT (event));
  g_return_if_fail (group);

  newgroup = g_strdup (group);

  if (event->priv->group)
    {
      g_free (event->priv->group);
    }
  event->priv->group = newgroup;
}

/** 
 * t_event_get_group:
 * @event 
 * 
 * @Returns: 
 */
const gchar*
t_event_get_group (TEvent *event)
{
  g_return_val_if_fail (T_IS_EVENT (event), NULL);

  return event->priv->group;
}

/** 
 * t_event_set_program:
 * @event 
 * @program 
 */
void
t_event_set_program (TEvent      *event,
		     const gchar *program)
{
  gchar *newprog;

  g_return_if_fail (T_IS_EVENT (event));
  g_return_if_fail (program);

  newprog = g_strdup (program);

  if (event->priv->program)
    {
      g_free (event->priv->program);
    }
  event->priv->program = newprog;
}

/** 
 * t_event_get_program:
 * @event 
 * 
 * @Returns: 
 */
const gchar*
t_event_get_program (TEvent *event)
{
  g_return_val_if_fail (T_IS_EVENT (event), NULL);

  return event->priv->program;
}

/** 
 * t_event_get_child_pid:
 * @event 
 * 
 * @Returns: 
 */
GPid
t_event_get_child_pid (TEvent *event)
{
  g_return_val_if_fail (T_IS_EVENT (event), -1);

  return event->priv->child_pid;
}

/** 
 * t_event_add_condition:
 * @event 
 * @cond 
 * 
 * @Returns: 
 */
static gboolean
t_event_add_condition (TEvent          *event,
		       TEventCondition *cond)
{
  gint ret;

  g_return_val_if_fail (T_IS_EVENT (event), FALSE);
  g_return_val_if_fail (cond, FALSE);

  switch (cond->ec_type)
    {
    case EVENT_TYPE_CURSOR:
    case EVENT_TYPE_HOTKEY:
    case EVENT_TYPE_KEYBOARD:
      break;

    case EVENT_TYPE_CONTENT:
      if (!cond->ec_content_regexpstr)
	{
	  g_warning (_("event condition must has regexp."));
	  return FALSE;
	}

      ret = regcomp (&cond->ec_content_regexp,
		     cond->ec_content_regexpstr,
		     REG_EXTENDED | REG_NOSUB);
      if (ret != 0)
	{
	  gchar buff[256];

	  regerror (ret, &cond->ec_content_regexp, buff, sizeof (buff));
	  g_warning (_("construct regexp from '%s' error: %s"),
		     cond->ec_content_regexpstr, buff);
	  return FALSE;
	}
      cond->ec_content_regexpstr = g_strdup (cond->ec_content_regexpstr);

      t_println ("add event matcher condition [%ld,%ld:%ld,'%s']",
		 cond->ec_content_row,
		 cond->ec_content_col,
		 cond->ec_content_length,
		 cond->ec_content_regexpstr);
      break;

    default:
      g_warning (_("event condition type %d unsupported."), cond->ec_type);
      return FALSE;
    }

  event->priv->matcher = g_slist_append (event->priv->matcher, cond);

  return TRUE;
}

/** 
 * t_event_get_matcher:
 * @event 
 * 
 * @Returns: 
 */
GSList*
t_event_get_matcher (TEvent *event)
{
  g_return_val_if_fail (T_IS_EVENT (event), NULL);

  return event->priv->matcher;
}

/** 
 * t_event_print_matcher:
 * @data 
 * @user_data 
 */
#ifdef T_DEBUG
static void
t_event_print_matcher (gpointer data, gpointer user_data)
{
  TEventCondition *cond = (TEventCondition*) data;

  switch (cond->ec_type)
    {
    case EVENT_TYPE_CURSOR:
      g_print ("[cursor,%d,%d'] ", cond->ec_cursor_row,
	       cond->ec_cursor_col);
      break;

    case EVENT_TYPE_CONTENT:
      g_print ("[content,%d,%d:%d,'%s'] ", cond->ec_content_row,
	       cond->ec_content_col, cond->ec_content_length,
	       cond->ec_content_regexpstr);
      break;

    default:
      g_assert_not_reached ();
    }
}
#endif

/** 
 * t_event_print:
 * @event 
 * @no 
 */
#ifdef T_DEBUG
void
t_event_print (TEvent *event, gint *no)
{
  g_return_if_fail (T_IS_EVENT (event));
  g_return_if_fail (no);

  g_print ("  %d. name   : '%s', group '%s':\n", *no,
	   t_event_get_name (event), t_event_get_group (event));
  *no = *no + 1;

  g_print ("     matcher: ");
  g_slist_foreach (event->priv->matcher, t_event_print_matcher, event);
  g_print ("\n");

  g_print ("     program : %s\n", t_event_get_program (event));
}
#endif

/** 
 * t_event_child_setup:
 * @data:
 *
 * be called before child process exec but after fork.
 */
static void
t_event_child_setup (gpointer data)
{
  TEvent *event = T_EVENT (data);
  TApp *app = t_app_get ();
  gchar buff[64];

  t_println ("event child program setup.");

  g_setenv ("TTYEXEC_TTY",
            g_object_get_data (G_OBJECT (event), "ttyname"), TRUE);
  g_setenv ("TTYEXEC_PTS", t_term_ptsname (app->term), TRUE);

  /* use parent pid of child as ttyexec pid.
   */
  g_snprintf (buff, sizeof (buff), "%d", getppid ());
  g_setenv ("TTYEXEC_PID", buff, TRUE);

  g_setenv ("TTYEXEC_EVENT_NAME", event->priv->name, TRUE);
  g_setenv ("TTYEXEC_EVENT_GROUP", event->priv->group ?
	                           event->priv->group : "", TRUE);
}

/** 
 * t_event_activate:
 * @event 
 */
gboolean
t_event_activate (TEvent *event)
{
  gchar **argv;
  gchar *command;
  GError *error = NULL;
  TApp *app = t_app_get ();
  gint32 flags;
  gchar *ttyn;

  g_return_val_if_fail (T_IS_EVENT (event), FALSE);

  if (!event->priv->program)
    {
      g_warning (_("invalid program, event be cancel."));
      return FALSE;
    }
  t_println ("try to exec event program '%s'.", event->priv->program);

  argv = g_strsplit_set (event->priv->program, " \t", -1);

  /* search the program in the PATH.
   */
  if (!g_file_test (argv[0], G_FILE_TEST_IS_EXECUTABLE))
    {
      command = g_find_program_in_path (argv[0]);
      if (!command)
	{
	  g_warning (_("%s not found in PATH."), argv[0]);
	  g_strfreev (argv);
	  return FALSE;
	}
      g_free (argv[0]);
      argv[0] = command;
    }

  g_signal_emit (event, event_signals[START], 0, NULL);

  if (event->priv->has_stdio)
    {
      flags = G_SPAWN_CHILD_INHERITS_STDIN;

      /* child process will use stdin, temporary disconnect
       * stdin of ttyexec to avoid conflict.
       */
      t_stdio_disconnect_stdin (app->stdio);
    }
  else
    {
      flags = G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL;
    }
  flags |= G_SPAWN_DO_NOT_REAP_CHILD;

  ttyn = g_strdup (ttyname (STDIN_FILENO));
  g_object_set_data (G_OBJECT (event), "ttyname", ttyn);

  /* start a child process.
   */
  if (!g_spawn_async (NULL, argv, NULL, flags,
		      t_event_child_setup, event,
		      &event->priv->child_pid, &error))
    {
      if (error)
	{
	  g_warning (_("execute event program '%s' error: %s"),
		     argv[0], error->message);
	  g_error_free (error);
	}
      g_strfreev (argv);

      g_object_set_data (G_OBJECT (event), "ttyname", NULL);
      g_free (ttyn);

      g_signal_emit (event, event_signals[STOP], 0, NULL);
      return FALSE;
    }
  g_strfreev (argv);

  g_object_set_data (G_OBJECT (event), "ttyname", NULL);
  g_free (ttyn);

  t_println ("add event program id %d to reaper.", event->priv->child_pid);

  t_reaper_add_child (event->priv->child_pid);

  if (event->priv->has_stdio)
    {
      t_app_add_fore_child (app, event->priv->child_pid);
    }

  return TRUE;
}

/**
 * t_event_activate_with_args:
 * 
 * @event 
 * @args 
 * @nargs 
 * 
 * @Returns: 
 */
gboolean
t_event_activate_with_args (TEvent          *event,
			    gchar     	   **args,
			    gint             nargs)
{
  GString *newprog;
  gint i;

  g_return_val_if_fail (T_IS_EVENT (event), FALSE);
  g_return_val_if_fail (args, FALSE);

  if (!event->priv->program)
    {
      g_warning (_("invalid program, event be cancel."));
      return FALSE;
    }

  newprog = g_string_new (event->priv->program);

  for (i = 0; i < nargs; i++)
    {
      g_string_append_c (newprog, ' ');
      g_string_append (newprog, args[i]);
    }

  g_free (event->priv->program);
  event->priv->program = newprog->str;
  g_string_free (newprog, FALSE);

  return t_event_activate (event);
}

/** 
 * t_event_check_matche:
 * @data:
 * @user_data:
 */
static void
t_event_check_matcher (gpointer data,
		       gpointer user_data)
{
  TEventCondition *cond = (TEventCondition*) data;
  TEvent *event = T_EVENT (user_data);

  if (cond->ec_type == EVENT_TYPE_HOTKEY)
    {
      g_critical (_("hotkey condition of event '%s' cannot be "
		    "combine with other condition."),
		  t_event_get_name (event));
    }
}

/** 
 * t_event_parse_matcher:
 * @event:
 * @string:
 * 
 * @Returns: 
 */
gboolean
t_event_parse_matcher (TEvent       *event,
		       const gchar  *string)
{
  gchar **matcher;
  gint i;
  gboolean ret = TRUE;

  g_return_val_if_fail (T_IS_EVENT (event), FALSE);
  g_return_val_if_fail (string, FALSE);

  matcher = g_strsplit (string, ";;", -1);
  if (!matcher)
    {
      g_warning (_("event matcher string '%s' invalid."), string);
      return FALSE;
    }

  for (i = 0; i < g_strv_length (matcher); i++)
    {
      TEventCondition *cond;
      gchar **strv, **conds;

      strv = g_strsplit (matcher[i], ",", 2);
      if (!strv)
	{
	  g_warning (_("event condition '%s' invalid."), matcher[i]);
	  ret = FALSE;
	  break;
	}

      if (g_strv_length (strv) != 2)
	{
	  g_warning (_("event condition '%s' invalid."), matcher[i]);
	  g_strfreev (strv);
	  ret = FALSE;
	  break;
	}

      cond = g_new0 (TEventCondition, 1);

      cond->ec_type = g_strtod (strv[0], NULL);

      switch (cond->ec_type)
	{
	case EVENT_TYPE_CURSOR:
	  conds = g_strsplit (strv[1], ",", -1);
	  g_strfreev (strv);

	  if (g_strv_length (conds) < 2)
	    {
	      g_warning (_("cursor event condition '%s' invalid."), matcher[i]);
	      g_free (cond);
	      g_strfreev (conds);
	      ret = FALSE;
	      goto end;
	    }
	  cond->ec_cursor_row = g_strtod (conds[0], NULL);
	  cond->ec_cursor_col = g_strtod (conds[1], NULL);
	  g_strfreev (conds);
	  break;

	case EVENT_TYPE_CONTENT:
	  conds = g_strsplit (strv[1], ",", 3);
	  g_strfreev (strv);

	  if (g_strv_length (conds) < 3)
	    {
	      g_warning (_("content event condition '%s' invalid."), matcher[i]);
	      g_free (cond);
	      g_strfreev (conds);
	      ret = FALSE;
	      goto end;
	    }
	  cond->ec_content_row = g_strtod (conds[0], NULL);

	  strv = g_strsplit (conds[1], ":", -1);

	  cond->ec_content_col = g_strtod (strv[0], NULL);
	  if (strv[1])
	    {
	      cond->ec_content_length = g_strtod (strv[1], NULL);
	    }
	  g_strfreev (strv);

	  cond->ec_content_regexpstr = g_strdup (conds[2]);
	  g_strfreev (conds);
	  break;

	case EVENT_TYPE_HOTKEY:
	  conds = g_strsplit (strv[1], ",", -1);
	  g_strfreev (strv);

	  cond->ec_hotkey_key = t_hotkey_from_string (conds[0]);
	  cond->ec_hotkey_omit = conds[1] ? g_strtod (conds[1], NULL) : TRUE;
	  g_strfreev (conds);

	  if (cond->ec_hotkey_key == 0)
	    {
	      g_free (cond);
	      ret = FALSE;
	      goto end;
	    }
	  break;

	case EVENT_TYPE_KEYBOARD:
	  break;

	default:
	  g_warning (_("event condition type %s unsupported."), conds[0]);
	  g_free (cond);
	  g_strfreev (strv);
	  ret = FALSE;
	  goto end;
	}

      if (!t_event_add_condition (event, cond))
	{
	  g_warning (_("add event condition failed"));
	  g_free (cond);
	  ret = FALSE;
	  break;
	}
    }

  /* check
   */
  if (g_slist_length (event->priv->matcher) > 1)
    {
      g_slist_foreach (event->priv->matcher,
		       t_event_check_matcher, event);
    }

end:
  g_strfreev (matcher);
  return ret;
}
