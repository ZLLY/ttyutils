/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_DEBUG_H__
#define __T_DEBUG_H__

#include <glib.h>

G_BEGIN_DECLS

void		t_debug_log_handler	(const gchar   *domain,
					 GLogLevelFlags level,
					 const gchar   *message,
					 gpointer       user_data);
void		t_debug_print_handler	(const gchar   *string);

void		t_debug_print		(const char    *fname,
					 int            lineno,
					 const char    *func,
					 const char    *format,
					 ...);
void		t_debug_println		(const char    *fname,
					 int            lineno,
					 const char    *func,
					 const char    *format,
					 ...);


#ifdef G_HAVE_ISO_VARARGS

# if defined(T_DEBUG) || defined(DEBUG)
# define t_print(...)		t_debug_print (__FILE__, __LINE__, G_STRFUNC, __VA_ARGS__)
# define t_println(...)		t_debug_println (__FILE__, __LINE__, G_STRFUNC, __VA_ARGS__)
# else	/* !T_DEBUG */
# define t_print(...)
# define t_println(...)
# endif	/* T_DEBUG */

#elif defined(G_HAVE_GNUC_VARARGS)

# if defined(T_DEBUG) || defined(DEBUG)
# define t_print(format...)	t_debug_print (__FILE__, __LINE__, G_STRFUNC, format)
# define t_println(format...)	t_debug_println (__FILE__, __LINE__, G_STRFUNC, format)
# else	/* !T_DEBUG */
# define t_print(format...)
# define t_println(format...)
# endif	/* T_DEBUG */

#else

# if defined(T_DEBUG) || defined(DEBUG)

# include <stdarg.h>
# include <glib/gstdio.h>
static void
t_print (const char *fmt, ...)
{
  va_list  ap;

  va_start (ap, fmt);
  g_vfprintf (stdout, fmt, ap);
  va_end (ap);
}

static void
t_println (const char *fmt, ...)
{
  va_list  ap;

  va_start (ap, fmt);
  g_vfprintf (stdout, fmt, ap);
  va_end (ap);
  g_print ("\n");
}

# else	/* ! T_DEBUG */

static void
t_print (const char *fmt, ...) {}
static void
t_println (const char *fmt, ...) {}

# endif /* T_DEBUG */

#endif /* G_HAVE_ISO_VARARGS */

G_END_DECLS

#endif  /* __T_DEBUG_H__ */
