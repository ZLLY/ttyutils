/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_REAPER_H__
#define __T_REAPER_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define T_TYPE_REAPER			(t_reaper_get_type ())
#define T_REAPER(obj)			(G_TYPE_CHECK_INSTANCE_CAST((obj), T_TYPE_REAPER, TReaper))
#define T_REAPER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), T_TYPE_REAPER, TReaperClass))
#define T_IS_REAPER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj), T_TYPE_REAPER))
#define T_IS_REAPER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), T_TYPE_REAPER))
#define T_REAPER_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS((obj), T_TYPE_REAPER, TReaperClass))

typedef struct _TReaper 	TReaper;
typedef struct _TReaperClass 	TReaperClass;

struct _TReaper
{
  GObject object;
};

struct _TReaperClass
{
  GObjectClass parent_class;

  guint child_exited_signal;
};

GType 		t_reaper_get_type	(void);

TReaper*	t_reaper_get		(void);

gint 		t_reaper_add_child	(GPid     pid);

G_END_DECLS

#endif /* __T_REAPER_H__ */
