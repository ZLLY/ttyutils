/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <grp.h>
#include <sys/types.h>
#include <glib.h>
#include <glib/gi18n.h>

#include "tforkpty.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

/*
 * HAVE_OPENPTY => HAVE_FORKPTY
 */

/** 
 * login_tty:
 * @fd 
 * 
 * @Returns: 
 */
#ifndef HAVE_LOGIN_TTY
int
login_tty (int fd)
{
  pid_t pid = getpid ();

  /* Create the session
   */
  setsid ();

#ifdef TIOCSCTTY
  if (ioctl (fd, TIOCSCTTY, 0) == -1)
    return -1;
#else /* !TIOCSTTY */
  /* Hackery to set controlling tty on SVR4 - on SVR4 the first
   * terminal we open after sesid() becomes our controlling
   * terminal, thus we must find the name of, open, and re-close
   * the tty since we already have it open at this point.
   */
    {
      char *ctty;
      int ct_fdes;

      ctty = ttyname (fd);
      ct_fdes = open (ctty, O_RDWR);
      close (ct_fdes);
    }
#endif /* !TIOCSTTY */

#if defined (_POSIX_VERSION) || defined (__svr4__)
  tcsetpgrp (0, pid);
#elif defined (TIOCSPGRP)
  ioctl (0, TIOCSPGRP, &pid);
#endif

  dup2 (fd, 0);
  dup2 (fd, 1);
  dup2 (fd, 2);

  if (fd > 2)
    close (fd);

  return 0;
}
#endif

/** 
 * pty_open_master_bsd:
 * @pty_name 
 * @used_bsd 
 * 
 * @Returns: 
 */
#if !defined(HAVE_OPENPTY) || defined(BIND_PTY)
static int
pty_open_master_bsd (char *pty_name, int *used_bsd)
{
  int pty_master;
  char *ptr1, *ptr2;
#ifdef SCO_SV
  int i;
#endif

  *used_bsd = 1;

  /* if pty_name is not empty, then open it.
   */
  if (strlen (pty_name) > 0)
    {
      pty_master = open (pty_name, O_RDWR | O_NOCTTY);
      if (pty_master == -1)
	{
	  g_warning (_("open %s error: %s"), pty_name, g_strerror (errno));
	  return -1;
	}
      pty_name [5] = 't';
      if (access (pty_name, 6))
	{
	  close (pty_master);
	  pty_name [5] = 'p';
	  g_warning (_("open %s success, but corresponding slave doesn't exists."), pty_name);
	  return -1;
	}
      return pty_master;
    }

#ifdef SCO_SV
  for (i = 0; i < 2000; i++)
    {
      /* SCO style ptys naming.
       */
      sprintf (pty_name, "/dev/ptyp%d", i);

      /* Try to open master
       */
      pty_master = open (pty_name, O_RDWR | O_NOCTTY);
      if (pty_master == -1)
	{
#if 0
	  if (errno == ENOENT)		/* Different from EIO */
	    {
	      g_warning (_("%s not found."), pty_name);
	      return -1;		/* Out of pty devices */
	    }
	  else
#endif
	    continue;	      		/* Try next pty device */
	}

      /* Change "pty" to "tty" to get pty slave name.
       */
      pty_name [5] = 't';
      if (access (pty_name, 6))
	{
	  close (pty_master);
	  pty_name [5] = 'p';
	  continue;
	}
      return pty_master;
    }
  g_warning (_("try all 2000 pty, no one usable."));
#else
  strcpy (pty_name, "/dev/ptyXX");

  for (ptr1 = "pqrstuvwxyzPQRST"; *ptr1; ++ptr1)
    {
      pty_name [8] = *ptr1;

      for (ptr2 = "0123456789abcdef"; *ptr2; ++ptr2)
	{
	  pty_name [9] = *ptr2;

	  /* Try to open master
	   */
	  if ((pty_master = open (pty_name, O_RDWR)) == -1)
	    {
	      if (errno == ENOENT)	/* Different from EIO */
		return -1;	      	/* Out of pty devices */
	      else
		continue;	      	/* Try next pty device */
	    }
	  /* Change "pty" to "tty" to get pty slave name.
	   */
	  pty_name [5] = 't';
	  if (access (pty_name, 6))
	    {
	      close (pty_master);
	      pty_name [5] = 'p';
	      continue;
	    }
	  return pty_master;
	}
    }
#endif

  /* Ran out of pty devices
   */
  return -1;
}

/** 
 * pty_open_slave_bsd:
 * @pty_name 
 * 
 * @Returns: 
 */
static int
pty_open_slave_bsd (const char *pty_name)
{
  int pty_slave;
  struct group *group_info = getgrnam ("tty");

  if (group_info != NULL)
    {
      /* The following two calls will only succeed if we are root
       */
      chown (pty_name, getuid (), group_info->gr_gid);
      chmod (pty_name, S_IRUSR | S_IWUSR | S_IWGRP);
    }
  else
    {
      chown (pty_name, getuid (), -1);
      chmod (pty_name, S_IRUSR | S_IWUSR | S_IWGRP);
    }

#ifdef HAVE_REVOKE
  revoke (pty_name);
#endif

  if ((pty_slave = open (pty_name, O_RDWR)) == -1)
    {
      return -1;
    }

  return pty_slave;
}

/* SystemVish pty opening
 */
#if defined (HAVE_GRANTPT)

#ifdef HAVE_STROPTS_H
# include <stropts.h>
#endif

static int
pty_open_slave (const char *pty_name)
{
  int pty_slave = open (pty_name, O_RDWR);

  if (pty_slave == -1)
    return -1;

#ifdef HAVE_STROPTS_H
#if !defined(__osf__)
  if (!ioctl (pty_slave, I_FIND, "ptem"))
    if (ioctl (pty_slave, I_PUSH, "ptem") == -1)
      {
	close (pty_slave);
	return -1;
      }

  if (!ioctl (pty_slave, I_FIND, "ldterm"))
    if (ioctl (pty_slave, I_PUSH, "ldterm") == -1)
      {
	close (pty_slave);
	return -1;
      }

#if !defined(sgi) && !defined(__sgi)
  if (!ioctl (pty_slave, I_FIND, "ttcompat"))
    if (ioctl (pty_slave, I_PUSH, "ttcompat") == -1)
      {
	g_warning ("ioctl (pty_slave, I_PUSH, \"ttcompat\") error: %s.",
		   g_strerror (errno));
	close (pty_slave);
	return -1;
      }
#endif /* sgi || __sgi */
#endif /* __osf__ */
#endif /* HAVE_STROPTS_H */

  return pty_slave;
}

/** 
 * pty_open_master:
 * @pty_name 
 * @used_bsd 
 * 
 * @Returns: 
 */
static gint
pty_open_master (gchar *pty_name, gint *used_bsd)
{
  gint pty_master;
  gchar *slave_name;

  if (access ("/dev/ptmx", R_OK) == 0)
    {
      strcpy (pty_name, "/dev/ptmx");
    }
  else if (access ("/dev/ptc", R_OK) == 0)	/* AIX */
    {
      strcpy (pty_name, "/dev/ptc");
    }
  else
    {
      *used_bsd = 1;
      return pty_open_master_bsd (pty_name, used_bsd);
    }

  pty_master = open (pty_name, O_RDWR);

  /* Try BSD open, this is needed for Linux which might
   * have Unix98 devices but no kernel support for those.
   */
  if (pty_master == -1)
    {
      *used_bsd = 1;
      return pty_open_master_bsd (pty_name, used_bsd);
    }
  *used_bsd = 0;

  if (grantpt  (pty_master) == -1 || unlockpt (pty_master) == -1)
    {
      close (pty_master);
      return -1;
    }
  if ((slave_name = ptsname (pty_master)) == NULL)
    {
      close (pty_master);
      return -1;
    }
  strcpy (pty_name, slave_name);
  return pty_master;
}
#else
# define pty_open_master	pty_open_master_bsd
# define pty_open_slave  	pty_open_slave_bsd
#endif

/* defined in texec.c
 */
extern const gchar*		t_get_opt_ptyname (void);

/** 
 * openpty:
 * @master_fd:
 * @slave_fd:
 * @name:
 * @termp:
 * @winp:
 * 
 * @Returns: 
 */
int
openpty (int            *master_fd,
	 int            *slave_fd,
	 char           *name,
	 struct termios *termp,
	 struct winsize *winp)
{
  int pty_master, pty_slave, used_bsd = 0;
  struct group *group_info;
  char line [256] = { '\0', };
  const gchar *opt_ptyname;

  /* try open specify pty device.
   */
  opt_ptyname = t_get_opt_ptyname ();
  if (opt_ptyname)
    {
      gchar *ptyname = t_guess_ttyname (opt_ptyname);
      if (!ptyname)
	{
	  g_warning (_("device %s not found."), opt_ptyname);
	  return -1;
	}
      g_strlcpy (line, ptyname, sizeof (line));
      g_free (ptyname);
      pty_master = pty_open_master_bsd (line, &used_bsd);
    }
  else
    {
      pty_master = pty_open_master (line, &used_bsd);
    }
  if (pty_master == -1)
    {
      g_warning (_("open pty master error: %s"), g_strerror (errno));
      return -1;
    }

  fcntl (pty_master, F_SETFD, FD_CLOEXEC);

  group_info = getgrnam ("tty");
	
  if (group_info != NULL)
    {
      chown (line, getuid (), group_info->gr_gid);
      chmod (line, S_IRUSR | S_IWUSR | S_IWGRP);
    }
  else
    {
      chown (line, getuid (), -1);
      chmod (line, S_IRUSR | S_IWUSR | S_IWGRP);
    }

#ifdef HAVE_REVOKE
  revoke (line);
#endif

  /* Open slave side */
  if (used_bsd)
    pty_slave = pty_open_slave_bsd (line);
  else
    pty_slave = pty_open_slave (line);

  if (pty_slave == -1)
    {
      close (pty_master);
      errno = ENOENT;
      return -1;
    }
  fcntl (pty_slave, F_SETFD, FD_CLOEXEC);

  *master_fd = pty_master;
  *slave_fd  = pty_slave;

  if (termp)
    tcsetattr (pty_slave, TCSAFLUSH, termp);

  if (winp)
    ioctl (pty_slave, TIOCSWINSZ, winp);

  if (name)
    strcpy (name, line);

  return 0;
}

/** 
 * forkpty:
 * @master_fd 
 * @name 
 * @termp 
 * @winp 
 * 
 * @Returns: 
 */
pid_t
forkpty (int            *master_fd,
	 char           *name,
	 struct termios *termp,
	 struct winsize *winp)
{
  int master, slave;
  pid_t pid;

  if (openpty (&master, &slave, name, termp, winp) == -1)
    {
      g_warning ("openpty error: %s", g_strerror (errno));
      return -1;
    }

  pid = fork ();
  if (pid == -1)
    {
      g_warning ("fork error: %s", g_strerror (errno));
      return -1;
    }

  if (pid == 0)		/* child */
    {
      close (master);
      login_tty (slave);
    }
  else			/* parent */
    {
      *master_fd = master;
      close (slave);
    }

  return pid;
}
#endif /* HAVE_OPENPTY */
