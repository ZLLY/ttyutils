/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tforkpty.h"
#include "tpty.h"
#include "tscreen.h"
#include "treaper.h"
#include "tterm.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif


typedef struct termios		termios_t;

/* save terminal originality state.
 */
extern termios_t	 t_terminal_state;


enum
{
  INCOMING,
  REFRESHED,

  LAST_SIGNAL
};

struct _TTermPrivate
{
  gchar       *command;

  gint         pty_master;
  GPid         pty_pid;

  GIOChannel  *input_channel;
  guint        input_source;
  gchar       *cache;
  gsize        cache_size;

  GIOChannel  *output_channel;

  gboolean     is_to_auxport;
  const gchar *screen_data;
  gsize        screen_size;

  char         ptyname[128];
};

#define T_TERM_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), T_TYPE_TERM, TTermPrivate))

static guint term_signals[LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (TTerm, t_term, G_TYPE_OBJECT);

/** 
 * t_term_init:
 * @term 
 */
static void
t_term_init (TTerm *term)
{
  struct winsize size;

  memset (&size, 0, sizeof (size));

  if (ioctl (STDIN_FILENO, TIOCGWINSZ, &size) == 0)
    {
      t_println ("tty size was %dx%d", size.ws_col, size.ws_row);

      if (size.ws_col == 0 || size.ws_row == 0)
	{
	  size.ws_col = 80;
	  size.ws_row = 24;
	}
    }
  else
    {
      size.ws_col = 80;
      size.ws_row = 24;
    }
  term->screen = t_screen_create (size.ws_col, size.ws_row);

  term->priv = T_TERM_PRIVATE (term);

  term->priv->pty_master = -1;
  term->priv->pty_pid = -1;

  term->priv->input_channel = NULL;
  term->priv->input_source = T_INVALID_SOURCE;

  term->priv->cache = g_malloc0 (2048);
  term->priv->cache_size = 2048;

  term->priv->output_channel = NULL;

  term->priv->is_to_auxport = FALSE;
  term->priv->screen_data = NULL;
  term->priv->screen_size = 0;
}

/** 
 * t_term_finalize:
 * @object 
 */
static void
t_term_finalize (GObject *object)
{
  TTerm *term = T_TERM (object);

  if (term->priv->command)
    {
      g_free (term->priv->command);
      term->priv->command = NULL;
    }
  if (term->priv->cache)
    {
      g_free (term->priv->cache);
      term->priv->cache = NULL;
    }

  if (term->screen)
    {
      t_screen_destroy (term->screen);
      term->screen = NULL;
    }

  G_OBJECT_CLASS (t_term_parent_class)->finalize (object);
}

/** 
 * t_term_incoming:
 * @term 
 */
static void
t_term_incoming (TTerm       *term,
		 const gchar *cache,
		 guint        cache_size)
{
  g_return_if_fail (T_IS_TERM (term));

  t_println ("terminal incoming %d bytes be procssed.", cache_size);
}

/** 
 * t_term_refreshed:
 * @term 
 */
static void
t_term_refreshed (TTerm *term)
{
  g_return_if_fail (T_IS_TERM (term));

  t_println ("terminal refreshed, update screen freeze list now.");

  t_screen_update_freezes (term->screen);

#ifdef T_DEBUG
    {
      gchar *name = t_term_ptsname (term);
      t_screen_dump (term->screen, name);
      g_free (name);
    }
#endif
}

/** 
 * t_term_class_init:
 * @klass 
 */
static void
t_term_class_init (TTermClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TTermPrivate));

  object_class->finalize = t_term_finalize;

  klass->incoming = t_term_incoming;
  klass->refreshed = t_term_refreshed;

  term_signals[INCOMING] = g_signal_new ("incoming",
					 G_OBJECT_CLASS_TYPE (klass),
					 G_SIGNAL_RUN_LAST,
					 G_STRUCT_OFFSET (TTermClass, incoming),
					 NULL, NULL,
					 t_marshal_VOID__POINTER_UINT,
					 G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_UINT);
  term_signals[REFRESHED] = g_signal_new ("refreshed",
					  G_OBJECT_CLASS_TYPE (klass),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (TTermClass, refreshed),
					  NULL, NULL,
					  g_cclosure_marshal_VOID__VOID,
					  G_TYPE_NONE, 0);
}

/** 
 * t_term_new:
 * @Returns:
 */
TTerm*
t_term_new (void)
{
  return g_object_new (T_TYPE_TERM, NULL);
}

/** 
 * t_term_ptsname:
 * @term 
 * 
 * @Returns: 
 */
gchar*
t_term_ptsname (TTerm *term)
{
  char *p;

  g_return_val_if_fail (T_IS_TERM (term), NULL);

  p = t_pty_ptsname (term->priv->pty_master);
  if (p)
    return p;

  return g_strdup (term->priv->ptyname);
}

/** 
 * t_term_set_size:
 * @term 
 * @cols 
 * @rows 
 */
void
t_term_set_size (TTerm    *term,
		 guint16   cols,
		 guint16   rows)
{
  glong old_cols, old_rows;

  g_return_if_fail (T_IS_TERM (term));

  old_rows = term->screen->rows;
  old_cols = term->screen->cols;

  if (term->priv->pty_master != -1)
    {
      if (t_pty_set_size (term->priv->pty_master, cols, rows) == 0)
	{
	  t_screen_destroy (term->screen);
	  term->screen = t_screen_create (cols, rows);
	}
    }
}

/** 
 * t_term_get_size:
 * @term 
 * @cols 
 * @rows 
 */
void
t_term_get_size (TTerm     *term,
		 guint16   *cols,
		 guint16   *rows)
{
  g_return_if_fail (T_IS_TERM (term));

#ifdef T_DEBUG
  if (term->priv->pty_master != -1)
    {
      gint rows, cols;

      if (t_pty_get_size (term->priv->pty_master, &cols, &rows) != 0)
	{
	  t_println ("error reading PTY size.");
	}
      else
	{
	  g_assert (cols == term->screen->cols);
	  g_assert (rows == term->screen->rows);
	}
    }
#endif

  if (cols)
    {
      *cols = term->screen->cols;
    }
  if (rows)
    {
      *rows = term->screen->rows;
    }
}

/** 
 * t_term_disconnect_pty_read:
 * @term 
 */
void
t_term_disconnect_pty_read (TTerm *term)
{
  g_return_if_fail (T_IS_TERM (term));

  t_println ("terminal discconect pty read.");

  if (term->priv->pty_master == -1)
    return;

  if (term->priv->input_source != T_INVALID_SOURCE)
    {
      g_source_remove (term->priv->input_source);
      term->priv->input_source = T_INVALID_SOURCE;
    }
  if (term->priv->input_channel != NULL)
    {
      g_io_channel_unref (term->priv->input_channel);
      term->priv->input_channel = NULL;
    }
}

/** 
 * t_term_eof:
 * @channel 
 * @term 
 */
void
t_term_eof (GIOChannel *channel, TTerm *term)
{
  if (channel == term->priv->input_channel)
    {
      t_term_disconnect_pty_read (term);
    }
  if (term->priv->pty_master != -1)
    {
      close (term->priv->pty_master);
      term->priv->pty_master = -1;
    }
}

/** 
 * t_term_io_read:
 * @channel 
 * @condition 
 * @term 
 * 
 * @Returns: 
 */
gboolean
t_term_io_read (GIOChannel   *channel,
		GIOCondition  condition,
		TTerm        *term)
{
  GIOStatus status;
  gsize nread = 0;
  GError *error = NULL;
#ifdef T_DEBUG
  gint i;
#endif

  if (condition & G_IO_IN)
    {
      status = g_io_channel_read_chars (channel, term->priv->cache,
					term->priv->cache_size, &nread, &error);
      switch (status)
	{
	case G_IO_STATUS_NORMAL:
	  break;

	case G_IO_STATUS_EOF:
	  /* get this status when child process open terminal device.
	   */
	  t_println ("read terminal pty get eof flag.");
	  break;

	case G_IO_STATUS_AGAIN:
	  return TRUE;

	case G_IO_STATUS_ERROR:
	  if (error)
	    {
	      g_warning (_("read pty error: %s"), error->message);
	      g_error_free (error);
	    }
	  return FALSE;
	}

      if (nread == 0)
	return TRUE;

      term->priv->screen_data = term->priv->cache;
      term->priv->screen_size = nread;

      t_println ("read %d bytes, content was:", nread);

#ifdef T_DEBUG
      for (i = 0; i < nread; i += 12)
	{
	  gint k, j;

	  j = i;

	  g_print ("%03d  ", i / 12 + 1);

	  for (k = 0; j + k < nread && k < 12; k++)
	    {
	      g_print ("%c", g_ascii_iscntrl (term->priv->cache[j + k]) ?
		             '.' : term->priv->cache[j + k]);
	    }
	  for (; k < 12; k++)
	    {
	      g_print (" ");
	    }
	  g_print (" ");

	  for (k = 0; j + k < nread && k < 12; k++)
	    {
	      g_print (" 0x%02x", (guchar) term->priv->cache[j + k]);
	    }
	  g_print ("\n");
	}
#endif

      g_signal_emit (term, term_signals[INCOMING], 0,
		     term->priv->cache, nread, NULL);

      if (term->screen && t_screen_get_changed (term->screen))
	{
	  g_signal_emit (term, term_signals[REFRESHED], 0, NULL);
	}
    }

  if (condition & G_IO_HUP)
    {
      t_println ("read terminal pty get G_IO_HUP.");
      t_term_eof (channel, term);
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_term_connect_pty_read:
 * @term 
 */
void
t_term_connect_pty_read (TTerm *term)
{
  g_return_if_fail (T_IS_TERM (term));
  g_return_if_fail (term->priv->pty_master != -1);

  if (term->priv->input_channel == NULL)
    {
      term->priv->input_channel = g_io_channel_unix_new (term->priv->pty_master);
      g_io_channel_set_encoding (term->priv->input_channel, NULL, NULL);
      g_io_channel_set_buffered (term->priv->input_channel, FALSE);
      //g_io_channel_set_flags (term->priv->input_channel, G_IO_FLAG_NONBLOCK, NULL);
    }
  if (term->priv->input_source == T_INVALID_SOURCE)
    {
      term->priv->input_source = g_io_add_watch (term->priv->input_channel,
						 G_IO_IN | G_IO_HUP | G_IO_ERR,
						 (GIOFunc) t_term_io_read, term);
    }
}

/** 
 * t_term_fork_command:
 * @term:
 * @command:
 * 
 * @Returns: 
 */
gboolean
t_term_fork_command (TTerm        *term,
		     const gchar  *command)
{
  GPid pid;
  gint n;
  struct winsize ws;

  g_return_val_if_fail (T_IS_TERM (term), -1);
  g_return_val_if_fail (command, -1);

  term->priv->command = g_strdup (command);

  ws.ws_col = term->screen->cols;
  ws.ws_row = term->screen->rows;
  ws.ws_xpixel = ws.ws_ypixel = 0;

  pid = forkpty (&n, term->priv->ptyname, NULL, &ws);
  if (pid < 0)
    {
      g_warning (_("forkpty error: %s"), g_strerror (errno));
      return FALSE;
    }

  if (pid == 0)		/* child */
    {
      tcsetattr (0, TCSAFLUSH, &t_terminal_state);

      execl ("/bin/sh", "/bin/sh", "-c", command, NULL);

      g_critical ("execute command '%s' failed.", command);
      exit(127);
    }
  else			/* parent */
    {
      term->priv->pty_pid = pid;
      term->priv->pty_master = n;

      t_reaper_add_child (pid);

      t_pty_set_utf8 (term->priv->pty_master, TRUE);

      /* set the pty to be non-blocking.
       */
      n = fcntl (term->priv->pty_master, F_GETFL);
      if ((n & O_NONBLOCK) == 0)
	{
	  fcntl (term->priv->pty_master, F_SETFL, n | O_NONBLOCK);
	}

      /* open a channel to listen for input on.
       */
      t_term_connect_pty_read (term);
    }

  return TRUE;
}

/** 
 * t_term_feed_child:
 * @term 
 * @text 
 * @length 
 *
 * Sends a block of text to the child as if it were entered by the user
 * at the keyboard.
 */
void
t_term_feed_child (TTerm       *term,
		   const gchar *text,
		   gsize        length)
{
  GIOStatus status;
  GError *error = NULL;
  gsize nwritten = 0;
  const gchar *p = text;

  g_return_if_fail (T_IS_TERM (term));
  g_return_if_fail (text);

  if (term->priv->pty_master == -1)
    return;

  if (length < 0)
    length = strlen (text);

  if (length == 0)
    return;

  if (!term->priv->output_channel)
    {
      term->priv->output_channel = g_io_channel_unix_new (term->priv->pty_master);
      if (!term->priv->output_channel)
	{
	  return;
	}
      g_io_channel_set_encoding (term->priv->output_channel, NULL, NULL);
      g_io_channel_set_buffered (term->priv->output_channel, FALSE);
    }

  t_println ("feed %d bytes to terminal pty slave(child input).", length);

  while (length > 0)
    {
      status = g_io_channel_write_chars (term->priv->output_channel,
					 p, length, &nwritten, &error);
      switch (status)
	{
	case G_IO_STATUS_NORMAL:
	  break;

	case G_IO_STATUS_AGAIN:
	  g_usleep (10);
	  break;

	case G_IO_STATUS_EOF:
	  t_println ("write pty got end of file error.");
	  return;

	case G_IO_STATUS_ERROR:
	  if (error)
	    {
	      g_warning (_("write pty error: %s"), error->message);
	      g_error_free (error);
	    }
	  return;
	}

      t_println ("terminal feed written %d bytes.", nwritten);

      length -= nwritten;
      p += nwritten;
    }
}

/** 
 * t_term_get_child_pid:
 * @term 
 * 
 * @Returns: 
 */
GPid
t_term_get_child_pid (TTerm *term)
{
  g_return_val_if_fail (T_IS_TERM (term), -1);
  return term->priv->pty_pid;
}

/** 
 * t_term_is_to_auxport:
 * @term:
 * 
 * @Returns: 
 */
gboolean
t_term_is_to_auxport (TTerm *term)
{
  g_return_val_if_fail (T_IS_TERM (term), FALSE);
  return term->priv->is_to_auxport;
}

/** 
 * t_term_get_screen_data:
 * @term:
 * 
 * @Returns: 
 */
const gchar*
t_term_get_screen_data (TTerm *term)
{
  g_return_val_if_fail (T_IS_TERM (term), NULL);
  return term->priv->screen_data;
}

/** 
 * t_term_get_screen_size:
 * @term:
 * 
 * @Returns: 
 */
gsize
t_term_get_screen_size (TTerm *term)
{
  g_return_val_if_fail (T_IS_TERM (term), 0);
  return term->priv->screen_size;
}
