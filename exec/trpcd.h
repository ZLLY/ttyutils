/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_RPCD_H__
#define __T_RPCD_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _TRpcd	TRpcd;

gboolean	t_rpcd_boot		(void);
void		t_rpcd_shut		(void);

const gchar*	t_rpcd_get_unixpath	(void);

G_END_DECLS

#endif /* __T_RPCD_H__ */
