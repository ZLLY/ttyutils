/* GNet - Networking library
 * Copyright (C) 2000  David Helder
 * Copyright (C) 2001  Mark Ferlatte
 * Copyright (C) 2008, 2009  xiaohu
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <glib/gi18n-lib.h>

#include "tunixsock.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

/* for ttymonitor debug
 */
#undef HAVE_ABSTRACT_SOCKETS


typedef struct sockaddr		sockaddr_t;
typedef struct sockaddr_un	sockaddr_un_t;

/* patch for SCO_SV, 200901 by WK
 */
#ifdef SCO_SV

typedef int     socklen_t;

#include <sys/stat.h>
#include <sys/times.h>
#include <sys/select.h>

#ifndef   S_ISSOCK
# ifdef   _S_ISSOCK
#  define   S_ISSOCK(m)         _S_ISSOCK(m)
# else
#  ifdef   _S_IFSOCK
#   define   S_ISSOCK(m)        ((m   &   S_IFMT)   ==   _S_IFSOCK)
#  else
#   ifdef   S_IFSOCK
#    define   S_ISSOCK(m)       ((m   &   S_IFMT)   ==   S_IFSOCK)
#   else
#    define   S_ISSOCK(m)       (0)
#   endif
#  endif
# endif
#endif /* !S_ISSOCK */

#endif /* SCO_SV */

/**
 * TUnixSocket
 *
 * A #TUnixSocket structure represents a Unix socket.  The
 * implementation is hidden.
 **/
struct _TUnixSocket
{
  gint           sockfd;
  guint          ref_count;
  GIOChannel    *iochannel;
  sockaddr_un_t  sa;
  gboolean       server;
  gboolean       abstract;	/* Abstract unix socket? */
};

#define T_PATH(S)		(((sockaddr_un_t*) (&(S)->sa))->sun_path)

#ifndef SUN_LEN
#define SUN_LEN(sa_un)		G_STRUCT_OFFSET(sockaddr_un_t, sun_path) + strlen (sa_un->sun_path)
#endif

/* T_SUN_LEN:
 * our own version of SUN_LEN that also works for abstract
 * sockets where SUN_LEN fails because it doesn't take into account the
 * initial zero in the path string in the case of abstract sockets.
 */
#define T_SUN_LEN(sa_un)	t_sun_len(sa_un)

static gboolean t_unix_socket_unlink	(const gchar *path);

/** 
 * t_sun_len:
 * @sa_un 
 * 
 * @Returns: 
 */
static guint
t_sun_len (sockaddr_un_t *sa_un)
{
  /* normal unix socket
   */
  if (sa_un->sun_path[0] != '\0')
    {
      return SUN_LEN (sa_un);
    }

  /* abstract socket, has a zero byte before the path
   */
  return G_STRUCT_OFFSET (sockaddr_un_t, sun_path) + 1 +
    strlen (sa_un->sun_path + 1);
}

/** 
 * t_unix_socket_new_internal:
 * @path:
 * @abstract:
 * 
 * @Returns: 
 */
static TUnixSocket*
t_unix_socket_new_internal (const gchar *path,
			    gboolean     abstract)
{
  TUnixSocket *sock;
  sockaddr_un_t *sa_un;

  g_return_val_if_fail (path != NULL, NULL);

#ifndef HAVE_ABSTRACT_SOCKETS
  /* if abstract unix sockets are not available, create a normal unix socket
   */
  abstract = FALSE;
#endif

  /* Create socket
   */
  sock = g_new0 (TUnixSocket, 1);

  sa_un = (sockaddr_un_t *) & sock->sa;
  sock->ref_count = 1;
  sock->server = FALSE;
  sock->sockfd = socket (AF_UNIX, SOCK_STREAM, 0);

  if (sock->sockfd < 0)
    {
      g_warning (_("socket(%s) failed: %s"), path, g_strerror (errno));
      g_free (sock);
      return NULL;
    }

  if (abstract)
    {
      sa_un->sun_path[0] = '\0';
      strncpy (sa_un->sun_path + 1, path, sizeof (sa_un->sun_path) - 2);
      sock->abstract = TRUE;
    }
  else
    {
      strncpy (sa_un->sun_path, path, sizeof (sa_un->sun_path) - 1);
      sock->abstract = FALSE;
    }

  sa_un->sun_family = AF_UNIX;

  if (connect (sock->sockfd, (sockaddr_t *) sa_un, T_SUN_LEN (sa_un)) != 0)
    {
      g_warning (_("connect(%s) failed: %s"), path, g_strerror (errno));
      close (sock->sockfd);
      g_free (sock);
      return NULL;
    }

  return sock;
}

/**
 *  t_unix_socket_new
 *  @path: path
 *
 *  Creates a #TUnixSocket and connects to @path.  This function will
 *  block to connect.  Use this constructor to create a #TUnixSocket
 *  for a client.
 *
 *  Returns: a new #TUnixSocket; NULL on failure.
 **/
TUnixSocket *
t_unix_socket_new (const gchar *path)
{
  return t_unix_socket_new_internal (path, FALSE);
}

/**
 *  t_unix_socket_new_abstract
 *  @path: path
 *
 *  Creates a #TUnixSocket and connects to @path in the abstract
 *  unix socket domain. This function will block to connect.  Use this
 *  constructor to create a #GUnixSocket for a client.
 *
 *  If the abstract unix sockets are not available on the platform in use,
 *  this function will behave like t_unix_socket_new().
 *
 *  Returns: a new #TUnixSocket, or NULL on failure.
 **/
TUnixSocket *
t_unix_socket_new_abstract (const gchar *path)
{
  return t_unix_socket_new_internal (path, TRUE);
}


/**
 *  t_unix_socket_delete
 *  @socket: a #TUnixSocket
 *
 *  Deletes a #TUnixSocket.
 *
 **/
void
t_unix_socket_delete (TUnixSocket *socket)
{
  if (socket != NULL)
    {
      t_unix_socket_unref (socket);
    }
}


/**
 *  t_unix_socket_ref
 *  @socket: a #TUnixSocket
 *
 *  Adds a reference to a #TUnixSocket.
 **/
void
t_unix_socket_ref (TUnixSocket *socket)
{
  g_return_if_fail (socket != NULL);

  socket->ref_count++;
}

/**
 *  t_unix_socket_unref
 *  @socket: a #TUnixSocket
 *
 *  Removes a reference from a #GUnixSocket.  A #GUnixSocket is
 *  deleted when the reference count reaches 0.
 **/
void
t_unix_socket_unref (TUnixSocket *socket)
{
  g_return_if_fail (socket != NULL);

  socket->ref_count--;

  if (socket->ref_count == 0)
    {
      /* Don't care if this fails.
       */
      close (socket->sockfd);

      if (socket->iochannel)
	{
	  g_io_channel_unref (socket->iochannel);
	}
      if (socket->server && !socket->abstract)
	{
	  t_unix_socket_unlink (T_PATH (socket));
	}
      g_free (socket);
    }
}

/**
 *  t_unix_socket_get_io_channel
 *  @socket: a #TUnixSocket
 *
 *  Gets the #GIOChannel of a #TUnixSocket.
 *
 *  For a client socket, the #GIOChannel represents the data stream.
 *  Use it like you would any other #GIOChannel.
 *
 *  For a server socket, however, the #GIOChannel represents the
 *  listening socket.  When it's readable, there's a connection
 *  waiting to be accepted.
 *
 *  Every #GUnixSocket has one and only one #GIOChannel.  If you ref
 *  the channel, then you must unref it eventually.  Do not close the
 *  channel.  The channel is closed by GNet when the socket is
 *  deleted.
 *
 *  Returns: a #GIOChannel.
 **/
GIOChannel*
t_unix_socket_get_io_channel (TUnixSocket *socket)
{
  g_return_val_if_fail (socket != NULL, NULL);

  if (socket->iochannel == NULL)
    {
      socket->iochannel = g_io_channel_unix_new (socket->sockfd);
      if (socket->iochannel)
	{
	  g_io_channel_set_encoding (socket->iochannel, NULL, NULL);
	  g_io_channel_set_buffered (socket->iochannel, FALSE);
	}
    }
  return socket->iochannel;
}


/**
 *  t_unix_socket_get_path
 *  @socket: a #TUnixSocket
 *
 *  Gets the path of a #TUnixSocket.
 *
 *  Returns: the path.
 **/
gchar*
t_unix_socket_get_path (const TUnixSocket *socket)
{
  g_return_val_if_fail (socket != NULL, NULL);

  return g_strdup (T_PATH (socket));
}

/** 
 * t_unix_socket_server_new_internal:
 * @path:
 * @abstract:
 * 
 * @Returns:
 */
static TUnixSocket*
t_unix_socket_server_new_internal (const gchar *path,
				   gboolean     abstract)
{
  sockaddr_un_t *sa_un;
  TUnixSocket *sock;
  gint flags;
  socklen_t len;
  mode_t mask;

  g_return_val_if_fail (path != NULL, NULL);

  /* if abstract unix sockets are not available,
   * create a normal unix socket.
   */
#ifndef HAVE_ABSTRACT_SOCKETS
  abstract = FALSE;
#endif

  sock = g_new0 (TUnixSocket, 1);

  sa_un = (sockaddr_un_t *) & sock->sa;
  sa_un->sun_family = AF_UNIX;
  sock->ref_count = 1;
  sock->server = TRUE;

  if (abstract)
    {
      sa_un->sun_path[0] = '\0';
      strncpy (sa_un->sun_path + 1, path, sizeof (sa_un->sun_path) - 2);
      sock->abstract = TRUE;
    }
  else
    {
      strncpy (sa_un->sun_path, path, sizeof (sa_un->sun_path) - 1);
      sock->abstract = FALSE;
      if (!t_unix_socket_unlink (T_PATH (sock)))
	{
	  goto error;
	}
    }

  sock->sockfd = socket (AF_UNIX, SOCK_STREAM, 0);
  if (sock->sockfd < 0)
    {
      g_warning (_("socket(%s) failed: %s"), path, g_strerror (errno));
      goto error;
    }

  flags = fcntl (sock->sockfd, F_GETFL, 0);
  if (flags == -1)
    {
      g_warning (_("fcntl(%s) failed: %s"), path, g_strerror (errno));
      goto error;
    }

  mask = umask (0000);
  if (bind (sock->sockfd, (sockaddr_t *) sa_un, T_SUN_LEN (sa_un)) != 0)
    {
      umask (mask);
      goto error;
    }
  umask (mask);

  /* Make the socket non-blocking
   */
  if (fcntl (sock->sockfd, F_SETFL, flags | O_NONBLOCK) == -1)
    {
      g_warning (_("fcntl(%s) failed: %s"), path, g_strerror (errno));
      goto error;
    }

  /* Get the socket name FIXME (why? -DAH)
   */
  len = sizeof (sock->sa);
  if (getsockname (sock->sockfd, (sockaddr_t*) &sock->sa, &len) != 0)
    {
      goto error;
    }

  if (listen (sock->sockfd, 10) != 0)
    {
      goto error;
    }

  return sock;

error:
  t_unix_socket_delete (sock);
  return NULL;
}

/**
 *  t_unix_socket_server_new
 *  @path: path
 *
 *  Creates a #TUnixSocket bound to @path.  Use this constructor to
 *  create a #TUnixSocket for a server.
 *
 *  Returns: a new #TUnixSocket; NULL on error.
 *
 **/
TUnixSocket*
t_unix_socket_server_new (const gchar *path)
{
  return t_unix_socket_server_new_internal (path, FALSE);
}

/**
 *  t_unix_socket_server_new_abstract
 *  @path: path
 *
 *  Creates a #TUnixSocket bound to @path in the abstract unix socket
 *  domain.  Use this constructor to create a #TUnixSocket for a
 *  server.
 *
 *  If the abstract unix sockets are not available on the platform in use,
 *  this function will behave the same as t_unix_socket_server_new().
 *
 *  Returns: a new #TUnixSocket, or NULL on error.
 **/
TUnixSocket*
t_unix_socket_server_new_abstract (const gchar *path)
{
  return t_unix_socket_server_new_internal (path, TRUE);
}

/**
 *  t_unix_socket_server_accept
 *  @socket: a #TUnixSocket
 *
 *  Accepts a connection from a #TUnixSocket.  The socket must have
 *  been created using t_unix_socket_server_new(). This function
 *  will block.  Even if the socket's #GIOChannel is readable, the
 *  function may still block.
 *
 *  Returns: a new #GUnixSocket representing a new connection; NULL on
 *  error.
 **/
TUnixSocket*
t_unix_socket_server_accept (const TUnixSocket *socket)
{
  gint sockfd;
  sockaddr_t sa;
  fd_set fdset;
  socklen_t len;
  TUnixSocket *sock;

  g_return_val_if_fail (socket != NULL, NULL);

try_again:

  FD_ZERO (&fdset);
  FD_SET (socket->sockfd, &fdset);

  if (select (socket->sockfd + 1, &fdset, NULL, NULL, NULL) == -1)
    {
      if (errno == EINTR)
	{
	  goto try_again;
	}
      return NULL;
    }

  len = sizeof (sa);

  if ((sockfd = accept (socket->sockfd, &sa, &len)) == -1)
    {
      if (errno == EWOULDBLOCK || errno == ECONNABORTED ||
#ifdef EPROTO			/* OpenBSD does not have EPROTO */
	  errno == EPROTO ||
#endif
	  errno == EINTR)
	{
	  goto try_again;
	}
      return NULL;
    }

  sock = g_new0 (TUnixSocket, 1);

  sock->ref_count = 1;
  sock->sockfd = sockfd;
  memcpy (&sock->sa, &sa, sizeof (sock->sa));

  return sock;
}


/**
 *  t_unix_socket_server_accept_nonblock
 *  @socket: a #TUnixSocket
 *
 *  Accepts a connection from a #GUnixSocket without blocking.  The
 *  socket must have been created using t_unix_socket_server_new().
 *
 *  Note that if the socket's #GIOChannel is readable, then there is
 *  PROBABLY a new connection.  It is possible for the connection
 *  to close by the time this function is called, so it may return
 *  NULL.
 *
 *  Returns: a new #TUnixSocket representing a new connection; NULL
 *  otherwise.
 **/
TUnixSocket*
t_unix_socket_server_accept_nonblock (const TUnixSocket *socket)
{
  gint sockfd;
  sockaddr_t sa;
  fd_set fdset;
  socklen_t len;
  TUnixSocket *sock;
  struct timeval tv = { 0, 0 };

  g_return_val_if_fail (socket != NULL, NULL);

try_again:

  FD_ZERO (&fdset);
  FD_SET (socket->sockfd, &fdset);

  if (select (socket->sockfd + 1, &fdset, NULL, NULL, &tv) == -1)
    {
      if (errno == EINTR)
	{
	  goto try_again;
	}
      return NULL;
    }

  len = sizeof (sa);

  /* If we get an error, return.  We don't want to try again
   * as we do in t_unix_socket_server_accept() - it might
   * cause a block.
   */
  if ((sockfd = accept (socket->sockfd, &sa, &len)) == -1)
    {
      return NULL;
    }

  sock = g_new0 (TUnixSocket, 1);

  sock->ref_count = 1;
  sock->sockfd = sockfd;

  memcpy (&sock->sa, &sa, sizeof (sock->sa));

  return sock;
}

/** 
 * t_unix_socket_unlink:
 * @path 
 * 
 * @Returns: 
 */
static gboolean
t_unix_socket_unlink (const gchar * path)
{
  struct stat stbuf;

  g_return_val_if_fail (path != NULL, FALSE);

  if (stat (path, &stbuf) == 0)
    {
      if (S_ISSOCK (stbuf.st_mode))
	{
	  if (unlink (path) == 0)
	    {
	      return TRUE;
	    }
	  else
	    {
	      return FALSE;	/* Can't unlink */
	    }
	}
      else
	{
	  return FALSE;		/* path is not a socket */
	}
    }
  else if (errno == ENOENT)
    {
      return TRUE;		/* File doesn't exist, so we're okay */
    }
  return FALSE;
}
