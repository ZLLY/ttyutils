/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <glib/gi18n-lib.h>

#include "tdebug.h"
#include "ttypes.h"
#include "tmisc.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

/** 
 * t_print_version:
 */
void
t_print_version (void)
{
  g_print ("\n%s for %s.%d\n\n", g_get_application_name (),
	   PACKAGE_STRING, INTERFACE_AGE);
  g_print ("\n");
  g_print (_("Copyright (C) 2008,2009,2010,2011 Xiaohu, All rights reserved."));
  g_print ("\n");
  g_print (_("\nsee also:\n"));
  g_print ("    %s\n\n", PACKAGE_WEBSITE);
}

/** 
 * t_guess_ttyname:
 * @ttyn:
 * 
 * @Returns: a new alloced string.
 */
gchar*
t_guess_ttyname (const gchar *ttyn)
{
  gchar *newn;

  if (g_path_is_absolute (ttyn))
    {
      if (!g_file_test (ttyn, G_FILE_TEST_EXISTS))
	{
	  g_warning (_("%s not found."), ttyn);
	  return NULL;
	}
      return g_strdup (ttyn);
    }

  /* prefix '/dev/'
   */
  newn = g_build_filename ("/", "dev", ttyn, NULL);
  if (g_file_test (newn, G_FILE_TEST_EXISTS))
    {
      t_println (_("%s found."), newn);
      return newn;
    }
  g_free (newn);

  /* prefix '/dev/pts/'
   */
  newn = g_build_filename ("/", "dev", "pts", ttyn, NULL);
  if (g_file_test (newn, G_FILE_TEST_EXISTS))
    {
      t_println (_("%s found."), newn);
      return newn;
    }
  g_free (newn);

  return NULL;
}

typedef struct _TKeyname	TKeyname;

struct _TKeyname
{
  const gchar *name;
  gint key;
};

static TKeyname	t_key_name[] =
{
    { "HOME",	T_KEY_HOME },
    { "END",	T_KEY_END },
    { "DOWN",	T_KEY_DOWN },
    { "UP",	T_KEY_UP },
    { "LEFT",	T_KEY_LEFT },
    { "RIGHT",	T_KEY_RIGHT },
    { "PGDOWN",	T_KEY_PGDOWN },
    { "PGUP",	T_KEY_PGUP },
    { "INSERT",	T_KEY_INSERT },
    { "DELETE",	T_KEY_DELETE },

    { "F0",	T_KEY_F0 },
    { "F1",	T_KEY_F1 },
    { "F2",	T_KEY_F2 },
    { "F3",	T_KEY_F3 },
    { "F4",	T_KEY_F4 },
    { "F5",	T_KEY_F5 },
    { "F6",	T_KEY_F6 },
    { "F7",	T_KEY_F7 },
    { "F8",	T_KEY_F8 },
    { "F9",	T_KEY_F9 },
    { "F10",	T_KEY_F10 },
    { "F11",	T_KEY_F11 },
    { "F12",	T_KEY_F12 },
    { "F13",	T_KEY_F13 },
    { "F14",	T_KEY_F14 },
    { "F15",	T_KEY_F15 },
    { "F16",	T_KEY_F16 },
    { "F17",	T_KEY_F17 },
    { "F18",	T_KEY_F18 },
    { "F19",	T_KEY_F19 },
    { "F20",	T_KEY_F20 },
    { "F21",	T_KEY_F21 },
    { "F22",	T_KEY_F22 },
    { "F23",	T_KEY_F23 },
    { "F24",	T_KEY_F24 },
    { "F25",	T_KEY_F25 },
    { "F26",	T_KEY_F26 },
    { "F27",	T_KEY_F27 },
    { "F28",	T_KEY_F28 },
    { "F29",	T_KEY_F29 },
    { "F30",	T_KEY_F30 },
    { "F31",	T_KEY_F31 },
    { "F32",	T_KEY_F32 },
    { "F33",	T_KEY_F33 },
    { "F34",	T_KEY_F34 },
    { "F35",	T_KEY_F35 },
    { "F36",	T_KEY_F36 },
    { "F37",	T_KEY_F37 },
    { "F38",	T_KEY_F38 },
    { "F39",	T_KEY_F39 },
    { "F40",	T_KEY_F40 },
    { "F41",	T_KEY_F41 },
    { "F42",	T_KEY_F42 },
    { "F43",	T_KEY_F43 },
    { "F44",	T_KEY_F44 },
    { "F45",	T_KEY_F45 },
    { "F46",	T_KEY_F46 },
    { "F47",	T_KEY_F47 },
    { "F48",	T_KEY_F48 },
    { "F49",	T_KEY_F49 },
    { "F50",	T_KEY_F50 },
    { "F51",	T_KEY_F51 },
    { "F52",	T_KEY_F52 },
    { "F53",	T_KEY_F53 },
    { "F54",	T_KEY_F54 },
    { "F55",	T_KEY_F55 },
    { "F56",	T_KEY_F56 },
    { "F57",	T_KEY_F57 },
    { "F58",	T_KEY_F58 },
    { "F59",	T_KEY_F59 },
    { "F60",	T_KEY_F60 },

    { NULL, 0 }
};

/** 
 * t_hotkey_from_string:
 * @string:
 * 
 * @Returns: key value
 */
gint
t_hotkey_from_string (const gchar *string)
{
  gint i;

  g_return_val_if_fail (string, 0);

  if (g_ascii_isdigit (string[0]))
    {
      return g_strtod (string, NULL);
    }

  /* ^c is ctrl + c
   */
  if (string[0] == '^' && strlen (string) == 2)
    {
      if (g_ascii_isupper (string[1]))
	{
	  return string[1] - 'A' + 1;
	}
      else if (g_ascii_islower (string[1]))
	{
	  return string[1] - 'a' + 1;
	}
      else
	{
	  g_warning (_("hotkey %s invalid, beacuse after ^ must be a alpha."),
		     string);
	  return -1;
	}
    }

  for (i = 0; t_key_name[i].name; i++)
    {
      if (g_ascii_strcasecmp (t_key_name[i].name, string) == 0)
	{
	  return t_key_name[i].key;
	}
    }

  return string[0];
}

/** 
 * t_hotkey_to_string:
 * @key:
 * 
 * @Returns: 
 */
gchar*
t_hotkey_to_string (gint key)
{
  gint i;

  g_return_val_if_fail (key > 0, NULL);

  if (key <= 'z' - 'a' + 1)
    {
      return g_strdup_printf ("0x%02x(^%c)", key, key + 'A' - 1);
    }

  if (key < 256)
    {
      return g_strdup_printf ("0x%02x(%c)", key, key);
    }

  for (i = 0; t_key_name[i].name; i++)
    {
      if (t_key_name[i].key == key)
	{
	  return g_strdup_printf ("%s", t_key_name[i].name);
	}
    }

  return NULL;
}
