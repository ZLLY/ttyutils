/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tscreen.h"

#define MAX_CSI_ES_PARAMS	32

/** 
 * clamp_cursor_to_bounds:
 * @screen 
 */
static inline void
clamp_cursor_to_bounds (TScreen *screen)
{
  if (screen->crow < 0)
    {
      screen->curpos_dirty = TRUE;
      screen->crow = 0;
    }
  if (screen->ccol < 0)
    {
      screen->curpos_dirty = TRUE;
      screen->ccol = 0;
    }
  if (screen->crow >= screen->rows)
    {
      screen->curpos_dirty = TRUE;
      screen->crow = screen->rows - 1;
    }
  if (screen->ccol >= screen->cols)
    {
      screen->curpos_dirty = TRUE;
      screen->ccol = screen->cols - 1;
    }
}

/** 
 * interpret_csi_SGR:
 * @screen 
 * @param 
 * @pcount 
 *
 * interprets a 'set attribute' (SGR) CSI escape sequence
 */
static void
interpret_csi_SGR (TScreen *screen, gint param[], gint pcount)
{
  gint i;

  /* special case: reset attributes
   */
  if (pcount == 0)
    {
      screen->curattr = 0x70;
      return;
    }

  /* From http://vt100.net/docs/vt510-rm/SGR table 5-16
   * 0    All attributes off
   * 1    Bold
   * 4    Underline
   * 5    Blinking
   * 7    Negative image
   * 8    Invisible image
   * 10   The ASCII character set is the current 7-bit
   *      display character set (default) - SCO Console only.
   * 11   Map Hex 00-7F of the PC character set codes
   *      to the current 7-bit display character set
   *      - SCO Console only.
   * 12   Map Hex 80-FF of the current character set to
   *      the current 7-bit display character set - SCO
   *      Console only.
   * 22   Bold off
   * 24   Underline off
   * 25   Blinking off
   * 27   Negative image off
   * 28   Invisible image off
   */
  for (i = 0; i < pcount; i++)
    {
      if (param[i] == 0)
	{
	  screen->curattr = 0x70;
	}
      else if (param[i] == 1 || param[i] == 2 || param[i] == 4)	/* set bold */
	{
	  SCREEN_ATTR_MOD_BOLD (screen->curattr, 1);
	}
      else if (param[i] == 5)				/* set blink */
	{
	  SCREEN_ATTR_MOD_BLINK (screen->curattr, 1);
	}
      else if (param[i] == 7 || param[i] == 27)		/* reverse video */
	{
	  gint fg = SCREEN_ATTR_FG (screen->curattr);
	  gint bg = SCREEN_ATTR_BG (screen->curattr);

	  SCREEN_ATTR_MOD_FG (screen->curattr, bg);
	  SCREEN_ATTR_MOD_BG (screen->curattr, fg);
	}
      else if (param[i] == 8)				/* invisible */
	{
	  screen->curattr = 0x0;
	}
      else if (param[i] == 22 || param[i] == 24)	/* bold off */
	{
	  SCREEN_ATTR_MOD_BOLD (screen->curattr, 0);
	}
      else if (param[i] == 25)				/* blink off */
	{
	  SCREEN_ATTR_MOD_BLINK (screen->curattr, 0);
	}
      else if (param[i] == 28)				/* invisible off */
	{
	  screen->curattr = 0x70;
	}
      else if (param[i] >= 30 && param[i] <= 37)	/* set fg */
	{
	  SCREEN_ATTR_MOD_FG (screen->curattr, param[i] - 30);
	}
      else if (param[i] >= 40 && param[i] <= 47)	/* set bg */
	{
	  SCREEN_ATTR_MOD_BG (screen->curattr, param[i] - 40);
	}
      else if (param[i] == 39)		/* reset foreground to default */
	{
	  SCREEN_ATTR_MOD_FG (screen->curattr, 7);
	}
      else if (param[i] == 49)		/* reset background to default */
	{
	  SCREEN_ATTR_MOD_BG (screen->curattr, 0);
	}
    }
}

/* interprets an 'erase display' (ED) escape sequence
 */
static void
interpret_csi_ED (TScreen *screen, gint param[], gint pcount)
{
  gint r, c;
  gint start_row, start_col, end_row, end_col;

  /* decide range */
  if (pcount && param[0] == 2)
    {
      start_row = start_col = 0;
      end_row = screen->rows - 1;
      end_col = screen->cols - 1;
    }
  else if (pcount && param[0] == 1)
    {
      start_row = start_col = 0;
      end_row = screen->crow;
      end_col = screen->ccol;
    }
  else
    {
      start_row = screen->crow;
      start_col = screen->ccol;
      end_row = screen->rows - 1;
      end_col = screen->cols - 1;
    }

  /* clean range
   */
  for (r = start_row; r <= end_row; r++)
    {
      screen->line_dirty[r] = TRUE;

      for (c = (r == start_row ? start_col : 0);
	   c <= (r == end_row ? end_col : screen->cols - 1); c++)
	{
	  screen->cells[r][c].ch = 0x20;
	  screen->cells[r][c].attr = screen->curattr;
	}
    }
}

/* interprets a 'move cursor' (CUP) escape sequence
 */
static void
interpret_csi_CUP (TScreen *screen, gint param[], gint pcount)
{
  /* special case */
  if (pcount == 0)
    {
      screen->crow = screen->ccol = 0;
      return;
    }
  else if (pcount < 2)
    return;			/* malformed */

  screen->crow = param[0] - 1;	/* convert from 1-based to 0-based */
  screen->ccol = param[1] - 1;	/* convert from 1-based to 0-based */

  screen->curpos_dirty = TRUE;

  clamp_cursor_to_bounds (screen);
}

/* Interpret the 'relative mode' sequences: CUU, CUD, CUF, CUB, CNL,
 * CPL, CHA, HPR, VPA, VPR, HPA
 */
static void
interpret_csi_C (TScreen *screen, gchar verb, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;

  switch (verb)
    {
    case 'A':
      screen->crow -= n;
      break;
    case 'B':
    case 'e':
      screen->crow += n;
      break;
    case 'C':
    case 'a':
      screen->ccol += n;
      break;
    case 'D':
      screen->ccol -= n;
      break;
    case 'E':
      screen->crow += n;
      screen->ccol = 0;
      break;
    case 'F':
      screen->crow -= n;
      screen->ccol = 0;
      break;
    case 'G':
    case '`':
      screen->ccol = param[0] - 1;
      break;
    case 'd':
      screen->crow = param[0] - 1;
      break;
    }

  screen->curpos_dirty = TRUE;
  clamp_cursor_to_bounds (screen);
}

/* Interpret the 'erase line' escape sequence
 */
static void
interpret_csi_EL (TScreen *screen, gint param[], gint pcount)
{
  gint erase_start, erase_end, i;
  gint cmd = pcount ? param[0] : 0;

  switch (cmd)
    {
    case 1:
      erase_start = 0;
      erase_end = screen->ccol;
      break;
    case 2:
      erase_start = 0;
      erase_end = screen->cols - 1;
      break;
    default:
      erase_start = screen->ccol;
      erase_end = screen->cols - 1;
      break;
    }

  for (i = erase_start; i <= erase_end; i++)
    {
      screen->cells[screen->crow][i].ch = 0x20;
      screen->cells[screen->crow][i].attr = screen->curattr;
    }

  screen->line_dirty[screen->crow] = TRUE;
}

/* Interpret the 'insert blanks' sequence (ICH)
 */
static void
interpret_csi_ICH (TScreen *screen, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;
  gint i;

  for (i = screen->cols - 1; i >= screen->ccol + n; i--)
    {
      screen->cells[screen->crow][i] = screen->cells[screen->crow][i - n];
    }
  for (i = screen->ccol; i < screen->ccol + n; i++)
    {
      screen->cells[screen->crow][i].ch = 0x20;
      screen->cells[screen->crow][i].attr = screen->curattr;
    }

  screen->line_dirty[screen->crow] = TRUE;
}

/* Interpret the 'delete chars' sequence (DCH)
 */
static void
interpret_csi_DCH (TScreen *screen, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;
  gint i;

  for (i = screen->ccol; i < screen->cols; i++)
    {
      if (i + n < screen->cols)
	{
	  screen->cells[screen->crow][i] = screen->cells[screen->crow][i + n];
	}
      else
	{
	  screen->cells[screen->crow][i].ch = 0x20;
	  screen->cells[screen->crow][i].attr = screen->curattr;
	}
    }

  screen->line_dirty[screen->crow] = TRUE;
}

/* Interpret an 'insert line' sequence (IL)
 */
static void
interpret_csi_IL (TScreen *screen, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;
  gint i, j;

  for (i = screen->priv->scrollbottom; i >= screen->crow + n; i--)
    {
      memcpy (screen->cells[i], screen->cells[i - n],
	      sizeof (TScreenCell) * screen->cols);
    }
  for (i = screen->crow; i < screen->crow + n && i <= screen->priv->scrollbottom; i++)
    {
      screen->line_dirty[i] = TRUE;
      for (j = 0; j < screen->cols; j++)
	{
	  screen->cells[i][j].ch = 0x20, screen->cells[i][j].attr = screen->curattr;
	}
    }
}

/* Interpret a 'delete line' sequence (DL)
 */
static void
interpret_csi_DL (TScreen *screen, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;
  gint i, j;

  for (i = screen->crow; i <= screen->priv->scrollbottom; i++)
    {
      screen->line_dirty[i] = TRUE;

      if (i + n <= screen->priv->scrollbottom)
	{
	  memcpy (screen->cells[i], screen->cells[i + n],
		  sizeof (TScreenCell) * screen->cols);
	}
      else
	{
	  for (j = 0; j < screen->cols; j++)
	    {
	      screen->cells[i][j].ch = 0x20;
	      screen->cells[i][j].attr = screen->curattr;
	    }
	}
    }
}

/* Interpret an 'erase characters' (ECH) sequence
 */
static void
interpret_csi_ECH (TScreen * screen, gint param[], gint pcount)
{
  gint n = (pcount && param[0] > 0) ? param[0] : 1;
  gint i;

  for (i = screen->ccol; i < screen->ccol + n && i < screen->cols; i++)
    {
      screen->cells[screen->crow][i].ch = 0x20;
      screen->cells[screen->crow][i].attr = screen->curattr;
    }

  screen->line_dirty[screen->crow] = TRUE;
}

/* Interpret a 'set scrolling region' (DECSTBM) sequence
 */
static void
interpret_csi_DECSTBM (TScreen *screen, gint param[], gint pcount)
{
  gint newtop, newbottom;

  if (!pcount)
    {
      newtop = 0;
      newbottom = screen->rows - 1;
    }
  else if (pcount < 2)		/* malformed */
    {
      return;
    }
  else
    {
      newtop = param[0] - 1;
      newbottom = param[1] - 1;
    }

  t_println ("set scroll range to %d-%d", newtop, newbottom);

  /* clamp to bounds
   */
  if (newtop < 0)
    {
      newtop = 0;
    }
  if (newtop >= screen->rows)
    {
      newtop = screen->rows - 1;
    }
  if (newbottom < 0)
    {
      newbottom = 0;
    }
  if (newbottom >= screen->rows)
    {
      newbottom = screen->rows - 1;
    }

  /* check for range validity
   */
  if (newtop > newbottom)
    {
      return;
    }
  screen->priv->scrolltop = newtop;
  screen->priv->scrollbottom = newbottom;
}

/** 
 * interpret_csi_SAVECUR:
 * @screen 
 * @param 
 * @pcount 
 */
static void
interpret_csi_SAVECUR (TScreen *screen, gint param[], gint pcount)
{
  screen->priv->saved_x = screen->ccol;
  screen->priv->saved_y = screen->crow;
}

/** 
 * interpret_csi_RESTORECUR:
 * @screen 
 * @param 
 * @pcount 
 */
static void
interpret_csi_RESTORECUR (TScreen *screen, gint param[], gint pcount)
{
  screen->ccol = screen->priv->saved_x;
  screen->crow = screen->priv->saved_y;
  screen->curpos_dirty = TRUE;
}

/** 
 * t_es_interpret_csi:
 * @screen:
 */
void
t_es_interpret_csi (TScreen *screen)
{
  static gint csiparam[MAX_CSI_ES_PARAMS];
  gint param_count = 0;
  gchar verb;
  const gchar *p;

  p = screen->priv->esbuf + 1;
  verb = screen->priv->esbuf[screen->priv->esbuf_len - 1];

  /* private-mode CSI, ignore
   */
  if (!strncmp (screen->priv->esbuf, "[?", 2))
    {
      t_println ("ignoring private-mode CSI: <%s>", screen->priv->esbuf);
      return;
    }

  /* parse numeric parameters
   */
  while ((*p >= '0' && *p <= '9') || *p == ';')
    {
      if (*p == ';')
	{
	  if (param_count >= MAX_CSI_ES_PARAMS)
	    {
	      t_println ("param count %d too long", param_count);
	      return;
	    }
	  csiparam[param_count++] = 0;
	}
      else
	{
	  if (param_count == 0)
	    {
	      csiparam[param_count++] = 0;
	    }
	  csiparam[param_count - 1] *= 10;
	  csiparam[param_count - 1] += *p - '0';
	}
      p++;
    }

  /* delegate handling depending on command character (verb)
   */
  switch (verb)
    {
    case 'm':			/* it's a 'set attribute' sequence */
      interpret_csi_SGR (screen, csiparam, param_count);
      break;
    case 'J':			/* it's an 'erase display' sequence */
      interpret_csi_ED (screen, csiparam, param_count);
      break;
    case 'H':
    case 'f':			/* it's a 'move cursor' sequence */
      interpret_csi_CUP (screen, csiparam, param_count);
      break;
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'E':
    case 'F':
    case 'G':
    case 'e':
    case 'a':
    case 'd':
    case '`':			/* it is a 'relative move' */
      interpret_csi_C (screen, verb, csiparam, param_count);
      break;
    case 'K':			/* erase line */
      interpret_csi_EL (screen, csiparam, param_count);
      break;
    case '@':			/* insert characters */
      interpret_csi_ICH (screen, csiparam, param_count);
      break;
    case 'P':			/* delete characters */
      interpret_csi_DCH (screen, csiparam, param_count);
      break;
    case 'L':			/* insert lines */
      interpret_csi_IL (screen, csiparam, param_count);
      break;
    case 'M':			/* delete lines */
      interpret_csi_DL (screen, csiparam, param_count);
      break;
    case 'X':			/* erase chars */
      interpret_csi_ECH (screen, csiparam, param_count);
      break;
    case 'r':			/* set scrolling region */
      interpret_csi_DECSTBM (screen, csiparam, param_count);
      break;
    case 's':			/* save cursor location */
      interpret_csi_SAVECUR (screen, csiparam, param_count);
      break;
    case 'u':			/* restore cursor location */
      interpret_csi_RESTORECUR (screen, csiparam, param_count);
      break;
    case 'l':			/* for auxport */
    case 'h':			/* for auxport */
      break;
    default:
      t_println (_("unrecogized CSI: '%s'"), screen->priv->esbuf);
      break;
    }
}

/** 
 * cursor_line_down:
 * @screen:
 */
static void
cursor_line_down (TScreen *screen)
{
  gint i;

  screen->crow++;
  screen->curpos_dirty = TRUE;

  if (screen->crow <= screen->priv->scrollbottom)
    return;

  /* must scroll the scrolling region up by 1 line, and put cursor on 
   * last line of it
   */
  screen->crow = screen->priv->scrollbottom;

  for (i = screen->priv->scrolltop; i < screen->priv->scrollbottom; i++)
    {
      screen->line_dirty[i] = TRUE;
      memcpy (screen->cells[i], screen->cells[i + 1],
	      sizeof (TScreenCell) * screen->cols);
    }
  screen->line_dirty[screen->priv->scrollbottom] = TRUE;

  /* clear last row of the scrolling region
   */
  for (i = 0; i < screen->cols; i++)
    {
      screen->cells[screen->priv->scrollbottom][i].ch = 0x20;
      screen->cells[screen->priv->scrollbottom][i].attr = 0x70;
    }
}

/** 
 * cursor_line_up:
 * @screen:
 */
static void
cursor_line_up (TScreen *screen)
{
  gint i;

  screen->crow--;
  screen->curpos_dirty = TRUE;

  if (screen->crow >= screen->priv->scrolltop)
    return;

  /* must scroll the scrolling region up by 1 line, and put cursor on 
   * first line of it
   */
  screen->crow = screen->priv->scrolltop;

  for (i = screen->priv->scrollbottom; i > screen->priv->scrolltop; i--)
    {
      screen->line_dirty[i] = TRUE;
      memcpy (screen->cells[i], screen->cells[i - 1],
	      sizeof (TScreenCell) * screen->cols);
    }

  screen->line_dirty[screen->priv->scrolltop] = TRUE;

  /* clear first row of the scrolling region
   */
  for (i = 0; i < screen->cols; i++)
    {
      screen->cells[screen->priv->scrolltop][i].ch = 0x20;
      screen->cells[screen->priv->scrolltop][i].attr = 0x70;
    }
}

/** 
 * put_normal_char:
 * @screen 
 * @c 
 */
static inline void
put_normal_char (TScreen *screen, gchar c)
{
  if (screen->ccol >= screen->cols)
    {
      screen->ccol = 0;
      cursor_line_down (screen);
    }
  screen->cells[screen->crow][screen->ccol].ch = c;
  screen->cells[screen->crow][screen->ccol].attr = screen->curattr;
  screen->ccol++;

  screen->line_dirty[screen->crow] = TRUE;
  screen->curpos_dirty = TRUE;
}

/** 
 * put_graphmode_char:
 * @screen:
 * @c:
 */
static inline void
put_graphmode_char (TScreen *screen, gchar c)
{
  gchar nc;

  /* do some very pitiful translation to regular ascii chars
   */
  switch (c)
    {
    case 'j':
    case 'k':
    case 'l':
    case 'm':
    case 'n':
    case 't':
    case 'u':
    case 'v':
    case 'w':
      nc = '+';
      break;
    case 'x':
      nc = '|';
      break;
    default:
      nc = '%';
    }
  put_normal_char (screen, nc);
}

/** 
 * new_escape_sequence:
 * @screen:
 */
static inline void
new_escape_sequence (TScreen *screen)
{
  screen->priv->escaped = TRUE;
  screen->priv->esbuf_len = 0;
  screen->priv->esbuf[0] = '\0';
}

/** 
 * cancel_escape_sequence:
 * @screen:
 */
static inline void
cancel_escape_sequence (TScreen *screen)
{
  screen->priv->escaped = FALSE;
  screen->priv->esbuf_len = 0;
  screen->priv->esbuf[0] = '\0';
}

/** 
 * handle_control_char:
 * @screen:
 * @c:
 */
static void
handle_control_char (TScreen *screen, gchar c)
{
  switch (c)
    {
    case '\r':			/* carriage return */
      if (screen->ccol != 0)
	{
	  screen->curpos_dirty = TRUE;
	}
      screen->ccol = 0;
      break;
    case '\n':			/* line feed */
//#ifndef SCO_SV
#if 0
      screen->ccol = 0;
#endif
      cursor_line_down (screen);
      screen->curpos_dirty = TRUE;
      break;
    case '\b':			/* backspace */
      if (screen->ccol > 0)
	{
	  screen->ccol--;
	  screen->curpos_dirty = TRUE;
	}
      break;
    case '\t':			/* tab */
#ifndef SCO_SV
      while (screen->ccol % 8)
	{
	  put_normal_char (screen, ' ');
	}
#else
      if (screen->ccol % 8)
	{
	  put_normal_char (screen, ' ');
	}
      else
	{
	  int i;
	  for (i = 0; i < 8; i++)
	    {
	      put_normal_char (screen, ' ');
	    }
	}
#endif
      break;
    case '\x1B':		/* begin escape sequence (aborting previous one if any) */
      new_escape_sequence (screen);
      break;
    case '\x0E':		/* enter graphical character mode */
      t_println ("enter graphics mode.");
      screen->priv->graphmode = TRUE;
      break;
    case '\x0F':		/* exit graphical character mode */
      t_println ("leave graphics mode.");
      screen->priv->graphmode = FALSE;
      break;
    case '\x9B':		/* CSI character. equivalent to ESC [ */
      new_escape_sequence (screen);
      screen->priv->esbuf[screen->priv->esbuf_len++] = '[';
      break;
    case '\x18':
    case '\x1A':		/* these interrupt escape sequences */
      cancel_escape_sequence (screen);
      break;
    case '\a':			/* bell, ignore unless it is end of Set Text Parameters */
      break;

    case '\x1D':		/* graphics mode */
      break;

    default:
      t_println (_("unrecognized control char: %d (^%c)"), c, c + '@');
      break;
    }
}

/** 
 * is_valid_csi_ender:
 * @c:
 * 
 * @Returns: 
 */
static inline gboolean
is_valid_csi_ender (gchar c)
{
  /* < and = is add for device control sequences.
   */
  return ((c >= 'a' && c <= 'z') ||
	  (c >= 'A' && c <= 'Z') ||
	  c == '@' || c == '`' ||
	  c == '=' || c == '-');
}

/** 
 * try_interpret_escape_seq:
 * @screen 
 */
static void
try_interpret_escape_seq (TScreen *screen)
{
  gchar firstchar;
  gchar lastchar;

  firstchar = screen->priv->esbuf[0];
  lastchar = screen->priv->esbuf[screen->priv->esbuf_len - 1];

  /* too early to do anything
   */
  if (!firstchar)
    return;

  /* interpret ESC-M as reverse line-feed
   */
  if (firstchar == 'M')
    {
      cursor_line_up (screen);
      cancel_escape_sequence (screen);
      return;
    }

  /* unrecognized escape sequence. Let's forget about it.
   */
  if (firstchar != '[' && firstchar != ']' &&
      firstchar != '(' && firstchar != ')' &&
      firstchar != 'O' && firstchar != 'F' &&
      firstchar != 'P' && firstchar != '^' &&
      firstchar != '_' && firstchar != '=')
    {
      t_println (_("unrecognized ES: '%s'"), screen->priv->esbuf);
      cancel_escape_sequence (screen);
      return;
    }

  /* we have a csi escape sequence: interpret it
   */
  if (firstchar == '[' && is_valid_csi_ender (lastchar))
    {
      t_es_interpret_csi (screen);
      cancel_escape_sequence (screen);
    }
  /* we have an xscreen escape sequence: interpret it
   */
  else if (firstchar == ']' && lastchar == '\a')
    {
      /* TODO!
       * t_es_interpret_xterm_es (screen);
       */
      t_println (_("ignored xscreen ES: ^[]ps;pt\\a."));
      cancel_escape_sequence (screen);
    }
  /* designate g0 character set (ISO 2022)
   */
  else if ((firstchar == '(' || firstchar == ')') &&
	   screen->priv->esbuf_len == 2)
    {
      t_println (_("ignored character set switch: ^[(n."));
      cancel_escape_sequence (screen);
    }
  else if (firstchar == 'O')
    {
      t_println (_("ignored single shift select of G3 character set (SS3)."));
      cancel_escape_sequence (screen);
    }
  else if (firstchar == 'F')
    {
      /* cursor to lower left corner of screen (if enabled by the
       * hpLowerleftBugCompat resource).
       */
      screen->crow = 1000;
      screen->ccol = 1000;
      clamp_cursor_to_bounds (screen);
      cancel_escape_sequence (screen);
    }
  else if (firstchar == 'P' && lastchar == '\\')
    {
      t_println (_("ignore device control string (DCS) ^[Ppt^[\\"));
      cancel_escape_sequence (screen);
    }
  else if (firstchar == '^' && lastchar == '\\')
    {
      t_println (_("ignore privacy message (PM) ^[^pt^[\\"));
      cancel_escape_sequence (screen);
    }
  else if (firstchar == '_' && lastchar == '\\')
    {
      t_println (_("ignore application program command (APC) ^[_pt^[\\"));
      cancel_escape_sequence (screen);
    }
  else if (firstchar == '=')
    {
      t_println (_("ignore Application Keypad (DECPAM) ^[="));
      cancel_escape_sequence (screen);
    }

  /* if the escape sequence took up all available space and could
   * not yet be parsed, abort it
   */
  if (screen->priv->esbuf_len + 1 >= SCREEN_ESEQ_SIZE)
    {
      cancel_escape_sequence (screen);
    }
}

typedef struct _TAuxCommand	TAuxCommand;
struct _TAuxCommand
{
  gint     type;
  gboolean for_open;
  gchar   *sequence;
  gint     length;
};

static TAuxCommand	t_aux_commands[] =
{
    /* parallel port (printer).
     */
    { 0, TRUE,		"\x1B[5i",	4 },	/* open parallel port.  */
    { 0, FALSE,		"\x1B[4i",	4 },	/* close parallel port. */

    /* CreatWall.
     */
    /* open auxiliary port 1/2/3/4. */
    { 1, TRUE,		"\x1B[>\x1B[0-",	7 },
    { 1, TRUE,		"\x1B[1>\x1B[0-",	8 },
    { 1, TRUE,		"\x1B[2>\x1B[0-",	8 },
    { 1, TRUE,		"\x1B[3>\x1B[0-",	8 },
    { 1, TRUE,		"\x1B[0-",		4 },
    { 1, FALSE,		"\x1B[<\x1B[0=",	7 }, /* close auxiliary port. */

    /* Star.
     */
    /* open auxiliary port 1/2/3 */
    { 2, TRUE,		"\x1B!0;0;0;0Y",	10 },
    { 2, TRUE,		"\x1B!0;0;0;0Z",	10 },
    { 2, TRUE,		"\x1B!0;0;0;0X",	10 },
    { 2, TRUE,		"\x1B!0;0;0;0V",	10 },
    { 2, TRUE,		"\x1B!1;0;0;0Y",	10 },
    { 2, TRUE,		"\x1B!1;0;0;0Z",	10 },
    { 2, TRUE,		"\x1B!1;0;0;0X",	10 },
    { 2, TRUE,		"\x1B!1;0;0;0V",	10 },
    { 2, TRUE,		"\x1B!2;0;0;0Y",	10 },
    { 2, TRUE,		"\x1B!2;0;0;0Z",	10 },
    { 2, TRUE,		"\x1B!2;0;0;0X",	10 },
    { 2, TRUE,		"\x1B!2;0;0;0V",	10 },
    { 2, TRUE,		"\x1B!3;0;0;0Y",	10 },
    { 2, TRUE,		"\x1B!3;0;0;0Z",	10 },
    { 2, TRUE,		"\x1B!3;0;0;0X",	10 },
    { 2, TRUE,		"\x1B!3;0;0;0V",	10 },
    { 2, TRUE,		"\x1B!3;0;0;1Z",	10 },
    { 2, FALSE,		"\x1B[/50l",		6 },	/* close auxiliary port. */
    { 2, FALSE,		"\x1B[/51h",		6 },	/* close auxiliary port. */

    { -1, FALSE, NULL, 0 }
};
static gint		t_aux_current = -1;

/**
 * t_screen_check_aux_command:
 */
static inline gint
t_screen_check_aux_command (TScreen      *screen,
			    const guchar *data,
			    gsize         size)
{
  gint i;

  for (i = 0; t_aux_commands[i].sequence; i++)
    {
      if (size < t_aux_commands[i].length)
	continue;

      if (strncmp (t_aux_commands[i].sequence, (gchar*) data,
		   t_aux_commands[i].length) == 0)
	{
	  if (t_aux_commands[i].for_open)
	    {
	      t_println ("terminal %d open auxility port command.",
			 t_aux_commands[i].type);

	      screen->priv->aux_is_open = TRUE;
	      t_aux_current = i;
	    }
	  else
	    {
	      t_println ("terminal %d close auxility port command.",
			 t_aux_commands[i].type);

	      if (t_aux_current == -1)
		{
#ifdef T_DEBUG
		  g_warning (_("found auxility close command for terminal type"
			     " %d, but no open command been found before."),
			     t_aux_commands[i].type);
		  g_warning (_("the close sequences was `%s'"),
			     t_aux_commands[i].sequence);
#endif
		}
	      else if (t_aux_commands[i].type != t_aux_commands[t_aux_current].type)
		{
		  g_warning (_("auxility close command type dismatch open command."));
		}
	      screen->priv->aux_is_open = FALSE;
	      t_aux_current = -1;
	    }
	  return t_aux_commands[i].length;
	}
    }
  return -1;
}

/** 
 * t_screen_inject:
 * @screen:
 * @data:
 * @len:
 *
 * Inject data into the terminal. <data> needs NOT be 0-terminated:
 * its length is solely determined by the <length> parameter. Please
 * notice that this writes directly to the terminal, that is,
 * this function does NOT send the data to the forked process
 * running in the terminal (if any). For that, you might want
 * to use rote_vt_write.
 */
void
t_screen_inject (TScreen *screen, const guchar *data, gint len)
{
  gint i, size;

  g_return_if_fail (screen);

  if (!data || len <= 0)
    return;

  t_println ("inject to screen %d bytes", len);

  for (i = 0; i < len; i++, data++)
    {
      if (i > 0 && (i % 500) == 0)
	g_usleep (10);

      if (*data == 0x1B)
	{
	  size = t_screen_check_aux_command (screen, data, len - i);
	  if (size > 0)
	    {
	      t_println ("check auxility port command, skip %d bytes.", size);
	      data = data + size - 1;
	      i += size - 1;
	      continue;
	    }
	}
      /* ignore incoming data when auxility is open.
      */
      if (screen->priv->aux_is_open)
	{
	  continue;
	}

      if (*data == 0)
	{
	  t_println ("completely ingore NUL.");
	  continue;
	}

      if ((*data >= 1 && *data <= 31) || *data == '\x9B')
	{
	  t_println ("process control character 0x%02x.", *data);

	  handle_control_char (screen, *data);

	  /* \a is end of Set Text Parameters command.
	   */
	  if (*data != '\a')
	    continue;
	}

      if (screen->priv->escaped && screen->priv->esbuf_len < SCREEN_ESEQ_SIZE)
	{
	  /* append character to ongoing escape sequence
	   */
	  screen->priv->esbuf[screen->priv->esbuf_len] = *data;
	  screen->priv->esbuf[++screen->priv->esbuf_len] = 0;

	  t_println ("process escape sequences '%s'", screen->priv->esbuf);

	  try_interpret_escape_seq (screen);
	}
      else if (screen->priv->graphmode)
	{
	  t_println ("put a graphics mode character '0x%02x(%c)'.", *data, *data);
	  put_graphmode_char (screen, *data);
	}
      else
	{
	  t_println ("put a normal character '0x%02x(%c)'.", *data, *data);

	  if (*data > 31)
	    put_normal_char (screen, *data);
	}
    }
}
