#!/bin/sh
#
# TtyUtils -- UNIX Terminal Utilities.
# Copyright (C) 2008,2009,2010,2011 xiaohu, All Rights Reserved.
#
# This is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 675 Mass Ave, Cambridge, MA 02139, USA.
#

cflags=-I"`pwd`/usr/include -I`pwd`/usr/include/cdk"
libs="-L`pwd`/usr/lib"

PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/freeware/lib/pkgconfig:$PWD/usr/lib/pkgconfig:/dcc/ttyutils/lib/pkgconfig
export PKG_CONFIG_PATH

PATH=$HOME/usr/bin:$PATH
export PATH

osname=`uname`
case $osname in
	AIX)
	./configure CC=xlc_r CFLAGS="-D_SYSV_IPC $cflags" LIBS="$libs" \
	--prefix=$HOME/ttyutils --disable-ncursesw --disable-manual --disable-debug
	;;

	Linux)
	./configure CC=gcc CFLAGS="-D_SYSV_IPC -Wall $cflags" LIBS="$libs" \
	--prefix=/dcc/ttyutils --includedir=$HOME/usr/include --disable-manual --disable-ncursesw --disable-static --disable-debug
	;;

	SCO_SV)
	./configure CC=gcc CFLAGS="-DSCO_SV -D_SYSV_IPC $cflags" LIBS="$libs" \
	--prefix=$HOME/ttyutils --disable-ncursesw --disable-manual
	;;
esac
