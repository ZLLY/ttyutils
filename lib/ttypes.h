/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_TYPES_H__
#define __T_TYPES_H__

#include <sys/types.h>
#include <glib.h>

G_BEGIN_DECLS

#define T_KEY_ESC	0x1b

#define T_KEY_HOME	256
#define T_KEY_END	257
#define T_KEY_DOWN	258
#define T_KEY_UP	259
#define T_KEY_LEFT	260
#define T_KEY_RIGHT	261
#define T_KEY_PGDOWN	262
#define T_KEY_PGUP	263
#define T_KEY_INSERT	264
#define T_KEY_DELETE	265

#define	T_KEY_F0	300
#define	T_KEY_F1	T_KEY_F0 + 1
#define	T_KEY_F2	T_KEY_F0 + 2
#define	T_KEY_F3	T_KEY_F0 + 3
#define	T_KEY_F4	T_KEY_F0 + 4
#define	T_KEY_F5	T_KEY_F0 + 5
#define	T_KEY_F6	T_KEY_F0 + 6
#define	T_KEY_F7	T_KEY_F0 + 7
#define	T_KEY_F8	T_KEY_F0 + 8
#define	T_KEY_F9	T_KEY_F0 + 9
#define	T_KEY_F10	T_KEY_F0 + 10
#define	T_KEY_F11	T_KEY_F0 + 11
#define	T_KEY_F12	T_KEY_F0 + 12
#define	T_KEY_F13	T_KEY_F0 + 13
#define	T_KEY_F14	T_KEY_F0 + 14
#define	T_KEY_F15	T_KEY_F0 + 15
#define	T_KEY_F16	T_KEY_F0 + 16
#define	T_KEY_F17	T_KEY_F0 + 17
#define	T_KEY_F18	T_KEY_F0 + 18
#define	T_KEY_F19	T_KEY_F0 + 19
#define	T_KEY_F20	T_KEY_F0 + 20
#define	T_KEY_F21	T_KEY_F0 + 21
#define	T_KEY_F22	T_KEY_F0 + 22
#define	T_KEY_F23	T_KEY_F0 + 23
#define	T_KEY_F24	T_KEY_F0 + 24
#define	T_KEY_F25	T_KEY_F0 + 25
#define	T_KEY_F26	T_KEY_F0 + 26
#define	T_KEY_F27	T_KEY_F0 + 27
#define	T_KEY_F28	T_KEY_F0 + 28
#define	T_KEY_F29	T_KEY_F0 + 29
#define	T_KEY_F30	T_KEY_F0 + 30
#define	T_KEY_F31	T_KEY_F0 + 31
#define	T_KEY_F32	T_KEY_F0 + 32
#define	T_KEY_F33	T_KEY_F0 + 33
#define	T_KEY_F34	T_KEY_F0 + 34
#define	T_KEY_F35	T_KEY_F0 + 35
#define	T_KEY_F36	T_KEY_F0 + 36
#define	T_KEY_F37	T_KEY_F0 + 37
#define	T_KEY_F38	T_KEY_F0 + 38
#define	T_KEY_F39	T_KEY_F0 + 39
#define	T_KEY_F40	T_KEY_F0 + 40
#define	T_KEY_F41	T_KEY_F0 + 41
#define	T_KEY_F42	T_KEY_F0 + 42
#define	T_KEY_F43	T_KEY_F0 + 43
#define	T_KEY_F44	T_KEY_F0 + 44
#define	T_KEY_F45	T_KEY_F0 + 45
#define	T_KEY_F46	T_KEY_F0 + 46
#define	T_KEY_F47	T_KEY_F0 + 47
#define	T_KEY_F48	T_KEY_F0 + 48
#define	T_KEY_F49	T_KEY_F0 + 49
#define	T_KEY_F50	T_KEY_F0 + 50
#define	T_KEY_F51	T_KEY_F0 + 51
#define	T_KEY_F52	T_KEY_F0 + 52
#define	T_KEY_F53	T_KEY_F0 + 53
#define	T_KEY_F54	T_KEY_F0 + 54
#define	T_KEY_F55	T_KEY_F0 + 55
#define	T_KEY_F56	T_KEY_F0 + 56
#define	T_KEY_F57	T_KEY_F0 + 57
#define	T_KEY_F58	T_KEY_F0 + 58
#define	T_KEY_F59	T_KEY_F0 + 59
#define	T_KEY_F60	T_KEY_F0 + 60


#define T_PARAM_READABLE	G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB
#define T_PARAM_WRITABLE	G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB
#define T_PARAM_READWRITE	G_PARAM_READWRITE| G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB

typedef struct _TRpcRequest	TRpcRequest;
typedef struct _TRpcResponse	TRpcResponse;

/* rpc request struct.
 */
struct _TRpcRequest
{
  guint16 id;
  guint16 code;
  pid_t   pid;
  uid_t   uid;
  gid_t   gid;
  guint32 size;
};

/* rpc response struct.
 */
struct _TRpcResponse
{
  guint16 id;
  guint16 code;
  guint32 size;
};

/* rpc request code.
 */
typedef enum
{
  RPC_CODE_SNAP,
  RPC_CODE_FEED,
  RPC_CODE_GET_EVENTS,
  RPC_CODE_INSERT_EVENT,
  RPC_CODE_REMOVE_EVENT,
  RPC_CODE_REMOVE_EVENT_GROUP,
  RPC_CODE_STDIN_SUSPEND,
  RPC_CODE_STDIN_RESUME,
  RPC_CODE_STDOUT_SUSPEND,
  RPC_CODE_STDOUT_RESUME,
  RPC_CODE_TRIGGER_EVENT,
  RPC_CODE_CURSOR_XY,
  RPC_CODE_REFRESH,
  RPC_CODE_SET_ENV,
} TRpcCode;

/* rpc error code.
 */
typedef enum
{
  RPC_ERR_OK,
  RPC_ERR_BADCODE,
  RPC_ERR_BADSIZE,
  RPC_ERR_FORMAT,
  RPC_ERR_TIMEOUT,
  RPC_ERR_EOF,
  RPC_ERR_SYSTEM,
} TRpcErrno;

typedef struct _TRpcSnapOut
{
  guint16 rows;
  guint16 cols;
  gchar   data[0];
} TRpcSnapOut;

typedef struct _TRpcFeedIn
{
  guint32 length;
  gchar   string[0];
} TRpcFeedIn;

typedef struct _TRpcCurPosOut
{
  guint16 row;
  guint16 col;
} TRpcCurPosOut;

/* usable event type.
 */
typedef enum
{
  EVENT_TYPE_CURSOR   = 1,
  EVENT_TYPE_CONTENT  = 2,
  EVENT_TYPE_HOTKEY   = 3,
  EVENT_TYPE_KEYBOARD = 4,
} TEventType;

typedef struct _TEventEntry
{
  gchar   name[64];
  gchar   group[64];
  gchar   matcher[128];
  gchar   program[128];
} TEventEntry;


G_END_DECLS

#endif /* __T_TYPES_H__ */
