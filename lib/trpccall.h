/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_RPC_CALL_H__
#define __T_RPC_CALL_H__

#include <glib.h>

#include "ttypes.h"

G_BEGIN_DECLS

typedef enum
{
  RPC_TARGET_TTY,
  RPC_TARGET_PTS,
  RPC_TARGET_PID,
  RPC_TARGET_PATH
} TRpcTargetType;

typedef struct _TRpcTarget	TRpcTarget;
struct _TRpcTarget
{
  TRpcTargetType rt_type;
  guint32        rt_timeout;
  union
    {
      gchar *t_tty;
      gchar *t_pts;
      pid_t  t_pid;
      gchar *t_path;
    } rt_target;
};
#define rt_tty	rt_target.t_tty
#define rt_pts	rt_target.t_pts
#define rt_pid	rt_target.t_pid
#define rt_path	rt_target.t_path

gboolean	t_rpc_call_set_target		(TRpcTarget  *target);

gboolean	t_rpc_call_snap			(guint16      *rows,
						 guint16      *cols,
						 gchar      ***snap);
gboolean	t_rpc_call_feed			(const gchar  *string,
						 gssize        length);

gboolean	t_rpc_call_get_events		(TEventEntry **events,
						 gsize        *number);
gboolean	t_rpc_call_insert_event		(const gchar *name,
						 const gchar *group,
						 const gchar *matcher,
						 const gchar *program);
gboolean	t_rpc_call_remove_event		(const gchar *name);
gboolean	t_rpc_call_remove_event_group	(const gchar *group);
gboolean	t_rpc_call_trigger_event	(const gchar *name);

gboolean	t_rpc_call_stdin_suppend	(void);
gboolean	t_rpc_call_stdin_resume		(void);
gboolean	t_rpc_call_stdout_suppend	(void);
gboolean	t_rpc_call_stdout_resume	(void);
gboolean	t_rpc_call_get_cursor_pos	(guint16     *row,
						 guint16     *col);
gboolean	t_rpc_call_refresh		(void);
gboolean        t_rpc_call_set_env		(const gchar *env);

G_END_DECLS

#endif /* __T_RPC_CALL_H__ */
