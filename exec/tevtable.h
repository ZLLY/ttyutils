/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef _T_EVENT_TABLE_H_
#define _T_EVENT_TABLE_H_

#include <glib-object.h>

#include "tevent.h"

G_BEGIN_DECLS

#define T_TYPE_EVENT_TABLE             (t_event_table_get_type ())
#define T_EVENT_TABLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), T_TYPE_EVENT_TABLE, TEventTable))
#define T_EVENT_TABLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), T_TYPE_EVENT_TABLE, TEventTableClass))
#define T_IS_EVENT_TABLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), T_TYPE_EVENT_TABLE))
#define T_IS_EVENT_TABLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), T_TYPE_EVENT_TABLE))
#define T_EVENT_TABLE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), T_TYPE_EVENT_TABLE, TEventTableClass))

typedef struct _TEventTable		TEventTable;
typedef struct _TEventTableClass	TEventTableClass;
typedef struct _TEventTablePrivate	TEventTablePrivate;

struct _TEventTable
{
  GObject object;

  TEventTablePrivate *priv;
};

struct _TEventTableClass
{
  GObjectClass object_class;
};

GType		t_event_table_get_type		(void) G_GNUC_CONST;

TEventTable*	t_event_table_new		(void);

gboolean	t_event_table_insert		(TEventTable     *etable,
						 TEvent          *event);
gboolean	t_event_table_remove		(TEventTable     *etable,
						 const gchar     *name);
gboolean	t_event_table_remove_group	(TEventTable     *etable,
						 const gchar     *group);
TEvent*		t_event_table_lookup		(TEventTable     *etable,
						 const gchar     *name);
void		t_event_table_foreach		(TEventTable     *etable,
						 GHFunc           func,
						 gpointer         user_data);
guint		t_event_table_size		(TEventTable     *etable);

gboolean	t_event_table_has_hotkey	(TEventTable     *etable);
TEvent*		t_event_table_lookup_hotkey	(TEventTable     *etable,
						 gint             key);
TEvent*		t_event_table_get_keyboard_event	(TEventTable     *etable);

G_END_DECLS

#endif /* _T_EVENT_TABLE_H_ */
