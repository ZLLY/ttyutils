/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "treaper.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

static TReaper	*singleton_reaper = NULL;

G_DEFINE_TYPE (TReaper, t_reaper, G_TYPE_OBJECT)

/** 
 * t_reaper_child_watch_cb
 * @pid 
 * @status 
 * @data 
 */
static void
t_reaper_child_watch_cb (GPid     pid,
			 gint     status,
			 gpointer data)
{
  TReaper *reaper = T_REAPER (data);

  t_println ("Reaper emitting child-exited signal, pid=%d,status=%d.",
	     pid, status);

  g_signal_emit_by_name (reaper, "child-exited", pid, status, NULL);

  g_spawn_close_pid (pid);
}

/**
 * t_reaper_add_child:
 * @pid: the ID of a child process which will be monitored
 *
 * Ensures that child-exited signals will be emitted when @pid exits.
 * This is necessary for correct operation when running with glib
 * versions >= 2.4.
 *
 * Returns: the new source ID
 */
gint
t_reaper_add_child (GPid pid)
{
  g_return_val_if_fail (T_IS_REAPER (singleton_reaper), -1);

  return g_child_watch_add_full (G_PRIORITY_HIGH, pid,
				 t_reaper_child_watch_cb,
				 g_object_ref (singleton_reaper),
				 (GDestroyNotify) g_object_unref);
}

/** 
 * t_reaper_init:
 * @reaper 
 */
static void
t_reaper_init (TReaper *reaper)
{
}

/** 
 * t_reaper_constructor:
 * @type 
 * @n_construct_properties 
 * @construct_properties 
 * 
 * @Returns: 
 */
static GObject *
t_reaper_constructor (GType                  type,
		      guint                  n_construct_properties,
		      GObjectConstructParam *construct_properties)
{
  GObject *obj;

  if (singleton_reaper)
    {
      return G_OBJECT (singleton_reaper);
    }
  obj = G_OBJECT_CLASS (t_reaper_parent_class)->constructor (type,
							     n_construct_properties,
							     construct_properties);
  singleton_reaper = T_REAPER (obj);

  return obj;
}

/** 
 * t_reaper_finalize:
 * @reaper 
 */
static void
t_reaper_finalize (GObject *reaper)
{
  t_println ("TReaper finalize ...");

  G_OBJECT_CLASS (t_reaper_parent_class)->finalize (reaper);
  singleton_reaper = NULL;
}

/** 
 * t_reaper_class_init:
 * @klass 
 */
static void
t_reaper_class_init (TReaperClass * klass)
{
  GObjectClass *gobject_class;

  gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->constructor = t_reaper_constructor;
  gobject_class->finalize = t_reaper_finalize;

  klass->child_exited_signal = g_signal_new ("child-exited",
					     G_OBJECT_CLASS_TYPE (klass),
					     G_SIGNAL_RUN_LAST,
					     0,
					     NULL,
					     NULL,
					     t_marshal_VOID__UINT_INT,
					     G_TYPE_NONE,
					     2, G_TYPE_INT, G_TYPE_INT);
}

/**
 * t_reaper_get:
 *
 * Finds the address of the global #TReaper object, creating the object if
 * necessary.
 *
 * Returns: the global #TReaper object, which should not be unreffed.
 */
TReaper*
t_reaper_get (void)
{
  return g_object_new (T_TYPE_REAPER, NULL);
}
