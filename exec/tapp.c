/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <glib/gi18n.h>
#include <glib-object.h>
#include <ttyutils.h>

#include "tstdio.h"
#include "tlookd.h"
#include "tevent.h"
#include "tevtable.h"
#include "treaper.h"
#include "tapp.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef struct _TKeySequence	TKeySequence;
struct _TKeySequence
{
  gint   key;
  gchar  chars[10];
  gint   len;
};

struct _TAppPrivate
{
  TReaper    *reaper;
  GByteArray *stdout_cache;
  GArray     *keyseqs;
  GArray     *fore_childs;

  gchar      *last_event_name; /* to avoid same event run more than once
				  between 1/10 second. */
  guint       timeout_id;
};

#define T_APP_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), T_TYPE_APP, TAppPrivate))


static TApp	*t_uniquely_app = NULL;

G_DEFINE_TYPE (TApp, t_app, G_TYPE_OBJECT);

static void		t_app_event_start	(TEvent     *event,
						 gpointer    user_data);
static void		t_app_event_stop	(TEvent     *event,
						 gpointer    user_data);

/** 
 * t_app_reaper_event_program:
 * @key 
 * @value 
 * @data 
 */
static void
t_app_reaper_event_program (gpointer key,
			    gpointer value,
			    gpointer data)
{
  TApp *app = T_APP (data);
  TEvent *event = T_EVENT (value);
  guint pid;
  gint status;

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (T_IS_EVENT (event));

  pid = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (app),
					     "child_pid"));
  status = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (app),
					       "child_status"));
  if (t_event_get_child_pid (event) == pid)
    {
      t_println ("event %s program %d exit with status %d",
		 (gchar*) key, pid, status);

      g_signal_emit_by_name (event, "stop", NULL);
    }
}

/** 
 * t_app_catch_child_exit:
 * @pid 
 * @status 
 * @app 
 */
static void
t_app_catch_child_exit (TReaper  *reaper,
			guint     pid,
			gint      status,
			TApp     *app)
{
  gint i;

  /* terminate ttyexec when terminal child exit.
   */
  if (app->term)
    {
      if (pid == t_term_get_child_pid (app->term))
	{
	  t_println ("terminal child %d exit with %d.", pid, status);

	  t_main_quit ();
	  return;
	}
    }

  /* event program exit?
   */
  if (app->etable)
    {
      g_object_set_data (G_OBJECT (app), "child_pid",
			 GUINT_TO_POINTER (pid));
      g_object_set_data (G_OBJECT (app), "child_status",
			 GINT_TO_POINTER (status));

      t_event_table_foreach (app->etable, t_app_reaper_event_program, app);

      g_object_set_data (G_OBJECT (app), "child_pid", NULL);
      g_object_set_data (G_OBJECT (app), "child_status", NULL);
    }

  /* reconnect stdin to read if needed.
   */
  for (i = 0; i < app->priv->fore_childs->len; i++)
    {
      if (pid == g_array_index (app->priv->fore_childs, GPid, i))
	{
	  t_println ("foreground process %d exit.", pid);

	  g_array_remove_index_fast (app->priv->fore_childs, i);

	  if (app->priv->fore_childs->len == 0)
	    {
	      t_stdio_connect_stdin (app->stdio);
	    }
	  break;
	}
    }
}

/** 
 * t_app_init:
 * @app:
 */
static void
t_app_init (TApp *app)
{
  app->term = t_term_new ();
  app->stdio = t_stdio_new ();
  app->etable = t_event_table_new ();

  app->priv = T_APP_PRIVATE (app);

  app->priv->reaper = t_reaper_get ();
  app->priv->stdout_cache = g_byte_array_new ();
  app->priv->keyseqs = g_array_new (TRUE, TRUE, sizeof (TKeySequence));
  app->priv->fore_childs = g_array_new (FALSE, TRUE, sizeof (GPid));
  app->priv->last_event_name = NULL;
  app->priv->timeout_id = 0;

  g_signal_connect (G_OBJECT (app->priv->reaper), "child_exited",
		    G_CALLBACK (t_app_catch_child_exit), app);
}

/** 
 * t_app_finalize:
 * @object 
 */
static void
t_app_finalize (GObject *object)
{
  TApp *app = T_APP (object);

  t_println ("TApp Finalize...");

  if (app->priv->fore_childs)
    {
      g_array_free (app->priv->fore_childs, TRUE);
      app->priv->fore_childs = NULL;
    }
  if (app->priv->reaper)
    {
      g_object_unref (app->priv->reaper);
      app->priv->reaper = NULL;
    }

  if (app->priv->keyseqs)
    {
      g_array_free (app->priv->keyseqs, TRUE);
      app->priv->keyseqs = NULL;
    }

  if (app->priv->stdout_cache)
    {
      g_byte_array_free (app->priv->stdout_cache, TRUE);
      app->priv->stdout_cache = NULL;
    }

  if (app->etable)
    {
      g_object_unref (app->etable);
      app->etable = NULL;
    }
  if (app->term)
    {
      g_object_unref (app->term);
      app->term = NULL;
    }
  if (app->stdio)
    {
      g_object_unref (app->stdio);
      app->stdio = NULL;
    }

  G_OBJECT_CLASS (t_app_parent_class)->finalize (object);
  t_uniquely_app = NULL;
}

/** 
 * t_app_class_init:
 * @klass 
 */
static void
t_app_class_init (TAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TAppPrivate));

  object_class->finalize = t_app_finalize;
}

/** 
 * t_app_new:
 * @Returns: a #TApp object
 */
TApp*
t_app_new (void)
{
  TApp *app = g_object_new (T_TYPE_APP, NULL);

  t_uniquely_app = app;
  return app;
}

/** 
 * t_app_get:
 * @Returns: exists #TApp object
 */
TApp*
t_app_get (void)
{
  if (!T_IS_APP (t_uniquely_app))
    {
      g_critical ("TApp has not instance yet, please check.");
      return t_app_new ();
    }
  return t_uniquely_app;
}

/** 
 * t_app_write_stdout:
 * @app:
 * @data:
 * @size:
 */
static inline void
t_app_write_stdout (TApp        *app,
		    const gchar *data,
		    guint        size)
{
  gboolean to_auxport;

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (data);

  t_lookd_send (data, size);

  to_auxport = t_screen_aux_is_open (app->term->screen);

  if (!t_term_is_to_auxport (app->term))
    {
      t_println ("inject to screen %d bytes.",
		 t_term_get_screen_size (app->term));

      t_screen_inject (app->term->screen,
		       (guchar*) t_term_get_screen_data (app->term),
		       t_term_get_screen_size (app->term));
    }
  t_stdio_feed_stdout (app->stdio, data, size,
		       to_auxport || t_screen_aux_is_open (app->term->screen));
}

/** 
 * t_app_get_hotkey:
 * @sequence: key code sequence
 *
 * @Returns: a key
 */
static gint
t_app_get_hotkey (TApp        *app,
		  const gchar *sequence,
		  gsize        length,
		  gchar      **endp)
{
  gint i, j;
  TKeySequence *seq;

  g_return_val_if_fail (T_IS_APP (app), -1);
  g_return_val_if_fail (sequence, -1);
  g_return_val_if_fail (endp, -1);

  if (length == 0)
    return -1;

  if (sequence[0] != T_KEY_ESC)
    {
      *endp = (gchar*) sequence + 1;
      return sequence[0];
    }

  t_println ("keyseqs len =  %d", app->priv->keyseqs->len);

  for (i = 0; i < app->priv->keyseqs->len; i++)
    {
      seq = &g_array_index (app->priv->keyseqs, TKeySequence, i);

      if (seq->key == 0 || seq->len <= 0 || length < seq->len)
	continue;

      for (j = 0; j < seq->len; j++)
	{
	  if (sequence[j] != seq->chars[j])
	    break;
	}
      if (j == seq->len)
	{
	  *endp = (gchar*) sequence + j;
	  return seq->key;
	}
    }

  *endp = (gchar*) sequence + 1;
  return sequence[0];
}

/** 
 * t_app_stdio_incoming:
 * @stdio 
 * @user_data 
 */
static void
t_app_stdio_incoming (TStdio      *stdio,
		      const gchar *incoming,
		      guint        size,
		      gpointer     user_data)
{
  TApp *app = T_APP (user_data);
  TEvent *event;
  TEvent *keyboard_event;
  GSList *node;
  GString *string;
  const gchar *p;
#ifdef T_DEBUG
  GTimer *timer;
#endif
  gboolean has_hotkey = FALSE;

  g_return_if_fail (T_IS_STDIO (stdio));
  g_return_if_fail (incoming);
  g_return_if_fail (T_IS_APP (app));

  if (!app->term)
    return;

#ifdef T_DEBUG
  timer = g_timer_new ();
#endif

  /* check whether global event trigger flag was enabled.
   * if share memory unusable, ignore.
   */
  if (t_shmem_open (FALSE))
    {
      if (!t_shmem_get_event_enabled (getpid ()))
	{
	  t_println ("global event trigger flag been disabled.");

	  t_shmem_close ();
	  t_term_feed_child (app->term, incoming, size);
	  return;
	}
      t_shmem_close ();
    }

#ifdef T_DEBUG
  t_println ("access share memory elapsed %f second.",
	     g_timer_elapsed (timer, NULL));
  g_timer_destroy (timer);
#endif

  keyboard_event = t_event_table_get_keyboard_event (app->etable);
  if (keyboard_event)
    {
      gint i;
      gchar *args[3];
      gchar buff[8];

      for (i = 0; i < size; i++)
	{
	  g_signal_connect (G_OBJECT (keyboard_event), "start",
			    G_CALLBACK (t_app_event_start), app);
	  g_signal_connect (G_OBJECT (keyboard_event), "stop",
			    G_CALLBACK (t_app_event_stop), app);

	  g_snprintf (buff, sizeof (buff), "%d", incoming[i]);
	  args[0] = "-key";
	  args[1] = buff;
	  args[2] = NULL;

	  t_event_activate_with_args (keyboard_event, args, 1);
	}
      return;
    }

  if (!t_event_table_has_hotkey (app->etable) ||
      t_screen_aux_is_open (app->term->screen))
    {
      t_term_feed_child (app->term, incoming, size);
      return;
    }

  string = g_string_new ("");
  p = incoming;

  while (1)
    {
      gint key;
      gchar *endp = NULL;

      /* trigger hotkeys */
      key = t_app_get_hotkey (app, p, strlen (p), &endp);
      if (key == -1)
	break;

      t_println ("get key %d, endp=%s", key, endp);

      event = t_event_table_lookup_hotkey (app->etable, key);
      if (!T_IS_EVENT (event))
	{
	  g_string_append_len (string, p, endp - p);
	}
      else
	{
	  TEventCondition *cond;

	  g_signal_connect (G_OBJECT (event), "start",
			    G_CALLBACK (t_app_event_start), app);
	  g_signal_connect (G_OBJECT (event), "stop",
			    G_CALLBACK (t_app_event_stop), app);

	  t_event_activate (event);

	  node = t_event_get_matcher (event);
#ifdef T_DEBUG
	  if (g_slist_length (node) > 1)
	    {
	      g_critical (_("hotkey condition cannot combine with other condition."));
	    }
#endif
	  cond = (TEventCondition*) node->data;
#ifdef T_DEBUG
	  g_assert (cond->ec_type == EVENT_TYPE_HOTKEY);
#endif
	  if (!cond->ec_hotkey_omit)
	    {
	      g_string_append_len (string, p, endp - p);
	    }
	  has_hotkey = TRUE;
	}
      p = endp;
    }

  if (!has_hotkey && string->len < size)
    g_string_append_len (string, incoming + string->len, size - string->len);

  t_term_feed_child (app->term, string->str, string->len);
  g_string_free (string, TRUE);
}

/** 
 * t_app_stdio_suspend_stdin:
 * @stdio 
 * @user_data 
 */
static void
t_app_stdio_suspend_stdin (TStdio     *stdio,
			   gpointer    user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_STDIO (stdio));
  g_return_if_fail (T_IS_APP (app));
}

/** 
 * t_app_stdio_resume_stdin:
 * @stdio 
 * @user_data 
 */
static void
t_app_stdio_resume_stdin (TStdio     *stdio,
			  gpointer    user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_STDIO (stdio));
  g_return_if_fail (T_IS_APP (app));
}

/** 
 * t_app_stdio_suspend_stdout:
 * @stdio 
 * @user_data 
 */
static void
t_app_stdio_suspend_stdout (TStdio     *stdio,
			    gpointer    user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_STDIO (stdio));
  g_return_if_fail (T_IS_APP (app));
}

/** 
 * t_app_stdio_resume_stdout:
 * @stdio 
 * @user_data 
 */
static void
t_app_stdio_resume_stdout (TStdio     *stdio,
			   gpointer    user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_STDIO (stdio));
  g_return_if_fail (T_IS_APP (app));

  /* write cached data.
   */
  if (app->priv->stdout_cache->data && app->priv->stdout_cache->len > 0)
    {
      t_app_write_stdout (app, (gchar*) app->priv->stdout_cache->data,
			  app->priv->stdout_cache->len);

      g_byte_array_set_size (app->priv->stdout_cache, 0);

      /* emit a refresh signal for cache data.
       */
      g_signal_emit_by_name (app->term, "refreshed", NULL);
    }
}

/** 
 * t_app_setup_stdio:
 * @app: TApp object
 * 
 * @Returns: TRUE on success.
 */
static void
t_app_setup_stdio (TApp *app)
{
  g_signal_connect (app->stdio, "incoming",
		    G_CALLBACK (t_app_stdio_incoming), app);
  g_signal_connect (app->stdio, "suspend_stdin",
		    G_CALLBACK (t_app_stdio_suspend_stdin), app);
  g_signal_connect (app->stdio, "resume_stdin",
		    G_CALLBACK (t_app_stdio_resume_stdin), app);
  g_signal_connect (app->stdio, "suspend_stdout",
		    G_CALLBACK (t_app_stdio_suspend_stdout), app);
  g_signal_connect (app->stdio, "resume_stdout",
		    G_CALLBACK (t_app_stdio_resume_stdout), app);

  t_stdio_connect_stdin (app->stdio);
}

/** 
 * t_app_term_incoming:
 * @term:
 * @cache:
 * @cache_size:
 * @user_data:
 */
static void
t_app_term_incoming (TTerm       *term,
		     const gchar *cache,
		     guint        cache_size,
		     gpointer     user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_TERM (term));
  g_return_if_fail (T_IS_APP (app));

  if (!app->stdio)
    {
      if (!t_term_is_to_auxport (app->term))
	{
	  t_println ("inject to screen %d bytes.",
		     t_term_get_screen_size (term));

	  if (term->screen)
	    {
	      t_screen_inject (term->screen,
			       (guchar*) t_term_get_screen_data (term),
			       t_term_get_screen_size (term));
	    }
	}
      else
	{
	  t_println ("ignore data that send to auxport.");
	}
      return;
    }

  if (t_stdio_is_suspend (app->stdio, FALSE))
    {
      t_println ("feed to stdout been discard beacuse been suspend,"
		 "append %d bytes to cache.", cache_size);

      app->priv->stdout_cache = g_byte_array_append (app->priv->stdout_cache,
						     (guint8*) cache, cache_size);
      return;
    }

  /* write cached data.
   */
  if (app->priv->stdout_cache->data && app->priv->stdout_cache->len > 0)
    {
      t_app_write_stdout (app, (gchar*) app->priv->stdout_cache->data,
			  app->priv->stdout_cache->len);

      g_byte_array_set_size (app->priv->stdout_cache, 0);
    }

  t_app_write_stdout (app, cache, cache_size);
}

/** 
 * t_app_event_start:
 * @event 
 * @user_data 
 */
static void
t_app_event_start (TEvent     *event,
		   gpointer    user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (T_IS_EVENT (event));
}

/** 
 * t_event_stop:
 * @event 
 * @user_data 
 */
static void
t_app_event_stop (TEvent     *event,
		  gpointer    user_data)
{
  TApp *app = T_APP (user_data);
  gint n;

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (T_IS_EVENT (event));

  /* disconnect the connected callback of this event.
   */
  n = g_signal_handlers_disconnect_by_func (event, (void*) t_app_event_start, app);
  t_println ("disconnect %d event start handler.", n);

  n = g_signal_handlers_disconnect_by_func (event, (void*) t_app_event_stop, app);
  t_println ("disconnect %d event stop handler.", n);

  /* t_stdio_refresh (app->stdio);
   */
}

/** 
 * t_app_avoid_more_event:
 * @data 
 * 
 * @Returns: 
 */
gboolean
t_app_avoid_more_event (gpointer data)
{
  TApp *app = T_APP (data);

  if (app->priv->last_event_name)
    {
      g_free (app->priv->last_event_name);
      app->priv->last_event_name = NULL;
    }
  app->priv->timeout_id = 0;

  return FALSE;
}

/** 
 * t_app_refreshed_event:
 * @key: 
 * @value: 
 * @user_data: 
 * 
 * @Returns: 
 */
static void
t_app_refreshed_event (gpointer key,
		       gpointer value,
		       gpointer user_data)
{
  TEvent *event = T_EVENT (value);
  TApp *app = T_APP (user_data);
  GSList *matcher;
  const gchar *name;
  gboolean has_cursor;

  g_return_if_fail (T_IS_EVENT (event));

  /* check whether global event trigger flag was enabled.
   * if share memory unusable, ignore.
   */
  if (t_shmem_open (FALSE))
    {
      if (!t_shmem_get_event_enabled (getpid ()))
	{
	  t_shmem_close ();
	  t_println ("global event trigger flag been disabled.");
	  return;
	}
      t_shmem_close ();
    }

  g_assert (app);
  g_assert (app->term);
  g_assert (app->term->screen);

  name = t_event_get_name (event);

  if (t_screen_is_freeze (app->term->screen, name))
    {
      t_println ("find event %s in freeze list, skip.", name);
      return;
    }

  matcher = t_event_get_matcher (event);
  if (!matcher)
    {
      g_critical (_("event %s has a empty matcher, skip!"), name);
      return;
    }

  has_cursor = FALSE;

  for (; matcher; matcher = matcher->next)
    {
      TEventCondition *cond;

      cond = (TEventCondition*) matcher->data;
      g_assert (cond);

      /* ignore hotkey event.
       */
      if (cond->ec_type == EVENT_TYPE_HOTKEY)
	{
	  t_println ("skip hotkey event %s when screen refresh.", name);
	  return;
	}

      if (cond->ec_type == EVENT_TYPE_CURSOR)
	has_cursor = TRUE;

      if (!t_screen_match (app->term->screen, cond))
	{
	  t_println ("event condition does not match.");
	  return;
	}
    }

  /* reset cursor dirty flag
   */
  t_screen_reset_cursor (app->term->screen);

  if (!has_cursor)
    {
      /* add conditions to screen freeze list.
       */
      matcher = t_event_get_matcher (event);

      for (; matcher; matcher = matcher->next)
	{
	  TEventCondition *cond;

	  cond = (TEventCondition*) matcher->data;
	  g_assert (cond);

	  if (cond->ec_type == EVENT_TYPE_CONTENT)
	    {
	      t_screen_add_freeze (app->term->screen,
				   cond->ec_content_row,
				   cond->ec_content_col,
				   cond->ec_content_length,
				   name);
	    }
	}
    }

  matcher = t_event_get_matcher (event);

  if (app->priv->last_event_name &&
      strcmp (app->priv->last_event_name, name) == 0 &&
      has_cursor &&
      g_slist_length (matcher) == 1)
    {
      g_warning (_("event %s try to start more than once between short time, skip"), name);
    }
  else
    {
      g_free (app->priv->last_event_name);
      app->priv->last_event_name = g_strdup (name);

      t_println ("event '%s' matched, program was '%s'",
		 name, t_event_get_program (event));

      g_signal_connect (G_OBJECT (event), "start",
			G_CALLBACK (t_app_event_start), app);
      g_signal_connect (G_OBJECT (event), "stop",
			G_CALLBACK (t_app_event_stop), app);

      t_event_activate (event);

      if (app->priv->timeout_id != 0)
	{
	  g_source_remove (app->priv->timeout_id);
	}
      app->priv->timeout_id = g_timeout_add (100, t_app_avoid_more_event, app);
    }
}

/** 
 * t_app_term_refreshed:
 * @terminal 
 * @user_data 
 */
static void
t_app_term_refreshed (TTerm *term, gpointer user_data)
{
  TApp *app = T_APP (user_data);

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (T_IS_TERM (term));

  t_println ("try to check event table now.");

  t_event_table_foreach (app->etable, t_app_refreshed_event, app);
}

/** 
 * t_sigwinch_handler:
 * @signo: SIGWINCH
 *
 * called when screen size changed.
 */
static void
t_sigwinch_handler (gint signo)
{
  struct winsize size;

  memset (&size, 0, sizeof (size));

  /* apply new terminal size.
   */
  if (ioctl (STDIN_FILENO, TIOCGWINSZ, &size) == 0)
    {
      TApp *app = t_app_get ();

      if (app->term)
	{
	  t_println ("tty size change to %dx%d", size.ws_col, size.ws_row);
	  t_term_set_size (app->term, size.ws_col, size.ws_row);
	}
    }
}

/** 
 * t_app_open_term:
 * @app:
 * @command:
 * 
 * @Returns: 
 */
gboolean
t_app_open_term (TApp        *app,
		 const gchar *command)
{
  struct sigaction action;

  g_return_val_if_fail (app, FALSE);
  g_return_val_if_fail (command, FALSE);

  if (!app->term)
    return FALSE;

  /* catch terminal size change signal.
   */
  action.sa_handler = t_sigwinch_handler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = SA_NOCLDSTOP | SA_RESTART;

  sigaction (SIGWINCH, &action, NULL);

  g_signal_connect (app->term, "incoming",
		    G_CALLBACK (t_app_term_incoming), app);
  g_signal_connect_after (app->term, "refreshed",
			  G_CALLBACK (t_app_term_refreshed), app);

  /* execute command at pseudo terminal.
   */
  if (!t_term_fork_command (app->term, command))
    {
      g_critical (_("failure to execute command '%s'."), command);
      g_object_unref (app->term);
      app->term = NULL;
      return FALSE;
    }

  /* startup stdandard i/o, get data from keyboard.
   */

  t_app_setup_stdio (app);

  return TRUE;
}

/** 
 * t_app_add_fore_child:
 * @pid 
 */
void
t_app_add_fore_child (TApp *app, GPid pid)
{
  g_return_if_fail (T_IS_APP (app));

  g_array_append_val (app->priv->fore_childs, pid);
}

#ifdef T_DEBUG
static void
t_app_keyseq_print (TApp *app)
{
  gint i, j;
  TKeySequence *seq;

  t_println ("key sequences:");

  for (i = 0; i < app->priv->keyseqs->len; i++)
    {
      seq = &g_array_index (app->priv->keyseqs, TKeySequence, i);

      g_print ("  %d. key: %d, length: %d, seq:", i + 1,
	       seq->key, seq->len);
      for (j = 0; j < seq->len; j++)
	{
	  g_print (" 0x%02x", seq->chars[j]);
	}
      g_print ("\n");
    }
}
#endif

/** 
 * t_app_keyseq_set:
 * @app:
 * @key:
 * @sequences:
 * @length:
 */
void
t_app_keyseq_set (TApp        *app,
		  gint         key,
		  const gchar *sequences,
		  gsize        length)
{
  TKeySequence keyseq, *seq;
  guint i;

  g_return_if_fail (T_IS_APP (app));
  g_return_if_fail (sequences);

  if (length >= sizeof (keyseq.chars))
    {
      g_warning ("key sequence length too long.");
      return;
    }

  t_println ("set key %d to sequence '%s', length %d", key, sequences, length);

  for (i = 0; i < app->priv->keyseqs->len; i++)
    {
      seq = &g_array_index (app->priv->keyseqs, TKeySequence, i);

      if (seq->key == key)
	{
	  keyseq.len = length;
	  g_strlcpy (seq->chars, sequences, sizeof (seq->chars));
#ifdef T_DEBUG
	  t_app_keyseq_print (app);
#endif
	  return;
	}
    }

  keyseq.key = key;
  keyseq.len = length;
  g_strlcpy (keyseq.chars, sequences, sizeof (keyseq.chars));

  g_array_append_val (app->priv->keyseqs, keyseq);

#ifdef T_DEBUG
  t_app_keyseq_print (app);
#endif
}

/** 
 * t_app_keyseq_unset:
 * @app:
 * @key:
 */
void
t_app_keyseq_unset (TApp      *app,
		    gint       key)
{
  guint i;

  g_return_if_fail (T_IS_APP (app));

  for (i = 0; i < app->priv->keyseqs->len; i++)
    {
      TKeySequence *seq;

      seq = &g_array_index (app->priv->keyseqs, TKeySequence, i);
      if (seq->key == key)
	{
	  t_println ("unset key %d.", seq->key);

	  g_array_remove_index_fast (app->priv->keyseqs, i);

#ifdef T_DEBUG
	  t_app_keyseq_print (app);
#endif
	  break;
	}
    }
}

/** 
 * t_app_keyseq_clear:
 * @app:
 */
void
t_app_keyseq_clear (TApp *app)
{
  g_return_if_fail (T_IS_APP (app));

  t_println ("clear key sequences.");

  g_array_set_size (app->priv->keyseqs, 0);

#ifdef T_DEBUG
  t_app_keyseq_print (app);
#endif
}
