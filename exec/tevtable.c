/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <glib/gi18n.h>

#include "tevtable.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef struct _TEventGroup	TEventGroup;
struct _TEventGroup
{
  gchar  *group_name;
  GSList *events;
};

struct _TEventTablePrivate
{
  GHashTable *event_table;
  GSList     *event_group;
  TEvent     *keyboard_event;
  GHashTable *hotkey_table;
};

#define T_EVENT_TABLE_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), T_TYPE_EVENT_TABLE, TEventTablePrivate))

G_DEFINE_TYPE (TEventTable, t_event_table, G_TYPE_OBJECT);

/** 
 * t_event_table_init:
 * @object:
 */
static void
t_event_table_init (TEventTable *etable)
{
  etable->priv = T_EVENT_TABLE_PRIVATE (etable);

  etable->priv->event_table = g_hash_table_new_full (g_str_hash,
						     g_str_equal,
						     NULL,
						     g_object_unref);
  if (!etable->priv->event_table)
    {
      g_warning (_("create event table failed."));
    }
  etable->priv->event_group = NULL;

  etable->priv->hotkey_table = g_hash_table_new (NULL, NULL);
  if (!etable->priv->hotkey_table)
    {
      g_warning (_("create hotkey table failed."));
    }
  etable->priv->keyboard_event = NULL;
}

/** 
 * t_event_table_group_free:
 * @data 
 * @user_data 
 */
static void
t_event_table_group_free (gpointer data, gpointer user_data)
{
  TEventGroup *group = (TEventGroup*) data;

  if (group)
    {
      g_free (group->group_name);
      g_free (group);
    }
}

/** 
 * t_event_table_finalize:
 * @object 
 */
static void
t_event_table_finalize (GObject *object)
{
  TEventTable *etable = T_EVENT_TABLE (object);

  if (etable->priv->event_group)
    {
      g_slist_foreach (etable->priv->event_group,
		       t_event_table_group_free, NULL);
      g_slist_free (etable->priv->event_group);
      etable->priv->event_group = NULL;
    }

  if (etable->priv->hotkey_table)
    {
      g_hash_table_destroy (etable->priv->hotkey_table);
      etable->priv->hotkey_table = NULL;
    }
  if (etable->priv->event_table)
    {
      g_hash_table_destroy (etable->priv->event_table);
      etable->priv->event_table = NULL;
    }

  G_OBJECT_CLASS (t_event_table_parent_class)->finalize (object);
}

/** 
 * t_event_table_class_init:
 * @klass 
 */
static void
t_event_table_class_init (TEventTableClass *klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TEventTablePrivate));

  object_class->finalize = t_event_table_finalize;
}


/** 
 * t_event_table_new:
 * @Returns: new #TEventTable object
 */
TEventTable*
t_event_table_new (void)
{
  return g_object_new (T_TYPE_EVENT_TABLE, NULL);
}

/** 
 * t_event_table_find_group:
 * @a:
 * @b:
 * 
 * @Returns: 
 */
static gint
t_event_table_find_group (gconstpointer a,
			  gconstpointer b)
{
  TEventGroup *group = (TEventGroup*) a;
  const gchar *group_name = (const gchar*) b;

  g_return_val_if_fail (group, -1);
  g_return_val_if_fail (group_name, -1);

  return strcmp (group->group_name, group_name);
}

/** 
 * t_event_table_insert:
 * @event 
 */
gboolean
t_event_table_insert (TEventTable *etable,
		      TEvent      *event)
{
  const gchar *name;
  const gchar *group_name;
  GSList *node;

  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), FALSE);
  g_return_val_if_fail (T_IS_EVENT (event), FALSE);

  if (!etable->priv->event_table)
    {
      g_warning (_("events hash table invalid."));
      return FALSE;
    }

  name = t_event_get_name (event);
  if (!name)
    {
      g_critical (_("event must has a name before insert to table."));
      return FALSE;
    }

  if (g_hash_table_lookup (etable->priv->event_table, name))
    {
      g_warning (_("event %s already exists, insert failed"), name);
      return FALSE;
    }

  g_hash_table_insert (etable->priv->event_table, (gpointer) name, event);

  node = t_event_get_matcher (event);

  /* hotkey event can only has one condition.
   */
  if (g_slist_length (node) == 1)
    {
      TEventCondition *cond = (TEventCondition*) node->data;

      /* insert to hotkey table.
       */
      if (cond->ec_type == EVENT_TYPE_HOTKEY)
	{
#ifndef SCO_SV
	  TEvent *event2;

	  event2 = g_hash_table_lookup (etable->priv->hotkey_table,
					(gpointer) cond->ec_hotkey_key);
	  if (event2)
	    {
	      /* FIXME: Core Dump on SCO OpenServer 5, Why ???
	       */
	      const gchar *en = t_event_get_name (event2);
	      g_message (_("hotkey event '%s' will be replace by '%s'"),
			 en ? en : "", name);
	    }
#endif

	  g_hash_table_replace (etable->priv->hotkey_table,
				(gpointer) cond->ec_hotkey_key,
				event);
	}
      else if (cond->ec_type == EVENT_TYPE_KEYBOARD)
	{
	  g_message ("insert keyboard event");
	  if (etable->priv->keyboard_event)
	    g_warning (_("there already has a keyboard event in event table!!!"));
	  else
	    etable->priv->keyboard_event = event;
	}
    }

  t_println ("event table has %d hotkey event condition.",
	     g_hash_table_size (etable->priv->hotkey_table));

  /* insert to group list if event has a group.
   */
  group_name = t_event_get_group (event);
  if (group_name)
    {
      TEventGroup *group = NULL;
      GSList *node;

      if (etable->priv->event_group)
	{
	  node = g_slist_find_custom (etable->priv->event_group, group_name,
				      t_event_table_find_group);
	  if (node)
	    {
	      group = (TEventGroup*) node->data;
	      group->events = g_slist_prepend (group->events, event);
	    }
	}

      if (!group)
	{
	  group = g_new0 (TEventGroup, 1);
	  group->group_name = g_strdup (group_name);
	  group->events = g_slist_prepend (group->events, event);

	  etable->priv->event_group = g_slist_prepend (etable->priv->event_group, group);
	}
    }

  return TRUE;
}

/** 
 * t_event_table_remove:
 * @name:
 */
gboolean
t_event_table_remove (TEventTable *etable,
		      const gchar *name)
{
  TEvent *event;
  const gchar *group_name;
  GSList *node;

  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), FALSE);
  g_return_val_if_fail (name, FALSE);

  if (!etable->priv->event_table)
    {
      return FALSE;
    }

  event = g_hash_table_lookup (etable->priv->event_table, name);
  if (!event)
    {
      g_warning (_("event '%s' not found."), name);
      return FALSE;
    }

  node = t_event_get_matcher (event);

  /* hotkey event can only has one condition.
   */
  if (g_slist_length (node) == 1)
    {
      TEventCondition *cond = (TEventCondition*) node->data;

      /* remove from hotkey table.
       */
      if (cond->ec_type == EVENT_TYPE_HOTKEY)
	{
	  g_hash_table_remove (etable->priv->hotkey_table,
			       (gpointer) cond->ec_hotkey_key);
	}
      else if (cond->ec_type == EVENT_TYPE_KEYBOARD)
	{
	  g_message ("remove keyboard event");
	  etable->priv->keyboard_event = NULL;
	}
    }

  t_println ("event table has %d hotkey event condition.",
	     g_hash_table_size (etable->priv->hotkey_table));

  /* remove from group.
   */
  group_name = t_event_get_group (event);
  if (group_name)
    {
      TEventGroup *group = NULL;
      GSList *node;

      if (etable->priv->event_group)
	{
	  node = g_slist_find_custom (etable->priv->event_group, group_name,
				      t_event_table_find_group);
	  if (node)
	    {
	      group = (TEventGroup*) node->data;
	      group->events = g_slist_remove (group->events, event);

	      /* remove the group if it is empty.
	       */
	      if (!group->events)
		{
		  etable->priv->event_group = g_slist_delete_link (etable->priv->event_group, node);
		  g_free (group->group_name);
		  g_free (group);
		}
	    }
	}
    }

  return  g_hash_table_remove (etable->priv->event_table, name);
}

/** 
 * t_event_remove_from_group:
 * @data 
 * @user_data 
 */
static void
t_event_remove_from_group (gpointer data,
			   gpointer user_data)
{
  TEvent *event = T_EVENT (data);
  TEventTable *etable = T_EVENT_TABLE (user_data);
  const gchar *name;

  g_return_if_fail (T_IS_EVENT (event));
  g_return_if_fail (T_IS_EVENT_TABLE (etable));

  name = t_event_get_name (event);

  if (name && etable->priv->event_table)
    {
      g_hash_table_remove (etable->priv->event_table, name);
    }
}

/** 
 * t_event_table_remove_group:
 * @group:
 * 
 * @Returns: 
 */
gboolean
t_event_table_remove_group (TEventTable *etable,
			    const gchar *group_name)
{
  TEventGroup *group = NULL;
  GSList *node;

  g_return_val_if_fail (group_name, FALSE);

  if (!etable->priv->event_group)
    {
#if T_DEBUG
      g_message (_("group '%s' does not exists."), group_name);
#endif
      return TRUE;
    }

  node = g_slist_find_custom (etable->priv->event_group,
			      group_name, t_event_table_find_group);
  if (!node)
    {
#if T_DEBUG
      g_warning (_("group '%s' does not exists."), group_name);
#endif
      return TRUE;
    }

  group = (TEventGroup*) node->data;

  if (!group->events)
    {
      g_warning (_("no event in group '%s'."), group_name);
      return FALSE;
    }

  g_slist_foreach (group->events, t_event_remove_from_group, etable);
  g_slist_free (group->events);

  etable->priv->event_group = g_slist_delete_link (etable->priv->event_group, node);
  g_free (group->group_name);
  g_free (group);

  return TRUE;
}

/** 
 * t_event_table_lookup:
 * @name 
 * 
 * @Returns: 
 */
TEvent*
t_event_table_lookup (TEventTable *etable,
		      const gchar *name)
{
  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), NULL);
  g_return_val_if_fail (name, NULL);

  if (!etable->priv->event_table)
    {
      return FALSE;
    }
  return g_hash_table_lookup (etable->priv->event_table, name);
}

/** 
 * t_event_table_foreach:
 * @func 
 * @user_data 
 */
void
t_event_table_foreach (TEventTable *etable,
		       GHFunc       func,
		       gpointer     user_data)
{
  g_return_if_fail (T_IS_EVENT_TABLE (etable));

  if (etable->priv->event_table)
    {
      g_hash_table_foreach (etable->priv->event_table, func, user_data);
    }
}

/** 
 * t_event_table_size:
 * 
 * @Returns: 
 */
guint
t_event_table_size (TEventTable *etable)
{
  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), 0);

  if (etable->priv->event_table)
    {
      return g_hash_table_size (etable->priv->event_table);
    }
  return 0;
}

/** 
 * t_event_table_has_hotkey:
 * @etable:
 * 
 * @Returns: 
 */
gboolean
t_event_table_has_hotkey (TEventTable *etable)
{
  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), FALSE);

  if (!etable->priv->hotkey_table)
    return FALSE;

  return g_hash_table_size (etable->priv->hotkey_table) > 0;
}

/** 
 * t_event_table_lookup_hotkey:
 * @etable:
 * @key:
 * 
 * @Returns: 
 */
TEvent*
t_event_table_lookup_hotkey (TEventTable *etable,
			     gint         key)
{
  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), NULL);

  if (!etable->priv->hotkey_table)
    return NULL;

  return g_hash_table_lookup (etable->priv->hotkey_table,
			      (gpointer) key);
}

/**
 * t_event_table_get_keyboard_event:
 * 
 * @etable 
 * 
 * @Returns: 
 */
TEvent*
t_event_table_get_keyboard_event (TEventTable *etable)
{
  g_return_val_if_fail (T_IS_EVENT_TABLE (etable), NULL);
  return etable->priv->keyboard_event;
}
