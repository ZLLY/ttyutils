/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_LOOKD_H__
#define __T_LOOKD_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _TLookd	TLookd;

gboolean	t_lookd_boot		(void);
void		t_lookd_shut		(void);

const gchar*	t_lookd_get_unixpath	(void);
gint		t_lookd_get_nclients	(void);

void		t_lookd_send		(const gchar *data,
					 gssize      length);

G_END_DECLS

#endif /* __T_LOOKD_H__ */
