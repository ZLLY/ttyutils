/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef _T_RING_BUFFER_H_
#define _T_RING_BUFFER_H_

#include <glib.h>

G_BEGIN_DECLS

typedef struct _TRingBuffer	TRingBuffer;

TRingBuffer*	t_ring_buffer_new		(void);
void		t_ring_buffer_free		(TRingBuffer *ring);
void		t_ring_buffer_clear		(TRingBuffer *ring);
gint		t_ring_buffer_n_chunk		(TRingBuffer *ring);
gint		t_ring_buffer_size		(TRingBuffer *ring);
gboolean	t_ring_buffer_can_write		(TRingBuffer *ring,
						 gint         size);
gint		t_ring_buffer_write		(TRingBuffer *ring,
						 const gchar *buff,
						 gint         size);
gboolean	t_ring_buffer_can_read		(TRingBuffer *ring,
						 gint         size);
gint		t_ring_buffer_read		(TRingBuffer *ring,
						 gchar       *buff,
						 gint         size);
gboolean	t_ring_buffer_can_read_line	(TRingBuffer *ring,
						 gint         size);
gint		t_ring_buffer_read_line		(TRingBuffer *ring,
						 gchar       *buff,
						 gint         size);
gint		t_ring_buffer_read_to		(TRingBuffer *ring,
						 gchar       *buff,
						 gint         atleast,
						 gint         maxsize,
						 gchar       *str,
						 gint         nstr);
gint		t_ring_buffer_read_with		(TRingBuffer *ring,
						 gchar       *buff,
						 gint         atleast,
						 gint         maxsize,
						 gchar       *str,
						 gint         nstr);
gint		t_ring_buffer_discard_to	(TRingBuffer *ring,
						 gint         atleast,
						 gint         maxsize,
						 gchar       *str,
						 gint         nstr);
gint		t_ring_buffer_discard_with	(TRingBuffer *ring,
						 gint         atleast,
						 gint         maxsize,
						 gchar       *str,
						 gint         nstr);

G_END_DECLS

#endif /* _T_RING_BUFFER_H_ */
