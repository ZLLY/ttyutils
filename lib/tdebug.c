/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gprintf.h>

#include "tdebug.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

static FILE		*t_debug_file = NULL;


/** 
 * t_log_handler_syslog:
 * @domain 
 * @level 
 * @message 
 * @user_data 
 */
void
t_debug_log_handler (const gchar    *domain,
		     GLogLevelFlags  level,
		     const gchar    *message,
		     gpointer        user_data)
{
  FILE *logfile = (FILE*) user_data;
  char message2[1024];
  int priority = LOG_ALERT;
  char *level_str = NULL;

  /* map glib log level to syslog priority level.
   */
  switch (level)
    {
    case G_LOG_LEVEL_DEBUG:
      level_str = "debug";
      priority = LOG_DEBUG;
      break;
    case G_LOG_LEVEL_INFO:
      level_str = "info";
      priority = LOG_INFO;
      break;
    case G_LOG_LEVEL_MESSAGE:
      level_str = "msg";
      priority = LOG_NOTICE;
      break;
    case G_LOG_LEVEL_WARNING:
      level_str = "warn";
      priority = LOG_WARNING;
      break;
    case G_LOG_LEVEL_CRITICAL:
      level_str = "crit";
      priority = LOG_CRIT;
      break;
    case G_LOG_LEVEL_ERROR:
    default:
      level_str = "error";
      priority = LOG_ERR;
      break;
    }

  g_snprintf (message2, sizeof (message2), "*<%s-%s>* %s",
	      domain ? domain : "NULL", level_str, message);

  if (logfile)
    {
      fprintf (logfile, "%s\n", message2);
      fflush (logfile);
    }
  else
    {
      syslog (priority, message2);
    }
}

/** 
 * t_debug_print:
 * @fname 
 * @lineno 
 * @func 
 * @format 
 * @... 
 */
void
t_debug_print (const char    *fname,
	       int            lineno,
	       const char    *func,
	       const char    *format,
	       ...)
{
  time_t tme;
  struct tm *tmp;
  char buff[1024];
  gulong size;
  va_list args;
  
  tme = time (NULL);
  tmp = localtime (&tme);

  size = g_sprintf (buff, "%02d:%02d:%02d [%s:%d %s()] ",
		    tmp->tm_hour, tmp->tm_min, tmp->tm_sec,
		    fname ? fname : "<NULL>",
		    lineno, func ? func : "unknown");

  va_start (args, format);
  g_vsnprintf (buff + size, sizeof (buff) - size, format, args);
  va_end (args);

  g_print ("%s", buff);
}

/** 
 * t_debug_println:
 * @fname 
 * @lineno 
 * @func 
 * @format 
 * @... 
 */
void
t_debug_println (const char    *fname,
		 int            lineno,
		 const char    *func,
		 const char    *format,
		 ...)
{
  time_t tme;
  struct tm *tmp;
  char buff[1024];
  gulong size;
  va_list args;

  tme = time (NULL);
  tmp = localtime (&tme);

  size = g_sprintf (buff, "%02d:%02d:%02d [%s:%d %s()] ",
		    tmp->tm_hour, tmp->tm_min, tmp->tm_sec,
		    fname ? fname : "<NULL>",
		    lineno, func ? func : "unknown");

  va_start (args, format);
  g_vsnprintf (buff + size, sizeof (buff) - size, format, args);
  va_end (args);

  g_print ("%s\n", buff);
}


/** 
 * t_debug_print_close:
 */
static void
t_debug_print_close (void)
{
  if (t_debug_file)
    {
      fclose (t_debug_file);
      t_debug_file = NULL;
    }
}

/** 
 * t_debug_print_handler:
 * @string 
 */
void
t_debug_print_handler (const gchar *string)
{
  gchar fname[1024], *p;
  gint fd;
  mode_t mode;
  static gint count = 0;

#define REFRESH_FREQUENCY	0

  g_return_if_fail (string);

  if (!t_debug_file)
    {
      g_snprintf (fname, sizeof (fname), "/tmp/ttyutils-%s-%s",
		  cuserid (NULL), ttyname (STDIN_FILENO));
      for (p = &fname[6]; *p != '\0'; p++)
	{
	  if (*p == '/')
	    *p = '_';
	}

      mode = umask (0);
      fd = open (fname, O_WRONLY | O_CREAT | O_TRUNC, 0644);
      umask (mode);

      if (fd > 0)
	{
	  t_debug_file = fdopen (fd, "w");
	}
      if (t_debug_file)
	{
	  g_atexit (t_debug_print_close);
	}
      else
	{
	  g_warning (_("open debug file %s error: %s"),
		     fname, g_strerror (errno));
	}
    }

  if (t_debug_file)
    {
      fputs (string, t_debug_file);
      if (count++ >= REFRESH_FREQUENCY)
	{
	  fflush (t_debug_file);
	  count = 0;
	}
    }
}
