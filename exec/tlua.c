/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <glib.h>
#include <glib/gi18n.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "tapp.h"
#include "tevent.h"
#include "tstdio.h"
#include "tlua.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

static void	t_lua_openlibs	(lua_State *state);


/** 
 * t_lua_add_path:
 * @path:
 */
void
t_lua_add_path (const gchar *path)
{
  const gchar *env;
  gchar luapath[1024], *p;

  g_return_if_fail (path);

  p = g_build_filename (path, "?", NULL);

  env = g_getenv ("LUA_PATH");
  g_snprintf (luapath, sizeof (luapath),
              "%s;%s.lua;%s", p, p, env ? env : "");
  g_free (p);

  t_println ("set LUA_PATH to [%s].", luapath);
  g_setenv ("LUA_PATH", luapath, TRUE);
}

/** 
 * t_lua_init:
 * @Returns: TRUE on success.
 */
lua_State*
t_lua_open (void)
{
  lua_State *state;
  gchar *p;

  if (!(state = luaL_newstate ()))
    {
      g_warning (_("open lua state failed."));
      return NULL;
    }

  /* add search path.
   */
  p = g_build_filename (LIBDIR, PACKAGE_NAME, "lua", NULL);

  t_lua_add_path (p);
  g_free (p);

  /* register standard and customized lua extensions.
   */
  luaL_openlibs (state);
  t_lua_openlibs (state);

  return state;
}

/** 
 * t_lua_close:
 */
void
t_lua_close (lua_State *state)
{
  if (state)
    {
      lua_close (state);
    }
}

/** 
 * t_lua_dofile:
 * @state:
 * @file:
 * 
 * @Returns: 
 */
gboolean
t_lua_dofile (lua_State   *state,
	      const gchar *file)
{
  g_return_val_if_fail (state, FALSE);
  g_return_val_if_fail (file, FALSE);

  if (luaL_dofile (state, file) != 0)
    {
      g_warning (_("execute script '%s' error: %s"),
		 file, lua_tostring (state, -1));
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_lua_event_insert:
 * @state: lua state
 * 
 * @Returns: number of the results.
 */
static gint
t_lua_event_insert (lua_State *state)
{
  TApp *app = t_app_get ();
  const gchar *name, *group, *matcher, *program;
  TEvent *event;

  if (!(name = luaL_checkstring (state, -4))    ||
      !(group = luaL_checkstring (state, -3))   ||
      !(matcher = luaL_checkstring (state, -2)) ||
      !(program = luaL_checkstring (state, -1)))
    {
      g_warning (_("argument for event.insert() invalid."));
      lua_pushnil (state);
      return 1;
    }

  event = t_event_new (name, group, program);

  if (!t_event_parse_matcher (event, matcher))
    {
      g_object_unref (event);
      lua_pushnil (state);
      return 1;
    }

  if (!t_event_table_insert (app->etable, event))
    {
      g_object_unref (event);
      lua_pushnil (state);
      return 1;
    }

  lua_pushboolean (state, TRUE);
  return 1;
}

/** 
 * t_lua_event_remove:
 * @state: lua state
 * 
 * @Returns: 0 on success
 */
static gint
t_lua_event_remove (lua_State *state)
{
  TApp *app = t_app_get ();
  const gchar *name;

  g_return_val_if_fail (state, 0);

  if (!(name = luaL_checkstring (state, -1)))
    {
      g_warning (_("argument for event.remove() invalid."));
      lua_pushnil (state);
      return 1;
    }

  if (!t_event_table_remove (app->etable, name))
    {
      lua_pushnil (state);
      return 1;
    }

  lua_pushboolean (state, TRUE);
  return 1;
}

static luaL_Reg	_lua_event[] =
{
    { "insert",		t_lua_event_insert },
    { "remove",		t_lua_event_remove },

    { NULL, NULL }
};

/** 
 * t_lua_hotkey_keyseq_set:
 * @state:
 * 
 * @Returns: 
 */
static gint
t_lua_hotkey_keyseq_set (lua_State *state)
{
  gint key;
  const gchar *seq;
  gint length;

  if (!(key = luaL_checknumber (state, -3)) ||
      !(seq = luaL_checkstring (state, -2)) ||
      !(length = luaL_checknumber (state, -1)))
    {
      g_warning (_("argument for hotkey.keyseq_set() invalid."));
      lua_pushnil (state);
      return 1;
    }

  t_app_keyseq_set (t_app_get (), key, seq, length);

  lua_pushboolean (state, TRUE);
  return 1;
}

/** 
 * t_lua_hotkey_keyseq_unset:
 * @state:
 * 
 * @Returns: 
 */
static gint
t_lua_hotkey_keyseq_unset (lua_State *state)
{
  gint key;

  if (!(key = luaL_checknumber (state, -1)))
    {
      g_warning (_("argument for hotkey.keyseq_unset() invalid."));
      lua_pushnil (state);
      return 1;
    }

  t_app_keyseq_unset (t_app_get (), key);

  lua_pushboolean (state, TRUE);
  return 1;
}

/**
 * t_lua_hotkey_keyseq_clear:
 * @state:
 * 
 * @Returns: 
 */
static gint
t_lua_hotkey_keyseq_clear (lua_State *state)
{
  t_app_keyseq_clear (t_app_get ());

  lua_pushboolean (state, TRUE);
  return 1;
}

static luaL_Reg	_lua_hotkey[] =
{
    { "keyseq_set",	t_lua_hotkey_keyseq_set },
    { "keyseq_unset",	t_lua_hotkey_keyseq_unset },
    { "keyseq_clear",	t_lua_hotkey_keyseq_clear },

    { NULL, NULL }
};

/* defined in texec.c
 */
extern const gchar*	t_get_opt_ptyname (void);
extern void		t_set_opt_ptyname (const gchar *ptyname);

/** 
 * t_lua_set_ptyname:
 * @state 
 * 
 * @Returns: 
 */
static gboolean
t_lua_set_opt_ptyname (lua_State *state)
{
  const gchar *name;

  if (!(name = luaL_checkstring (state, -1)))
    {
      g_warning (_("argument for t_lua_set_ptyname() invalid."));
      lua_pushnil (state);
      return 1;
    }
  t_set_opt_ptyname (name);

  lua_pushboolean (state, TRUE);
  return 1;
}

/** 
 * t_lua_get_opt_ptyname:
 * @state 
 * 
 * @Returns: 
 */
static gint
t_lua_get_opt_ptyname (lua_State *state)
{
  const gchar *name;

  name = t_get_opt_ptyname ();

  lua_pushstring (state, name ? name : "");

  return 1;
}

static luaL_Reg	_lua_ttyexec[] =
{
    { "set_ptyname",	t_lua_set_opt_ptyname },
    { "get_ptyname",	t_lua_get_opt_ptyname },

    { NULL, NULL }
};

/** 
 * t_lua_ttyname:
 * @state 
 * 
 * @Returns: 
 */
static gint
t_lua_ttyname (lua_State *state)
{
  const gchar *name = ttyname (STDIN_FILENO);

  lua_pushstring (state, name ? name : "");

  return 1;
}

static luaL_Reg	_lua_system[] =
{
    { "ttyname",	t_lua_ttyname },

    { NULL, NULL }
};

/** 
 * lua_event_open:
 * @state: lua state
 * 
 * @Returns: 0 on success
 */
LUALIB_API void
t_lua_openlibs (lua_State *state)
{
  luaL_register (state, "event", _lua_event);
  luaL_register (state, "hotkey", _lua_hotkey);
  luaL_register (state, "ttyexec", _lua_ttyexec);
  luaL_register (state, "system", _lua_system);
}
