/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tscreen.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef struct _TFreeze		TFreeze;
struct _TFreeze
{
  guint16  row;
  guint16  col;
  glong    length;
  gchar   *content;
  gchar   *event_name;
};


/** 
 * t_screen_get_range:
 * @Returns: 
 */
static inline gchar*
t_screen_get_range (TScreen   *screen,
		    guint16    row,
		    guint16    col,
		    guint16    length)
{
  gchar *ret;
  gint i;

  g_return_val_if_fail (screen, NULL);

  if (row >= screen->rows || col >= screen->cols)
    {
      g_warning (_("row(%d) or col(%d) exceed screen size(%dx%d)."),
		 row, col, screen->rows, screen->cols);
      return NULL;
    }

  if (col + length > screen->cols || length == 0)
    {
      length = screen->cols - col;
    }

  /* append a eol character.
   */
  ret = g_malloc0 (length + 1);

  for (i = 0; i < length; i++, col++)
    {
      ret[i] = screen->cells[row][col].ch;
    }

  return ret;
}

/** 
 * t_screen_freeze_free:
 * @freeze 
 */
static void
t_screen_freeze_free (TFreeze *freeze)
{
  if (freeze)
    {
      g_free (freeze->content);
      g_free (freeze->event_name);
      g_free (freeze);
    }
}

/** 
 * t_screen_freeze_find:
 * @a:
 * @b:
 * 
 * @Returns: 
 */
static gint
t_screen_freeze_find (gconstpointer a,
		      gconstpointer b)
{
  TFreeze *freeze = (TFreeze*) a;
  gchar *event_name = (gchar*) b;

  g_return_val_if_fail (freeze, 1);
  g_return_val_if_fail (event_name, 1);

  if (freeze->event_name)
    {
      return strcmp (freeze->event_name, event_name);
    }
  return 1;
}

/** 
 * t_screen_add_freeze:
 * @screen 
 * @row 
 * @col 
 * @length 
 * @event_name 
 * 
 * @Returns: 
 */
gboolean
t_screen_add_freeze (TScreen     *screen,
		     guint16      row,
		     guint16      col,
		     glong        length,
		     const gchar *event_name)
{
  gchar *content;
  TFreeze *freeze;

  g_return_val_if_fail (screen, FALSE);

  content = t_screen_get_range (screen, row, col, length);
  if (!content)
    {
      g_warning (_("get screen content at %dx%d:%ld failed."),
		 row, col, length);
      return FALSE;
    }


  t_println ("add event %s with %ldx%ld:%ld and '%s' to freeze list.",
	     event_name, row, col, length, content);

  freeze = g_new0 (TFreeze, 1);

  freeze->row = row;
  freeze->col = col;
  freeze->length = length;
  freeze->content = content;
  freeze->event_name = g_strdup (event_name);

  screen->priv->freeze_list = g_slist_prepend (screen->priv->freeze_list,
					       freeze);
  return TRUE;
}

/** 
 * t_screen_is_freeze:
 * @screen:
 * @event_name:
 * 
 * @Returns: 
 */
gboolean
t_screen_is_freeze (TScreen     *screen,
		    const gchar *event_name)
{
  g_return_val_if_fail (screen, FALSE);
  g_return_val_if_fail (event_name, FALSE);

  if (g_slist_find_custom (screen->priv->freeze_list,
			   event_name,
			   t_screen_freeze_find))
    {
      return TRUE;
    }
  return FALSE;
}

/** 
 * t_screen_check_freezes:
 * @screen: vt
 */
void
t_screen_update_freezes (TScreen *screen)
{
  GSList *node, *next;
  TFreeze *freeze;
  gint i;
  GSList *events = NULL;

  g_return_if_fail (screen);

  for (node = screen->priv->freeze_list; node; node = node->next)
    {
      gchar *content;

      freeze = (TFreeze*) node->data;

      /* only update if that row has changed.
       */
      if (!screen->line_dirty[freeze->row])
	{
	  t_println (_("row %d unchanged since last match, skip."),
		     freeze->row);
	  continue;
	}

      /* compare content.
       */
      content = t_screen_get_range (screen, freeze->row,
				    freeze->col, freeze->length);
      if (content)
	{
	  if (strcmp (content, freeze->content) != 0)
	    {
	      t_println ("remove event '%s' from freeze list beacuse content has changed.",
			 freeze->event_name);

	      /* prepend to remove list.
	       */
	      if (!events || !g_slist_find_custom (events, freeze->event_name,
						   (GCompareFunc) strcmp))
		{
		  events = g_slist_prepend (events, g_strdup (freeze->event_name));
		}
	    }
	  g_free (content);
	}
    }

  /* remove all freeze node from list which event condition has changed.
   */
  if (events)
    {
      for (node = events; node; node = node->next)
	{
	  while (1)
	    {
	      if (!screen->priv->freeze_list)
		break;

	      next = g_slist_find_custom (screen->priv->freeze_list,
					  node->data,
					  t_screen_freeze_find);
	      if (!next)
		break;

	      freeze = (TFreeze*) next->data;

	      t_println ("remove a freeze node(%dx%d:%d) of event '%s'",
			 freeze->row, freeze->col, freeze->length,
			 (gchar*) node->data);

	      t_screen_freeze_free (freeze);
	      screen->priv->freeze_list = g_slist_delete_link (screen->priv->freeze_list, next);
	    }
	}
      g_slist_foreach (events, (GFunc) g_free, NULL);
      g_slist_free (events);
    }

  /* reset line dirty flags.
   */
  for (i = 0; i < screen->rows; i++)
    {
      screen->line_dirty[i] = FALSE;
    }
}

/** 
 * t_screen_create:
 * @cols:
 * @rows:
 * 
 * Creates a new virtual terminal with the given dimensions. You
 * must destroy it with t_screen_destroy after you are done with it.
 * The terminal will be initially blank and the cursor will
 * be at the top-left corner. 
 *
 * @Returns: NULL on error.
 */
TScreen*
t_screen_create (guint16 cols, guint16 rows)
{
  TScreen *screen;
  gint i, j;

  if (rows <= 0 || cols <= 0)
    return NULL;

  screen = g_new0 (TScreen, 1);

  screen->rows = rows;
  screen->cols = cols;

  screen->cells = g_new0 (TScreenCell*, screen->rows);
  for (i = 0; i < screen->rows; i++)
    {
      screen->cells[i] = g_new0 (TScreenCell, screen->cols);

      for (j = 0; j < screen->cols; j++)
	{
	  screen->cells[i][j].ch = 0x20;
	  screen->cells[i][j].attr = 0x70;
	}
    }

  screen->line_dirty = g_new0 (gboolean, screen->rows);
  for (i = 0; i < screen->rows; i++)
    {
      screen->line_dirty[i] = TRUE;
    }
  screen->crow = screen->ccol = 0;
  screen->curattr = 0x70;

  screen->priv = g_new0 (TScreenPrivate, 1);

  /* initial scrolling area is the whole window
   */
  screen->priv->scrolltop = 0;
  screen->priv->scrollbottom = screen->rows - 1;
  screen->priv->freeze_list = NULL;
  screen->priv->aux_is_open = FALSE;

  t_println ("created a %d x %d virtual screen.",
	     screen->cols, screen->rows);

  return screen;
}

/** 
 * t_screen_destroy:
 * @screen:
 *
 * Destroys a virtual terminal previously created with
 * t_screen_create. If screen == NULL, does nothing.
 */
void
t_screen_destroy (TScreen *screen)
{
  gint i;

  if (!screen)
    return;

  if (screen->priv->freeze_list)
    {
      g_slist_foreach (screen->priv->freeze_list,
		       (GFunc) t_screen_freeze_free,
		       NULL);
      g_slist_free (screen->priv->freeze_list);
    }
  g_free (screen->priv);

  for (i = 0; i < screen->rows; i++)
    {
      g_free (screen->cells[i]);
    }
  g_free (screen->cells);

  g_free (screen->line_dirty);
  g_free (screen);
}

/** 
 * t_screen_dump:
 * @screen 
 * @ptsname 
 */
#ifdef T_DEBUG
void
t_screen_dump (TScreen     *screen,
	       const gchar *ptsname)
{
  gchar fname[512], *p;
  FILE *fp;
  int i, j;

  g_return_if_fail (screen);
  g_return_if_fail (ptsname);

  g_snprintf (fname, sizeof (fname), "/tmp/screen%s", ptsname);
  for (p = fname + 8; *p != '\0'; p++)
    {
      if (*p == '/')
	*p = '_';
    }
  fp = fopen (fname, "w+");
  if (fp)
    {
      for (i = 0; i < screen->rows; i++)
	{
	  for (j = 0; j < screen->cols; j++)
	    {
	      fputc (screen->cells[i][j].ch, fp);
	    }
	  fputc ('\n', fp);
	}
      fclose (fp);
    }
}
#endif

/** 
 * t_screen_get_changed:
 * @screen:
 * 
 * @Returns: 
 */
gboolean
t_screen_get_changed (TScreen  *screen)
{
  gint i;

  g_return_val_if_fail (screen, FALSE);

  for (i = 0; i < screen->rows; i++)
    {
      if (screen->line_dirty[i])
	return TRUE;
    }

  /* curses position change think screen was changed.
   */
  if (screen->curpos_dirty)
    {
      return TRUE;
    }

  return FALSE;
}

/** 
 * t_screen_reset_cursor:
 */
void
t_screen_reset_cursor (TScreen *screen)
{
  g_return_if_fail (screen);

  screen->curpos_dirty = FALSE;
}

/** 
 * t_screen_match:
 * @screen 
 * @cond 
 * 
 * @Returns: 
 */
gboolean
t_screen_match (TScreen         *screen,
		TEventCondition *cond)
{
  gchar *content;

  g_return_val_if_fail (screen, FALSE);
  g_return_val_if_fail (cond, FALSE);

  switch (cond->ec_type)
    {
    case EVENT_TYPE_CURSOR:	/* match cursor */
      t_println ("try match cursor position %dx%d with current position %dx%d",
		 cond->ec_cursor_row, cond->ec_cursor_col,
		 screen->crow, screen->ccol);

      /* only match if cursor has changed.
       */
      if (screen->curpos_dirty)
	{
	  if (screen->crow == cond->ec_cursor_row &&
	      screen->ccol == cond->ec_cursor_col)
	    {
#if 0
	      /* reset cursor dirty flag.
	       */
	      screen->curpos_dirty = FALSE;
#endif
	      return TRUE;
	    }
	}
      else
	{
	  t_println ("cursor unchanged, skip.");
	}
      break;

    case EVENT_TYPE_CONTENT:	/* match expression */

      content = t_screen_get_range (screen,
				    cond->ec_content_row,
				    cond->ec_content_col,
				    cond->ec_content_length);
      if (!content)
	{
	  g_warning (_("get screen content at %dx%d:%d failed."),
		     cond->ec_content_row,
		     cond->ec_content_col,
		     cond->ec_content_length);
	  return FALSE;
	}

      t_println ("try match condition '%s' at %dx%d:%d with screen content '%s'",
		 cond->ec_content_regexpstr,
		 cond->ec_content_row,
		 cond->ec_content_col,
		 cond->ec_content_length,
		 content);

      if (regexec (&cond->ec_content_regexp, content, 1, NULL, 0) == 0)
	{
	  g_free (content);
	  return TRUE;
	}
      else
	{
	  t_println (_("try match '%s' with '%s' failed."),
		     cond->ec_content_regexpstr, content);
	  g_free (content);
	}
      break;

    case EVENT_TYPE_HOTKEY:
    case EVENT_TYPE_KEYBOARD:
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  return FALSE;
}

/** 
 * t_screen_snapshot:
 * @screen:
 * @rows:
 * @cols:
 * 
 * takes a snapshot of the current contents of the screen and
 * saves them to a dynamically allocated buffer. returns a pointer
 * arrary to the newly created buffer.
 *
 * @Returns: a string array, caller is responsible for
 * g_strfreev()'ing when the snapshot is no longer needed.
 */
gchar**
t_screen_snapshot (TScreen *screen,
		   guint16 *rows,
		   guint16 *cols)
{
  gchar **ret;
  gint i, j;

  g_return_val_if_fail (screen, NULL);
  g_return_val_if_fail (rows, NULL);
  g_return_val_if_fail (cols, NULL);

  /* append a eoa(end of array) pad.
   */
  ret = g_new0 (gchar*, screen->rows + 1);

  for (i = 0; i < screen->rows; i++)
    {
      /* append a eol(end of line) pad.
       */
      ret[i] = g_malloc0 (screen->cols + 1);

      for (j = 0; j < screen->cols; j++)
	{
	  ret[i][j] = screen->cells[i][j].ch;
	}
    }
  *rows = screen->rows;
  *cols = screen->cols;

  return ret;
}

/** 
 * t_screen_aux_is_open:
 * @screen:
 * 
 * @Returns: 
 */
gboolean
t_screen_aux_is_open (TScreen *screen)
{
  return screen->priv->aux_is_open;
}
