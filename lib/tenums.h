/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Generated data (by glib-mkenums) */

#ifndef __T_ENUMS_H__
#define __T_ENUMS_H__

#include <glib-object.h>

G_BEGIN_DECLS


/* enumerations from "ttypes.h" */

GType t_rpc_code_get_type(void) G_GNUC_CONST;
#define T_TYPE_RPC_CODE	(t_rpc_code_get_type())


GType t_rpc_errno_get_type(void) G_GNUC_CONST;
#define T_TYPE_RPC_ERRNO	(t_rpc_errno_get_type())


GType t_event_type_get_type(void) G_GNUC_CONST;
#define T_TYPE_EVENT_TYPE	(t_event_type_get_type())


/* enumerations from "tshmem.h" */

GType t_shmem_type_get_type(void) G_GNUC_CONST;
#define T_TYPE_SHMEM_TYPE	(t_shmem_type_get_type())


GType t_sheme_entry_flags_get_type(void) G_GNUC_CONST;
#define T_TYPE_SHEME_ENTRY_FLAGS	(t_sheme_entry_flags_get_type())


/* enumerations from "trpccall.h" */

GType t_rpc_target_type_get_type(void) G_GNUC_CONST;
#define T_TYPE_RPC_TARGET_TYPE	(t_rpc_target_type_get_type())


/* enumerations from "ttermop.h" */

GType t_term_char_attr_get_type(void) G_GNUC_CONST;
#define T_TYPE_TERM_CHAR_ATTR	(t_term_char_attr_get_type())


GType t_term_button_get_type(void) G_GNUC_CONST;
#define T_TYPE_TERM_BUTTON	(t_term_button_get_type())



G_END_DECLS

#endif /* __T_ENUMS_H__ */

/* Generated data ends here */

