#!/bin/sh

bindir=$HOME/bin

if [ ! -d $bindir ]; then
        mkdir $bindir
fi

for execable in `ls bin`; do
        if [ -h $bindir/$execable ]; then
                unlink $bindir/$execable
        elif [ -f $bindir/$execable ]; then
                echo "$bindir/$execable exists, please check."
                exit 1
        fi
        ln -s $PWD/bin/$execable $bindir/$execable
done
