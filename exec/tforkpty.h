/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_FORKPTY_H__
#define __T_FORKPTY_H__

#include <sys/types.h>
#include <glib.h>

G_BEGIN_DECLS

#if defined(HAVE_OPENPTY) && ! defined(BIND_PTY)
#if defined(HAVE_PTY_H)
# include <pty.h>
#elif defined(HAVE_UTIL_H)	/* OpenBSD */
# include <util.h>
#elif defined(HAVE_LIBUTIL_H)	/* FreeBSD */
# include <libutil.h>
#elif defined(HAVE_LIBUTIL)	/* BSDI has libutil, but no libutil.h */
/* Avoid pulling in all the include files for no need
 */
struct termios;
struct winsize;
struct utmp;
	    
void	login		(struct utmp    *ut);
int	login_tty	(int             fd);
int	logout		(char           *line);
void	logwtmp		(const char     *line,
			 const char     *name,
			 const char     *host);
int  	openpty		(int            *amaster,
			 int            *aslave,
			 char           *name,
			 struct termios *termp,
			 struct winsize *winp);
int  	forkpty		(int            *amaster,
			 char           *name,
			 struct termios *termp,
			 struct winsize *winp);
#endif

#else /* HAVE_OPENPTY */
int	openpty		(int            *master_fd,
			 int            *slavefd,
			 char           *name,
			 struct termios *termp,
			 struct winsize *winp);
pid_t	forkpty 	(int            *master_fd,
			 char           *name,
			 struct termios *termp,
			 struct winsize *winp);
#endif /* HAVE_OPENPTY */

#ifndef HAVE_LOGIN_TTY
int	login_tty	(int             fd);
#elif defined(HAVE_UTMP_H)
/* Get the prototype from utmp.h
 */
#include <utmp.h>
#endif /* HAVE_LOGIN_TTY */

pid_t	t_forkpty 	(int            *master_fd,
			 char           *name,
			 struct termios *termp,
			 struct winsize *winp);

G_END_DECLS

#endif /* __T_FORKPTY_H__ */
