/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_TERM_H__
#define __T_TERM_H__

#include <glib-object.h>

#include "tscreen.h"

G_BEGIN_DECLS


#define T_TYPE_TERM             (t_term_get_type ())
#define T_TERM(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), T_TYPE_TERM, TTerm))
#define T_TERM_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), T_TYPE_TERM, TTermClass))
#define T_IS_TERM(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), T_TYPE_TERM))
#define T_IS_TERM_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), T_TYPE_TERM))
#define T_TERM_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), T_TYPE_TERM, TTermClass))

typedef struct _TTerm		TTerm;
typedef struct _TTermClass 	TTermClass;
typedef struct _TTermPrivate 	TTermPrivate;

struct _TTerm
{
  GObject object;

  TScreen      *screen;
  TTermPrivate *priv;
};

struct _TTermClass
{
  GObjectClass object_class;

  void	(*incoming)		(TTerm       *term,
				 const gchar *cache,
				 guint        cache_size);
  void	(*refreshed)		(TTerm       *term);
  void	(*child_exited)		(TTerm       *term,
				 GPid         pid,
				 gint         status);
};

GType		t_term_get_type 	(void) G_GNUC_CONST;

TTerm*		t_term_new 		(void);

void		t_term_set_size		(TTerm      *term,
					 guint16     cols,
					 guint16     rows);
void		t_term_get_size 	(TTerm      *term,
					 guint16    *cols,
					 guint16    *rows);

gboolean	t_term_fork_command	(TTerm       *term,
					 const gchar *command);
void		t_term_feed_child	(TTerm       *term,
					 const gchar *text,
					 gsize        length);
const gchar*	t_term_get_cache	(TTerm       *term,
					 gsize       *size);
gchar*		t_term_ptsname		(TTerm       *term);
GPid		t_term_get_child_pid	(TTerm       *term);

gboolean	t_term_is_to_auxport	(TTerm       *term);
const gchar*	t_term_get_screen_data 	(TTerm       *term);
gsize		t_term_get_screen_size 	(TTerm       *term);

G_END_DECLS

#endif /* __T_TERM_H__ */
