/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_RPCDSVC_H__
#define __T_RPCDSVC_H__

#include <glib.h>
#include <ttyutils.h>

G_BEGIN_DECLS

gint		t_rpcdsvc_call	(TRpcRequest *request,
				 const gchar *buffer,
				 gsize        size,
				 gchar      **rbuffer,
				 gsize       *rsize);

G_END_DECLS

#endif /* __T_RPCDSVC_H__ */
