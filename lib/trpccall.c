/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>

#include "ttypes.h"
#include "tdebug.h"
#include "tmisc.h"
#include "trpc.h"
#include "trpccall.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

static TRpc	*t_rpc = NULL;


/** 
 * t_rpc_call_free_target:
 */
static void
t_rpc_call_free_target (void)
{
  if (t_rpc)
    {
      t_rpc_free (t_rpc);
      t_rpc = NULL;
    }
}

/** 
 * t_rpc_call_set_target:
 * @target: a #TRpcTarget object
 * 
 * @Returns: TRUE on success
 */
gboolean
t_rpc_call_set_target (TRpcTarget *target)
{
  gchar *name;

  g_return_val_if_fail (target, FALSE);

  if (t_rpc)
    {
      t_rpc_free (t_rpc);
      t_rpc = NULL;
    }

  switch (target->rt_type)
    {
    case RPC_TARGET_TTY:
      name = t_guess_ttyname (target->rt_tty);
      if (!name)
	{
	  g_warning (_("%s not found."), target->rt_tty);
	  return FALSE;
	}
      t_rpc = t_rpc_new_from_tty (name);
      g_free (name);
      break;

    case RPC_TARGET_PTS:
      name = t_guess_ttyname (target->rt_pts);
      if (!name)
	{
	  g_warning (_("%s not found."), target->rt_pts);
	  return FALSE;
	}
      t_rpc = t_rpc_new_from_pts (name);
      g_free (name);
      break;

    case RPC_TARGET_PID:
      t_rpc = t_rpc_new_from_pid (target->rt_pid);
      break;

    case RPC_TARGET_PATH:
      t_rpc = t_rpc_new_from_path (target->rt_path);
      break;

    default:
      g_critical (_("rpc target type %d unsupported"), target->rt_type);
      return FALSE;
    }

  if (!t_rpc)
    {
      return FALSE;
    }
  t_rpc_set_timeout (t_rpc, target->rt_timeout);

  /* free the resources when application exit.
   */
  g_atexit (t_rpc_call_free_target);

  return TRUE;
}

/** 
 * t_rpc_call_send_request:
 * @code:
 * @size:
 * @data:
 * 
 * @Returns: id of reqeust.
 */
static gboolean
t_rpc_call_send_request (guint16      code,
			 gpointer     data,
			 gsize        size,
			 guint16     *id)
{
  TRpcRequest *request;
  gsize total;

  g_return_val_if_fail (id, FALSE);

  if (!t_rpc)
    {
      g_critical (_("rpc target unknown, did you forget set target."));
      return FALSE;
    }

  *id = g_random_int_range (1, G_MAXUINT16 - 1);

  total = size + sizeof (TRpcRequest);

  request = g_malloc0 (total);

  request->id =   g_htons (*id);
  request->code = g_htons (code);
  request->pid =  g_htonl (getpid ());
  request->uid =  g_htonl (getuid ());
  request->gid =  g_htonl (getgid ());
  request->size = g_htonl (total);

  if (data && size > 0)
    {
      memcpy (&request[1], data, size);
    }

  t_println ("request code(%d), id(%d), pid(%d), uid(%d) gid(%d), size(%d)",
	     code, *id, getpid (), getuid (), getgid (), total);

  if (!t_rpc_send (t_rpc, request, total))
    {
      g_free (request);
      return FALSE;
    }
  g_free (request);

  return TRUE;
}

/**
 * t_rpc_check_response:
 * @response:
 * @id:
 * 
 * @Returns: 
 */
static gboolean
t_rpc_check_response (TRpcResponse *response,
		      guint16       id)
{
  guint16 code;

  if (!response)
    {
      g_warning (_("receive response failed."));
      return FALSE;
    }

  t_println ("response code(%d), id(%d)",
	     g_ntohs (response->code), g_ntohs (response->id));

  if (id != g_ntohs (response->id))
    {
      g_warning (_("response id %d and request id %d dismatch"),
		 g_ntohs (response->id), id);
      return FALSE;
    }

  code = g_ntohs (response->code);
  if (code != RPC_ERR_OK)
    {
      g_warning (_("receive response %d: %s"), code, t_rpc_strerror (code));
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_rpc_call_recv_response:
 * @id:
 * @data:
 * @size:
 * 
 * @Returns: 
 */
static gboolean
t_rpc_call_recv_response (guint16   id,
			  gpointer *data,
			  guint32  *data_size)
{
  TRpcResponse *response;
  guint32 size;

  if (!t_rpc)
    {
      g_critical (_("rpc target unknown, did you forget set target."));
      return 0;
    }

  response = t_rpc_recv (t_rpc, sizeof (TRpcResponse));
  if (!t_rpc_check_response (response, id))
    {
      g_free (response);
      return FALSE;
    }

  size = g_ntohl (response->size) - sizeof (TRpcResponse);
  g_free (response);

  /* receive response data.
   */
  if (size > 0)
    {
      gpointer recvdata = t_rpc_recv (t_rpc, size);

      if (!recvdata)
	{
	  g_warning (_("receive response data failed."));
	  return FALSE;
	}

      if (data_size)
	{
	  *data_size = size;
	}
      if (data)
	{
	  *data = recvdata;
	}
      else
	{
	  g_free (recvdata);
	}
    }
  return TRUE;
}

/** 
 * t_rpc_call_snap:
 * @rows: how many rows
 * @cols: how many cols
 * @snap: a string vector, use g_strfreev() to free.
 * 
 * @Returns: TRUE on success
 */
gboolean
t_rpc_call_snap (guint16     *rows,
		 guint16     *cols,
		 gchar     ***snap)
{
  TRpcSnapOut *snapout;
  guint16 id, i;
  guint32 size = 0;
  gchar **strv, *p;

  g_return_val_if_fail (rows, FALSE);
  g_return_val_if_fail (cols, FALSE);
  g_return_val_if_fail (snap, FALSE);

  if (!t_rpc_call_send_request (RPC_CODE_SNAP, NULL, 0, &id))
    {
      return FALSE;
    }
  if (!t_rpc_call_recv_response (id, (gpointer*) &snapout, &size))
    {
      return FALSE;
    }

  if (size == 0)
    {
      g_warning (_("response size too short."));
      return FALSE;
    }

  *rows = g_ntohs (snapout->rows);
  *cols = g_ntohs (snapout->cols);

  strv = g_new0 (gchar*, *rows + 1);
  p = snapout->data;

  for (i = 0; i < *rows; i++)
    {
      strv[i] = g_strndup (p, *cols);
      p += *cols;
    }
  g_free (snapout);

  *snap = strv;

  return TRUE;
}

/** 
 * t_rpc_call_feed:
 * @string 
 * @length 
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_feed (const gchar *string,
		 gssize       length)
{
  TRpcFeedIn *feedin;
  guint16 id;

  g_return_val_if_fail (string, FALSE);

  if (length <= 0)
    length = strlen (string);

  feedin = g_malloc0 (sizeof (TRpcFeedIn) + length);

  feedin->length = g_htonl (length);
  memcpy (feedin->string, string, length);

  t_println ("feed %d bytes, contents was '%s'", length, string);

  if (!t_rpc_call_send_request (RPC_CODE_FEED, feedin,
				sizeof (TRpcFeedIn) + length, &id))
    {
      g_free (feedin);
      return FALSE;
    }
  g_free (feedin);

  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_get_events:
 * @events 
 * @number 
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_get_events (TEventEntry   **events,
		       gsize          *number)
{
  guint16 id;
  guint32 size = 0;

  g_return_val_if_fail (events, FALSE);
  g_return_val_if_fail (number, FALSE);

  if (!t_rpc_call_send_request (RPC_CODE_GET_EVENTS, NULL, 0, &id))
    {
      return FALSE;
    }

  if (!t_rpc_call_recv_response (id, (gpointer*) events, &size))
    {
      return FALSE;
    }

  if (size == 0)
    {
      g_warning (_("get no events."));
      *number = 0;
      return TRUE;
    }

  if (size % sizeof (TEventEntry) != 0)
    {
      g_warning (_("rpc get events return size %u dismatch."), size);
      g_free (*events);
      *events = NULL;
      *number = 0;
    }
  else
    {
      *number = size / sizeof (TEventEntry);
    }
  return (*events) ? TRUE : FALSE;
}

/** 
 * t_rpc_call_insert_event:
 * @name: event name
 * @group: event group name
 * @matcher_exp: a matcher expression
 * @program: a command and its arguments.
 * 
 * @Returns: TRUE on success
 */
gboolean
t_rpc_call_insert_event (const gchar *name,
			 const gchar *group,
			 const gchar *matcher,
			 const gchar *program)
{
  TEventEntry event;
  guint16 id;

  g_return_val_if_fail (name, FALSE);
  g_return_val_if_fail (group, FALSE);
  g_return_val_if_fail (matcher, FALSE);
  g_return_val_if_fail (program, FALSE);

  if (strlen (name) > sizeof (event.name) ||
      strlen (group) > sizeof (event.group) ||
      strlen (matcher) > sizeof (event.matcher) ||
      strlen (program) > sizeof (event.program))
    {
      g_warning (_("argument exceed limit, the max length of event name is %d,"
		   "group name is %d, matcher is %d, program is %d"),
		 sizeof (event.name), sizeof (event.group),
		 sizeof (event.matcher), sizeof (event.program));
      return FALSE;
    }

  g_strlcpy (event.name, name, sizeof (event.name));
  g_strlcpy (event.group, group, sizeof (event.group));
  g_strlcpy (event.matcher, matcher, sizeof (event.matcher));
  g_strlcpy (event.program, program, sizeof (event.program));

  if (!t_rpc_call_send_request (RPC_CODE_INSERT_EVENT, &event,
				sizeof (TEventEntry), &id))
    {
      return FALSE;
    }

  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_remove_event:
 * @name: event name
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_remove_event (const gchar *name)
{
  TEventEntry entry;
  guint16 id;

  g_return_val_if_fail (name, FALSE);

  g_strlcpy (entry.name, name, sizeof (entry.name));

  if (!t_rpc_call_send_request (RPC_CODE_REMOVE_EVENT,
				&entry, sizeof (TEventEntry), &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_remove_event_group:
 * @group 
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_remove_event_group (const gchar *group)
{
  TEventEntry entry;
  guint16 id;

  g_return_val_if_fail (group, FALSE);

  g_strlcpy (entry.group, group, sizeof (entry.group));

  if (!t_rpc_call_send_request (RPC_CODE_REMOVE_EVENT_GROUP,
				&entry, sizeof (TEventEntry), &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_stdin_suppend:
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_rpc_call_stdin_suppend (void)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_STDIN_SUSPEND, NULL, 0, &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_stdin_resume:
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_stdin_resume (void)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_STDIN_RESUME, NULL, 0, &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_stdout_suppend:
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_stdout_suppend (void)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_STDOUT_SUSPEND, NULL, 0, &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_stdout_resume:
 * 
 * @Returns: 
 */
gboolean
t_rpc_call_stdout_resume (void)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_STDOUT_RESUME, NULL, 0, &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/** 
 * t_rpc_call_trigger_event:
 * 
 * trigger a event unconditional.
 *
 * @Returns: TRUE on success. 
 */
gboolean
t_rpc_call_trigger_event (const gchar *name)
{
  TEventEntry entry;
  guint16 id;

  g_return_val_if_fail (name, FALSE);

  g_strlcpy (entry.name, name, sizeof (entry.name));

  if (!t_rpc_call_send_request (RPC_CODE_TRIGGER_EVENT,
				&entry, sizeof (TEventEntry), &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}

/**
 * t_rpc_call_get_cursor_pos:
 *
 * get cursor current position.
 */
gboolean
t_rpc_call_get_cursor_pos (guint16 *row, guint16 *col)
{
  TRpcCurPosOut *curpos;
  guint32 size = 0;
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_CURSOR_XY, NULL, 0, &id))
    {
      return FALSE;
    }
  if (!t_rpc_call_recv_response (id, (gpointer*) &curpos, &size))
    {
      return FALSE;
    }

  if (size != sizeof (TRpcCurPosOut))
    {
      g_warning (_("response size dismatch"));
      return FALSE;
    }
  *row = g_ntohs (curpos->row);
  *col = g_ntohs (curpos->col);

  g_free (curpos);

  return TRUE;
}

/**
 * t_rpc_call_refresh:
 * 
 * @Returns:
 */
gboolean
t_rpc_call_refresh (void)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_REFRESH, NULL, 0, &id))
    return FALSE;

  return t_rpc_call_recv_response (id, NULL, NULL);
}

/**
 * t_rpc_call_set_env:
 *
 * set env for event share use.
 */
gboolean
t_rpc_call_set_env (const gchar *env)
{
  guint16 id;

  if (!t_rpc_call_send_request (RPC_CODE_SET_ENV, (gpointer)env,
				strlen(env), &id))
    {
      return FALSE;
    }
  return t_rpc_call_recv_response (id, NULL, NULL);
}
