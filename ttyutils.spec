# Ttyutils RPM package spec file
# Copyright (c) 2007,2008,2009,2010,2011 xiaohu, All rights reserved.
#

# Preamble Section
#
Name:		ttyutils
Summary:	UNIX Terminal Utilities
Version:	1.9.2
Release:	0
License:	GPLv2+
Group:		Applications/System
URL:		http://code.google.com/p/ttyutils
Source:		%{name}-1.9.2.tar.gz
Vendor:		WUXIAOHU
BuildRoot:	/tmp/%{name}-1.9.2
BuildPrereq:	ncurses-devel
BuildPrereq:	glib2-devel
Packager:	xiaohu <xiaohu417@gmail.com>
Requires: 	ncurses > 5.0.0
Requires: 	glib2 > 2.4.0

%description
Ttyutils is a suite of UNIX terminal tools, it includes ttyexec, ttylook, ttyadmin, and a few extension programs.

ttyexec executes program in a pseudo terminal, captures all stdandard output from program, passes them to built-in virtual terminal emulator and real terminal, virtual terminal then parse the data, construct a virtual screen in the memory which content was same with the real terminal.

by virtual terminal emulator, a event may be trigger by a specific condition. most conditions was output from program cause terminal changed, but hotkey and cursor position event is supported too.

ttyadmin is a administrator tool, which use ncurses(3X) window interface, view and control ttyexec instances.

ttylook similar to BSD watch(1) program, but has fewer limits, and can interact with exists ttyexec instance, snoop on which output, or input data to that terminal if writable mode was enabled.

%build
case `uname` in
"AIX")
%configure --disable-debug --disable-ncursesw
;;
*)
%configure --disable-debug
;;
esac
make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

# prepare
#
%prep
%setup -q

# pre-install
#
%pre

# post-install
#
%post
if [ `uname` = "AIX" ]; then
	unlink /usr/bin/ttyexec >/dev/null 2>&1
	unlink /usr/bin/ttyadmin >/dev/null 2>&1
	unlink /usr/bin/ttylook >/dev/null 2>&1
	unlink /usr/bin/ttysh >/dev/null 2>&1
	unlink /usr/lib/libttylib-1.a >/dev/null 2>&1
	ln -s /opt/freeware/bin/ttyexec /usr/bin/ttyexec
	ln -s /opt/freeware/bin/ttyadmin /usr/bin/ttyadmin
	ln -s /opt/freeware/bin/ttylook /usr/bin/ttylook
	ln -s /opt/freeware/bin/ttysh /usr/bin/ttysh
	ln -s /opt/freeware/lib/libttylib-1.a /usr/lib/libttylib-1.a
fi
echo ""
echo "  ttyutils 1.9.2 release 0"
echo "  Copyright (c) 2008,2009,2010,2011 xiaohu, all rights reserved."
echo "  Install finish!!!"
echo ""

# pre-uninstall
#
%preun

# post-uninstall
#
%postun
if [ `uname` = "AIX" ]; then
	if [ ! -x /opt/freeware/bin/ttyexec ]; then
		unlink /usr/bin/ttyexec >/dev/null 2>&1
	fi
	if [ ! -x /opt/freeware/bin/ttyadmin ]; then
		unlink /usr/bin/ttyadmin >/dev/null 2>&1
	fi
	if [ ! -x /opt/freeware/bin/ttylook ]; then
		unlink /usr/bin/ttylook >/dev/null 2>&1
	fi
	if [ ! -x /opt/freeware/bin/ttysh ]; then
		unlink /usr/bin/ttysh >/dev/null 2>&1
	fi
	if [ ! -x /opt/freeware/lib/libttylib-1.a ]; then
		unlink /usr/lib/libttylib-1.a >/dev/null 2>&1
	fi
fi
echo ""
echo "  ttyutils 1.9.2 release 0"
echo "  Copyright (c) 2008,2009,2010,2011 xiaohu, all rights reserved."
echo "  Uninstall finish!!!"
echo ""

# install files
#
%files
%defattr(-, root, root)
%{_bindir}/*
%{_libdir}/libttylib*
%{_libdir}/pkgconfig/*.pc
%{_datadir}/*
%dir %{_libdir}/ttyutils/lua
%config %{_libdir}/ttyutils/lua/*.lua
%dir %{_sysconfdir}/ttyutils
%config %{_sysconfdir}/ttyutils/*
%dir %{_includedir}/ttyutils-1
%{_includedir}/ttyutils-1/*.h
%dir %{_includedir}/ttyutils-1/lib
%{_includedir}/ttyutils-1/lib/*.h

%changelog
* Wed Aug 06 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.4.0 release

* Wed Jul 30 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.3.0 release

* Fri Jul 19 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.2.5 release

* Fri Jul 11 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.1.2 release

* Fri Jul 6 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.1.1 release

* Mon Jun 17 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-1.0.2 release

* Sat May 10 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-0.2.4 release

* Wed Apr 16 2008 xiaohu <xiaohu417@gmail.com>
- ttyutils-0.1.5 release
