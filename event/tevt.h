/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __TEVT_H__
#define __TEVT_H__

#include <glib.h>

G_BEGIN_DECLS

#define TTYHOME		"/dcc/ttyutils"
#define DBNAME		TTYHOME "/share/ttyutils/khmyd.db"
#define LOGFILE		TTYHOME "/log/ttyevt.log"

#define EVAL_MATCHER		"2,23,0,交易执行成功"

void	t_event_termtype(const char *tty);
void	t_event_login (const char *tty);
void	t_event_reset (void);
void	t_event_eval (const char *tty);

int term_openPort (int term, int port);
int term_closePort (int term);
int term_dataToDevice (int term, unsigned char *data, int size);
int term_readLenStr (unsigned char *string, int length, int timeout);
void* term_lock_keyboard (void *arg);
void term_unlock_keyboard (void);

int DCC_OpenDB();
int DCC_CloseDB();
int DCC_GetTermInfoByLogicNo(char *logicno,
		char *termtype,
		char *printertype,
		char *magcardtype,
		char *keypadtype,
		char *lptype,
		char *ictype,
		char *oth1type,
		char *oth2type
		);

G_END_DECLS

#endif
