/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_PTY_H__
#define __T_PTY_H__

#include <glib.h>

G_BEGIN_DECLS

gint	t_pty_open	(pid_t       *child,
			 const gchar *command,
			 gchar      **argv,
			 gint         columns,
			 gint         rows);
void 	t_pty_close	(gint         pty);

gint	t_pty_get_size	(gint         master,
			 gint        *columns,
			 gint        *rows);
gint	t_pty_set_size	(gint         master,
			 gint         columns,
			 gint         rows);
void 	t_pty_set_utf8	(gint         pty,
			 gboolean     utf8);
char*	t_pty_ptsname	(gint         master);

G_END_DECLS

#endif /* __T_PTY_H__ */
