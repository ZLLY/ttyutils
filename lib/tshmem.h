/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __T_SHMEM_H__
#define __T_SHMEM_H__

#include <sys/types.h>
#include <unistd.h>
#include <glib.h>

G_BEGIN_DECLS

typedef enum
{
  SHMEM_TYPE_POSIX,
  SHMEM_TYPE_SYSV
} TShmemType;

typedef enum
{
  SHMEM_FLAG_NOEVENT = 1 << 0,
} TShemeEntryFlags;

typedef struct _TShmemEntry	TShmemEntry;

struct _TShmemEntry
{
  pid_t   pid;
  uid_t   uid;
  guint32 flags;
  time_t  ctime;
  gchar   tty[64];
  gchar   pts[64];
  gchar   lookpath[128];
  gchar   rpcpath[128];
  gchar   command[64];
  guint16 looknum;
  gchar   unused[46];
};

typedef gboolean TShmemLookupFunc	(TShmemEntry     *entry,
					 gpointer         user_data);

gboolean	t_shmem_open			(gboolean         writable);
void		t_shmem_close			(void);
gboolean	t_shmem_remove			(void);

#ifdef _SYSV_IPC
gint		t_shmem_get_shmid		(void);
gint		t_shmem_get_semid		(void);
#endif
gsize		t_shmem_get_size		(void);

gint		t_shmem_get_version		(void);
gint		t_shmem_get_nentries		(void);
uid_t		t_shmem_get_uid			(void);
TShmemType	t_shmem_get_type 		(void);

void		t_shmem_cleanup			(void);

gboolean	t_shmem_append_entry		(TShmemEntry     *entry);
TShmemEntry*	t_shmem_lookup_entry		(TShmemLookupFunc func,
						 gpointer         data);
void		t_shmem_remove_entry		(void);

void		t_shmem_set_event_enabled	(pid_t            pid,
						 gboolean         enable);
gboolean	t_shmem_get_event_enabled	(pid_t            pid);

void		t_shmem_looknum_increase	(void);
void		t_shmem_looknum_decrease	(void);


G_END_DECLS

#endif /* __T_SHMEM_H__ */
