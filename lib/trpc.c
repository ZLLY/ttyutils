/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include <glib/gi18n-lib.h>

#include "tdebug.h"
#include "ttypes.h"
#include "tshmem.h"
#include "tunixsock.h"
#include "trpc.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

struct _TRpc
{
  TUnixSocket *unixsock;
  GIOChannel  *unixchnl;
  guint        timeout;
};

/** 
 * t_rpc_new_from_path:
 * @path: server unix rpc name
 * 
 * @Returns: a New #TRpc object.
 */
TRpc*
t_rpc_new_from_path (const gchar *path)
{
  TRpc *rpc;

  g_return_val_if_fail (path, NULL);

  rpc = g_new0 (TRpc, 1);

  rpc->unixsock = t_unix_socket_new_abstract (path);
  if (!rpc->unixsock)
    {
      g_warning (_("connect %s failed."), path);
      g_free (rpc);
      return NULL;
    }
  rpc->unixchnl = t_unix_socket_get_io_channel (rpc->unixsock);
  if (!rpc->unixchnl)
    {
      t_unix_socket_delete (rpc->unixsock);
      g_free (rpc);
      return FALSE;
    }

  /* change channel attributes.
   */
  g_io_channel_set_buffered (rpc->unixchnl, TRUE);
  g_io_channel_set_buffer_size (rpc->unixchnl, 0);
  g_io_channel_set_encoding (rpc->unixchnl, NULL, NULL);
  g_io_channel_set_flags (rpc->unixchnl, G_IO_FLAG_NONBLOCK, NULL);

  return rpc;
}

/** 
 * t_rpc_lookup_path_by_tty:
 * @entry 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_rpc_lookup_path_by_tty (TShmemEntry *entry,
			  gpointer     data)
{
  gchar *tty = (gchar*) data;

  g_return_val_if_fail (entry, FALSE);
  g_return_val_if_fail (tty, FALSE);

  return (strcmp (entry->tty, tty) == 0);
}

/** 
 * t_rpc_lookup_path_by_pts:
 * @entry 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_rpc_lookup_path_by_pts (TShmemEntry *entry,
			  gpointer     data)
{
  gchar *pts = (gchar*) data;

  g_return_val_if_fail (entry, FALSE);
  g_return_val_if_fail (pts, FALSE);

  return (strcmp (entry->pts, pts) == 0);
}

/** 
 * t_rpc_lookup_path_by_pid:
 * @entry 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_rpc_lookup_path_by_pid (TShmemEntry *entry,
			  gpointer     data)
{
  pid_t pid = GPOINTER_TO_UINT (data);

  g_return_val_if_fail (entry, FALSE);
  g_return_val_if_fail (pid > 0, FALSE);

  return (entry->pid == pid);
}

/** 
 * t_rpc_lookup_unixpath:
 * @tty 
 * @pts 
 * @pid 
 * 
 * @Returns: 
 */
static const gchar*
t_rpc_lookup_unixpath (const gchar *tty,
		       const gchar *pts,
		       pid_t        pid)
{
  static gchar path[512];
  TShmemEntry *entry = NULL;

  if (!t_shmem_open (FALSE))
    return NULL;

  if (tty)
    {
      entry = t_shmem_lookup_entry (t_rpc_lookup_path_by_tty,
				    (gpointer) tty);
    }
  else if (pts)
    {
      entry = t_shmem_lookup_entry (t_rpc_lookup_path_by_pts,
				    (gpointer) pts);
    }
  else if (pid > 0)
    {
      entry = t_shmem_lookup_entry (t_rpc_lookup_path_by_pid,
				    GUINT_TO_POINTER (pid));
    }
  t_shmem_close ();

  if (!entry)
    {
      g_warning (_("cannot found matched entry."));
      return NULL;
    }

#ifdef T_DEBUG
  g_assert (sizeof (path) > strlen (entry->rpcpath));
#endif

  g_strlcpy (path, entry->rpcpath, sizeof (path));
  g_free (entry);

  return path;
}

/** 
 * t_rpc_new_from_pid:
 * @pid: server process id
 * 
 * @Returns: 
 */
TRpc*
t_rpc_new_from_pid (GPid pid)
{
  const gchar *path;

  g_return_val_if_fail (pid > 0, NULL);

  path = t_rpc_lookup_unixpath (NULL, NULL, pid);

  return path ? t_rpc_new_from_path (path) : NULL;
}

/** 
 * t_rpc_new_from_tty:
 * @tty 
 * 
 * @Returns: 
 */
TRpc*
t_rpc_new_from_tty (const gchar *tty)
{
  const gchar *path;

  g_return_val_if_fail (tty, NULL);

  path = t_rpc_lookup_unixpath (tty, NULL, -1);

  return path ? t_rpc_new_from_path (path) : NULL;
}

/** 
 * t_rpc_new_from_pts:
 * @pts 
 * 
 * @Returns: 
 */
TRpc*
t_rpc_new_from_pts (const gchar *pts)
{
  const gchar *path;

  g_return_val_if_fail (pts, NULL);

  path = t_rpc_lookup_unixpath (NULL, pts, -1);

  return path ? t_rpc_new_from_path (path) : NULL;
}

/** 
 * t_rpc_free:
 * @rpc: a #TRpc
 */
void
t_rpc_free (TRpc *rpc)
{
  if (!rpc)
    return;

  t_unix_socket_delete (rpc->unixsock);
  g_free (rpc);
}

/** 
 * t_rpc_set_timeout:
 * @rpc 
 * @timeout 
 */
void
t_rpc_set_timeout (TRpc *rpc, guint timeout)
{
  g_return_if_fail (rpc);

  rpc->timeout = timeout;
}

/** 
 * t_rpc_send:
 * @rpc:
 * @string:
 * @size:
 * 
 * @Returns:
 */
gboolean
t_rpc_send (TRpc          *rpc,
	    gconstpointer  data,
	    gssize         size)
{
  const gchar *string;

  g_return_val_if_fail (data, FALSE);
  g_return_val_if_fail (rpc, FALSE);
  g_return_val_if_fail (size > 0, FALSE);

  if (!rpc->unixsock || !rpc->unixchnl)
    return FALSE;

  if (size == 0)
    return TRUE;

  string = (const gchar*) data;

  while (size > 0)
    {
      GIOStatus status;
      gsize nwriten;
      GError *error = NULL;

      status = g_io_channel_write_chars (rpc->unixchnl, string, size,
					 &nwriten, &error);
      if (status != G_IO_STATUS_NORMAL)
	{
	  if (error)
	    {
	      g_warning (_("write rpc channel error: %s"), error->message);
	      g_error_free (error);
	    }
	  return FALSE;
	}
      else
	{
	  size -= nwriten;
	  string += nwriten;
	}
    }
  g_io_channel_flush (rpc->unixchnl, NULL);

  return TRUE;
}

/** 
 * t_rpc_recv:
 * @rpc:
 * @size:
 * 
 * @Returns: 
 */
gpointer
t_rpc_recv (TRpc *rpc, gsize size)
{
  gsize nread;
  gchar *data;
  gchar *recv_data;

  g_return_val_if_fail (rpc, NULL);
  g_return_val_if_fail (size > 0, NULL);

  if (!rpc->unixsock || !rpc->unixchnl)
    return NULL;

  recv_data = g_malloc0 (size);
  data = recv_data;

  while (size > 0)
    {
      GIOStatus status;
      GError *error = NULL;

      status = g_io_channel_read_chars (rpc->unixchnl, data, size,
					&nread, &error);
      if (status == G_IO_STATUS_ERROR)
	{
	  if (error)
	    {
	      g_warning (_("rpc read error: %s"), error->message);
	      g_error_free (error);
	    }
#ifdef SCO_SV
	  if (errno == 90)
	    {
	      g_usleep (10);
	      continue;
	    }
#endif
	  break;
	}
      else if (status == G_IO_STATUS_EOF)
	{
	  t_println ("receive end of file");
	  break;
	}
      else if (status == G_IO_STATUS_AGAIN)
	{
	  gint sockfd, ret;

	  /* setup timeout by select().
	   */
	  if (rpc->timeout > 0)
	    {
	      struct timeval tv;
	      fd_set fds;

	      sockfd =  g_io_channel_unix_get_fd (rpc->unixchnl);
	      g_assert (sockfd > 0);

	      FD_ZERO (&fds);
	      FD_SET (sockfd, &fds);
	      tv.tv_sec = rpc->timeout;
	      tv.tv_usec = 0;

	      ret = select (sockfd + 1, &fds, NULL, NULL, &tv);
	      if (ret == 0)
		{
		  g_warning (_("receive timeout."));
		  break;
		}
	      else if (ret < 0)
		{
		  g_warning (_("select() error: %s"), g_strerror (errno));
		  continue;
		}
	    }
	}
      else
	{
	  size -= nread;
	  data += nread;

	  t_println ("receive %d bytes, remain %d bytes.", nread, size);
	}
    }

  if (size > 0)
    {
      g_free (recv_data);
      return NULL;
    }
  return recv_data;
}

typedef struct _TRpcErrnoString	TRpcErrnoString;
struct _TRpcErrnoString
{
  gint   code;
  gchar *string;
};

/* rpc error code/string compare table.
 */
static TRpcErrnoString	t_rpc_errno_strings[] =
{
    { RPC_ERR_OK,		N_("no error") },
    { RPC_ERR_BADCODE,		N_("bad request code") },
    { RPC_ERR_BADSIZE,		N_("bad request size") },
    { RPC_ERR_FORMAT,		N_("format error") },
    { RPC_ERR_TIMEOUT,		N_("request timeout") },
    { RPC_ERR_EOF,		N_("end of file") },
    { RPC_ERR_SYSTEM,		N_("system error") },

    { -1, NULL }
};

/** 
 * t_rpc_strerror:
 * @code: rpc error code
 *
 * Returns: rpc error description.
 */
const gchar*
t_rpc_strerror (guint code)
{
  gint i;

  for (i = 0; t_rpc_errno_strings[i].code >= 0 &&
       t_rpc_errno_strings[i].string; i++)
    {
      if (code == t_rpc_errno_strings[i].code)
	{
	  return t_rpc_errno_strings[i].string;
	}
    }
  return _("unknown");
}
