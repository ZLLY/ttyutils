/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_SCREEN_H__
#define __T_SCREEN_H__

#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <glib.h>

#include "tevent.h"

G_BEGIN_DECLS

/* Color codes: 0 = black, 1 = red, 2 = green, 3 = yellow, 4 = blue,
 *              5 = magenta, 6 = cyan, 7 = white. 
 *
 * An 'attribute' as used in this library means an 8-bit value that conveys
 * a foreground color code, a background color code, and the bold
 * and blink bits. Each cell in the virtual terminal screen is associated
 * with an attribute that specifies its appearance. The bits of an attribute,
 * from most significant to least significant, are 
 *
 *  bit:      7 6 5 4 3 2 1 0
 *  content:  S F F F H B B B
 *            | `-,-' | `-,-'
 *            |   |   |   |
 *            |   |   |   `----- 3-bit background color code (0 - 7)
 *            |   |   `--------- blink bit (if on, text is blinking)
 *            |   `------------- 3-bit foreground color code (0 - 7)
 *            `----------------- bold bit
 *
 * It is however recommended that you use the provided macros rather
 * than dealing with this format directly.
 *
 * Sometimes we will call the 'SFFF' nibble above the 'extended
 * foreground color code', and the 'HBBB' nibble the 'extended background
 * color code'. So the extended color codes are just the regular
 * color codes except that they have an additional bit (the most significant
 * bit) indicating bold/blink.
 */

/* retrieve attribute fields
 */
#define SCREEN_ATTR_BG(attr)              	((attr) & 0x07)
#define SCREEN_ATTR_FG(attr)              	(((attr) & 0x70) >> 4)

/* retrieve 'extended' color codes (see above for info)
 */
#define SCREEN_ATTR_XBG(attr)             	((attr) & 0x0F)
#define SCREEN_ATTR_XFG(attr)             	(((attr) & 0xF0) >> 4)

/* set attribute fields. This requires attr to be an lvalue, and it will
 * be evaluated more than once. Use with care.
 */
#define SCREEN_ATTR_MOD_BG(attr, newbg)    	attr &= 0xF8, attr |= (newbg)
#define SCREEN_ATTR_MOD_FG(attr, newfg)    	attr &= 0x8F, attr |= ((newfg) << 4)
#define SCREEN_ATTR_MOD_XBG(attr, newxbg)  	attr &= 0xF0, attr |= (newxbg)
#define SCREEN_ATTR_MOD_XFG(attr, newxfg)  	attr &= 0x0F, attr |= ((newxfg) << 4)
#define SCREEN_ATTR_MOD_BOLD(attr, boldbit) 	attr &= 0x7F, attr |= (boldbit) ? 0x80 : 0x00
#define SCREEN_ATTR_MOD_BLINK(attr, blinkbit) 	attr &= 0xF7, attr |= (blinkbit) ?0x08 : 0x00

/* these return non-zero for 'yes', zero for 'no'. Don't rely on them being 
 * any more specific than that (e.g. being exactly 1 for 'yes' or whatever).
 */
#define SCREEN_ATTR_BOLD(attr)            	((attr) & 0x80)
#define SCREEN_ATTR_BLINK(attr)           	((attr) & 0x08)

/* size of escape sequence buffer
 */
#define SCREEN_ESEQ_SIZE			256 

typedef struct _TScreen			TScreen;
typedef struct _TScreenCell		TScreenCell;
typedef struct _TScreenPrivate		TScreenPrivate;

struct _TScreenCell
{
   guchar ch;    /* >= 32, that is, control characters are not
		  * allowed to be on the virtual screen */
   guchar attr;  /* a color attribute, as described previously */
};

struct _TScreenPrivate
{
   gboolean escaped;		/* whether we are currently reading an escape sequence */
   gboolean graphmode;		/* whether terminal is in graphical character mode or not */
   gint     scrolltop;
   gint     scrollbottom;		/* current scrolling region of terminal */
   gint     saved_x;
   gint     saved_y;			/* saved cursor position */
   gchar    esbuf[SCREEN_ESEQ_SIZE];	/* 0-terminated string. Does NOT include the
					 * initial escape (\x1B) character.  */
   gint     esbuf_len;		/* length of buffer. The following property is always kept:
				 * esbuf[esbuf_len] == '\0' */
   GSList  *freeze_list;

   gboolean aux_is_open;
};

struct _TScreen
{
   gint            cols;
   gint            rows;		/* terminal dimensions, READ-ONLY */
   TScreenCell   **cells;		/* matrix of cells */
   gint            crow;
   gint            ccol;		/* cursor coordinates. READ-ONLY. */
   guchar          curattr;		/* current attribute, used for new chars */
   gboolean        curpos_dirty;	/* whether cursor location has changed */
   gint            curpos_last_col;
   gint            curpos_last_row;
   gboolean       *line_dirty;		/* whether each row is dirty */
   TScreenPrivate *priv;		/* private state data */
};

TScreen*	t_screen_create		(guint16          cols,
					 guint16          rows);
void		t_screen_destroy	(TScreen         *screen);

#ifdef T_DEBUG
void		t_screen_dump		(TScreen         *screen,
					 const gchar     *ptsname);
#endif

void 		t_screen_inject		(TScreen         *screen,
					 const guchar    *data,
					 gint             length);
void		t_screen_reset_cursor	(TScreen	 *screen);
gboolean	t_screen_get_changed	(TScreen         *screen);

gboolean	t_screen_match		(TScreen         *screen,
					 TEventCondition *cond);

gchar**		t_screen_snapshot	(TScreen         *screen,
					 guint16         *rows,
					 guint16         *cols);

void		t_screen_update_freezes	(TScreen         *screen);
gboolean	t_screen_is_freeze 	(TScreen         *screen,
					 const gchar     *event_name);
gboolean	t_screen_add_freeze 	(TScreen         *screen,
					 guint16          row,
					 guint16          col,
					 glong            length,
					 const gchar     *event_name);
gboolean	t_screen_aux_is_open 	(TScreen         *screen);

G_END_DECLS

#endif /* __T_SCREEN_H__ */
