/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glib.h>
#include <ttyutils.h>
#include <sqlite3.h>
#include "tevt.h"

void
t_event_login (const char *tty)
{
  sqlite3 *db;
  int rc;
  char *sql, *errmsg = NULL;
  gchar org[16] = { 0, };
  gchar teller[16] = { 0, };

  t_rpc_call_snap_getxy (0, 6, 10, org);
  t_rpc_call_snap_getxy (22, 5, 13, teller);

  rc = sqlite3_open (DBNAME, &db);
  if (rc != SQLITE_OK)
    {
      g_warning ("打开数据库 %s 失败: %s\n", DBNAME, sqlite3_errmsg (db));
      sqlite3_close(db);
      return;
    }
  sqlite3_busy_timeout (db, 1000 * 10);

  sql = sqlite3_mprintf ("update Ttyinfo set teller = %Q, org = %Q where "
		  	 "ttyname = %Q;", teller, org, tty);
  rc = sqlite3_exec (db, sql, NULL, 0, &errmsg);
  sqlite3_free (sql);
  if (rc)
    {
      g_warning ("更新柜员和机构信息失败: %s\n", errmsg);
      sqlite3_free(errmsg);
    }
  sqlite3_close(db);

  t_rpc_call_remove_event ("login");
}
