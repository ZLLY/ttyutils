/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <grp.h>
#include <errno.h>
#include <string.h>
#include <semaphore.h>
#include <glib/gi18n-lib.h>

#include "tdebug.h"
#include "tshmem.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

#ifdef _SYSV_IPC
# define T_SHM_KEY		"/etc/inittab"
# define T_SEM_KEY		"/etc/passwd"
#endif

#define T_SHM_NAME		"/ttyutils_shm"
#define T_SEM_NAME		"/ttyutils_sem"

#if defined(_SYSV_IPC) || defined(_SEM_SEMUN_UNDEFINED)
union semun
{
  int              val;    	/* Value for SETVAL */
  struct semid_ds *buf;    	/* Buffer for IPC_STAT, IPC_SET */
  unsigned short  *array;  	/* Array for GETALL, SETALL */
  struct seminfo  *__buf;  	/* Buffer for IPC_INFO (Linux specific) */
};
#endif

typedef struct _TShmemContent	TShmemContent;

#define T_SHMEM_VERSION		3
#define T_SHMEM_ENTRY_MAX	4998

struct _TShmemContent
{
  guint16     version;
  guint16     nentries;
  uid_t	      uid;
  guint8      type;
  gchar       unused[1024 - 5 - sizeof (uid_t)];
  TShmemEntry entries[T_SHMEM_ENTRY_MAX];
};

#define T_SHMEM_LENGTH		sizeof(TShmemContent)

#ifdef _SYSV_IPC
static gint		t_shmid = -1;
static gint		t_semid = -1;
#else
static sem_t		*t_shmem_sem = NULL;
#endif
static TShmemContent	*t_shmem_content = NULL;

#ifdef _SYSV_IPC
# define t_sem_wait_n(_n) G_STMT_START {				\
  struct sembuf __buf[1];						\
  __buf[0].sem_num = _n;						\
  __buf[0].sem_op = -1;							\
  __buf[0].sem_flg = SEM_UNDO;						\
  while (1)								\
    {									\
      if (semop (t_semid, __buf, 1) == 0)				\
	break;								\
      else if (errno != EINTR)						\
	{								\
	  g_warning ("semop error: %s", g_strerror (errno));		\
	  break;							\
	}								\
      g_usleep (1000);							\
    }									\
} G_STMT_END

# define t_sem_post_n(_n) G_STMT_START {				\
  struct sembuf __buf[1];						\
  __buf[0].sem_num = _n;						\
  __buf[0].sem_op = 1;							\
  __buf[0].sem_flg = SEM_UNDO;						\
  while (1)								\
    {									\
      if (semop (t_semid, __buf, 1) == 0)				\
	break;								\
      else if (errno != EINTR)						\
	{								\
	  g_warning ("semop error: %s", g_strerror (errno));		\
	  break;							\
	}								\
      g_usleep (1000);							\
    }									\
} G_STMT_END

#define t_sem_wait()	t_sem_wait_n(1)
#define t_sem_post()	t_sem_post_n(1)

#else	/* !_SYSV_IPC */

# define t_sem_wait()	sem_wait(t_shmem_sem)
# define t_sem_post()	sem_post(t_shmem_sem)
#endif

/** 
 * t_shmem_open_read_posix:
 * @Returns: 
 */
#ifndef _SYSV_IPC
static gboolean
t_shmem_open_read_posix (void)
{
  gint shmfd;
  struct stat statbuf;

  /* open POSIX share memory object.
   */
  shmfd = shm_open (T_SHM_NAME, O_RDONLY, 0);
  if (shmfd < 0)
    {
      g_warning (_("shm_open error: %s"), g_strerror (errno));
      return FALSE;
    }

  /* get memory size.
   */
  if (fstat (shmfd, &statbuf) == -1)
    {
      g_warning (_("fstat error: %s"), g_strerror (errno));
      close (shmfd);
      return FALSE;
    }

  /* mmap share memory object.
   */
  t_shmem_content = mmap (NULL, statbuf.st_size, PROT_READ,
			  MAP_SHARED, shmfd, 0);
  if (!t_shmem_content)
    {
      g_warning (_("mmap error: %s"), g_strerror (errno));
      close (shmfd);
      return FALSE;
    }
  close (shmfd);

  /* check version.
   */
  if (t_shmem_content->version != T_SHMEM_VERSION)
    {
      g_warning (_("share memory version dismatch."));
      t_shmem_close ();
      return FALSE;
    }

  t_shmem_sem = sem_open (T_SEM_NAME, 0);
  if (!t_shmem_sem)
    {
      g_warning (_("sem_open error: %s"), g_strerror (errno));
      t_shmem_close ();
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_shmem_open_write_posix:
 * @Returns: 
 */
static gboolean
t_shmem_open_write_posix (void)
{
  gint shmfd;
  guint flags;
  mode_t mode;

  /* open or create POSIX share memory object.
   */
  flags = O_RDWR;
  shmfd = shm_open (T_SHM_NAME, flags, 0);
  if (shmfd < 0 && errno == ENOENT)
    {
      t_println ("share memory does not exists, create it.");

      flags |= O_CREAT;

      mode = umask (0);
      shmfd = shm_open (T_SHM_NAME, flags, 0666);
      umask (mode);
    }
  if (shmfd < 0)
    {
      g_warning (_("shm_open error: %s"), g_strerror (errno));
      return FALSE;
    }

  /* set memory size.
   */
  if (ftruncate (shmfd, T_SHMEM_LENGTH) == -1)
    {
      g_warning (_("ftruncate error: %s"), g_strerror (errno));
      close (shmfd);
      shm_unlink (T_SHM_NAME);
      return FALSE;
    }

  /* mmap share memory object.
   */
  t_shmem_content = mmap (NULL, T_SHMEM_LENGTH, PROT_READ | PROT_WRITE,
			  MAP_SHARED, shmfd, 0);
  if (!t_shmem_content)
    {
      g_warning (_("mmap error: %s"), g_strerror (errno));
      close (shmfd);
      shm_unlink (T_SHM_NAME);
      return FALSE;
    }
  close (shmfd);

  /* initial if it new create.
   */
  if (flags & O_CREAT)
    {
      t_println ("initial share memory.");

      memset (t_shmem_content, 0, T_SHMEM_LENGTH);

      t_shmem_content->version = T_SHMEM_VERSION;
      t_shmem_content->nentries = 0;
      t_shmem_content->uid = getuid ();
      t_shmem_content->type = SHMEM_TYPE_POSIX;
    }

  /* create shared unnamed semaphore.
   */
  mode = umask (0);
  t_shmem_sem = sem_open (T_SEM_NAME, O_CREAT, 0666, 1);
  umask (mode);

  if (!t_shmem_sem)
    {
      g_warning (_("sem_open error: %s"), g_strerror (errno));
      t_shmem_close ();
      shm_unlink (T_SHM_NAME);
      return FALSE;
    }

  return TRUE;
}

#else /* ! _SYSV_IPC */

/** 
 * t_shmem_open_read_sysv:
 * 
 * @Returns: 
 */
static gboolean
t_shmem_open_read_sysv (void)
{
  key_t key;
  struct shmid_ds ds;

#ifdef T_DEBUG
  g_return_val_if_fail (g_file_test (T_SHM_KEY, G_FILE_TEST_EXISTS), FALSE);
  g_return_val_if_fail (g_file_test (T_SEM_KEY, G_FILE_TEST_EXISTS), FALSE);
#endif

  key = ftok (T_SEM_KEY, T_SHMEM_VERSION);
  if (key == -1)
    {
      g_warning (_("ftok from '%s' error: %s"), T_SEM_KEY,
		 g_strerror (errno));
      return FALSE;
    }

  /* open sysv semaphore.
   */
  t_semid = semget (key, 0, 0);
  if (t_semid == -1)
    {
      g_warning (_("semget error: %s"), g_strerror (errno));
      t_shmem_close ();
      return FALSE;
    }

  key = ftok (T_SHM_KEY, T_SHMEM_VERSION);
  if (key == -1)
    {
      g_warning (_("ftok from '%s' error: %s"), T_SHM_KEY,
		 g_strerror (errno));
      return FALSE;
    }

  /* get the shared memory object from key.
   */
  t_shmid = shmget (key, 0, 0);
  if (t_shmid == -1)
    {
      g_warning (_("shmget error: %s"), g_strerror (errno));
      return FALSE;
    }

  /* check size.
   */
  if (shmctl (t_shmid, IPC_STAT, &ds) == -1)
    {
      g_warning (_("shmctl error: %s"), g_strerror (errno));
      return FALSE;
    }
  if (ds.shm_segsz != T_SHMEM_LENGTH)
    {
      g_warning (_("shmem size %d dismatch."), ds.shm_segsz);
      return FALSE;
    }

  /* attach to the shared memory object.
   */
  t_shmem_content = shmat (t_shmid, NULL, SHM_RDONLY);
  if (t_shmem_content == (void*) -1)
    {
      g_warning (_("shmat error: %s"), g_strerror (errno));
      return FALSE;
    }

  /* check version.
   */
  if (t_shmem_content->version != T_SHMEM_VERSION)
    {
      g_warning (_("share memory version dismatch."));
      t_shmem_close ();
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_shmem_open_write_sysv:
 * 
 * @Returns: 
 */
static gboolean
t_shmem_open_write_sysv (void)
{
  key_t key;
  gint flags = 0;
  union semun arg;

#ifdef T_DEBUG
  g_return_val_if_fail (g_file_test (T_SHM_KEY, G_FILE_TEST_EXISTS), FALSE);
  g_return_val_if_fail (g_file_test (T_SEM_KEY, G_FILE_TEST_EXISTS), FALSE);
#endif

  key = ftok (T_SEM_KEY, T_SHMEM_VERSION);
  if (key == -1)
    {
      g_warning (_("ftok from '%s' error: %s"), T_SEM_KEY,
		 g_strerror (errno));
      return FALSE;
    }

  /* create sysv semaphore.
   */
  t_semid = semget (key, 2, IPC_CREAT | IPC_EXCL | 0666);
  if (t_semid != -1)
    {
      unsigned short *values = g_new (unsigned short, 2);

      t_println ("initial new created semaphores.");

      values[0] = 1;
      values[1] = 1;
      arg.array = values;

      if (semctl (t_semid, 0, SETALL, arg) == -1)
	{
	  g_warning (_("semctl error: %s"), g_strerror (errno));
	  g_free (values);
	  semctl (t_semid, 0, IPC_RMID);
	  t_semid = -1;
	  return FALSE;
	}
      g_free (values);
    }
  else if (errno == EEXIST)
    {
      t_println ("semget failed beacuse semaphore exists, try to open it");

      t_semid = semget (key, 0, 0);
    }
  if (t_semid == -1)
    {
      g_warning (_("semget error: %s"), g_strerror (errno));
      return FALSE;
    }

  t_sem_wait_n (0);

  key = ftok (T_SHM_KEY, T_SHMEM_VERSION);
  if (key == -1)
    {
      g_warning (_("ftok from '%s' error: %s"), T_SHM_KEY,
		 g_strerror (errno));
      semctl (t_semid, 0, IPC_RMID);
      t_semid = -1;
      return FALSE;
    }

  /* try to create the shared memory object.
   */
  flags = IPC_CREAT | IPC_EXCL | 0666;
  t_shmid = shmget (key, T_SHMEM_LENGTH, flags);

  /* failed beacuse already exists, open it.
   */
  if (t_shmid == -1)
    {
      if (errno == EEXIST)
	{
	  t_println ("shmget failed beacuse shmem exists, try to open it.");

	  flags = 0;
	  t_shmid = shmget (key, T_SHMEM_LENGTH, flags);
	}
    }
  if (t_shmid == -1)
    {
      g_warning (_("shmget error: %s"), g_strerror (errno));
      semctl (t_semid, 0, IPC_RMID);
      t_semid = -1;
      return FALSE;
    }

  /* attach to the shared memory object.
   */
  t_shmem_content = shmat (t_shmid, NULL, 0);
  if (t_shmem_content == (void*) -1)
    {
      g_warning (_("shmat error: %s"), g_strerror (errno));
      semctl (t_semid, 0, IPC_RMID);
      t_semid = -1;
      return FALSE;
    }

  /* initial if it new create.
   */
  if (flags & IPC_CREAT)
    {
      t_println ("initial new created share memory.");

      memset (t_shmem_content, 0, T_SHMEM_LENGTH);

      t_shmem_content->version = T_SHMEM_VERSION;
      t_shmem_content->nentries = 0;
      t_shmem_content->uid = getuid ();
      t_shmem_content->type = SHMEM_TYPE_SYSV;
    }
  else
    {
      /* check version.
       */
      if (t_shmem_content->version != T_SHMEM_VERSION)
	{
	  g_warning (_("share memory version dismatch."));
	  t_shmem_close ();
	  return FALSE;
	}
    }

  t_sem_post_n (0);

  return TRUE;
}

#endif /* !_SYSV_IPC */

/** 
 * t_shmem_open:
 * @writable: open share memory.
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_shmem_open (gboolean writable)
{
  gboolean ret;

  /* already mmaped.
   */
  if (t_shmem_content)
    return TRUE;

#ifdef _SYSV_IPC
  ret = (writable) ? t_shmem_open_write_sysv () :
                     t_shmem_open_read_sysv ();
#else
  ret = (writable) ? t_shmem_open_write_posix () :
                     t_shmem_open_read_posix ();
#endif

#ifdef T_DEBUG
  if (ret)
    {
      t_println ("attach to shmem version %d, nentries %d, uid (%d).",
		 t_shmem_content->version, t_shmem_content->nentries,
		 t_shmem_content->uid);
    }
  else
    {
      t_println ("attach to shmem failed");
    }
#endif

  return ret;
}

/** 
 * t_shmem_close:
 */
void
t_shmem_close (void)
{
#ifndef _SYSV_IPC
  if (t_shmem_sem)
    {
      sem_close (t_shmem_sem);
      t_shmem_sem = NULL;
    }
  if (t_shmem_content)
    {
      munmap (t_shmem_content, T_SHMEM_LENGTH);
      t_shmem_content = NULL;
    }
#else
  if (t_shmem_content)
    {
      shmdt (t_shmem_content);
      t_shmem_content = NULL;
    }
#endif
}

/** 
 * t_shmem_remove:
 * 
 * @Returns: 
 */
gboolean
t_shmem_remove (void)
{
  gboolean ret = TRUE;

  g_return_val_if_fail (t_shmem_content, FALSE);

  t_shmem_close ();

#ifndef _SYSV_IPC
  t_println ("remove semid %s", T_SEM_NAME);
  t_println ("remove shmid %s", T_SHM_NAME);

  if (sem_unlink (T_SEM_NAME) != 0)
    {
      g_warning (_("unlink semaphore error: %s"), g_strerror (errno));
      ret = FALSE;
    }
  if (shm_unlink (T_SHM_NAME) != 0)
    {
      g_warning (_("unlink share memory error: %s"), g_strerror (errno));
      ret = FALSE;
    }

#else
  t_println ("remove semid %d", t_semid);
  t_println ("remove shmid %d", t_shmid);

  if (semctl (t_semid, 0, IPC_RMID, NULL) != 0)
    {
      g_warning (_("remove semaphore error: %s"), g_strerror (errno));
      ret = FALSE;
    }
  if (shmctl (t_shmid, IPC_RMID, NULL) != 0)
    {
      g_warning (_("remove share memory error: %s"), g_strerror (errno));
      ret = FALSE;
    }
#endif

  return ret;
}

/** 
 * g_shmem_get_shmid:
 * 
 * @Returns: 
 */
gint
t_shmem_get_shmid (void)
{
#ifdef _SYSV_IPC
  return t_shmid;
#else
  return -1;
#endif
}

/** 
 * g_shmem_get_semid:
 * 
 * @Returns: 
 */
gint
t_shmem_get_semid (void)
{
#ifdef _SYSV_IPC
  return t_semid;
#else
  return -1;
#endif
}

/** 
 * t_shmem_get_size:
 * 
 * @Returns: 
 */
gsize
t_shmem_get_size (void)
{
  return T_SHMEM_LENGTH;
}

/** 
 * t_shmem_get_version:
 * 
 * @Returns: version
 */
gint
t_shmem_get_version (void)
{
  g_return_val_if_fail (t_shmem_content, 0);
  return t_shmem_content->version;
}

/** 
 * t_shmem_get_nentries:
 * 
 * @Returns: 
 */
gint
t_shmem_get_nentries (void)
{
  g_return_val_if_fail (t_shmem_content, 0);
  return t_shmem_content->nentries;
}

/** 
 * t_shmem_get_uid:
 * 
 * @Returns: share memory user id.
 */
uid_t
t_shmem_get_uid (void)
{
  g_return_val_if_fail (t_shmem_content, 0);
  return t_shmem_content->uid;
}

/** 
 * t_shmem_get_type:
 * 
 * @Returns: 
 */
TShmemType
t_shmem_get_type (void)
{
  g_return_val_if_fail (t_shmem_content, 0);
  return t_shmem_content->type;
}

/** 
 * t_shmem_cleanup:
 */
void
t_shmem_cleanup (void)
{
  gint i;

  g_return_if_fail (t_shmem_content);

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid != 0)
	{
	  if (kill (t_shmem_content->entries[i].pid, SIGUSR1) != 0 &&
	      errno == ESRCH)
	    {
	      memset (&t_shmem_content->entries[i], 0, sizeof (TShmemEntry));
	      t_shmem_content->nentries--;
	    }
	}
    }

  t_sem_post ();
}

/** 
 * t_shmem_append_entry:
 * @tty 
 * @pid 
 * @dbus 
 */
gboolean
t_shmem_append_entry (TShmemEntry *entry)
{
  gint i;

  g_return_val_if_fail (entry, FALSE);
  g_return_val_if_fail (t_shmem_content, FALSE);

  t_sem_wait ();

  /* prevent register terminal which is pesudo terminal of
   * other instance.
   */
  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid != 0 &&
	  t_shmem_content->entries[i].pts != 0 &&
	  strcmp (t_shmem_content->entries[i].pts, entry->tty) == 0)
	{
	  if (kill (t_shmem_content->entries[i].pid, SIGUSR1) != 0 &&
	      errno == ESRCH)
	    {
	      memset (&t_shmem_content->entries[i], 0, sizeof (TShmemEntry));
	      t_shmem_content->nentries--;
	      break;
	    }
	  else
	    {
	      g_warning (_("terminal %s is pseudo terminal of pid %d."),
			 entry->tty, t_shmem_content->entries[i].pid);
	      t_sem_post ();
	      return FALSE;
	    }
	}
    }

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      gboolean overwrite = FALSE;

      /* overwrite if entry has same ttyname or ptsname.
       */
      if ((strcmp (entry->tty, t_shmem_content->entries[i].tty) == 0) ||
	  (strcmp (entry->pts, t_shmem_content->entries[i].pts) == 0))
	{
	  overwrite = TRUE;
	}

      if (overwrite || t_shmem_content->entries[i].pid == 0)
	{
	  t_println ("register pid(%d), tty(%s), pts(%s), lookpath(%sr) rpcpath(%s) to %d.",
		     getpid (), entry->tty, entry->pts, entry->lookpath, entry->rpcpath, i);

	  memcpy (&t_shmem_content->entries[i], entry, sizeof (TShmemEntry));
	  t_shmem_content->entries[i].pid = getpid ();
	  t_shmem_content->entries[i].uid = getuid ();
	  t_shmem_content->entries[i].ctime = time (NULL);

	  if (!overwrite)
	    {
	      t_shmem_content->nentries++;
	    }
	  break;
	}
    }

  t_sem_post ();

  return TRUE;
}

/** 
 * t_shmem_remove_entry:
 */
void
t_shmem_remove_entry (void)
{
  gint i;

  g_return_if_fail (t_shmem_content);

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid == getpid ())
	{
	  memset (&t_shmem_content->entries[i], 0, sizeof (TShmemEntry));
	  t_shmem_content->nentries--;
	  break;
	}
    }

  t_sem_post ();
}

/** 
 * t_shmem_lookup_entry:
 * @elem 
 * @data 
 * 
 * @Returns: 
 */
TShmemEntry*
t_shmem_lookup_entry (TShmemLookupFunc func,
		      gpointer         data)
{
  TShmemEntry *entry = NULL;
  gint i;

  g_return_val_if_fail (func, NULL);
  g_return_val_if_fail (t_shmem_content, NULL);

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      /* skip empty entry.
       */
      if (t_shmem_content->entries[i].pid == 0)
	continue;

      if (func (&t_shmem_content->entries[i], data))
	{
	  t_println ("found match entry, tty(%s), pts(%s), lookpath(%s) rpcpath(%s)",
		     t_shmem_content->entries[i].tty,
		     t_shmem_content->entries[i].pts,
		     t_shmem_content->entries[i].lookpath,
		     t_shmem_content->entries[i].rpcpath);

	  entry = g_memdup (&t_shmem_content->entries[i], sizeof (TShmemEntry));
	  break;
	}
    }

  t_sem_post ();

  return entry;
}

/** 
 * t_shmem_set_event_enabled:
 * @enable 
 */
void
t_shmem_set_event_enabled (pid_t pid, gboolean enable)
{
  gint i;

  g_return_if_fail (pid > 0);
  g_return_if_fail (t_shmem_content);

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid == pid)
	{
	  if (enable)
	    {
	      t_shmem_content->entries[i].flags &= ~SHMEM_FLAG_NOEVENT;
	    }
	  else
	    {
	      t_shmem_content->entries[i].flags |= SHMEM_FLAG_NOEVENT;
	    }
	  break;
	}
    }

  t_sem_post ();

#ifdef T_DEBUG
  if (i == T_SHMEM_ENTRY_MAX)
    {
      t_println ("no any entry has pid %d", pid);
    }
#endif
}

/** 
 * t_shmem_get_event_enabled:
 * @pid 
 * 
 * @Returns: 
 */
gboolean
t_shmem_get_event_enabled (pid_t pid)
{
  gint i;

  g_return_val_if_fail (pid > 0, FALSE);
  g_return_val_if_fail (t_shmem_content, FALSE);

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid == pid)
	{
	  t_sem_post ();
	  return !(t_shmem_content->entries[i].flags & SHMEM_FLAG_NOEVENT);
	}
    }

  t_sem_post ();

#ifdef T_DEBUG
  if (i == T_SHMEM_ENTRY_MAX)
    {
      t_println ("no any entry has pid %d", pid);
    }
#endif

  return FALSE;
}

/** 
 * t_shmem_look_number_increase:
 */
void
t_shmem_looknum_increase (void)
{
  pid_t pid;
  gint i;

  g_return_if_fail (t_shmem_content);

  pid = getpid ();

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid == pid)
	{
	  t_shmem_content->entries[i].looknum++;
	  break;
	}
    }

  t_sem_post ();
}

/** 
 * t_shmem_look_number_decrease:
 */
void
t_shmem_looknum_decrease (void)
{
  pid_t pid;
  gint i;

  g_return_if_fail (t_shmem_content);

  pid = getpid ();

  t_sem_wait ();

  for (i = 0; i < T_SHMEM_ENTRY_MAX; i++)
    {
      if (t_shmem_content->entries[i].pid == pid)
	{
	  t_shmem_content->entries[i].looknum--;
	  break;
	}
    }

  t_sem_post ();
}
