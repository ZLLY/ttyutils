/* Ttyutils Library
 * Copyright (C) 2008,2009,2010,2011 Wu Xiaohu.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef _T_TERMOP_H_
#define _T_TERMOP_H_

#include <glib.h>

G_BEGIN_DECLS

struct termios;

#ifndef T_CTRL
# define T_CTRL(c)	((c) & 0x1F)
#endif

#define T_KEY_ENTER	10
#ifndef T_KEY_ESC
# define T_KEY_ESC	'\033'
#endif
#define T_KEY_ESC_S	"\033"

#define T_DEVICE_IO_BUFFER_SIZE		2048

typedef enum
{
  CHAR_ATTR_NORMAL        = 0,
  CHAR_ATTR_BOLD          = 1,
  CHAR_ATTR_UNDERLINED    = 4,
  CHAR_ATTR_BLINK         = 5,
  CHAR_ATTR_INVERSE       = 7,
  CHAR_ATTR_INVISIBLE     = 8,
  CHAR_ATTR_NORMAL2       = 22,
  CHAR_ATTR_NOTUNDERLINED = 24,
  CHAR_ATTR_STEADY        = 25,		/* not blinking */
  CHAR_ATTR_POSITIVE      = 27,		/* not inverse */
  CHAR_ATTR_VISIBLE       = 28,		/* i.e., not hidden (VT300) */
  CHAR_ATTR_FORE_BLACK    = 30,		/* Set foreground color to Black */
  CHAR_ATTR_FORE_RED      = 31,		/* Set foreground color to Red */
  CHAR_ATTR_FORE_GREEN    = 32,		/* Set foreground color to Green */
  CHAR_ATTR_FORE_YELLOW   = 33,		/* Set foreground color to Yellow */
  CHAR_ATTR_FORE_BLUE     = 34,		/* Set foreground color to Blue */
  CHAR_ATTR_FORE_MAGENTA  = 35,		/* Set foreground color to Magenta */
  CHAR_ATTR_FORE_CYAN     = 36,		/* Set foreground color to Cyan */
  CHAR_ATTR_FORE_WHITE    = 37,		/* Set foreground color to White */
  CHAR_ATTR_FORE_DEFAULT  = 39,		/* Set foreground color to Default (original) */
  CHAR_ATTR_BACK_BLACK    = 40,		/* Set background color to Black */
  CHAR_ATTR_BACK_RED      = 41,		/* Set background color to Red */
  CHAR_ATTR_BACK_GREEN    = 42,		/* Set background color to Green */
  CHAR_ATTR_BACK_YELLOW   = 43,		/* Set background color to Yellow */
  CHAR_ATTR_BACK_BLUE     = 44,		/* Set background color to Blue */
  CHAR_ATTR_BACK_MAGENTA  = 45,		/* Set background color to Magenta */
  CHAR_ATTR_BACK_CYAN     = 46,		/* Set background color to Cyan */
  CHAR_ATTR_BACK_WHITE    = 47,		/* Set background color to White */
  CHAR_ATTR_BACK_DEFAULT  = 49,		/* Set background color to Default (original) */
} TTermCharAttr;

typedef enum
{
  TButtonBad	= 0 << 0,
  TButtonYes    = 1 << 0,
  TButtonNo     = 1 << 1,
  TButtonClose  = 1 << 2,
  TButtonOk     = 1 << 3,
  TButtonCancel = 1 << 4,
  TButtonSubmit = 1 << 5,
} TTermButton;

gint		t_tcgetattr			(gint            fd,
						 struct termios *ttmode);
gint		t_tcsetattr			(gint            fd,
						 struct termios *ttmode);
gint		t_bytes_available		(gint            fd);

void		t_term_set_echo			(gboolean        enable);
void		t_term_set_ixoff		(gboolean        enable);
void		t_term_set_ixon			(gboolean        enable);
void		t_term_set_canon		(gboolean        enable);
void		t_term_set_isig			(gboolean        enable);
void		t_term_set_noflsh		(gboolean        enable);
void		t_term_set_icrnl		(gboolean        enable);
void		t_term_set_inlcr		(gboolean        enable);
void		t_term_set_raw			(void);
gboolean	t_term_get_winsize		(guint16        *lines,
						 guint16        *columns);
gboolean	t_term_set_winsize		(guint16         lines,
						 guint16         columns);
gint		t_term_line_speed		(gint            fd);
gboolean	t_term_use_alter_screen		(void);
gboolean	t_term_use_normal_screen	(void);
void		t_term_alter_screen_print	(gint            timeout,
						 const gchar    *format,
						 ...);
void		t_term_reset			(void);
void		t_term_clear_screen		(void);
void		t_term_clear_line		(gint            lineno);
void		t_term_clear_lines		(gint            start_line,
						 gint            end_line);
void		t_term_clear_area		(gint            start_line,
						 gint            start_column,
						 gint            end_line,
						 gint            end_column);
void		t_term_set_cursor_xy		(gint            line,
						 gint            column);
void		t_term_cursor_xy_print		(gint            line,
						 gint            column,
						 const gchar    *format,
						 ...);
void		t_term_printer_print		(const gchar    *format,
						 ...);
void		t_term_printer_print_file	(const gchar    *fname);
void		t_term_printer_print_screen	(void);
const gchar*	t_term_get_char_attr_str	(TTermCharAttr   attr,
						 gchar          *buff,
						 gint            size);
void		t_term_set_char_attr		(TTermCharAttr   attr);
void		t_term_reset_char_attr		(void);
gint		t_term_char_attr_print		(TTermCharAttr   attr,
						 gchar          *format,
						 ...);
gint		t_term_char_attr_println	(TTermCharAttr   attr,
						 gchar          *format,
						 ...);
gint		t_term_char_attr_sprintf	(gchar          *buff,
						 gint            size,
						 TTermCharAttr   attr,
						 const gchar    *format,
						 ...);
TTermButton	t_term_popup_dialog		(gint            start_line,
						 gint            start_column,
						 gint            end_line,
						 gint            end_column,
						 const gchar    *title,
						 const gchar    *text[],
						 gint            n_texts,
						 TTermButton     buttons);
TTermButton	t_term_popup_dialog_center	(const gchar    *title,
						 const gchar    *text[],
						 gint            n_texts,
						 TTermButton     buttons);

G_END_DECLS

#endif /* _T_TERMOP_H_ */
