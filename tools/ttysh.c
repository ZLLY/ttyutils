/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#ifdef HAVE_READLINE_READLINE_H
#include <readline/readline.h>
#endif
#ifdef HAVE_READLINE_HISTORY_H
#include <readline/history.h>
#endif
#include <stdlib.h>
#include <pwd.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>

#include <ttyutils.h>

#ifdef DMALLOC
#include <dmalloc.h>
#endif

#ifdef  G_LOG_DOMAIN
# undef G_LOG_DOMAIN
#endif
#define G_LOG_DOMAIN	"Ttysh"


static gboolean     opt_version = FALSE;
static gchar       *opt_file = NULL;
static gchar       *opt_tty = NULL;

static GOptionEntry option_entries[] = 
{
    { "version", 'v', 0, G_OPTION_ARG_NONE, &opt_version,
      N_("Output version information and exit"), NULL },

    { "file", 'f', 0, G_OPTION_ARG_STRING, &opt_file,
      N_("Read command from file"), "file" },

    { "tty", 't', 0, G_OPTION_ARG_STRING, &opt_tty,
      N_("Connect to tty"), "ttyname" },

    { NULL, }
};

typedef gboolean	(*ck_t) (gint argc, gchar *argv[]);

typedef struct _sf
{
  gchar *string;
  ck_t   cb;
} sf_t;

static gboolean	t_press 		(gint argc, gchar *argv[]);
static gboolean t_dofile		(const gchar *file);
static gboolean	t_readfile		(gint argc, gchar *argv[]);
static gboolean	t_list_target		(gint argc, gchar *argv[]);
static gboolean	t_set_target_pid	(gint argc, gchar *argv[]);
static gboolean	t_set_target_pts	(gint argc, gchar *argv[]);
static gboolean	t_set_target_tty	(gint argc, gchar *argv[]);
static gboolean	t_snap			(gint argc, gchar *argv[]);
static gboolean	t_feed			(gint argc, gchar *argv[]);
static gboolean	t_get_events		(gint argc, gchar *argv[]);
static gboolean	t_insert_event		(gint argc, gchar *argv[]);
static gboolean	t_remove_event		(gint argc, gchar *argv[]);
static gboolean	t_remove_event_group	(gint argc, gchar *argv[]);
static gboolean	t_snapxy		(gint argc, gchar *argv[]);
static gboolean t_snaprow		(gint argc, gchar *argv[]);
static gboolean	t_stdin_suppend 	(gint argc, gchar *argv[]);
static gboolean	t_stdin_resume		(gint argc, gchar *argv[]);
static gboolean	t_stdout_suppend	(gint argc, gchar *argv[]);
static gboolean	t_stdout_resume 	(gint argc, gchar *argv[]);
static gboolean	t_trigger_event 	(gint argc, gchar *argv[]);
static gboolean	t_get_cursor_pos 	(gint argc, gchar *argv[]);
static gboolean	t_refresh	 	(gint argc, gchar *argv[]);

static sf_t	sfs[] =
{
    { "press",		t_press },
    { "read",		t_readfile },

    { "ls",		t_list_target },
    { "pid",		t_set_target_pid },
    { "pts",		t_set_target_pts },
    { "tty",		t_set_target_tty },

    { "snap",		t_snap },
    { "snapxy",		t_snapxy },
    { "snaprow",	t_snaprow },
    { "feed",		t_feed },
    { "ge",	        t_get_events },
    { "ie",	    	t_insert_event },
    { "reg",		t_remove_event_group },
    { "re",	        t_remove_event },
    { "te",	        t_trigger_event },
    { "ins",		t_stdin_suppend },
    { "inr",		t_stdin_resume },
    { "outs",		t_stdout_suppend },
    { "outr",		t_stdout_resume },
    { "curpos",         t_get_cursor_pos },
    { "rf",		t_refresh },

    { NULL,	NULL }
};

typedef struct help_struct	help_struct_t;
struct help_struct
{
  gchar *cmd;
  gchar *args;
  gchar *desc;
};


static help_struct_t	helps[] =
{
    { "q/quit/exit",	"",			N_("quit") },
    { "ls",		"",			N_("list target") },
    { "read",		"filename",		N_("read commond from a file") },
    { "pid",		"pid [timeout]",	N_("set target pid") },
    { "pts",		"pts [timeout]",	N_("set target pts") },
    { "tty",		"tty [timeout]",	N_("set target tty") },
    { "snap",		"",			N_("take screen snapshot") },
    { "snapxy",		"x y len",		N_("take screen x y snapshot") },
    { "snaprow",	"row",			N_("take screen row snapshot") },
#if 0
    { "feed",		"string length",	N_("feed data") },
#else
    { "feed",		"string",		N_("feed data") },
#endif
    { "ie",		"name group matcher program [args]  ",	N_("insert event") },
    { "re",		"event_name",        	N_("remove event") },
    { "te",		"event_name",        	N_("trigger event") },
    { "reg",		"group_name",        	N_("remove event group") },
    { "ge",		"",			N_("get events") },
    { "ins",		"",			N_("suppend stdin") },
    { "inr",		"",			N_("resume stdin") },
    { "outs",		"",			N_("suppend stdout") },
    { "outr",		"",			N_("resume stdout") },
    { "press",		"",			N_("view key value sequences") },
    { "curpos",		"",			N_("get cursor current position") },
    { "rf",		"",			N_("refresh screen") },

    { NULL, NULL, NULL }
};

/** 
 * help:
 */
static void help (void)
{
  gint i;

  printf ("%-12s%-30s%s\n",  _("command"), _("args"), _("description"));
  printf ("--------------------------------------------------------------------\n");

  for (i = 0; helps[i].cmd; i++)
    {
      printf ("%-12s%-30s%s\n", helps[i].cmd, helps[i].args,
	      gettext (helps[i].desc));
    }
  printf ("--------------------------------------------------------------------\n");
}

/** 
 * t_do_select:
 * @select 
 */
static void
t_do_select (const gchar *select)
{
  gchar **strv;
  gint i;

  strv = g_strsplit_set (select, " \t", -1);

  for (i = 0; sfs[i].string; i++)
    { 
      if (strcmp (strv[0], sfs[i].string) == 0)
	{
	  if (!sfs[i].cb(g_strv_length (strv), strv))
	    {
	      printf ("!!! error !!!\n");
	    }
	  break;
	}
    }
  g_strfreev (strv);

  if (!sfs[i].string)
    {
      printf (_("command unknown, '?' for help!\n"));
    }
}

/** 
 * main:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gint
main (gint argc, gchar **argv)
{
  GOptionContext *opt_ctx;
  GError *error = NULL;
  gchar *select;

  t_set_locale ();

  g_set_prgname (argv[0]);
  g_set_application_name ("ttysh");

  /* parse command line options
   */
  opt_ctx = g_option_context_new (_("[script file]"));
#if GLIB_MINOR_VERSION >= 12
  g_option_context_set_summary (opt_ctx, _("Ttytuils interactive shell"));
#endif
  g_option_context_add_main_entries (opt_ctx, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (opt_ctx, &argc, &argv, &error))
    {
      if (error)
	{
	  g_warning (_("parse command line error: %s"), error->message);
	  g_error_free (error);
	}
      else
	{
	  g_warning (_("An unknown error occurred when parse command line."));
	}
      return 1;
    }
  g_option_context_free (opt_ctx);
  opt_ctx = NULL;

  /* print version info and exit.
   */
  if (opt_version)
    {
      t_print_version ();
      return 0;
    }

  t_init ();

  if (opt_tty)
    {
      TRpcTarget target;

      target.rt_type = RPC_TARGET_TTY;
      target.rt_tty = opt_tty;
      target.rt_timeout = 3;

      if (!t_rpc_call_set_target (&target))
	{
	  printf ("set rpc target failed.\n");
	  return 1;
	}
      g_free (opt_tty);
    }

  if (opt_file)
    {
      t_dofile (opt_file);
      g_free (opt_file);
      return 0;
    }

  printf ("Ttysh for %s\n\n", PACKAGE_STRING);
  printf (_("Copyright (C) 2008,2009,2010,2011 Wu Xiaohu.\n"));
  printf (_("for help, press '?', 'q' for quit.\n"));
  printf ("\n");

  while (1)
    {
#ifdef HAVE_READLINE
      select = readline ("ttysh> ");
#else
      gchar buff[256];
      printf ("ttysh> ");
      select = fgets (buff, sizeof (buff), stdin);
      buff[strlen (buff) - 1] = '\0';
#endif

      if (!select || strlen (select) == 0)
	continue;

#ifdef HAVE_READLINE
      add_history (select);
#endif

      if (strcmp (select, "q") == 0 || strcmp (select, "quit") == 0 ||
	  strcmp (select, "exit") == 0)
	{
	  break;
	}
      if (strcmp (select, "?") == 0 || strcmp (select, "help") == 0)
	{
	  help ();
	  continue;
	}
      t_do_select (select);
    }
  t_uninit ();

  return 0;
}

/** 
 * t_press:
 * @argc:
 * @argv:
 * 
 * @Returns: 
 */
gboolean
t_press (gint argc, gchar *argv[])
{
  gint k;
  //gchar *p, *p2;
  gchar buff[256];

  while (1)
    {
      printf ("press key>> ");
      fgets (buff, sizeof (buff), stdin);
      buff[strlen (buff) - 1] = '\0';

      if (buff[0] == 'q')
	break;

      for (k = 0; k < strlen (buff); k++)
	{
	  printf ("0x%02x ", buff[k]);
	}
      printf (", len %d\n", strlen (buff));

#if 0
      p = buff;
      while (1)
	{
	  k = t_getkey (p, strlen (p), &p2);
	  if (k == -1)
	    break;
	  printf ("keyval: %d\n", k);
	  printf ("remain: %s\n", p2);
	  p = p2;
	}
#endif
    }

  return TRUE;
}

/** 
 * t_dofile:
 * @file:
 */
static gboolean
t_dofile (const gchar *file)
{
  FILE *fp;
  gchar buff[256];

  fp = fopen (file, "r");
  if (!fp)
    {
      printf ("open %s error: %s\n", file, g_strerror (errno));
      return FALSE;
    }

  while (!feof (fp))
    {
      if (!fgets (buff, sizeof (buff), fp))
	break;

      buff[strlen (buff) - 1] = '\0';
      g_strstrip (buff);

      if (strlen (buff) == 0 || buff[0] == '#')
	continue;

      t_do_select (buff);
    }
  fclose (fp);

  return TRUE;
}

/** 
 * t_readfile:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_readfile (gint argc, gchar *argv[])
{
  if (argc < 2)
    {
      printf ("usage: read filename\n");
      return FALSE;
    }
  return t_dofile (argv[1]);
}

/** 
 * t_list_entry:
 * @entry: a instance entry
 * @data: user data
 * 
 * @Returns: FALSE to continue, TRUE stop lookup.
 */
static gboolean
t_list_entry (TShmemEntry *entry, gpointer data)
{
  struct passwd *pswd;

  g_return_val_if_fail (entry, FALSE);

  pswd = getpwuid (entry->uid);

  g_print ("%10s %8d %12s %12s %s\n",
	   pswd ? pswd->pw_name : _("Unknown"),
	   entry->pid, entry->tty, entry->pts, entry->command);

  return FALSE;
}

/** 
 * t_list_target:
 * @argc:
 * @argv:
 * 
 * @Returns: 
 */
gboolean
t_list_target (gint argc, gchar *argv[])
{
  if (!t_shmem_open (FALSE))
    {
      g_warning (_("connect to share memory failed."));
      return FALSE;
    }
  g_print (_("protocal %d, total %d entry.\n"),
	   t_shmem_get_version (), t_shmem_get_nentries ());

  g_print ("%10s %8s %12s %12s %s\n",
	   "USER", "PID", "TTY", "PTS", "COMMAND");

  t_shmem_lookup_entry (t_list_entry, NULL);
  t_shmem_close ();

  return TRUE;
}

/** 
 * t_set_target_pid:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_set_target_pid (gint argc, gchar *argv[])
{
  TRpcTarget target;

  if (argc < 2)
    {
      printf ("usage: set_pid pid [timeout]\n");
      return FALSE;
    }

  target.rt_type = RPC_TARGET_PID;
  target.rt_pid = g_strtod (argv[1], NULL);
  if (argc > 2)
    target.rt_timeout = g_strtod (argv[2], NULL);
  else
    target.rt_timeout = 3;

  if (!t_rpc_call_set_target (&target))
    {
      printf ("set rpc target failed.\n");
      return FALSE;
    }

  return TRUE;
}

/** 
 * t_set_target_pts:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_set_target_pts (gint argc, gchar *argv[])
{
  TRpcTarget target;

  if (argc < 2)
    {
      printf ("usage: set_pts pts [timeout]\n");
      return FALSE;
    }

  target.rt_type = RPC_TARGET_PTS;
  target.rt_pts = argv[1];
  if (argc > 2)
    target.rt_timeout = g_strtod (argv[2], NULL);
  else
    target.rt_timeout = 3;

  if (!t_rpc_call_set_target (&target))
    {
      printf ("set rpc target failed.\n");
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_set_target_tty:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_set_target_tty (gint argc, gchar *argv[])
{
  TRpcTarget target;

  if (argc < 2)
    {
      printf ("usage: set_tty tty [timeout]\n");
      return FALSE;
    }

  target.rt_type = RPC_TARGET_TTY;
  target.rt_tty = argv[1];
  if (argc > 2)
    target.rt_timeout = g_strtod (argv[2], NULL);
  else
    target.rt_timeout = 3;

  if (!t_rpc_call_set_target (&target))
    {
      printf ("set rpc target failed.\n");
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_snap:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_snap (gint argc, gchar *argv[])
{
  guint16 col, row, i;
  FILE  *fp = NULL;
  gchar **snapshot = NULL;

  if (argc > 1)
    {
      fp = fopen (argv[1], "w");
      if (!fp)
	{
	  printf ("open %s error: %s\n", argv[1], g_strerror (errno));
	  return FALSE;
	}
    }
  else
    {
      fp = stdout;
    }

  if (!t_rpc_call_snap (&row, &col, &snapshot))
    {
      return FALSE;
    }

  for (i = 0; i < row; i++)
    {
      fprintf (fp, "%02d %s\n", i, snapshot[i]);
    }
  g_strfreev (snapshot);

  fprintf (fp, "   ");
  for (i = 0; i < col; i++)
    {
      fprintf (fp, "%d", (i % 10 == 0) ? i / 10 : i % 10);
    }
  fprintf (fp, "\n");

  if (fp != stdout)
    {
      fclose (fp);
    }

  return TRUE;
}

/** 
 * t_snapxy:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_snapxy (gint argc, gchar *argv[])
{
  char	szReStr[128];

  if (argc < 4)
    {
      printf ("usage: snapxy x y len\n");
      return FALSE;
    }

  memset (szReStr, 0, sizeof(szReStr));

  if (!t_rpc_call_snap_getxy (atoi(argv[1]), atoi(argv[2]),
			      atoi(argv[3]), szReStr))
    {
      return FALSE;
    }
  printf("----------%s\n", szReStr);

  return TRUE;
}

/** 
 * snap row:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_snaprow  (gint argc, gchar *argv[])
{
  gchar *retstr = NULL;

  if(argc < 1)
    {
      printf ("usage: getrow row\n");
      return FALSE;
    }
  if (!t_rpc_call_snap_getrow(atoi(argv[1]), &retstr))
    {
      printf("getrow false\n"); 
      return FALSE;
    }

  printf("------%s\n", retstr);
  g_free(retstr);

  return TRUE;
}

/** 
 * t_feed:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_feed (gint argc, gchar *argv[])
{
#if 0
  gint length;
#endif

  if (argc < 2)
    {
      printf ("usage: feed string [len].\n");
      return FALSE;
    }

#if 0
  length = (argc > 2) ? atoi (argv[2]) : strlen (argv[1]);
  t_rpc_call_feed (argv[1], length);
#else
  if (argc > 2)
    {
      GString *string;
      gint i;

      string = g_string_new ("");

      for (i = 1; i < argc; i++)
	{
	  g_string_append (string, argv[i]);
	  g_string_append_c (string, ' ');
	}
      t_rpc_call_feed (string->str, strlen (string->str));

      g_string_free (string, TRUE);
    }
  else
    t_rpc_call_feed (argv[1], strlen (argv[1]));
#endif

  return TRUE;
}

/** 
 * t_get_events:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_get_events (gint argc, gchar *argv[])
{
  TEventEntry *events = NULL;
  gsize number, i;

  if (!t_rpc_call_get_events (&events, &number))
    {
      return FALSE;
    }
  for (i = 0; i < number; i++)
    {
      printf ("%d. %s %s %s %s\n", i + 1,
	      events[i].name, events[i].group,
	      events[i].matcher, events[i].program);
    }
  g_free (events);

  return TRUE;
}

/** 
 * t_insert_event:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_insert_event (gint argc, gchar *argv[])
{
  gchar *command;

  if (argc < 5)
    {
      printf ("usage: ie name group matcher program [args]\n");
      return FALSE;
    }

  command = g_strjoinv (" ", &argv[4]);

  if (!t_rpc_call_insert_event (argv[1], argv[2],
				argv[3], command))
    {
      g_free (command);
      return FALSE;
    }
  g_free (command);
  return TRUE;
}

/** 
 * t_remove_event:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_remove_event  (gint argc, gchar *argv[])
{
  if (argc < 2)
    {
      printf ("usage: reg event_name\n");
      return FALSE;
    }

  if (!t_rpc_call_remove_event (argv[1]))
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_remove_event_group:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_remove_event_group  (gint argc, gchar *argv[])
{
  if (argc < 2)
    {
      printf ("usage: reg group_name\n");
      return FALSE;
    }

  if (!t_rpc_call_remove_event_group (argv[1]))
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_stdin_suppend:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_stdin_suppend  (gint argc, gchar *argv[])
{
  if (!t_rpc_call_stdin_suppend ())
    {
      return FALSE;
    }
  return TRUE;
}


/** 
 * t_stdin_resume:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_stdin_resume  (gint argc, gchar *argv[])
{
  if (!t_rpc_call_stdin_resume ())
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_stdout_suppend:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_stdout_suppend  (gint argc, gchar *argv[])
{
  if (!t_rpc_call_stdout_suppend ())
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_stdout_resume:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_stdout_resume  (gint argc, gchar *argv[])
{
  if (!t_rpc_call_stdout_resume ())
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_stdout_resume:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_trigger_event  (gint argc, gchar *argv[])
{
  if (argc < 2)
    {
      printf ("usage: te event_name\n");
      return FALSE;
    }

  if (!t_rpc_call_trigger_event (argv[1]))
    {
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_get_cursor_pos:
 * @argc:
 * @argv:
 * 
 * @Returns: 
 */
gboolean
t_get_cursor_pos (gint argc, gchar *argv[])
{
  guint16 row, col;

  if (!t_rpc_call_get_cursor_pos (&row, &col))
    {
      return FALSE;
    }
  printf ("row: %d, column: %d\n", row, col);

  return TRUE;
}

/**
 * t_refresh:
 * @argc 
 * @argv 
 * 
 * @Returns: 
 */
gboolean
t_refresh (gint argc, gchar *argv[])
{
  if (!t_rpc_call_refresh ())
    return FALSE;

  return TRUE;
}
