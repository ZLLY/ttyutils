/* Ttyutils Library
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <sys/types.h>
#include <sys/ioctl.h>
#ifdef HAVE_TERMIOS_H
# include <termios.h>
#endif
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <glib/gi18n-lib.h>
#include <glib/gprintf.h>
#include <glib-object.h>

#include "tdebug.h"
#include "ttermop.h"

#if defined (OS_FREEBSD) || defined (OS_MAC)
# define BYTES_AVAILABLE	TIOCOUTQ
#elif defined (TIOCINQ)
# define BYTES_AVAILABLE	TIOCINQ
#else
# define BYTES_AVAILABLE	FIONREAD
#endif

#define t_return_if_isnotatty(fd)	G_STMT_START {		\
  if (!isatty (fd))						\
    {								\
      t_println (_("file descriptor %d is not a tty."), fd);	\
      return;							\
    }								\
} G_STMT_END

#define t_return_val_if_isnotatty(fd, retval)	G_STMT_START {	\
  if (!isatty (fd))						\
    {								\
      g_warning (_("file descriptor %d is not a tty."), fd);	\
      return retval;						\
    }								\
} G_STMT_END

#ifdef  T_CSI
# undef T_CSI
#endif
#define T_CSI	"\033["


/**
 * t_tcgetattr:
 */
gint
t_tcgetattr (gint fd, struct termios *ttmode)
{
#if defined (__FreeBSD__) || defined (__NetBSD__) || defined (__OpenBSD__) || \
    defined (__bsdi__) || defined (__APPLE__) || defined (__DragonFly__)
# define _tcgetattr(_fd, ttmode)   ioctl(_fd, TIOCGETA, (char*) ttmode)
#else
# if defined (_HPUX_SOURCE) || defined (__Lynx__) || defined (__CYGWIN__) || defined (SCO_SV)
#  define _tcgetattr(_fd, ttmode)  tcgetattr(_fd, ttmode)
# else
#  define _tcgetattr(_fd, ttmode)  ioctl(_fd, TCGETS, (char*) ttmode)
# endif
#endif

  return _tcgetattr (fd, ttmode);
}

/**
 * t_tcsetattr:
 */
gint
t_tcsetattr (gint fd, struct termios *ttmode)
{
#if defined (__FreeBSD__) || defined (__NetBSD__) || defined (__OpenBSD__) || \
    defined (__bsdi__) || defined (__APPLE__) || defined (__DragonFly__)
# define _tcsetattr(_fd, ttmode)  ioctl(_fd, TIOCSETA, (char*) ttmode)
#else
# if defined (_HPUX_SOURCE) || defined (__CYGWIN__) || defined (SCO_SV)
#  define _tcsetattr(_fd, ttmode) tcsetattr(_fd, TCSANOW, ttmode)
# else
#  define _tcsetattr(_fd, ttmode) ioctl(_fd, TCSETS, (char*) ttmode)
# endif
#endif

  return _tcsetattr (fd, ttmode);
}

/**
 * t_bytes_available:
 */
gint
t_bytes_available (gint fd)
{
#ifndef SCO_SV
  gint available;

  if (ioctl (fd, BYTES_AVAILABLE, (gchar*) &available) != 0)
    {
      g_warning ("ioctl with BYTES_AVAILABLE error: %s", g_strerror (errno));
      return -1;
    }
  return available;
#else
  return 0;
#endif
}

/**
 * t_term_set_echo:
 *
 * If this bit is set, echoing of input characters back to the terminal is
 * enabled.
 */
void
t_term_set_echo (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~ECHO;
  else
    ttmode.c_lflag |= ECHO;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_ixon:
 *
 * If this bit is set, start/stop control on output is enabled.
 */
void
t_term_set_ixon (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDOUT_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_iflag &= ~IXON;
  else
    ttmode.c_iflag |= IXON;

  t_tcsetattr (STDOUT_FILENO, &ttmode);
}

/**
 * t_term_set_ixoff:
 *
 * If this bit is set, start/stop control on input is enabled.
 */
void
t_term_set_ixoff (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_iflag &= ~IXOFF;
  else
    ttmode.c_iflag |= IXOFF;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_canon:
 *
 * This bit, if set, enables canonical input processing mode. Otherwise, input
 * is processed in noncanonical mode.
 */
void
t_term_set_canon (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~ICANON;
  else
    ttmode.c_lflag |= ICANON;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_isig:
 *
 * This bit controls whether the INTR, QUIT, and SUSP characters are
 * recognized.
 */
void
t_term_set_isig (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~ISIG;
  else
    ttmode.c_lflag |= ISIG;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_noflsh:
 */
void
t_term_set_noflsh (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~NOFLSH;
  else
    ttmode.c_lflag |= NOFLSH;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_icrnl:
 */
void
t_term_set_icrnl (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~ICRNL;
  else
    ttmode.c_lflag |= ICRNL;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_inlcr:
 */
void
t_term_set_inlcr (gboolean enable)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  if (!enable)
    ttmode.c_lflag &= ~INLCR;
  else
    ttmode.c_lflag |= INLCR;

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_set_raw:
 */
void
t_term_set_raw (void)
{
  struct termios ttmode;

  t_return_if_isnotatty (STDIN_FILENO);

#ifdef HAVE_CFMAKERAW
  cfmakeraw (&ttmode);
#else
  if (t_tcgetattr (STDIN_FILENO, &ttmode) != 0)
    return;

  ttmode.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP |
		      INLCR | IGNCR | ICRNL | IXON);
  ttmode.c_oflag &= ~OPOST;
  ttmode.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  ttmode.c_cflag &= ~(CSIZE | PARENB);
  ttmode.c_cflag |= CS8;
#endif

  t_tcsetattr (STDIN_FILENO, &ttmode);
}

/**
 * t_term_get_winsize:
 */
gboolean
t_term_get_winsize (guint16 *lines, guint16 *columns)
{
  struct winsize size;

  g_return_val_if_fail (lines, FALSE);
  g_return_val_if_fail (columns, FALSE);
  t_return_val_if_isnotatty (STDOUT_FILENO, FALSE);

  memset (&size, 0, sizeof (size));

  if (ioctl (STDOUT_FILENO, TIOCGWINSZ, &size) == 0 &&
      size.ws_col > 0 && size.ws_row > 0)
    {
      *lines = size.ws_row;
      *columns = size.ws_col;
      return TRUE;
    }
  g_warning (_("get terminal winsize error: %s"), g_strerror (errno));
  return FALSE;
}

/**
 * t_term_set_winsize:
 */
gboolean
t_term_set_winsize (guint16 lines, guint16 columns)
{
  struct winsize win_size;

  t_return_val_if_isnotatty (STDOUT_FILENO, FALSE);

  memset (&win_size, 0, sizeof (win_size));

  win_size.ws_row = (unsigned short) lines;
  win_size.ws_col = (unsigned short) columns;

  if (ioctl (STDOUT_FILENO, TIOCSWINSZ, (gchar*) &win_size) != 0)
    {
      g_warning (_("change terminal winsize error: %s"), g_strerror (errno));
      return FALSE;
    }
  return TRUE;
}

/**
 * t_term_line_speed:
 */
gint
t_term_line_speed (gint fd)
{
  struct termios ttmode;
  speed_t speed;

  t_return_val_if_isnotatty (fd, 0);

  t_tcgetattr (fd, &ttmode);
  speed = cfgetospeed (&ttmode);

  switch (speed)
    {
    case B0:     return 0;
    case B50:    return 50;
    case B75:    return 75;
    case B110:   return 110;
    case B134:   return 134;
    case B150:   return 150;
    case B200:   return 200;
    case B300:   return 300;
    case B600:   return 600;
    case B1200:  return 1200;
    case B1800:  return 1800;
    case B2400:  return 2400;
    case B4800:  return 4800;
    case B9600:  return 9600;
    case B19200: return 19200;
    case B38400: return 38400;
    case B57600: return 57600;
    case B115200: return 115200;
    case B230400: return 230400;
    case B460800: return 460800;
    }
  return 0;
}

/**
 * t_term_use_alter_screen:
 */
gboolean
t_term_use_alter_screen (void)
{
  t_return_val_if_isnotatty (STDOUT_FILENO, FALSE);

  if (g_getenv ("TERM") &&
      g_ascii_strcasecmp (g_getenv ("TERM"), "xterm") == 0)
    {
      fprintf (stdout, T_CSI "?1049h" T_CSI "H");
      fflush (stdout);
      return TRUE;
    }
  return FALSE;
}

/**
 * t_term_use_normal_screen:
 */
gboolean
t_term_use_normal_screen (void)
{
  t_return_val_if_isnotatty (STDOUT_FILENO, FALSE);

  if (g_getenv ("TERM") &&
      g_ascii_strcasecmp (g_getenv ("TERM"), "xterm") == 0)
    {
      fprintf (stdout, T_CSI "?1049l");
      fflush (stdout);
      return TRUE;
    }
  return FALSE;
}

/**
 * t_alternate_screen_print:
 * @timeout: in milliseconds (1/1000ths of a second)
 */
void
t_term_alter_screen_print (gint timeout, const gchar *format, ...)
{
  g_return_if_fail (format);
  t_return_if_isnotatty (STDOUT_FILENO);

  if (t_term_use_alter_screen ())
    {
      va_list args;

      va_start (args, format);
      g_vfprintf (stdout, format, args);
      va_end (args);

      fflush (stdout);

      g_usleep (timeout * 1000);
      t_term_use_normal_screen ();
    }
}

/**
 * t_term_reset:
 */
void
t_term_reset (void)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, "\033c");
  fflush (stdout);
}

/**
 * t_term_clear_screen:
 */
void
t_term_clear_screen (void)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, T_CSI "H" T_CSI "2J");
  fflush (stdout);
}

/**
 * t_term_clear_line:
 */
void
t_term_clear_line (gint lineno)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, T_KEY_ESC_S "7" T_CSI "%d;0H" T_CSI "2K" T_KEY_ESC_S "8", lineno);
}

/**
 * t_term_clear_lines:
 */
void
t_term_clear_lines (gint start_line, gint end_line)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  for (; start_line <= end_line; start_line++)
    t_term_clear_line (start_line);
}

/**
 * t_term_clear_area:
 */
void
t_term_clear_area (gint start_line, gint start_column,
		   gint end_line, gint end_column)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  if (end_line <= start_line || end_column <= start_column)
    {
      g_warning ("coordinate arguments incorrect.");
      return;
    }

  fprintf (stdout, T_KEY_ESC_S "7");

  for (; start_line <= end_line; start_line++)
    {
      gint i;

      fprintf (stdout, T_CSI "%d;%dH", start_line, start_column);
      for (i = 0; i <= end_column - start_column; i++)
	fprintf (stdout, " ");
    }
  fprintf (stdout, T_KEY_ESC_S "8");
}

/**
 * t_term_set_cursor_xy:
 */
void
t_term_set_cursor_xy (gint line, gint column)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, T_CSI "%d;%dH", line, column);
}

/**
 * t_term_cursor_xy_print:
 */
void
t_term_cursor_xy_print (gint line, gint column, const gchar *format, ...)
{
  va_list args;

  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, T_KEY_ESC_S "7");

  t_term_set_cursor_xy (line, column);

  va_start (args, format);
  g_vfprintf (stdout, format, args);
  va_end (args);

  fprintf (stdout, T_KEY_ESC_S "8");
}

/**
 * t_term_printer_print:
 *
 * print a string to printer
 */
void
t_term_printer_print (const gchar *format, ...)
{
  va_list args;

  g_return_if_fail (format);
  t_return_if_isnotatty (STDOUT_FILENO);

  printf ("\033[5i");

  va_start (args, format);
  g_vfprintf (stdout, format, args);
  va_end (args);

  printf ("\033[4i");
  fflush (stdout);
}

/**
 * t_term_printer_print_file:
 *
 * print file content to printer
 */
void
t_term_printer_print_file (const gchar *fname)
{
  gchar buff[1024];
  FILE *fp;

  g_return_if_fail (fname);
  t_return_if_isnotatty (STDOUT_FILENO);

  fp = fopen (fname, "r");
  if (!fp)
    {
      g_warning (_("open %s error: %s"), fname, g_strerror (errno));
      return;
    }

  printf ("\033[5i");

  while (fgets (buff, sizeof (buff), fp))
    printf ("%s", buff);

  printf ("\033[4i");
  fflush (stdout);

  fclose (fp);
}

/**
 * t_term_printer_print_screen:
 *
 * print current screen content to printer
 */
void
t_term_printer_print_screen (void)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  printf ("\033[0i");
}

/**
 * t_term_get_char_attr_str:
 */
const gchar*
t_term_get_char_attr_str (TTermCharAttr attr, gchar *buff, gint size)
{
  static gchar ret[16];

  g_snprintf (ret, sizeof (ret), "\033[%dm", attr);
  if (buff)
    g_strlcpy (buff, ret, size);

  return ret;
}

/**
 * t_term_set_char_attr:
 *
 * set character attributes
 */
void
t_term_set_char_attr (TTermCharAttr attr)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, "%s", t_term_get_char_attr_str (attr, NULL, 0));
}

/**
 * t_term_reset_char_attr:
 *
 * clear character attributes
 */
void
t_term_reset_char_attr (void)
{
  t_return_if_isnotatty (STDOUT_FILENO);

  fprintf (stdout, "%s", t_term_get_char_attr_str (CHAR_ATTR_NORMAL, NULL, 0));
  fprintf (stdout, "%s", t_term_get_char_attr_str (CHAR_ATTR_FORE_DEFAULT, NULL, 0));
  fprintf (stdout, "%s", t_term_get_char_attr_str (CHAR_ATTR_BACK_DEFAULT, NULL, 0));
}

/**
 * t_term_char_attr_printf:
 *
 * printf a string with attributes
 */
gint
t_term_char_attr_print (TTermCharAttr attr, gchar *format, ...)
{
  va_list args;
  gint ret;

  g_return_val_if_fail (format, -1);
  t_return_val_if_isnotatty (STDOUT_FILENO, -1);

  fprintf (stdout, "%s", t_term_get_char_attr_str (attr, NULL, 0));

  va_start (args, format);
  ret = g_vfprintf (stdout, format, args);
  va_end (args);

  t_term_reset_char_attr ();
  fflush (stdout);

  return ret;
}

/**
 * t_term_char_attr_println:
 *
 * printf a string with attributes with a newline
 */
gint
t_term_char_attr_println (TTermCharAttr attr, gchar *format, ...)
{
  va_list args;
  gint ret;

  g_return_val_if_fail (format, -1);
  t_return_val_if_isnotatty (STDOUT_FILENO, -1);

  fprintf (stdout, "%s", t_term_get_char_attr_str (attr, NULL, 0));

  va_start (args, format);
  ret = g_vfprintf (stdout, format, args);
  va_end (args);

  t_term_reset_char_attr ();
  fprintf (stdout, "\n");
  fflush (stdout);

  return ret;
}

/**
 * t_term_char_attr_sprintf:
 *
 * printf a string with attributes
 */
gint
t_term_char_attr_sprintf (gchar        *buff,
			  gint          size,
			  TTermCharAttr attr,
			  const gchar  *format,
			  ...)
{
  va_list args;
  gint ret;

  g_return_val_if_fail (buff, -1);
  g_return_val_if_fail (size <= 0, -1);
  g_return_val_if_fail (format, -1);
  t_return_val_if_isnotatty (STDOUT_FILENO, -1);

  ret = g_snprintf (buff, size, "%s", t_term_get_char_attr_str (attr, NULL, 0));

  va_start (args, format);
  ret += g_vsnprintf (buff + ret, size - ret, format, args);
  va_end (args);

  ret += g_snprintf (buff, size, "%s", t_term_get_char_attr_str (CHAR_ATTR_NORMAL, NULL, 0));
  ret += g_snprintf (buff, size, "%s", t_term_get_char_attr_str (CHAR_ATTR_FORE_DEFAULT, NULL, 0));
  ret += g_snprintf (buff, size, "%s", t_term_get_char_attr_str (CHAR_ATTR_BACK_DEFAULT, NULL, 0));

  return ret;
}


typedef struct _TTermButtonPair	TTermButtonPair;
struct _TTermButtonPair
{
  gchar       *text;
  gint         select;
  TTermButton  button;
};

/**
 * t_term_popup_dialog:
 *
 * popup a dialog
 */
TTermButton
t_term_popup_dialog (gint         start_line,
		     gint         start_column,
		     gint         end_line,
		     gint         end_column,
		     const gchar *title,
		     const gchar *text[],
		     gint         n_texts,
		     TTermButton  buttons)
{
  struct termios ttmode;
  guint16 lines, columns;
  gint i, j;
  TTermButtonPair button_pairs[16];
  GString *string;
  TTermButton ret = TButtonBad;

  t_return_val_if_isnotatty (STDIN_FILENO, TButtonBad);
  t_return_val_if_isnotatty (STDOUT_FILENO, TButtonBad);

  if (end_line <= start_line || end_column <= start_column)
    {
      g_warning (_("coordinate arguments incorrect."));
      return TButtonBad;
    }

  /* check winsize
   */
  t_term_get_winsize (&lines, &columns);
  if (end_line >= lines || end_column >= columns)
    {
      g_warning (_("dialog rectangle (%dx%d,%dx%d) out of range"),
		 start_column, start_line, end_column, end_line);
      return TButtonBad;
    }

  /* make a box
   */
  t_term_set_char_attr (CHAR_ATTR_INVERSE);
  t_term_clear_area (start_line, start_column, end_line, end_column);
  t_term_reset_char_attr ();
  t_term_clear_area (start_line + 1, start_column + 1, end_line - 1, end_column - 1);

  if (title)
    {
      t_term_set_char_attr (CHAR_ATTR_BOLD);
      t_term_cursor_xy_print (start_line + 1, start_column + 2, "%s", title);
      t_term_reset_char_attr ();

      for (i = start_column + 1; i < end_column; i++)
	t_term_cursor_xy_print (start_line + 2, i, "=");
    }

  for (i = 0; i < n_texts && text[i]; i++)
    t_term_cursor_xy_print (start_line + i + 3, start_column + 2, "%s", text[i]);

  memset (&button_pairs, 0, sizeof (button_pairs));
  i = 0;

  if (buttons & TButtonOk)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.Ok"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonOk;
      i++;
    }
  if (buttons & TButtonCancel)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.Cancel"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonCancel;
      i++;
    }
  if (buttons & TButtonYes)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.Yes"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonYes;
      i++;
    }
  if (buttons & TButtonNo)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.No"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonNo;
      i++;
    }
  if (buttons & TButtonClose)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.Close"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonClose;
      i++;
    }
  if (buttons & TButtonSubmit)
    {
      button_pairs[i].text = g_strdup_printf (_("%d.Submit"), i + 1);
      button_pairs[i].select = i + 1;
      button_pairs[i].button = TButtonSubmit;
      i++;
    }

  string = g_string_new (NULL);

  for (i = 0, j = 2; button_pairs[i].text; i++)
    {
      if (string->len + strlen (button_pairs[i].text) + 2 > end_column - start_column)
	{
	  t_term_cursor_xy_print (end_line - j, start_column + 2, "%s", string->str);
	  j--;
	  g_string_truncate (string, 0);
	}
      g_string_append (string, button_pairs[i].text);
      g_string_append (string, "  ");
    }
  if (j == 2) j = 1;
  t_term_cursor_xy_print (end_line - j, start_column + 2, "%s", string->str);
  g_string_free (string, TRUE);

  t_term_reset_char_attr ();
  fflush (stdout);

  t_tcgetattr (STDIN_FILENO, &ttmode);
  /*
  ttsave = ttmode;

  ttmode.c_lflag &= ~(ICANON | ECHO);
  ttmode.c_cc[VMIN] = 1;
  ttmode.c_cc[VTIME] = 0;
  t_tcsetattr (STDIN_FILENO, &ttmode);
  */
  tcflush (STDIN_FILENO, TCIFLUSH);
  t_term_set_raw ();

  while (1)
    {
      gchar ch;

      if (read (STDIN_FILENO, &ch, 1) == -1)
	{
	  g_usleep (100);
	  continue;
	}
      if (ch == T_KEY_ESC)
	{
	  ret = TButtonBad;
	  break;
	}
      for (i = 0; button_pairs[i].text; i++)
	{
	  if (ch == button_pairs[i].select + '0')
	    {
	      ret = button_pairs[i].button;
	      goto breakout;
	    }
	}
    }

breakout:
  t_tcsetattr (STDIN_FILENO, &ttmode);

  for (i = 0; button_pairs[i].text; i++)
    g_free (button_pairs[i].text);

  return ret;
}

/**
 * t_term_popup_dialog_center:
 *
 * popup a dialog at screen center
 */
TTermButton
t_term_popup_dialog_center (const gchar *title,
			    const gchar *text[],
			    gint         n_text,
			    TTermButton  buttons)
{
  struct winsize size;
  gint maxlen, i;

  t_return_val_if_isnotatty (STDIN_FILENO, TButtonBad);
  t_return_val_if_isnotatty (STDOUT_FILENO, TButtonBad);

  if (ioctl (STDOUT_FILENO, TIOCGWINSZ, &size) == 0 &&
      size.ws_col > 0 && size.ws_row > 0)
    {
      gint start_line, end_line, start_column, end_column;

      maxlen = title ? strlen (title) : 0;
      for (i = 0; i < n_text && text[i]; i++)
	maxlen = MAX (maxlen, strlen (text[i]));

      start_line = (size.ws_row - n_text) / 2 - 3;
      end_line = start_line + n_text + 6;
      start_column = (size.ws_col - maxlen) / 2 - 2;
      end_column = start_column + maxlen + 4;

      return t_term_popup_dialog (start_line, start_column, end_line, end_column,
				  title, text, n_text, buttons);
    }
  return TButtonBad;
}
