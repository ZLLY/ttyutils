-- ttyexec configure script.
-- vim: ts=4 sw=4 expandtab ft=lua
-- copyright (C) 2008,2009,2010,2011 xiaohu, all rights reserved.
--
-- this file is part of ttyutils 1.9.1
--

-- load required packages
--
require "ttyutils"
require "keyseq"
require "hotkey"
require "event"
require "ttyexec"
require "system"

-- cleanup function-key sequences first.
--
hotkey.keyseq_clear ();

-- select right key sequences define for really terminal.
-- all usable key sequences is defined in file keyseq.lua,
-- if it is not enough or don't fit your terminal, you can
-- define a new.
keyseqs_current = keyseqs_greatwall;

for _, k in pairs (keyseqs_current) do
    hotkey.keyseq_set (k[1], k[2], k[3]);
end


evt_bin = "/dcc/ttyutils/bin/ttyevt --tty=@TTYEXEC_TTY@"
logfile = "/dcc/ttyutils/log/ttyevt.log"

evt_termtype = evt_bin .. " --event=termtype --logfile=" .. logfile
evt_login = evt_bin .. " --event=login --logfile=" .. logfile
evt_reset = evt_bin .. " --event=reset --logfile=" .. logfile
evt_eval = evt_bin .. " --event=eval --logfile=" .. logfile

event.insert ("termtype", "business",
              "2,7,35:18,.*[0-9]{9}-[A-Z]{1}-[0-9]{2}-[0-9]{3}.*",
              evt_termtype);
event.insert ("login", "default", "2,11,28:14,.*请输入交易代码.*", evt_login);
event.insert ("reset", "default", "2,11,28:14,.*请输入交易代码.*", evt_reset);
event.insert ("eval", "default", "3,^X", evt_eval);

