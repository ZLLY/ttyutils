/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_STDIO_H__
#define __T_STDIO_H__

#include <glib-object.h>

G_BEGIN_DECLS


#define T_TYPE_STDIO             (t_stdio_get_type ())
#define T_STDIO(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), T_TYPE_STDIO, TStdio))
#define T_STDIO_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), T_TYPE_STDIO, TStdioClass))
#define T_IS_STDIO(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), T_TYPE_STDIO))
#define T_IS_STDIO_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), T_TYPE_STDIO))
#define T_STDIO_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), T_TYPE_STDIO, TStdioClass))

typedef struct _TStdio		TStdio;
typedef struct _TStdioClass	TStdioClass;
typedef struct _TStdioPrivate	TStdioPrivate;

struct _TStdio
{
  GObject object;

  TStdioPrivate *priv;
};

struct _TStdioClass
{
  GObjectClass object_class;

  void	(*incoming)		(TStdio      *stdio,
				 const gchar *incoming,
				 guint        size);
  void	(*suspend_stdin)	(TStdio      *stdio);
  void	(*resume_stdin)		(TStdio      *stdio);
  void	(*suspend_stdout)	(TStdio      *stdio);
  void	(*resume_stdout)	(TStdio      *stdio);
};


GType		t_stdio_get_type	(void) G_GNUC_CONST;

TStdio*		t_stdio_new		(void);

void		t_stdio_connect_stdin	(TStdio      *stdio);
void		t_stdio_disconnect_stdin(TStdio      *stdio);

void		t_stdio_connect_stdout	(TStdio      *stdio);
void		t_stdio_feed_stdout	(TStdio      *stdio,
					 const gchar *data,
					 gssize       length,
					 gboolean     to_auxport);

void		t_stdio_suspend_stdin	(TStdio      *stdio);
void		t_stdio_resume_stdin	(TStdio      *stdio);
void		t_stdio_suspend_stdout	(TStdio      *stdio);
void		t_stdio_resume_stdout	(TStdio      *stdio);
gboolean	t_stdio_is_suspend	(TStdio      *stdio,
					 gboolean     in);
void		t_stdio_refresh		(TStdio      *stdio);

G_END_DECLS

#endif /* __T_STDIO_H__ */
