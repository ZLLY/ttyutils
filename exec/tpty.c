/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#ifdef HAVE_SYS_TERMIOS_H
#include <sys/termios.h>
#endif
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "tpty.h"


/**
 * t_pty_get_size:
 * @master: the file descriptor of the pty master
 * @columns: a place to store the number of columns
 * @rows: a place to store the number of rows
 *
 * Attempts to read the pseudo terminal's window size.
 *
 * Returns: 0 on success, -1 on failure.
 */
gint
t_pty_get_size (gint master, gint *columns, gint *rows)
{
  struct winsize size;

  memset (&size, 0, sizeof (size));

  if (ioctl (master, TIOCGWINSZ, &size) != 0)
    {
      g_warning (_("read size from fd %d error: %s"), master,
		   g_strerror (errno));
      return -1;
    }

  if (columns != NULL)
    {
      *columns = size.ws_col;
    }
  if (rows != NULL)
    {
      *rows = size.ws_row;
    }

  t_println ("Size on fd %d is (%dx%d).", master,
	     size.ws_col, size.ws_row);
  return 0;
}

/**
 * t_pty_set_size:
 * @master: the file descriptor of the pty master
 * @cols: the desired number of columns
 * @rows: the desired number of rows
 *
 * Attempts to resize the pseudo terminal's window size.  If successful, the
 * OS kernel will send #SIGWINCH to the child process group.
 *
 * Returns: 0 on success, -1 on failure.
 */
gint
t_pty_set_size (gint master, gint cols, gint rows)
{
  struct winsize size;

  memset (&size, 0, sizeof (size));

  size.ws_row = rows ? rows : 24;
  size.ws_col = cols ? cols : 80;

  t_println ("setting size on fd %d to (%dx%d).", master, cols, rows);

  if (ioctl (master, TIOCSWINSZ, &size) != 0)
    {
      g_warning (_("set terminal size on %d error: %s."), master,
		 strerror (errno));
      return -1;
    }
  return 0;
}

/**
 * t_pty_set_utf8:
 * @pty: The pty master descriptor.
 * @utf8: Whether or not the pty is in UTF-8 mode.
 *
 * Tells the kernel whether the terminal is UTF-8 or not, in case it can make
 * use of the info.  Linux 2.6.5 or so defines IUTF8 to make the line
 * discipline do multibyte backspace correctly.
 */
void
t_pty_set_utf8 (gint pty, gboolean utf8)
{
#if defined(HAVE_TCSETATTR) && defined(IUTF8)
  struct termios tio;
  tcflag_t saved_cflag;

  if (pty != -1)
    {
      if (tcgetattr (pty, &tio) != -1)
	{
	  saved_cflag = tio.c_iflag;
	  tio.c_iflag &= ~IUTF8;

	  if (utf8)
	    {
	      tio.c_iflag |= IUTF8;
	    }
	  if (saved_cflag != tio.c_iflag)
	    {
	      tcsetattr (pty, TCSANOW, &tio);
	    }
	}
    }
#endif
}


/** 
 * t_pty_ptsname:
 * @master:
 * 
 * @Returns: 
 */
gchar*
t_pty_ptsname (gint master)
{
#if defined(HAVE_PTSNAME_R)
  gsize len = 1024;
  gchar *buf = NULL;

  do
    {
      buf = g_malloc0 (len);

      if (ptsname_r (master, buf, len - 1) == 0)
	{
	  return buf;
	}
      else if (errno == ERANGE)
	{
	  g_free (buf);
	  len *= 2;
	}
      else
	{
	  g_warning (_("ptsname_r error: %s"), g_strerror (errno));
	  g_free (buf);
	  return NULL;
	}
    } while (1);

#elif defined(HAVE_PTSNAME)
  gchar *p;

  if ((p = ptsname (master)) != NULL)
    {
      return g_strdup (p);
    }

#elif defined(TIOCGPTN)
  gint pty = 0;

  if (ioctl (master, TIOCGPTN, &pty) == 0)
    {
      return g_strdup_printf ("/dev/pts/%d", pty);
    }
#endif

  return NULL;
}
