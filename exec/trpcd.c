/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <uuid/uuid.h>
#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <ttyutils.h>

#include "trpcd.h"
#include "trpcdsvc.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

struct _TRpcd
{
  gchar       *unixpath;
  TUnixSocket *unixsock;
  GSList      *clients;
};

static TRpcd	*t_rpcd = NULL;


/** 
 * t_rpcd_read:
 * @channel: the connected socket channel.
 * @buffer: data will be save in
 * @size: @buffer size
 * 
 * @Returns: 0 on success
 */
static gint
t_rpcd_read (GIOChannel *channel,
	     void       *buffer,
	     gsize       size)
{
  gchar *buf = (gchar*) buffer;
  GError *error = NULL;

  g_return_val_if_fail (channel, FALSE);
  g_return_val_if_fail (buffer, FALSE);

  while (size > 0)
    {
      GIOStatus status;
      gsize nread;

      status = g_io_channel_read_chars (channel, buf, size,
					&nread, &error);
      if (status == G_IO_STATUS_ERROR)
	{
	  if (error)
	    {
	      g_warning (_("Read rpcd channel error: %s."), error->message);
	      g_error_free (error);
	    }
	  return RPC_ERR_SYSTEM;
	}
      else if (status == G_IO_STATUS_EOF)
	{
	  t_println (_("end of file"));
	  return RPC_ERR_EOF;
	}
      else if (status == G_IO_STATUS_AGAIN)
	{
	  return RPC_ERR_TIMEOUT;
	}

      size -= nread;
      buf += nread;

      t_println ("read %d bytes, remain %d bytes.", nread, size);
    }
  return RPC_ERR_OK;
}

/** 
 * t_rpcd_client_disconnect:
 * 
 * @data 
 */
static void
t_rpcd_client_disconnect (gpointer data)
{
  TUnixSocket *sock = (TUnixSocket*) data;

  t_println ("client disconnect.");

  t_rpcd->clients = g_slist_remove (t_rpcd->clients, sock);
  t_unix_socket_delete (sock);
}

/** 
 * t_rpcd_io_read:
 * @channel: the rpc client - server socket channel.
 * @cond: io condition
 * @data: connection socket object
 * 
 * @Returns: return TRUE to continue read, return FALSE to
 *           close the connection.
 */
static gboolean
t_rpcd_io_read (GIOChannel  *channel,
		GIOCondition cond,
		gpointer     data)
{
  TRpcRequest request;
  TRpcResponse response;
  GError *error = NULL;

  if (cond & G_IO_HUP)
    {
      t_println (_("disconect from client."));
      return FALSE;
    }
  if (cond & G_IO_ERR)
    {
      g_warning (_("read rpc client channel error."));
      return FALSE;
    }

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      gchar *buff = NULL, *rbuff = NULL;
      gsize size, rsize = 0;
      gint rcode;

      rcode = t_rpcd_read (channel, &request, sizeof (TRpcRequest));
      if (rcode != RPC_ERR_OK)
	{
	  if (rcode != RPC_ERR_EOF)
	    {
	      g_warning (_("read reqeust header error: %s"), t_rpc_strerror (rcode));
	    }
	  return FALSE;
	}

      t_println ("rpcd receive id(%d) pid(%d) uid(%d) gid(%d).",
		 g_ntohs (request.id), g_ntohl (request.pid),
		 g_ntohl (request.uid), g_ntohl (request.gid));

      size = g_ntohl (request.size) - sizeof (TRpcRequest);
      if (size > 0)
	{
	  buff = g_malloc0 (size);

	  t_println ("continue read request data %d bytes.", size);

	  rcode = t_rpcd_read (channel, buff, size);
	  if (rcode != RPC_ERR_OK)
	    {
	      g_warning (_("read reqeust data error: %s"), t_rpc_strerror (rcode));
	      g_free (buff);
	      return FALSE;
	    }
	}

      rcode = t_rpcdsvc_call (&request, buff, size, &rbuff, &rsize);
      g_free (buff);

      response.id = request.id;
      response.code = g_htons (rcode);
      response.size = g_htonl (rsize + sizeof (TRpcResponse));

      /* send response header.
       */
      t_println ("rpcd send response id %d.", g_ntohs (request.id));

      g_io_channel_write_chars (channel, (const gchar*) &response,
				sizeof (TRpcResponse), NULL, &error);
      if (error)
	{
	  g_warning (_("write to rpc error: %s"), error->message);
	  g_error_free (error);
	  g_free (rbuff);
	  return FALSE;
	}

      /* if has addition data, send them.
       */
      if (rcode == RPC_ERR_OK && rbuff && rsize > 0)
	{
	  t_println ("rpcd send response data %d bytes.", rsize);

	  g_io_channel_write_chars (channel, rbuff, rsize, NULL, &error);
	  if (error)
	    {
	      g_warning (_("write to rpc error: %s"), error->message);
	      g_error_free (error);
	      g_free (rbuff);
	      return FALSE;
	    }
	  g_free (rbuff);
	}
      g_io_channel_flush (channel, NULL);
    }

  return TRUE;
}

/** 
 * t_rpcd_accept:
 * @channel 
 * @cond 
 * @data 
 * 
 * @Returns: 
 */
static gboolean
t_rpcd_accept (GIOChannel  *channel,
	       GIOCondition cond,
	       gpointer     data)
{
  TRpcd *server = (TRpcd*) data;
  TUnixSocket *clisock = NULL;
  GIOChannel *iochannel = NULL;

  if (cond & G_IO_IN || cond & G_IO_PRI)
    {
      clisock = t_unix_socket_server_accept (server->unixsock);
      if (!clisock)
	{
	  g_warning (_("accept rpc connect failed."));
	  return TRUE;
	}

      iochannel = t_unix_socket_get_io_channel (clisock);
      if (!iochannel)
	{
	  g_critical (_("no io channel for new rpc connect"));
	  return TRUE;
	}

      server->clients = g_slist_append (server->clients, clisock);

      g_io_channel_set_buffered (iochannel, TRUE);
      g_io_channel_set_buffer_size (iochannel, 0);
      g_io_channel_set_encoding (iochannel, NULL, NULL);
      g_io_channel_set_flags (iochannel, G_IO_FLAG_NONBLOCK, NULL);

      t_println ("accept client connect.");

      g_io_add_watch_full (iochannel, G_PRIORITY_DEFAULT,
			   G_IO_IN | G_IO_ERR | G_IO_HUP,
			   t_rpcd_io_read, clisock,
			   t_rpcd_client_disconnect);
    }
  if (cond & G_IO_ERR)
    {
      g_warning (_("Socket accept error."));
      return FALSE;
    }
  return TRUE;
}

/** 
 * t_rpcd_disconnect:
 * @data 
 */
static void
t_rpcd_disconnect (gpointer data)
{
  TRpcd *rpcd = (TRpcd*) data;

  t_unix_socket_delete (rpcd->unixsock);
  rpcd->unixsock = NULL;
}

/** 
 * t_rpcd_boot:
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_rpcd_boot (void)
{
  uuid_t uuid;
  gchar buff[128];
  gchar *path;
  GIOChannel *channel;

  g_return_val_if_fail (!t_rpcd, TRUE);

  t_rpcd = g_new0 (TRpcd, 1);

  uuid_generate (uuid);
  uuid_unparse (uuid, buff);
  uuid_clear (uuid);

  path = g_build_filename ("/", "tmp", buff, NULL);
  t_rpcd->unixsock = t_unix_socket_server_new_abstract (path);
  t_rpcd->unixpath = path;

  if (!t_rpcd->unixsock)
    {
      g_warning (_("create rpcd socket failed."));
      g_free (t_rpcd);
      t_rpcd = NULL;
      return FALSE;
    }

  channel = t_unix_socket_get_io_channel (t_rpcd->unixsock);

  g_io_channel_set_buffered (channel, TRUE);
  g_io_channel_set_encoding (channel, NULL, NULL);
  g_io_channel_set_buffered (channel, FALSE);
  g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);

  g_io_add_watch_full (channel, G_PRIORITY_DEFAULT,
		       G_IO_IN | G_IO_PRI | G_IO_ERR, 
		       t_rpcd_accept, t_rpcd,
		       t_rpcd_disconnect);
  return TRUE;
}

/** 
 * t_rpcd_shut:
 */
void
t_rpcd_shut (void)
{
  g_return_if_fail (t_rpcd);

  if (t_rpcd->unixsock)
    {
      t_unix_socket_delete (t_rpcd->unixsock);
    }
  g_unlink (t_rpcd->unixpath);
  g_free (t_rpcd->unixpath);

  g_free (t_rpcd);
  t_rpcd = NULL;
}

/** 
 * t_rpcd_get_path:
 * 
 * @Returns: unix rpcet path name
 */
const gchar*
t_rpcd_get_unixpath (void)
{
  g_return_val_if_fail (t_rpcd, NULL);

  return t_rpcd->unixpath;
}
