/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_APP_H__
#define __T_APP_H__

#include <sys/types.h>
#include <glib-object.h>

#include "tstdio.h"
#include "tterm.h"
#include "tevtable.h"

G_BEGIN_DECLS

#define T_TYPE_APP             (t_app_get_type ())
#define T_APP(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), T_TYPE_APP, TApp))
#define T_APP_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), T_TYPE_APP, TAppClass))
#define T_IS_APP(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), T_TYPE_APP))
#define T_IS_APP_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), T_TYPE_APP))
#define T_APP_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), T_TYPE_APP, TAppClass))

typedef struct _TApp		TApp;
typedef struct _TAppClass	TAppClass;
typedef struct _TAppPrivate	TAppPrivate;

struct _TApp
{
  GObject      object;

  TTerm       *term;
  TStdio      *stdio;
  TEventTable *etable;

  TAppPrivate *priv;
};

struct _TAppClass
{
  GObjectClass parent_class;
};


GType		t_app_get_type		(void) G_GNUC_CONST;

TApp*		t_app_new		(void);
TApp*		t_app_get		(void);

gboolean	t_app_open_term		(TApp        *app,
					 const gchar *command);

void		t_app_add_fore_child	(TApp        *app,
					 GPid         pid);

void		t_app_keyseq_set	(TApp        *app,
					 gint         key,
					 const gchar *sequences,
					 gsize        length);
void		t_app_keyseq_unset	(TApp        *app,
					 gint         key);
void		t_app_keyseq_clear	(TApp        *app);

G_END_DECLS

#endif /* __T_APP_H__ */
