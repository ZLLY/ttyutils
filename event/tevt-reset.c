/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glib.h>
#include <ttyutils.h>

#include "tevt.h"

void
t_event_reset(void)
{
  t_rpc_call_remove_event_group ("business");

/*
  if (!t_rpc_call_insert_event ("business", "default", BUSINESS_MATCHER,
			  "/dcc/ttyutils/bin/ttyevt --eventno=1 "
			  "--tty=@TTYEXEC_TTY@ --logfile="LOGFILE))
    {
      g_warning ("Unable to insert `business' event.");
      return;
    }
*/
}
