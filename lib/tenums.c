/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Generated data (by glib-mkenums) */

#include <glib-object.h>


/* enumerations from "ttypes.h" */
#include "ttypes.h"

GType
t_rpc_code_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { RPC_CODE_SNAP, "RPC_CODE_SNAP", "snap" },
      { RPC_CODE_FEED, "RPC_CODE_FEED", "feed" },
      { RPC_CODE_GET_EVENTS, "RPC_CODE_GET_EVENTS", "get-events" },
      { RPC_CODE_INSERT_EVENT, "RPC_CODE_INSERT_EVENT", "insert-event" },
      { RPC_CODE_REMOVE_EVENT, "RPC_CODE_REMOVE_EVENT", "remove-event" },
      { RPC_CODE_REMOVE_EVENT_GROUP, "RPC_CODE_REMOVE_EVENT_GROUP", "remove-event-group" },
      { RPC_CODE_STDIN_SUSPEND, "RPC_CODE_STDIN_SUSPEND", "stdin-suspend" },
      { RPC_CODE_STDIN_RESUME, "RPC_CODE_STDIN_RESUME", "stdin-resume" },
      { RPC_CODE_STDOUT_SUSPEND, "RPC_CODE_STDOUT_SUSPEND", "stdout-suspend" },
      { RPC_CODE_STDOUT_RESUME, "RPC_CODE_STDOUT_RESUME", "stdout-resume" },
      { RPC_CODE_TRIGGER_EVENT, "RPC_CODE_TRIGGER_EVENT", "trigger-event" },
      { RPC_CODE_CURSOR_XY, "RPC_CODE_CURSOR_XY", "cursor-xy" },
      { RPC_CODE_REFRESH, "RPC_CODE_REFRESH", "refresh" },
      { RPC_CODE_SET_ENV, "RPC_CODE_SET_ENV", "set-env" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TRpcCode", values);
  }

  return etype;
}


GType
t_rpc_errno_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { RPC_ERR_OK, "RPC_ERR_OK", "ok" },
      { RPC_ERR_BADCODE, "RPC_ERR_BADCODE", "badcode" },
      { RPC_ERR_BADSIZE, "RPC_ERR_BADSIZE", "badsize" },
      { RPC_ERR_FORMAT, "RPC_ERR_FORMAT", "format" },
      { RPC_ERR_TIMEOUT, "RPC_ERR_TIMEOUT", "timeout" },
      { RPC_ERR_EOF, "RPC_ERR_EOF", "eof" },
      { RPC_ERR_SYSTEM, "RPC_ERR_SYSTEM", "system" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TRpcErrno", values);
  }

  return etype;
}


GType
t_event_type_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { EVENT_TYPE_CURSOR, "EVENT_TYPE_CURSOR", "cursor" },
      { EVENT_TYPE_CONTENT, "EVENT_TYPE_CONTENT", "content" },
      { EVENT_TYPE_HOTKEY, "EVENT_TYPE_HOTKEY", "hotkey" },
      { EVENT_TYPE_KEYBOARD, "EVENT_TYPE_KEYBOARD", "keyboard" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TEventType", values);
  }

  return etype;
}


/* enumerations from "tshmem.h" */
#include "tshmem.h"

GType
t_shmem_type_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { SHMEM_TYPE_POSIX, "SHMEM_TYPE_POSIX", "posix" },
      { SHMEM_TYPE_SYSV, "SHMEM_TYPE_SYSV", "sysv" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TShmemType", values);
  }

  return etype;
}


GType
t_sheme_entry_flags_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GFlagsValue values[] = {
      { SHMEM_FLAG_NOEVENT, "SHMEM_FLAG_NOEVENT", "noevent" },
      { 0, NULL, NULL }
    };

    etype = g_flags_register_static("TShemeEntryFlags", values);
  }

  return etype;
}


/* enumerations from "trpccall.h" */
#include "trpccall.h"

GType
t_rpc_target_type_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { RPC_TARGET_TTY, "RPC_TARGET_TTY", "tty" },
      { RPC_TARGET_PTS, "RPC_TARGET_PTS", "pts" },
      { RPC_TARGET_PID, "RPC_TARGET_PID", "pid" },
      { RPC_TARGET_PATH, "RPC_TARGET_PATH", "path" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TRpcTargetType", values);
  }

  return etype;
}


/* enumerations from "ttermop.h" */
#include "ttermop.h"

GType
t_term_char_attr_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GEnumValue values[] = {
      { CHAR_ATTR_NORMAL, "CHAR_ATTR_NORMAL", "normal" },
      { CHAR_ATTR_BOLD, "CHAR_ATTR_BOLD", "bold" },
      { CHAR_ATTR_UNDERLINED, "CHAR_ATTR_UNDERLINED", "underlined" },
      { CHAR_ATTR_BLINK, "CHAR_ATTR_BLINK", "blink" },
      { CHAR_ATTR_INVERSE, "CHAR_ATTR_INVERSE", "inverse" },
      { CHAR_ATTR_INVISIBLE, "CHAR_ATTR_INVISIBLE", "invisible" },
      { CHAR_ATTR_NORMAL2, "CHAR_ATTR_NORMAL2", "normal2" },
      { CHAR_ATTR_NOTUNDERLINED, "CHAR_ATTR_NOTUNDERLINED", "notunderlined" },
      { CHAR_ATTR_STEADY, "CHAR_ATTR_STEADY", "steady" },
      { CHAR_ATTR_POSITIVE, "CHAR_ATTR_POSITIVE", "positive" },
      { CHAR_ATTR_VISIBLE, "CHAR_ATTR_VISIBLE", "visible" },
      { CHAR_ATTR_FORE_BLACK, "CHAR_ATTR_FORE_BLACK", "fore-black" },
      { CHAR_ATTR_FORE_RED, "CHAR_ATTR_FORE_RED", "fore-red" },
      { CHAR_ATTR_FORE_GREEN, "CHAR_ATTR_FORE_GREEN", "fore-green" },
      { CHAR_ATTR_FORE_YELLOW, "CHAR_ATTR_FORE_YELLOW", "fore-yellow" },
      { CHAR_ATTR_FORE_BLUE, "CHAR_ATTR_FORE_BLUE", "fore-blue" },
      { CHAR_ATTR_FORE_MAGENTA, "CHAR_ATTR_FORE_MAGENTA", "fore-magenta" },
      { CHAR_ATTR_FORE_CYAN, "CHAR_ATTR_FORE_CYAN", "fore-cyan" },
      { CHAR_ATTR_FORE_WHITE, "CHAR_ATTR_FORE_WHITE", "fore-white" },
      { CHAR_ATTR_FORE_DEFAULT, "CHAR_ATTR_FORE_DEFAULT", "fore-default" },
      { CHAR_ATTR_BACK_BLACK, "CHAR_ATTR_BACK_BLACK", "back-black" },
      { CHAR_ATTR_BACK_RED, "CHAR_ATTR_BACK_RED", "back-red" },
      { CHAR_ATTR_BACK_GREEN, "CHAR_ATTR_BACK_GREEN", "back-green" },
      { CHAR_ATTR_BACK_YELLOW, "CHAR_ATTR_BACK_YELLOW", "back-yellow" },
      { CHAR_ATTR_BACK_BLUE, "CHAR_ATTR_BACK_BLUE", "back-blue" },
      { CHAR_ATTR_BACK_MAGENTA, "CHAR_ATTR_BACK_MAGENTA", "back-magenta" },
      { CHAR_ATTR_BACK_CYAN, "CHAR_ATTR_BACK_CYAN", "back-cyan" },
      { CHAR_ATTR_BACK_WHITE, "CHAR_ATTR_BACK_WHITE", "back-white" },
      { CHAR_ATTR_BACK_DEFAULT, "CHAR_ATTR_BACK_DEFAULT", "back-default" },
      { 0, NULL, NULL }
    };

    etype = g_enum_register_static("TTermCharAttr", values);
  }

  return etype;
}


GType
t_term_button_get_type(void)
{
  static GType etype = 0;

  if (etype == 0) {
    static const GFlagsValue values[] = {
      { TButtonBad, "TButtonBad", "bad" },
      { TButtonYes, "TButtonYes", "yes" },
      { TButtonNo, "TButtonNo", "no" },
      { TButtonClose, "TButtonClose", "close" },
      { TButtonOk, "TButtonOk", "ok" },
      { TButtonCancel, "TButtonCancel", "cancel" },
      { TButtonSubmit, "TButtonSubmit", "submit" },
      { 0, NULL, NULL }
    };

    etype = g_flags_register_static("TTermButton", values);
  }

  return etype;
}

#define __T_ENUMS_C__


/* Generated data ends here */

