dnl Process this file with autoconf to produce a configure script.
dnl
dnl Ttyutils -- UNIX Terminal Utilities.
dnl Copyright (C) 2008,2009,2010,2011 xiaohu, All Rights Reserved.
dnl
dnl This is free software; you can redistribute it and/or
dnl modify it under the terms of the GNU Library General
dnl Public License as published by the Free Software Foundation;
dnl either version 2 of the License, or (at your option) any
dnl later version.
dnl
dnl This program is distributed in the hope that it will be
dnl useful, but WITHOUT ANY WARRANTY; without even the implied
dnl warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
dnl See the GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU Library General Public
dnl License along with this program; if not, write to the
dnl Free Software Foundation, Inc.,
dnl 675 Mass Ave, Cambridge, MA 02139, USA.

AC_PREREQ(2.56)

dnl ***************************************************************************
dnl The following version number definitions apply to ttyutils,
dnl so if changes occoured in any of them, they are all
dnl treated with the same interface and binary age.
dnl
dnl Making releases:
dnl   micro_version += 1;
dnl   interface_age += 1;
dnl   binary_age += 1;
dnl if any functions have been added, set interface_age to 0.
dnl if backwards compatibility has been broken,
dnl set binary_age _and_ interface_age to 0.
dnl ***************************************************************************
m4_define([major_version], [1])
m4_define([minor_version], [9])
m4_define([micro_version], [2])
m4_define([interface_age], [0])
m4_define([binary_age], [m4_eval(100 * minor_version + micro_version)])
m4_define([ttyutils_version], [major_version.minor_version.micro_version])

dnl ***************************************************************************
dnl libtool version related macros
dnl ***************************************************************************
m4_define([lt_release], [major_version.minor_version])
m4_define([lt_current], [m4_eval(100 * minor_version + micro_version - interface_age)])
m4_define([lt_revision], [interface_age])
m4_define([lt_age], [m4_eval(binary_age - interface_age)])
m4_define([lt_current_minus_age], [m4_eval(lt_current - lt_age)])
dnl if the minor version number is odd, then we want debugging.  Otherwise
dnl we only want minimal debugging support.
dnl m4_define([debug_default], [m4_if(m4_eval(minor_version % 2), [1], [yes], [minimum])])dnl

AC_INIT(ttyutils, [ttyutils_version], [xiaohu417@gmail.com])
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AM_CONFIG_HEADER(config.h)
AC_CONFIG_SRCDIR([exec/texec.c])
AM_MAINTAINER_MODE

dnl ***************************************************************************
dnl checks standard compatible
dnl ***************************************************************************
AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC

dnl ***************************************************************************
dnl output version information
dnl ***************************************************************************
MAJOR_VERSION=major_version
MINOR_VERSION=minor_version
MICRO_VERSION=micro_version
INTERFACE_AGE=interface_age
BINARY_AGE=binary_age
VERSION=ttyutils_version

AC_SUBST(MAJOR_VERSION)
AC_SUBST(MINOR_VERSION)
AC_SUBST(MICRO_VERSION)
AC_SUBST(VERSION)
AC_SUBST(INTERFACE_AGE)
AC_SUBST(BINARY_AGE)

AC_DEFINE(MAJOR_VERSION, [major_version], [Define to the major version])
AC_DEFINE(MINOR_VERSION, [minor_version], [Define to the minor version])
AC_DEFINE(MICRO_VERSION, [micro_version], [Define to the micro version])
AC_DEFINE(INTERFACE_AGE, [interface_age], [Define to the interface age])
AC_DEFINE(BINARY_AGE,    [binary_age],    [Define to the binary age])

LT_VERSION_INFO="lt_current:lt_revision:lt_age"
LT_VERSION_MINUS_AGE="lt_current_minus_age"
AC_SUBST(LT_VERSION_INFO)
AC_SUBST(LT_VERSION_MINUS_AGE)

dnl ***************************************************************************
dnl website
dnl ***************************************************************************
AC_DEFINE(PACKAGE_WEBSITE, ["http://www.ttyutils.net/"],
	  [Define to the ttyutils web site])

dnl ***************************************************************************
dnl Internatinalization
dnl ***************************************************************************
GETTEXT_PACKAGE=ttyutils
AC_SUBST(GETTEXT_PACKAGE)
ALL_LINGUAS="zh_CN"
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [GETTEXT package name])
AM_GLIB_GNU_GETTEXT
AC_CHECK_DECLS(bind_textdomain_codeset,,,[#include "libintl.h"])
AM_GLIB_DEFINE_LOCALEDIR(LOCALEDIR)

IT_PROG_INTLTOOL([0.35.0])
AM_PROG_LIBTOOL


dnl ***************************************************************************
dnl Search for the required modules.
dnl ***************************************************************************
PKG_CHECK_MODULES(GLIB, [glib-2.0 > 2.6.0 gobject-2.0],
		  [LIBS="$LIBS $GLIB_LIBS" CFLAGS="$CFLAGS $GLIB_CFLAGS"])
#		  AC_MSG_ERROR(Cannot find GLib: do you have the GLib development packages installed?))
# PKG_CHECK_MODULES(GNET, gnet-2.0 > 2.0.0,
# 		  [LIBS="$LIBS $GNET_LIBS" CFLAGS="$CFLAGS $GNET_CFLAGS"],
# 		  AC_MSG_ERROR(Cannot find GNet: do you have the GNet development packages installed?))

dnl ***************************************************************************
dnl Check for headers.
dnl ***************************************************************************
AC_CHECK_HEADERS(sys/select.h sys/syslimits.h sys/termios.h sys/un.h sys/sockio.h sys/wait.h)
AC_CHECK_HEADERS(stropts.h termios.h wchar.h pty.h util.h libutil.h ttyent.h)
AC_CHECK_HEADERS(readline/readline.h readline/history.h)
AC_HEADER_TIOCGWINSZ

AC_CHECK_LIB(c,grantpt,true,[AC_CHECK_LIB(pt,grantpt)])
AC_CHECK_LIB(c,openpty,true,[AC_CHECK_LIB(util,openpty)])
AC_CHECK_LIB(rt,shm_open,[LIBS="$LIBS -lrt"])
AC_CHECK_LIB(readline,readline,[AC_DEFINE(HAVE_READLINE,1,[Defined if readline is usable.]) READLINE_LIBS="-lreadline"])
AC_SUBST(READLINE_LIBS)

dnl ***************************************************************************
dnl Check for PTY handling functions.
dnl ***************************************************************************
AC_CHECK_FUNCS([cfmakeraw fork setsid setpgid setpgrp getpgid getpgrp])
AC_CHECK_FUNCS([getpt grantpt unlockpt ptsname ptsname_r tcgetattr tcsetattr])
AC_CHECK_FUNCS([forkpty getttyent getutent getutmax login_tty openpty revoke daemon])

dnl ***************************************************************************
dnl Pull in the right libraries for various functions which might not be
dnl bundled into an exploded libc.
dnl ***************************************************************************
AC_CHECK_FUNC(socket, [have_socket=1],
	      AC_CHECK_LIB(socket, socket, [have_socket=1; LIBS="$LIBS -lsocket"]))
AC_CHECK_FUNC(socketpair, [have_socketpair=1],
	      AC_CHECK_LIB(socket, socketpair, [have_socketpair=1; LIBS="$LIBS -lsocket"]))
AC_CHECK_FUNC(recvmsg, [have_recvmsg=1],
	      AC_CHECK_LIB(socket, recvmsg, [have_recvmsg=1; LIBS="$LIBS -lsocket -lnsl"]))
if test x$have_socket = x1 ; then
	AC_DEFINE(HAVE_SOCKET, 1, [Define if you have the socket function.])
fi
if test x$have_socketpair = x1 ; then
	AC_DEFINE(HAVE_SOCKETPAIR, 1, [Define if you have the socketpair function.])
fi
if test x$have_recvmsg = x1 ; then
	AC_DEFINE(HAVE_RECVMSG, 1, [Define if you have the recvmsg function.])
fi
AC_CHECK_FUNC(floor,, AC_CHECK_LIB(m, floor, LIBS=["$LIBS -lm"]))
AC_CHECK_FUNCS([ceil floor])

dnl ***************************************************************************
dnl Look for ncurses or curses.
dnl ***************************************************************************
AC_ARG_ENABLE(ncursesw,
	      AC_HELP_STRING([--enable-ncursesw], [use wide-character ncurses @<:@default=yes@:>@]),
	      [case "${enableval}" in
	        yes) enable_ncursesw="yes" ;;
	        no)  enable_ncursesw="no" ;;
	        *)   AC_MSG_ERROR(bad value ${enableval} for --enable-ncursesw) ;;
       	      esac], [enable_ncursesw="yes"])
if test x$enable_ncursesw = xyes ; then
	AC_CHECK_LIB(ncursesw, initscr, [CURSES_LIBS="-lcdkw -lncursesw";
					 AC_DEFINE(HAVE_NCURSESW, 1, [Define if you have libncursesw])])
	if test x$ac_cv_lib_ncursesw_initscr != xyes ; then
		AC_MSG_ERROR([libncursesw not found.])
	fi
else
	AC_CHECK_LIB(ncurses, initscr, [CURSES_LIBS="-lcdk -lncurses";
					AC_DEFINE(HAVE_NCURSES, 1, [Define if you have libncurses])])
	if test x$ac_cv_lib_ncurses_initscr != xyes ; then
		AC_CHECK_LIB(curses, initscr, [CURSES_LIBS="-lcdk -lcurses";
					       AC_DEFINE(HAVE_CURSES,1,[Define if you have libcurses])])
		if test x$ac_cv_lib_curses_initscr != xyes ; then
			AC_MSG_ERROR([You must have at least one of ncurses, curses installed to build.])
		fi
	fi
fi
AC_SUBST(CURSES_LIBS)

wcs_funcs_includes="
#ifdef HAVE_STRING_H
# if !STDC_HEADERS && HAVE_MEMORY_H
#  include <memory.h>
# endif
# include <string.h>
#else
# ifdef HAVE_STRINGS_H
#  include <strings.h>
# endif
#endif
#ifdef HAVE_WCHAR_H
# include <wchar.h>
#endif
"
AC_CHECK_TYPES(wint_t, AC_DEFINE(HAVE_WINT_T, ,
	       [Defined when the wint_t type is supported]), ,$wcs_funcs_includes)

dnl ***************************************************************************
dnl Check if abstract sockets are supported
dnl ***************************************************************************
AC_LANG_PUSH(C)
AC_CACHE_CHECK([for abstract socket namespace availability],
		ac_cv_have_abstract_sockets,
		[AC_RUN_IFELSE([AC_LANG_PROGRAM(
[[
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
]],
[[
  int listen_fd, len;
  struct sockaddr_un addr;
  
  listen_fd = socket (PF_UNIX, SOCK_STREAM, 0);
  if (listen_fd < 0)
  {
    perror ("socket() failed: ");
    exit (1);
  }
  memset (&addr, '\0', sizeof (addr));
  addr.sun_family = AF_UNIX;
  strcpy (addr.sun_path, "X/tmp/gnet-fake-socket-path-used-in-configure-test");
  len = SUN_LEN (&addr);	/* calculate size before adding the \0 */
  addr.sun_path[0] = '\0';	/* this is what makes it abstract */
  
  if (bind (listen_fd, (struct sockaddr*) &addr, len) < 0)
  {
    perror ("Abstract socket namespace bind() failed: ");
    exit (1);
  }
  exit (0);
]])],
  [ac_cv_have_abstract_sockets=yes],
  [ac_cv_have_abstract_sockets=no]
)])
AC_LANG_POP(C)

if test x$ac_cv_have_abstract_sockets = xyes ; then
   AC_DEFINE(HAVE_ABSTRACT_SOCKETS, 1, [Have abstract socket namespace])
fi

dnl ***************************************************************************
dnl Use all available features under glibc, and disable accidental use of
dnl deprecated functionality.
dnl ***************************************************************************
AC_EGREP_CPP(glibc, [
		     #include <stdio.h>
		     #ifdef __GLIBC__
		     glibc
		     #endif
		     ],
		     AC_DEFINE(_GNU_SOURCE, 1, [Use all glibc features.]))


dnl ***************************************************************************
dnl enable debug
dnl ***************************************************************************
AC_ARG_ENABLE(debug,
	      AC_HELP_STRING([--enable-debug], [turn on debugging @<:@default=yes@:>@]),
	      [case "${enableval}" in
	        yes) enable_debug="yes" ;;
	        no)  enable_debug="no" ;;
	        *)   AC_MSG_ERROR(bad value ${enableval} for --enable-debug) ;;
       	      esac], [enable_debug="yes"])
# if test x$enable_debug = xyes ; then
#   AC_DEFINE(ENABLE_DEBUG, [1], [Define to turn on debug])
#   AC_CHECK_HEADER(dmalloc.h, [have_dmalloc="yes"], [have_dmalloc="no"])
#   if test x$have_dmalloc = xyes ; then
#     AC_SUBST(DMALLOC_CFLAGS, "-DDMALLOC -DDMALLOC_FUNC_CHECK")
#     AC_SUBST(DMALLOC_LIBS, "-ldmallocth")
#   else
#     AC_SUBST(DMALLOC_CFLAGS, "-DDMALLOC_DISABLE")
#     AC_SUBST(DMALLOC_LIBS, "")
#   fi
# else
#   AC_SUBST(DMALLOC_CFLAGS, "-DDMALLOC_DISABLE")
#   AC_SUBST(DMALLOC_LIBS, "")
# fi
AM_CONDITIONAL(ENABLE_DEBUG, test x$enable_debug = xyes)

dnl ***************************************************************************
dnl enable manual
dnl ***************************************************************************
AC_ARG_ENABLE(manual,
	      AC_HELP_STRING([--enable-manual], [turn on manual @<:@default=yes@:>@]),
	      [case "${enableval}" in
	        yes) enable_manual="yes" ;;
	        no)  enable_manual="no" ;;
	        *)   AC_MSG_ERROR(bad value ${enableval} for --enable-manual) ;;
       	      esac], [enable_manual="yes"])
AM_CONDITIONAL(ENABLE_MANUAL, test x$enable_manual = xyes)

dnl ***************************************************************************
dnl Miscellaneous definitions.
dnl ***************************************************************************
AC_DEFINE_UNQUOTED(PACKAGE,"$PACKAGE",[Package name.])
AC_DEFINE(T_INVALID_SOURCE,(guint)-1,[A number which can never be a valid source ID.])
AC_DEFINE(T_UTF8_BPC,6,[Maximum number of bytes used per UTF-8 character.])

dnl ***************************************************************************
dnl output files
dnl ***************************************************************************
AC_CONFIG_FILES([Makefile
		 lib/Makefile
		 exec/Makefile
		 tools/Makefile
		 event/Makefile
		 po/Makefile.in
		 ])
# if test x$enable_manual = xyes ; then
#	AC_CONFIG_FILES([manual/Makefile])
# fi
AC_CONFIG_FILES([ttyutils.spec
		 ttyutils-1.pc
		 public.sh
		 exec/ttyexec.lua
		 exec/ttyutils.lua
		 exec/keyseq.lua
		 ])
AC_OUTPUT
