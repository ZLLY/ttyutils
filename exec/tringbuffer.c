/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <string.h>

#include <glib/gi18n.h>

#include "tringbuffer.h"

#if GLIB_MINOR_VERSION < 10
#define g_slice_new0(t)		g_new0 (t, 1)
#define g_slice_free(t,p)	g_free (p)
#endif

#define RING_CHUNK_SIZE		(4096 - sizeof (char*) * 2)
#define RING_MAX_CHUNKS		 1280  /* ~5M */

typedef struct _TRingChunk	TRingChunk;
struct _TRingChunk
{
  gchar  buffer[RING_CHUNK_SIZE];
  gchar *read_ptr;
  gchar *write_ptr;
};

struct _TRingBuffer
{
  GList      *chunks;
  TRingChunk *read_chunk;
  TRingChunk *write_chunk;
  TRingChunk *cache_chunk;
  gint        total_size;
};

static gint	t_ring_buffer_offset		(TRingBuffer *ring, gint size,
						 gchar *str, gint tomatch);
static gint	t_ring_buffer_line_size		(TRingBuffer *ring, gint size);

/**
 * t_ring_buffer_new:
 */
TRingBuffer*
t_ring_buffer_new (void)
{
  TRingBuffer *ring;
  TRingChunk *chunk;

  ring = g_slice_new0 (TRingBuffer);

  chunk = g_slice_new0 (TRingChunk);
  chunk->read_ptr = chunk->write_ptr = chunk->buffer;

  ring->chunks = g_list_append (ring->chunks, chunk);
  ring->read_chunk = ring->write_chunk = chunk;

  return ring;
}

/** 
 * t_ring_buffer_free_chunk:
 */
static void
t_ring_buffer_free_chunk (gpointer data, gpointer user_data)
{
  TRingChunk *chunk = (TRingChunk*) data;

  g_slice_free (TRingChunk, chunk);
}

/**
 * t_ring_buffer_free:
 */
void
t_ring_buffer_free (TRingBuffer *ring)
{
  if (ring)
    {
      if (ring->chunks)
	{
	  g_list_foreach (ring->chunks, t_ring_buffer_free_chunk, NULL);
	  g_list_free (ring->chunks);
	}
      if (ring->cache_chunk)
	{
	  g_slice_free (TRingChunk, ring->cache_chunk);
	}
      g_slice_free (TRingBuffer, ring);
    }
}

/** 
 * t_ring_buffer_clear:
 */
void
t_ring_buffer_clear (TRingBuffer *ring)
{
  TRingChunk *chunk;

  g_return_if_fail (ring);

  if (ring->chunks)
    {
      g_list_foreach (ring->chunks, t_ring_buffer_free_chunk, NULL);
      g_list_free (ring->chunks);
      ring->chunks = NULL;
    }

  if (ring->cache_chunk)
    {
      chunk = ring->cache_chunk;
      ring->cache_chunk = NULL;
    }
  else
    {
      chunk = g_slice_new0 (TRingChunk);
    }
  chunk->read_ptr = chunk->write_ptr = chunk->buffer;

  ring->read_chunk = ring->write_chunk = chunk;
  ring->chunks = g_list_append (ring->chunks, chunk);
  ring->total_size = 0;
}

/** 
 * t_ring_buffer_n_chunk:
 */
gint
t_ring_buffer_n_chunk (TRingBuffer *ring)
{
  g_return_val_if_fail (ring, -1);

  return g_list_length (ring->chunks);
}

/**
 * t_ring_buffer_size:
 */
gint
t_ring_buffer_size (TRingBuffer *ring)
{
  g_return_val_if_fail (ring, -1);
  return ring->total_size;
}

/** 
 * t_ring_buffer_can_write:
 */
gboolean
t_ring_buffer_can_write (TRingBuffer *ring, int size)
{
  gint chunks, remainder, writable;

  g_return_val_if_fail (ring, FALSE);

  chunks = size / RING_CHUNK_SIZE;
  remainder = size % RING_CHUNK_SIZE;

  writable = RING_CHUNK_SIZE - (ring->write_chunk->write_ptr -
				ring->write_chunk->buffer);
  if (remainder > writable)
    chunks += 1;

  if (g_list_length (ring->chunks) + chunks > RING_MAX_CHUNKS)
    {
      return FALSE;
    }
  return TRUE;
}

/**
 * t_ring_buffer_write:
 */
gint
t_ring_buffer_write (TRingBuffer *ring, const gchar *data, gint size)
{
  gchar *writep;
  const gchar *datap;
  gint size_to_write, remain, bytes;

  g_return_val_if_fail (ring, -1);
  g_return_val_if_fail (data, -1);

  g_assert (ring->write_chunk);

  if (size < 0)
    size = strlen (data);

  if (!t_ring_buffer_can_write (ring, size))
    return -1;

  datap = data;
  size_to_write = size;

  while (size > 0)
    {
      writep = ring->write_chunk->write_ptr;
      remain = RING_CHUNK_SIZE - (writep - ring->write_chunk->buffer);
      bytes = MIN (size, remain);
      memcpy (writep, datap, bytes);
      size -= bytes;
      datap += bytes;

      ring->write_chunk->write_ptr += bytes;

      if (ring->write_chunk->write_ptr == ring->write_chunk->buffer + RING_CHUNK_SIZE)
	{
	  TRingChunk *chunk;

	  if (ring->cache_chunk)
	    {
	      chunk = ring->cache_chunk;
	      ring->cache_chunk = NULL;
	    }
	  else
	    {
	      chunk = g_slice_new0 (TRingChunk);
	    }
	  chunk->read_ptr = chunk->write_ptr = chunk->buffer;
	  ring->chunks = g_list_append (ring->chunks, chunk);
	  ring->write_chunk = chunk;
	}
    }
  ring->total_size += size_to_write;

  return size_to_write;
}

/** 
 * t_ring_buffer_can_read:
 */
gboolean
t_ring_buffer_can_read (TRingBuffer *ring, gint size)
{
  g_return_val_if_fail (ring, FALSE);

  return ring->total_size >= size;
}

/** 
 * t_ring_buffer_read:
 */
gint
t_ring_buffer_read (TRingBuffer *ring, gchar *buff, gint size)
{
  gchar *readp, *buffp;
  gint size_to_read, remain, bytes;

  g_return_val_if_fail (ring, -1);
  g_return_val_if_fail (size >= 0, -1);

  size_to_read = size = MIN (ring->total_size, size);
  buffp = buff;

  while (size > 0)
    {
      readp = ring->read_chunk->read_ptr;
      remain = ring->read_chunk->write_ptr - ring->read_chunk->read_ptr;
      bytes = MIN (remain, size);
      if (buffp)
	{
	  memcpy (buffp, readp, bytes);
	  buffp += bytes;
	}
      size -= bytes;

      ring->read_chunk->read_ptr += bytes;

      if (ring->read_chunk->read_ptr == ring->read_chunk->buffer + RING_CHUNK_SIZE)
	{
	  g_assert (ring->chunks->next);

	  ring->chunks = g_list_remove (ring->chunks, ring->read_chunk);

	  if (!ring->cache_chunk)
	    {
	      ring->cache_chunk = ring->read_chunk;
	    }
	  else
	    {
	      g_slice_free (TRingChunk, ring->read_chunk);
	    }
	  ring->read_chunk = (TRingChunk*) ring->chunks->data;
	}
    }
  ring->total_size -= size_to_read;

  return size_to_read;
}

/** 
 * t_ring_buffer_offset:
 */
gint
t_ring_buffer_offset (TRingBuffer *ring, gint size, gchar *str, gint tomatch)
{
  gchar *readp;
  GList *node;
  gint nret = 0;
  gint nmatch;

  g_return_val_if_fail (ring, -1);
  g_return_val_if_fail (str, -1);
  g_return_val_if_fail (size > 0, -1);

  node = g_list_find (ring->chunks, ring->read_chunk);
  g_assert (node);

  if (tomatch < 0)
    {
      tomatch = strlen (str);
    }
  nmatch = 0;

  for (; node && nret < size; node = node->next)
    {
      TRingChunk *chunk = (TRingChunk*) node->data;

      for (readp = chunk->read_ptr; readp < chunk->write_ptr && nret < size;
	   readp++, nret++)
	{
	  if (str[nmatch] == *readp)
	    {
	      if (++nmatch == tomatch)
		{
		  return nret - nmatch + 1;
		}
	    }
	  else
	    {
	      nmatch = 0;
	    }
	}
    }
  return -1;
}

/** 
 * t_ring_buffer_line_size:
 */
gint
t_ring_buffer_line_size (TRingBuffer *ring, gint size)
{
  gint nret;

  /* if terminal transfer `\r\n' as newline character sequence,
   * set IGNCR flag of terminal input mode.
   * if terminal transfer `\r' as newline character,
   * set ICRNL flag of terminal input mode.
   * if terminal transfer `\n' as newline character,
   * don't set INLCR flag of terminal input mode.
   */
  nret = t_ring_buffer_offset (ring, size, "\n", 1);

  /* include `\n' itself
   */
  return (nret > 0) ?  nret + 1 : nret;
}

/** 
 * t_ring_buffer_can_read_line:
 */
gboolean
t_ring_buffer_can_read_line (TRingBuffer *ring, int size)
{
  return t_ring_buffer_line_size (ring, size) > 0;
}

/** 
 * t_ring_buffer_read_line:
 */
gint
t_ring_buffer_read_line (TRingBuffer *ring, gchar *buff, gint size)
{
  gint length;

  length = t_ring_buffer_line_size (ring, size);
  if (length > 0)
    {
      return t_ring_buffer_read (ring, buff, length);
    }
  return -1;
}

/**
 * t_ring_buffer_read_to:
 */
gint
t_ring_buffer_read_to (TRingBuffer *ring, gchar *buff, gint atleast,
		       gint maxsize, gchar *str, gint nstr)
{
  gint nread, nret = 0;

  g_return_val_if_fail (ring, -1);
  g_return_val_if_fail (atleast >= 0, -1);

  if (atleast >= maxsize)
    {
      g_warning (_("arguments of ring_buffer_read_to() incorrect, atleast %d must "
		   "less than or equal maxsize %d"), atleast, maxsize);
      return -1;
    }

  if (atleast > 0)
    {
      nret = t_ring_buffer_read (ring, buff, atleast);
      if (nret < atleast)
	return nret;
    }
  if (!str)
    return nret;

  nread = t_ring_buffer_offset (ring, maxsize - atleast, str, nstr);
  if (nread >= 0)
    {
      nread = t_ring_buffer_read (ring, buff, nread);
      if (nread > 0)
	nret += nread;
    }
  return nret;
}

/** 
 * t_ring_buffer_read_with:
 */
gint
t_ring_buffer_read_with (TRingBuffer *ring, gchar *buff, gint atleast,
			 gint maxsize, gchar *str, gint nstr)
{
  gint nread, nret = 0;

  g_return_val_if_fail (ring, -1);
  g_return_val_if_fail (atleast >= 0, -1);

  if (atleast >= maxsize)
    {
      g_warning (_("arguments of ring_buffer_read_with() incorrect, atleast %d must "
		   "less than or equal maxsize %d"), atleast, maxsize);
      return -1;
    }

  if (atleast > 0)
    {
      nret = t_ring_buffer_read (ring, buff, atleast);
      if (nret < atleast)
	return nret;
    }
  if (!str)
    return nret;

  nread = t_ring_buffer_offset (ring, maxsize - atleast, str, nstr);
  if (nread >= 0)
    {
      if (nstr < 0)
	{
	  nstr = strlen (str);
	}
      nread = t_ring_buffer_read (ring, buff, nread + nstr);
      if (nread > 0)
	nret += nread;
    }
  return nret;
}

/** 
 * t_ring_buffer_discard_to:
 */
gint
t_ring_buffer_discard_to (TRingBuffer *ring, gint atleast,
			  gint maxsize, gchar *str, gint nstr)
{
  g_return_val_if_fail (ring, -1);

  return t_ring_buffer_read_to (ring, NULL, atleast, maxsize, str, nstr);
}

/** 
 * t_ring_buffer_discard_with:
 */
gint
t_ring_buffer_discard_with (TRingBuffer *ring, gint atleast,
			    gint maxsize, gchar *str, gint nstr)
{
  g_return_val_if_fail (ring, -1);

  return t_ring_buffer_read_with (ring, NULL, atleast, maxsize, str, nstr);
}
