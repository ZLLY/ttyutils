/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#include "config.h"

#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <pwd.h>
#include <signal.h>
#include <string.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <ttyutils.h>

#include "tlua.h"
#include "tlookd.h"
#include "trpcd.h"
#include "tapp.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef struct termios		termios_t;

/* save terminal originality state.
 */
termios_t	 	 t_terminal_state;

static FILE		*t_logfile = NULL;

/* save options value.
 */
static gboolean     	  opt_version = FALSE;
static gchar     	 *opt_configure = NULL;
static gboolean     	  opt_nolookd = FALSE;
static gboolean     	  opt_noevent = FALSE;
static gchar     	 *opt_logfile = NULL;
static gchar     	 *opt_luapath = NULL;
static gchar     	 *opt_ptyname = NULL;

static GOptionEntry option_entries[] = 
{
    { "version", 'v', 0, G_OPTION_ARG_NONE, &opt_version,
      N_("Output version information and exit"), NULL },

    { "configure", 'c', 0, G_OPTION_ARG_STRING, &opt_configure,
      N_("Specify a configure script file instead default"), "file" },

    { "nolookd", 'k', 0, G_OPTION_ARG_NONE, &opt_nolookd,
      N_("Disable ttylook connect"), NULL },

    { "noevent", 'e', 0, G_OPTION_ARG_NONE, &opt_noevent,
      N_("Disable event check and trigger"), NULL },

    { "logfile", 'l', 0, G_OPTION_ARG_STRING, &opt_logfile,
      N_("Log message to file"), "file" },

    { "luapath", 'p', 0, G_OPTION_ARG_STRING, &opt_luapath,
      N_("Set lua extensions path"), "path" },

    { "ptyname", 'n', 0, G_OPTION_ARG_STRING, &opt_ptyname,
      N_("Specify pty name to open"), "name" },

    { NULL, }
};

/* main loop object.
 */
static GMainLoop	*t_main_loop = NULL;


/** 
 * t_reset_terminal:
 */
static void
t_reset_terminal (void)
{
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &t_terminal_state);
}

/** 
 * t_setup_terminal:
 * 
 * @Returns: TRUE if success.
 */
gboolean
t_setup_terminal (void)
{
  termios_t state;

  if (!isatty (STDOUT_FILENO))
    {
      g_warning (_("STDIN_FILENO does not refer a terminal"));
      return FALSE;
    }

  /* save default terminal attributes.
   */
  tcgetattr (STDIN_FILENO, &state);

  t_terminal_state = state;

  /* change terminal attribute.
   */
  //state.c_iflag &= ~(ICRNL | INPCK | ISTRIP | IXON | BRKINT);
  state.c_iflag &= ~(ICRNL | INPCK | ISTRIP | BRKINT);
  state.c_iflag |= IXON;
  state.c_lflag &= ~(ICANON | IEXTEN | ISIG | ECHO);
  state.c_oflag &= ~OPOST;
  state.c_cflag |= CS8;
  state.c_cflag |= CREAD;

  state.c_cc[VMIN]  = 1;
  state.c_cc[VTIME] = 0;

  tcsetattr (STDIN_FILENO, TCSAFLUSH, &state);

  g_atexit (t_reset_terminal);

  return TRUE;
}

/** 
 * t_sigterm_handler:
 * @signo 
 */
static void
t_sigterm_handler (gint signo)
{
  t_main_quit ();
}

/** 
 * t_sigpipe_handler:
 * @signo 
 */
static void
t_sigpipe_handler (gint signo)
{
  t_println ("sigpipe trigger.");
  t_main_quit ();
}

/** 
 * t_sighup_handler:
 * @signo:
 *
 * trigger when terminal disconnect.
 */
static void
t_sighup_handler (gint signo)
{
  t_println ("sighup trigger.");
  t_main_quit ();
}

static void
t_sigjob_handler (gint signo)
{
  g_warning ("signal %s trigger", strsignal (signo));
}

/** 
 * t_unregister_services:
 */
static void
t_unregister_services (void)
{
  if (t_shmem_open (TRUE))
    {
      t_shmem_remove_entry ();
      t_shmem_close ();
    }
}

/** 
 * t_register_services:
 *
 * register our services to share memory(YP).
 *
 * Returns: TRUE on success.
 */
static gboolean
t_register_services (const gchar *command)
{
  TApp *app = t_app_get ();
  TShmemEntry entry;
  gchar *name;
#ifdef T_DEBUG
  GTimer *timer;
#endif

  if (!app->term)
    {
      g_critical (_("terminal must be created before register services."));
      return FALSE;
    }

#ifdef T_DEBUG
  timer = g_timer_new ();
#endif

  if (!t_shmem_open (TRUE))
    {
      return FALSE;
    }
  memset (&entry, 0, sizeof (entry));

  entry.flags = 0;
  g_strlcpy (entry.tty, ttyname (STDIN_FILENO), sizeof (entry.tty));
  name = t_term_ptsname (app->term);
  if (name)
    {
      g_strlcpy (entry.pts, name, sizeof (entry.pts));
      g_free (name);
    }
  g_strlcpy (entry.command, command, sizeof (entry.command));
  if (!opt_nolookd)
    {
      g_strlcpy (entry.lookpath, t_lookd_get_unixpath (), sizeof (entry.lookpath));
    }
  g_strlcpy (entry.rpcpath, t_rpcd_get_unixpath (), sizeof (entry.rpcpath));

  if (!t_shmem_append_entry (&entry))
    {
      g_warning (_("register services failed."));
      t_shmem_close ();
      return FALSE;
    }
  t_shmem_close ();

#ifdef T_DEBUG
  timer = g_timer_new ();
  t_println ("register services elapsed %f second.",
	     g_timer_elapsed (timer, NULL));
  g_timer_destroy (timer);
#endif

  g_atexit (t_unregister_services);

  return TRUE;
}

/** 
 * t_exec_print_handler:
 * @string:
 */
static void
t_exec_print_handler (const gchar *string)
{
  if (t_logfile)
    {
      fprintf (t_logfile, "%s", string);
      fflush (t_logfile);
    }
}

/** 
 * t_exec_close_logfile:
 */
static void
t_exec_close_logfile (void)
{
  if (t_logfile)
    {
      fclose (t_logfile);
      t_logfile = NULL;
    }
}

/** 
 * main:
 * @Returns: 0 on success.
 */
gint
main (gint argc, gchar **argv)
{
  GOptionContext *opt_ctx;
  GError *error = NULL;
  gchar *command = NULL;
  gchar *path = NULL;
  TApp *app = NULL;
  gchar pid[32];
  lua_State *state = NULL;

  if (!GLIB_CHECK_VERSION (2, 4, 0))
    {
      g_critical ("%s need glib version 2.4.0 or above.", PACKAGE_NAME);
      return FALSE;
    }

#ifdef T_DEBUG
  g_mem_set_vtable (glib_mem_profiler_table);
#endif

  t_set_locale ();

  g_sprintf (pid, "%d", getpid ());
  g_setenv ("TTYUTILS_PID", pid, TRUE);
  g_setenv ("TTYUTILS_TTY", ttyname (STDIN_FILENO), TRUE);

  g_set_prgname (argv[0]);
  g_set_application_name ("ttyexec");

  /* parse command line options
   */
  opt_ctx = g_option_context_new (_("command [args...]"));
#if GLIB_MINOR_VERSION > 12
  g_option_context_set_summary (opt_ctx, _("Execute command at pseudo terminal."));
#endif
  g_option_context_add_main_entries (opt_ctx, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (opt_ctx, &argc, &argv, &error))
    {
      g_option_context_free (opt_ctx);
		
      if (error)
	{
	  g_warning (_("parse command line error: %s"), error->message);
	  g_error_free (error);
	}
      else
	{
	  g_warning (_("An unknown error occurred when parse command line."));
	}
      return 1;
    }
  g_option_context_free (opt_ctx);
  opt_ctx = NULL;

  /* print version info and exit.
   */
  if (opt_version)
    {
      t_print_version ();
      return 0;
    }

  /* check configure file is readable.
   */
  if (opt_configure && access (opt_configure, R_OK) != 0)
    {
      g_warning (_("configure file '%s' is not readable."),
		 opt_configure);
      g_free (opt_configure);
      return 1;
    }

  /* initial gtype object system
   */
#ifdef T_DEBUG
  g_type_init_with_debug_flags (G_TYPE_DEBUG_MASK);
#else
  g_type_init ();
#endif

  /* setup signal handler.
   */
  signal (SIGTERM, t_sigterm_handler);
  signal (SIGPIPE, t_sigpipe_handler);
  signal (SIGHUP, t_sighup_handler);
  signal (SIGUSR1, SIG_IGN);	/* for ttyadmin */
  /* Set the handling for job control signals back to the default.
   */
  signal (SIGINT, t_sigjob_handler);
  signal (SIGQUIT, t_sigjob_handler);
  signal (SIGTSTP, t_sigjob_handler);
  signal (SIGTTIN, t_sigjob_handler);
  signal (SIGTTOU, t_sigjob_handler);
#if 0
  signal (SIGCHLD, t_sigjob_handler);
#endif

  /* redirect all glib message to syslog or file if
   * --logfile is specified.
   */
  if (opt_logfile)
    {
      t_logfile = fopen (opt_logfile, "a+");
      if (!t_logfile)
	{
	  g_warning (_("open %s error: %s"), opt_logfile, g_strerror (errno));
	  return 1;
	}
      g_free (opt_logfile);
      opt_logfile = NULL;

      g_log_set_default_handler (t_debug_log_handler, t_logfile);
      g_atexit (t_exec_close_logfile);

      g_set_print_handler (t_exec_print_handler);
      g_set_printerr_handler (t_exec_print_handler);
    }
  else
    {
      openlog (g_get_prgname (), LOG_PID, LOG_LOCAL3);
      g_log_set_default_handler (t_debug_log_handler, NULL);

      g_set_print_handler (t_debug_print_handler);
      g_set_printerr_handler (t_debug_print_handler);
    }

  if (opt_luapath)
    {
      t_lua_add_path (opt_luapath);
      g_free (opt_luapath);
    }

  /* create TApp object to manage internal object.
   */
  app = t_app_new ();

  /* embed the Lua Interpreter.
   */
  if (!(state = t_lua_open ()))
    {
      g_critical (_("load lua interpreter failed."));
      return 1;
    }

  /* execute system and user configure script.
   * system-wide configure script is optional.
   */
  path = g_build_filename (SYSCONFDIR, PACKAGE_NAME, "ttyexec.lua", NULL);
  if (g_file_test (path, G_FILE_TEST_EXISTS))
    {
      t_println ("execute configure script '%s'", path);

      if (!t_lua_dofile (state, path))
	{
	  g_warning (_("load system configure file failed."));
	  return 1;
	}
    }
  g_free (path);

  if (!(path = opt_configure))
    {
#ifdef T_DEBUG
      path = g_build_filename ("..", "etc", "ttyexec.lua", NULL);
#else
      path = g_build_filename (g_getenv ("HOME"), "."PACKAGE_NAME,
			       "etc", "ttyexec.lua", NULL);
#endif
    }

  /* user configure script is optional too.
   */
  if (g_file_test (path, G_FILE_TEST_EXISTS))
    {
      t_println ("execute configure script '%s'", path);

      if (!t_lua_dofile (state, path))
	{
	  g_warning (_("load user configure file failed."));
	  return 1;
	}
    }
  g_free (path);

  t_lua_close (state);

  /* strip '--' option if has */
  if (argc > 1 && argv[1])
    {
      if (strcmp (argv[1], "--") == 0)
	{
	  argv = &argv[1];
	  argc--;
	}
    }
  /* remained args(if has) will be command and its args.
   */
  if (argc > 1 && argv[1])
    {
      gchar *prgname = NULL;

      if (!g_file_test (argv[1], G_FILE_TEST_IS_EXECUTABLE))
	{
	  prgname = g_find_program_in_path (argv[1]);
	  if (!prgname)
	    {
	      g_warning (_("unable to execute command '%s'."), argv[1]);
	      return -1;
	    }
	  path = argv[1];
	  argv[1] = prgname;
	}
      else
	{
	  /* convert to absolute path name.
	   */
	  if (!g_path_is_absolute (argv[1]))
	    {
	      gchar buff[1024];

	      getcwd (buff, sizeof (buff));
	      prgname = g_build_filename (buff, argv[1], NULL);
	      path = argv[1];
	      argv[1] = prgname;
	    }
	}
      command = g_strjoinv (" ", &argv[1]);

      /* reset argv (not need??).
       */
      if (prgname)
	{
	  argv[1] = path;
	  g_free (prgname);
	}
    }
  else
    {
      struct passwd *pswd;

      pswd = getpwuid (getuid ());

      /* use user's login shell or /bin/sh if missing.
       */
      if (pswd && pswd->pw_shell &&
	  g_file_test (pswd->pw_shell, G_FILE_TEST_IS_EXECUTABLE))
	{
	  command = g_strdup (pswd->pw_shell);
	}
      else
	{
	  command = g_strdup ("/bin/sh");
	}
    }
  t_println ("execute command '%s'", command);

  /* start lookd service if allowed.
   */
  if (!opt_nolookd)
    {
      if (!t_lookd_boot ())
	{
	  g_warning (_("start lookd service failed."));
	  g_object_unref (app);
	  return 1;
	}
    }

  /* start rpcd service.
   */
  if (!t_rpcd_boot ())
    {
      g_warning (_("start rpcd service failed."));
      t_lookd_shut ();
      g_object_unref (app);
      return 1;
    }

  /* change terminal state.
   */
  t_setup_terminal ();

  /* execute command at pseudo terminal.
   */
  if (!t_app_open_term (app, command))
    {
      t_rpcd_shut ();
      t_lookd_shut ();
      g_object_unref (app);
      return 1;
    }

  /* register our services.
   */
  if (!t_register_services (command))
    {
      t_rpcd_shut ();
      t_lookd_shut ();
      g_object_unref (app);
      return 1;
    }
  g_free (command);

  t_main_loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (t_main_loop);

  t_lookd_shut ();
  t_rpcd_shut ();
  g_object_unref (app);

  /* restore terminal state.
   */
  t_reset_terminal ();

  g_free (opt_ptyname);

#ifdef T_DEBUG
  printf (_("Game Over!\n"));
  g_mem_profile ();
#endif

  return 0;
}

/** 
 * t_main_quit:
 *
 * Quit the main loop.
 */
void
t_main_quit (void)
{
  if (!t_main_loop)
    return;

  g_main_loop_quit (t_main_loop);
  t_main_loop = NULL;
}

/** 
 * t_get_opt_ptyname:
 * 
 * @Returns: 
 */
const gchar*
t_get_opt_ptyname (void)
{
  t_println ("option ptyname=%s", opt_ptyname);
  return opt_ptyname;
}

/** 
 * t_set_opt_ptyname:
 * @ptyname 
 */
void
t_set_opt_ptyname (const gchar *ptyname)
{
  g_return_if_fail (ptyname);

  t_println ("pty name set to %s", ptyname);

  g_free (opt_ptyname);
  opt_ptyname = g_strdup (ptyname);
}
