/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/**
 * SECTION: tinit
 * @short_description: initialize the tlib library
 * @see_also: #GMainLoop
 * @stability: Stable
 * @include: ttyutils.h
 *
 * Application use TLib should at least use t_init() to initial the
 * library.
 *
 * the t_main() family functions is a wrapper for GMainLoop.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <pwd.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib-object.h>

#include "tdebug.h"
#include "tenums.h"
#include "tshmem.h"
#include "tinit.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

typedef void (*TInitHookFunc)	(void);


static GMainLoop	*t_main_loop = NULL;

/** 
 * t_set_locale:
 */
void
t_set_locale (void)
{
  const gchar *locale;

#ifdef ENABLE_NLS
  locale = g_getenv (TTYUTILS_LOCALE);
  if (locale)
    {
      g_setenv ("LANG", locale, TRUE);
    }
  setlocale (LC_ALL, "");
#if 0
#ifdef HAVE_BIND_TEXTDOMAIN_CODESET
  if (g_get_charset (NULL))
    {
      bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    }
#endif
#endif
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  textdomain (GETTEXT_PACKAGE);
#endif
}

/** 
 * t_init:
 * @argc: the address of command line arguments count
 * @argv: the address of command line arguments vector
 * 
 * Initial tlib library, application use tlib should call this
 * function before any other tlib's function, note the t_atinit()
 * is a exception, see above.
 *
 * @Returns: TRUE if success.
 */
gboolean
t_init (void)
{
  if (!GLIB_CHECK_VERSION (2, 2, 0))
    {
      g_critical ("TTE need glib version 2.2.0 or above.");
      abort ();
    }

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
# ifdef HAVE_BIND_TEXTDOMAIN_CODESET
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
# endif
#endif  

  /* initial gtype system
   */
#ifdef T_DEBUG
  g_type_init_with_debug_flags (G_TYPE_DEBUG_MASK);
#else
  g_type_init ();
#endif

  return TRUE;
}

/** 
 * t_uninit:
 *
 * Clearup tlib library resources.
 * this function should be call when application exit.
 */
void
t_uninit (void)
{
}

/** 
 * t_main:
 *
 * Create and running main loop.
 */
void
t_main (void)
{
  t_main_loop = g_main_loop_new (NULL, FALSE);

  g_main_loop_run (t_main_loop);
}

/** 
 * t_main_quit:
 *
 * Quit the main loop.
 */
void
t_main_quit (void)
{
  if (!t_main_loop)
    return;

  g_main_loop_quit (t_main_loop);
  t_main_loop = NULL;
}
