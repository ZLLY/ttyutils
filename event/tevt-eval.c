/* Ttyutils  -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glib.h>
#include <ttyutils.h>
#include <sqlite3.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#include "tevt.h"

static gboolean
t_event_get_tty_info (const char *tty, int *termtype, int *port,
		      char *teller, char *org, time_t *last_eval)
{
  sqlite3 *db;
  int rc;
  char *sql;
  sqlite3_stmt *stmt;

  rc = sqlite3_open (DBNAME, &db);
  if (rc)
    {
      g_warning ("打开数据库'%s'失败: %s\n", DBNAME, sqlite3_errmsg(db));
      return FALSE;
    }
  sqlite3_busy_timeout (db, 1000 * 10);

  sql = sqlite3_mprintf ("select termtype, auxport, teller, org, last_eval "
		  	 "from Ttyinfo where ttyname=%Q;", tty);
  rc = sqlite3_prepare_v2 (db, sql, -1, &stmt, NULL);
  if (rc)
    {
      g_warning ("准备 SQL 语句'%s'出错: %s\n", sql, sqlite3_errmsg (db));
      sqlite3_free (sql);
      return FALSE;
    }
  sqlite3_free (sql);

  rc = sqlite3_step (stmt);
  if (rc == SQLITE_DONE)
    {
      g_warning ("没有找到对应于终端'%s'的终端信息.", tty);
      sqlite3_finalize (stmt);
      sqlite3_close (db);
      return FALSE;
    }
  if (rc != SQLITE_ROW)
    {
      g_warning ("查询终端信息错误: %s\n", sqlite3_errmsg (db));
      sqlite3_finalize (stmt);
      sqlite3_close (db);
      return FALSE;
    }
  *termtype = sqlite3_column_int (stmt, 0);
  *port = sqlite3_column_int (stmt, 1);
  strcpy (teller, sqlite3_column_text (stmt, 2));
  strcpy (org, sqlite3_column_text (stmt, 3));
  *last_eval = sqlite3_column_int (stmt, 4);

  sqlite3_finalize (stmt);
  sqlite3_close(db);

  return TRUE;
}


static gboolean
t_event_eval_is_finish (void)
{
  sqlite3 *db;
  int rc, row, col, len;
  char *sql;
  sqlite3_stmt *stmt;
  const gchar *text;
  gchar *text2;
  gboolean ret = FALSE;

  rc = sqlite3_open (DBNAME, &db);
  if (rc)
    {
      g_warning ("打开数据库'%s'失败: %s\n", DBNAME, sqlite3_errmsg(db));
      return FALSE;
    }
  sqlite3_busy_timeout (db, 1000 * 10);

  sql = sqlite3_mprintf ("select row, col, len, text from Finish;");
  rc = sqlite3_prepare_v2 (db, sql, -1, &stmt, NULL);
  if (rc)
    {
      g_warning ("准备 SQL 语句'%s'出错: %s\n", sql, sqlite3_errmsg (db));
      sqlite3_free (sql);
      return FALSE;
    }
  sqlite3_free (sql);

  while (1)
    {
      rc = sqlite3_step (stmt);
      if (rc == SQLITE_DONE)
        break;

      if (rc != SQLITE_ROW)
        {
          g_warning ("查询成功标志错误: %s\n", sqlite3_errmsg (db));
	  break;
	}
      row = sqlite3_column_int (stmt, 0);
      col = sqlite3_column_int (stmt, 1);
      len = sqlite3_column_int (stmt, 2);
      text = sqlite3_column_text (stmt, 3);

      text2 = malloc (len + 1);
      text2[len] = '\0';
      t_rpc_call_snap_getxy (row, col, len, text2);
      if (strcmp (text, text2) == 0)
        {
          ret = TRUE;
	  free (text2);
	  break;
	}
      free (text2);
    }
  sqlite3_finalize (stmt);
  sqlite3_close(db);

  return ret;
}


static void
t_event_eval_gather (int mind, const gchar *teller, const gchar *org)
{
  sqlite3 *db;
  int rc, i;
  char *sql, *errmsg = NULL;
  gchar business[8] = { 0, };
  gchar timestr[64];
  struct tm *tm;
  time_t tim;

  t_rpc_call_snap_getxy (0, 1, 4, business);
  for (i = 0; i < 4; i++)
    {
      if (!isdigit (business[i]))
        {
          strcpy(business, "8888");
	  break;
	}
    }

  rc = sqlite3_open (DBNAME, &db);
  if (rc)
    {
      g_warning ("打开数据库'%s'失败: %s\n", DBNAME, sqlite3_errmsg (db));
      sqlite3_close (db);
      return;
    }
  tim = time (NULL);
  tm = localtime (&tim);
  strftime (timestr, sizeof (timestr), "%F %T", tm);
  sql = sqlite3_mprintf ("insert into Tlslist values(NULL,"
		  	 "%Q, %Q, %Q, %Q, %d);",
			 org, teller, business, timestr, mind);
  rc = sqlite3_exec (db, sql, NULL, 0, &errmsg);
  sqlite3_free (sql);
  if (rc)
    {
      g_warning ("插入采集数据失败: %s\n", errmsg);
      sqlite3_free (errmsg);
    }
  sqlite3_close(db);
}


static void
t_event_eval_update_time (const char *tty)
{
  sqlite3 *db;
  int rc;
  char *sql, *errmsg = NULL;

  rc = sqlite3_open (DBNAME, &db);
  if (rc)
    {
      g_warning ("打开数据库'%s'失败: %s\n", DBNAME, sqlite3_errmsg (db));
      sqlite3_close (db);
      return;
    }
  sql = sqlite3_mprintf ("update Ttyinfo set last_eval = %d "
		  	 "where ttyname = %Q;", time (NULL), tty);
  rc = sqlite3_exec (db, sql, NULL, 0, &errmsg);
  sqlite3_free (sql);
  if (rc)
    {
      g_warning ("插入采集数据失败: %s\n", errmsg);
      sqlite3_free (errmsg);
    }
  sqlite3_close(db);
}


void
t_event_eval (const char *tty)
{
  gint termtype, port;
  time_t last_eval, curr_time;
  gchar org[16] = { 0, };
  gchar teller[16] = { 0, };
  guchar buf_send[3] = { 0x1B, 0x42, 0 };
  guchar buf_recv[2] = { 0, };
  pthread_t thread;

  if (!t_event_eval_is_finish  ())
    return;

  curr_time = time (NULL);

  if (!t_event_get_tty_info (tty, &termtype, &port, teller, org, &last_eval))
    {
      termtype = 1;
      port = 2;
      strcpy (teller, "000000000-000");
      strcpy (org, "000000-000");
    }
  if (curr_time - last_eval < 15)
    {
      //g_warning ("尝试对单比业务进行多次评价，不允许!");
      return;
    }

  if (pthread_create (&thread, NULL, term_lock_keyboard, NULL) != 0)
    g_warning ("创建线程失败: %s", strerror (errno));

  usleep (1);

  if (term_openPort (termtype, port) == -1)
    {
      g_warning ("打开终端(%d)辅口(%d)失败!", termtype, port);
      return;
    }
  term_dataToDevice (termtype, buf_send, 2);
  term_readLenStr (buf_recv, 1, 5000);
  term_unlock_keyboard ();
  term_closePort (termtype);

  if (buf_recv[0] == 0 || buf_recv[0] > 3)
    buf_recv[0] = 4;

  t_event_eval_gather (buf_recv[0], teller, org);

  t_event_eval_update_time (tty);
}
