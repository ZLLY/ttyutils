/* Ttyutils -- UNIX terminal monitor
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 */
#ifndef __T_LUA_H__
#define __T_LUA_H__

#include <glib.h>
#include <lua.h>

G_BEGIN_DECLS


lua_State*	t_lua_open	(void);
void		t_lua_close	(lua_State   *state);
gboolean	t_lua_dofile	(lua_State   *state,
				 const gchar *file);
void		t_lua_add_path	(const gchar *path);

G_END_DECLS

#endif /* __T_LUA_H__ */
