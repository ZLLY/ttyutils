/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/sem.h>
#ifdef SCO_SV
# include <sys/termio.h>
#endif
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>
#include <fnmatch.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <cdk/cdk.h>
#include <ttyutils.h>

#ifdef DMALLOC
#include <dmalloc.h>
#endif

#ifdef  G_LOG_DOMAIN
# undef G_LOG_DOMAIN
#endif
#define G_LOG_DOMAIN	"Ttyadmin"


#if defined(_SYSV_IPC) || defined(_SEM_SEMUN_UNDEFINED)
union semun
{
  int              val;    	/* Value for SETVAL */
  struct semid_ds *buf;    	/* Buffer for IPC_STAT, IPC_SET */
  unsigned short  *array;  	/* Array for GETALL, SETALL */
  struct seminfo  *__buf;  	/* Buffer for IPC_INFO (Linux specific) */
};
#endif

typedef enum
{
  SORT_NONE = 0,
  SORT_TTY_ASCEND,
  SORT_TTY_DESCEND,
  SORT_PTS_ASCEND,
  SORT_PTS_DESCEND,
  SORT_USER_DESCEND,
  SORT_USER_ASCEND,
  SORT_TIME_DESCEND,
  SORT_TIME_ASCEND,
} SortType;


typedef struct _TScreen
{
  gint         lines;
  gint         cols;
  gboolean     update;
  gint         protocal;
  TShmemType   shmem_type;
  gint         nentries;
  GSList      *entries;
  GSList      *visible;
  pid_t        selection;
  SortType     sorttype;
  gint         color;
  gchar       *users;
  CDKSCREEN   *screen;
  CDKSCROLL   *scroll;
  CDKLABEL    *head;
  CDKLABEL    *foot;
} TScreen;

static TScreen		*t_screen = NULL;
static gboolean		 t_quit = FALSE;
static FILE		*t_logfile = NULL;

static gboolean		opt_version = FALSE;
static gboolean		opt_ipcrm = FALSE;
static gboolean		opt_cleanup = FALSE;
static gboolean		opt_summary = FALSE;
static gint		opt_update = 0;
static gboolean		opt_readonly = FALSE;
static gchar           *opt_logfile = NULL;

static GOptionEntry option_entries[] = 
{
    { "version", 'v', 0, G_OPTION_ARG_NONE, &opt_version,
      N_("Output version information and exit"), NULL },

    { "ipcrm", 'm', 0, G_OPTION_ARG_NONE, &opt_ipcrm,
      N_("Cleanup IPC resources of ttyutils"), NULL },

    { "cleanup", 'c', 0, G_OPTION_ARG_NONE, &opt_cleanup,
      N_("Remove invalid register entry"), NULL },

    { "summary", 's', 0, G_OPTION_ARG_NONE, &opt_summary,
      N_("Summary information of all instances"), NULL },

    { "update", 'p', 0, G_OPTION_ARG_INT, &opt_update,
      N_("Set automatic update interval"), "second" },

    { "readonly", 'r', 0, G_OPTION_ARG_NONE, &opt_readonly,
      N_("Enter readonly mode"), NULL },

    { "logfile", 'g', 0, G_OPTION_ARG_STRING, &opt_logfile,
      N_("Log message to file"), "file" },

    { NULL, }
};

/** 
 * t_admin_msgbox:
 * @msg: a string to show
 *
 * create a dialog show @msg, if @error is TRUE, a exit
 * button is add, and use can select it quit application.
 */
static void
t_admin_msgbox (gboolean error, const gchar *msg)
{
  CDKDIALOG *dialog;
  gchar *message[16];
  gchar *prompt;
  gchar *buttons[] = { _("</B/2>Ignore"), _("           </B/2>Exit"), NULL };
  gint nbuttons = 1;

  g_return_if_fail (msg);
  g_return_if_fail (t_screen);

  t_screen->update = FALSE;

  if (error)
    {
      nbuttons = 2;
      message[0] = _("</B/2>A error been detected, the detail was: <!B!2>");
      prompt = _("Select </B/2>Ignore<!2!B> to continue, </B/2>Exit<!2!B> to quit program");
    }
  else
    {
      nbuttons = 1;
      message[0] = _("</B/2>Report a message: <!B!2>");
      prompt = _("Select </B/2>Ignore<!2!B> to continue");
    }

  message[1] = "";
  message[2] = g_strdup_printf ("<I=2></B>%s", msg);
  message[3] = "";
  message[4] = prompt;
  message[5] = NULL;

  dialog = newCDKDialog (t_screen->screen, 10, 5, message, 5,
			 buttons, nbuttons, 0, TRUE, TRUE, FALSE);
  g_free (message[2]);

  if (dialog)
    {
      gint selection;
      gint status;

      setCDKDialogBackgroundAttrib (dialog, A_REVERSE);
      moveCDKDialog (dialog, CENTER, CENTER, FALSE, FALSE);
      refreshCDKScreen (t_screen->screen);

      selection = activateCDKDialog (dialog, NULL);
      status = dialog->exitType;

      destroyCDKDialog (dialog);

      if (error && status == vNORMAL && selection == 1)
	{
	  raise (SIGINT);
	  g_assert_not_reached ();
	}
    }

  t_screen->update = TRUE;

  refreshCDKScreen (t_screen->screen);
}

#define t_msgbox(msg)	t_admin_msgbox(FALSE, msg)
#define t_errbox(msg)	t_admin_msgbox(TRUE, msg)

/** 
 * t_help_window_keypressed:
 * @type 
 * @object 
 * @data 
 * @action 
 * 
 * @Returns: 
 */
static int
t_help_window_keypressed (EObjectType type,
			  void       *object,
			  void       *data,
			  chtype      action)
{
  switch (action)
    {
    case 'q':
    case KEY_RETURN:
      exitOKCDKScreen ((CDKSCREEN*) data);
      break;
    }

  return FALSE;
}

/** 
 * t_admin_help_window:
 *
 * create a scroll window show help message.
 */
static void
t_admin_help_window (void)
{
  WINDOW *curwin;
  CDKSCREEN *screen;
  CDKSWINDOW *window;
  CDKLABEL *label;
  gchar *contents[] =
    {
      _("Below </B>^X<!B> means </B>Ctrl X<!B>, case of </B>X<!B> is insensitive"),
      "",
      _(" </B>? h<!B>         Show help"),
      _(" </B>a<!B>           Show version, license of ttyadmin"),
      _(" </B>^L<!B>          Refresh screen"),
      _(" </B>Space<!B>       Update ttyexec instance window"),
      _(" </B>Enter<!B>       View detail of selected ttyexec instance"),
      _(" </B>t T<!B>         Sort by tty name (ascend or descend)"),
      _(" </B>p P<!B>         Sort by pts name (ascend or descend)"),
      _(" </B>u U<!B>         Sort by user name (ascend or descend)"),
      _(" </B>m M<!B>         Sort by elapsed time (ascend or descend)"),
      _(" </B>^U<!B>          List only entry with user matching that given"),
      _(" </B>c<!B>           Change window color pair"),
      _(" </B>e ^E ^D<!B>     List/Enable/Disable events for selected entry"),
      _(" </B>F7 F8<!B>       Enable/Disable events for all entry, caution!!!"),
      _(" </B>l L<!B>         Startup ttylook on selected entry, </B>W<!B> for writable"),
      _(" </B>s<!B>           Show ttyutils IPC resources information"),
#ifdef T_DEBUG
      _(" </B>^K<!B>          Send a SIGTERM to selected entry (for debug version only)"),
#endif
      _(" </B>:<!B>           Execute a shell command"),
      _(" </B>q ^C<!B>        Quit"),
      "",
      _("</R/B>Edit Command:<!B!R>"),
      "",
      _(" </B>BackSpace<!B>   Delete character before cursor"),
      _(" </B>Delete<!B>      Delete character at the cursor"),
      _(" </B>^U<!B>          Erases the contents"),
      _(" </B>^X ^Y ^V<!B>    Cut/Copy/Paste the contents"),
      _(" </B>^E ^A<!B>       Move to Begin/End"),
      _(" </B>^B ^F<!B>       Move Left/Right"),
      "",
      NULL
    };
  gchar *message[] =
    {
      _("</R> </B>S<!B>: Save, </B>g/G<!B>: Top/Bottom, </B>B/F<!B>: Page Up/Down, </B>Enter/q<!B>: Close"),
      NULL
    };

  curwin = newwin (t_screen->lines, t_screen->cols, 0, 0);
  screen = initCDKScreen (curwin);

  label = newCDKLabel (screen, 0, t_screen->lines - 1, message, 1, FALSE, FALSE);
  drawCDKLabel (label, ObjOf (label)->box);

  window = newCDKSwindow (screen, 0, 0, t_screen->lines - 1, t_screen->cols,
			  _("</B></R>Help for Ttyadmin Interactive Commands<!R><!B>"),
			  100, FALSE, FALSE);
  if (window)
    {
      setCDKSwindowBackgroundColor (window, "</8>");
#ifdef T_DEBUG
      setCDKSwindowContents (window, contents, 28);
#else
      setCDKSwindowContents (window, contents, 27);
#endif
      bindCDKObject (ObjTypeOf (window), window, 'q',
		     t_help_window_keypressed, screen);
      bindCDKObject (ObjTypeOf (window), window, KEY_RETURN,
		     t_help_window_keypressed, screen);

      refreshCDKScreen (screen);
      traverseCDKScreen (screen);

      destroyCDKSwindow (window);
    }
  destroyCDKLabel (label);
  destroyCDKScreen (screen);
  delwin (curwin);
}

/** 
 * t_admin_about_dialog:
 *
 * create a dialog show program's version, copyright and so on ...
 */
static void
t_admin_about_dialog (void)
{
  CDKDIALOG *dialog;
  gchar *message[20];
  gchar *buttons[] = { _("</B/24>Close"), NULL };

  message[0] = _("<C></B/U>About Ttyadmin");
  message[1] = "";
  message[2] = g_strdup_printf ("%s for %s", g_get_application_name (), PACKAGE_STRING);
  message[3] = "";
  message[4] = _("Copyright (C) 2008,2009,2010,2011 xiaohu.");
  message[5] = "";
  message[6] = g_strdup_printf (_("Bug Report: </K/B>%s"), PACKAGE_BUGREPORT);
  message[7] = "";
  message[8] = g_strdup_printf ("xiaohu417@gmail.com  %s", PACKAGE_WEBSITE);
  message[9] = NULL;

  dialog = newCDKDialog (t_screen->screen, 10, 5, message, 9,
			 buttons, 1, 0, TRUE, TRUE, FALSE);
  g_free (message[2]);
  g_free (message[6]);
  g_free (message[8]);

  if (dialog)
    {
      setCDKDialogBackgroundAttrib (dialog, A_REVERSE);
      moveCDKDialog (dialog, CENTER, CENTER, FALSE, TRUE);
      refreshCDKScreen (t_screen->screen);

      activateCDKDialog (dialog, NULL);
      destroyCDKDialog (dialog);
    }
}

/** 
 * t_admin_color_window:
 */
static void
t_admin_color_window (void)
{
  CDKRADIO *radio;
  gint selection;
  gchar *colors[65];
  gint i;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->screen);

  if (!has_colors ())
    {
      t_msgbox (_("color is unusable at this terminal."));
      return;
    }

  for (i = 1; i < 65; i++)
    {
      colors[i - 1] = g_strdup_printf (_("Color %2d     </%d>  Example  "), i, i);
    }
  colors[i - 1] = NULL;

  radio = newCDKRadio (t_screen->screen, 0, 0, RIGHT,
		       t_screen->lines - 2, 30,
		       _("<C></B/R>Select Window Color\n"),
		       colors, 64, '#' | A_REVERSE, 7, A_REVERSE,
		       TRUE, FALSE);
  for (i = 0; i < 64; i++)
    {
      g_free (colors[i]);
    }

  if (radio)
    {
      setCDKRadioCurrentItem (radio, t_screen->color);
      moveCDKRadio (radio, CENTER, CENTER, FALSE, TRUE);
      refreshCDKScreen (t_screen->screen);

      selection = activateCDKRadio (radio, NULL);
      if (radio->exitType == vNORMAL)
	{
	  gchar buf[64];

	  g_snprintf (buf, sizeof (buf), "</%d>", selection + 1);

	  g_assert (t_screen->scroll);
	  setCDKScrollBackgroundColor (t_screen->scroll, buf);

	  t_screen->color = selection;
	}
      destroyCDKRadio (radio);
    }
}

/** 
 * t_admin_detail_window:
 */
static void
t_admin_detail_window (void)
{
  CDKDIALOG *dialog;
  gchar *message[16];
  gchar *buttons[] = { _("</B/24>Close"), NULL };
  TShmemEntry *entry;
  gint index;
  struct passwd *pswd;
  struct tm *tm;
  gchar *ctime;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->scroll);

  if (!t_screen->visible)
    {
      t_msgbox (_("no detail for such entry."));
      return;
    }

  g_assert (t_screen->scroll);
  index = getCDKScrollCurrent (t_screen->scroll);
  entry = g_slist_nth_data (t_screen->visible, index);
  if (!entry)
    {
      t_errbox (_("selected index out of internal list range."));
      return;
    }

  pswd = getpwuid (entry->uid);

  message[0] = g_strdup_printf (_("<C></U/B>Detail of entry %d"), index + 1);
  message[1] = "";

  tm = localtime (&entry->ctime);
  ctime = g_strdup_printf ("%04d/%02d/%02d %02d:%02d:%02d",
			   tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
			   tm->tm_hour, tm->tm_min, tm->tm_sec);
  message[2] = g_strdup_printf (_("</B>User<!B>:(%d)%s  </B>PID<!B>:%d  </B>CTime<!B>:%s"),
				entry->uid, pswd ? pswd->pw_name : _("Unknown"),
				entry->pid, ctime);
  g_free (ctime);

  message[3] = g_strdup_printf (_("</B>TTY<!B>:%s  </B>PTS<!B>:%s  </B>Flags<!B>:0x%04X  </B>NLook<!B>:%d"), 
				entry->tty, entry->pts, entry->flags, entry->looknum);
  message[4] = "";
  message[5] = g_strdup_printf (_("</B>Command<!B>:%s"), entry->command);
  message[6] = g_strdup_printf (_("</B>Look Path<!B>:%s"), entry->lookpath);
  message[7] = g_strdup_printf (_("</B> RPC Path<!B>:%s"), entry->rpcpath);
  message[8] = "";
  message[9] = NULL;

  dialog = newCDKDialog (t_screen->screen, 10, 5, message, 9,
			 buttons, 1, 0, TRUE, TRUE, FALSE);
  g_free (message[0]);
  g_free (message[2]);
  g_free (message[3]);
  g_free (message[5]);
  g_free (message[6]);
  g_free (message[7]);

  if (dialog)
    {
      setCDKDialogBackgroundAttrib (dialog, A_REVERSE);
      moveCDKDialog (dialog, CENTER, CENTER, FALSE, TRUE);
      refreshCDKScreen (t_screen->screen);

      activateCDKDialog (dialog, NULL);
      destroyCDKDialog (dialog);
    }
}

/** 
 * t_admin_user_window:
 */
static void
t_admin_user_window (void)
{
  CDKENTRY *entry;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->screen);

  entry = newCDKEntry (t_screen->screen, 0, 0,
		       _("<C> </B/5>Enter the user name\n\n"
			 " Wildcard(</B>*<!B>, </B>?<!B>) is supported\n"),
		       "->: ", A_NORMAL, '_', vMIXED, 20, 1, 100, TRUE, FALSE);
  if (entry)
    {
      gchar *name = NULL;

      moveCDKEntry (entry, CENTER, CENTER, FALSE, FALSE);
      refreshCDKScreen (t_screen->screen);

      name = activateCDKEntry (entry, NULL);
      if (entry->exitType == vNORMAL)
	{
	  /* t_screen->users = g_intern_string (name); */
	  t_screen->users = g_strdup (name);
	}
      destroyCDKEntry (entry);
    }
}

/**
 * t_admin_append_entry:
 * @entry: a entry
 * @data: user data
 *
 * @Returns: return TRUE to stop, FALSE to continue.
 */
static gboolean
t_admin_append_entry (TShmemEntry *entry, gpointer data)
{
  TShmemEntry *newentry;

  if (entry)
    {
      newentry = g_memdup (entry, sizeof (TShmemEntry));
      t_screen->entries = g_slist_prepend (t_screen->entries, newentry);
    }
  return FALSE;
}

/** 
 * t_sort_time_ascend:
 */
static gint
t_sort_time_ascend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return entry1->ctime - entry2->ctime;
}

/** 
 * t_sort_time_descend:
 */
static gint
t_sort_time_descend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return entry2->ctime - entry1->ctime;
}

/** 
 * t_sort_user_ascend:
 */
static gint
t_sort_user_ascend (gconstpointer a, gconstpointer b)
{
  gchar *uuser1 = NULL;
  gchar *uuser2 = NULL;
  struct passwd *pswd;
  gint ret;

  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  pswd = getpwuid (entry1->uid);
  if (pswd)
    {
      uuser1 = g_strdup (pswd->pw_name);
    }
  pswd = getpwuid (entry2->uid);
  if (pswd)
    {
      uuser2 = g_strdup (pswd->pw_name);
    }

  if (!uuser1 || !uuser2)
    {
      g_free (uuser1);
      g_free (uuser2);
      return entry1->uid - entry2->uid;
    }
  else
    {
      ret = strcmp (uuser1, uuser2);
      g_free (uuser1);
      g_free (uuser2);
      return ret;
    }
}

/** 
 * t_sort_user_descend:
 */
static gint
t_sort_user_descend (gconstpointer a, gconstpointer b)
{
  struct passwd *pswd;
  gchar *uuser1 = NULL, *uuser2 = NULL;
  gint ret;

  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  pswd = getpwuid (entry1->uid);
  if (pswd)
    {
      uuser1 = g_strdup (pswd->pw_name);
    }
  pswd = getpwuid (entry2->uid);
  if (pswd)
    {
      uuser2 = g_strdup (pswd->pw_name);
    }

  if (!uuser1 || !uuser2)
    {
      g_free (uuser1);
      g_free (uuser2);
      return entry2->uid - entry1->uid;
    }
  else
    {
      ret = strcmp (uuser2, uuser1);
      g_free (uuser1);
      g_free (uuser2);
      return ret;
    }
}

/** 
 * t_sort_tty_descend:
 */
static gint
t_sort_tty_descend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return strcmp (entry1->tty, entry2->tty);
}

/** 
 * t_sort_tty_ascend:
 */
static gint
t_sort_tty_ascend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return strcmp (entry2->tty, entry1->tty);
}

/** 
 * t_sort_pts_descend:
 */
static gint
t_sort_pts_descend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return strcmp (entry1->pts, entry2->pts);
}

/** 
 * t_sort_pts_ascend:
 */
static gint
t_sort_pts_ascend (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry1 = (TShmemEntry*) a;
  TShmemEntry *entry2 = (TShmemEntry*) b;

  return strcmp (entry2->pts, entry1->pts);
}

/** 
 * t_admin_save_selection:
 */
static void
t_admin_save_selection (void)
{
  TShmemEntry *entry;
  gint index;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->scroll);

  if (t_screen->visible)
    {
      index = getCDKScrollCurrent (t_screen->scroll);
      entry = g_slist_nth_data (t_screen->visible, index);
      if (entry)
	{
	  t_screen->selection = entry->pid;
	}
    }
}

/** 
 * t_admin_get_info:
 */
static void
t_admin_get_info (void)
{
  g_assert (t_screen);

  if (!t_shmem_open (FALSE))
    {
      g_warning (_("connect to share memory failed."));
      if (t_screen->entries)
	{
	  g_slist_foreach (t_screen->entries, (GFunc) g_free, NULL);
	  g_slist_free (t_screen->entries);
	  t_screen->entries = NULL;
	}
      t_screen->nentries = 0;
      return;
    }

  t_admin_save_selection ();

  if (t_screen->entries)
    {
      g_slist_foreach (t_screen->entries, (GFunc) g_free, NULL);
      g_slist_free (t_screen->entries);
      t_screen->entries = NULL;
    }
  t_shmem_lookup_entry (t_admin_append_entry, NULL);

  t_screen->protocal = t_shmem_get_version ();
  t_screen->shmem_type = t_shmem_get_type ();
  t_screen->nentries = t_shmem_get_nentries ();

  switch (t_screen->sorttype)
    {
    case SORT_TIME_ASCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_time_ascend);
      break;
    case SORT_TIME_DESCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_time_descend);
      break;
    case SORT_USER_ASCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_user_ascend);
      break;
    case SORT_USER_DESCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_user_descend);
      break;
    case SORT_TTY_ASCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_tty_ascend);
      break;
    case SORT_TTY_DESCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_tty_descend);
      break;
    case SORT_PTS_ASCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_pts_ascend);
      break;
    case SORT_PTS_DESCEND:
      t_screen->entries = g_slist_sort (t_screen->entries,
					t_sort_pts_descend);
      break;

    default:
      break;
    }

  t_shmem_close ();
}

/** 
 * t_admin_scroll_insert:
 */
static void
t_admin_scroll_insert (gpointer data, gpointer user_data)
{
  TShmemEntry *entry = (TShmemEntry*) data;
  gchar line[1024];
  gchar timebuf[128];
  time_t tmintvl;
  struct tm *tmp;
  struct passwd *pswd;

  g_return_if_fail (t_screen);
  g_return_if_fail (entry);

  pswd = getpwuid (entry->uid);

  /* user name filter.
   */
  if (pswd && t_screen->users)
    {
      if (fnmatch (t_screen->users, pswd->pw_name, FNM_NOESCAPE) != 0)
	return;
    }

  /* format display string.
   */
  tmintvl = time (NULL) - entry->ctime;
  tmp = gmtime (&tmintvl);
  if (tmp->tm_mday > 1)
    {
      g_snprintf (timebuf, sizeof (timebuf), "%02d+%02d:%02d",
		  tmp->tm_mday - 1, tmp->tm_hour, tmp->tm_min);
    }
  else
    {
      g_snprintf (timebuf, sizeof (timebuf), "%02d:%02d:%02d",
		  tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
    }

  g_snprintf (line, sizeof (line), "%c%c %10s %8d %12s %12s %9s %s",
	      (entry->flags & SHMEM_FLAG_NOEVENT) ? '*' : ' ',
	      (entry->looknum > 0) ? '%' : (strlen (entry->lookpath) > 0) ? '@' : ' ',
	      pswd ? pswd->pw_name : _("Unknown"),
	      entry->pid, entry->tty, entry->pts,
	      timebuf, entry->command);

  g_assert (t_screen->scroll);
  addCDKScrollItem (t_screen->scroll, line);

  t_screen->visible = g_slist_append (t_screen->visible, entry);
}

/** 
 * t_admin_find_entry_index:
 */
static gint
t_admin_find_entry_index (gconstpointer a, gconstpointer b)
{
  TShmemEntry *entry = (TShmemEntry*) a;
  pid_t pid = GPOINTER_TO_UINT (b);

  return entry->pid - pid;
}

/** 
 * t_admin_restore_selection:
 */
static void
t_admin_restore_selection (void)
{
  GSList *node;
  gint index;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->scroll);

  if (t_screen->selection == -1)
    return;

  node = g_slist_find_custom (t_screen->visible,
			      GUINT_TO_POINTER (t_screen->selection),
			      t_admin_find_entry_index);
  if (node)
    {
      index = g_slist_position (t_screen->visible, node);
      setCDKScrollCurrent (t_screen->scroll, index);
      drawCDKScroll (t_screen->scroll, ObjOf (t_screen->scroll)->box);
    }
}

/** 
 * t_admin_screen_update:
 */
static void
t_admin_screen_update (void)
{
  static gboolean noentry = FALSE;
  static gboolean nomatch = FALSE;
  GString *string;
  gchar *message[2];

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->scroll);

  /* clean the scroll window first.
   */
  while (t_screen->scroll->listSize > 0)
    {
      deleteCDKScrollItem (t_screen->scroll, 0);
    }
  if (t_screen->visible)
    {
      g_slist_free (t_screen->visible);
      t_screen->visible = NULL;
    }

  /* if no entry, add a default entry
   */
  if (t_screen->nentries == 0)
    {
      addCDKScrollItem (t_screen->scroll, "! </2/B>EMPTY");

      if (!noentry)
	{
	  t_errbox (_("there has not register instance."));
	  noentry = TRUE;
	}
      return;
    }

  /* insert entries into scroll window.
   */
  if (t_screen->entries)
    {
      g_slist_foreach (t_screen->entries, t_admin_scroll_insert, NULL);
    }

  /* no visible entry.
   */
  if (!t_screen->visible)
    {
      if (!nomatch)
	{
	  t_msgbox (_("there has not matched instance."));
	  nomatch = TRUE;
	}
      message[0] = g_strdup_printf ("! </2/B>(%s) UNMATCH",
				    t_screen->users ? t_screen->users : "");
      addCDKScrollItem (t_screen->scroll, message[0]);
      g_free (message[0]);
    }
  else
    {
      t_admin_restore_selection ();
    }

  /* update header.
   */
  string = g_string_new ("");
  g_string_append_printf (string,
			  _("%s on %s - prototol </B>%d<!B>(%s), </B>%d<!B>/</B>%d<!B> entry"),
			  g_get_application_name (),
			  ttyname (STDOUT_FILENO) + strlen ("/dev/"),
			  t_screen->protocal,
			  (t_screen->shmem_type == SHMEM_TYPE_POSIX) ? "posix" : "sysv",
			  t_screen->visible ? g_slist_length (t_screen->visible) : 0,
			  t_screen->nentries);
  if (opt_update > 0)
    {
      g_string_append_printf (string, _(", ui </B>%d<!B>s"), opt_update);
    }
  if (opt_readonly)
    {
      g_string_append (string, _(", </B>readonly<!B>"));
    }

  message[0] = g_string_free (string, FALSE);
  message[1] = NULL;

  g_assert (t_screen->head);
  setCDKLabel (t_screen->head, message, 1, ObjOf (t_screen->head)->box);
  g_free (message[0]);
}

/** 
 * t_admin_screen_destroy:
 */
static void
t_admin_screen_destroy (void)
{
  if (!t_screen)
    return;

  if (t_screen->head)
    {
      destroyCDKLabel (t_screen->head);
    }
  if (t_screen->foot)
    {
      destroyCDKLabel (t_screen->foot);
    }
  if (t_screen->scroll)
    {
      destroyCDKScroll (t_screen->scroll);
    }
  if (t_screen->screen)
    {
      destroyCDKScreen (t_screen->screen);
    }
  if (t_screen->visible)
    {
      g_slist_free (t_screen->visible);
    }
  if (t_screen->entries)
    {
      g_slist_foreach (t_screen->entries, (GFunc) g_free, NULL);
      g_slist_free (t_screen->entries);
    }

  g_free (t_screen->users);
  t_screen->users = NULL;

  g_free (t_screen);
  t_screen = NULL;
  refresh ();
}

/** 
 * t_admin_set_event_enabled:
 * @entry 
 * @enabled 
 */
static void
t_admin_set_event_enabled (TShmemEntry *entry, gboolean enabled)
{
  g_return_if_fail (entry);

  t_shmem_set_event_enabled (entry->pid, enabled);

  if (enabled)
    entry->flags &= ~SHMEM_FLAG_NOEVENT;
  else
    entry->flags |= SHMEM_FLAG_NOEVENT;
}

/** 
 * t_admin_set_event_enabled_select:
 * @enabled 
 */
static void
t_admin_set_event_enabled_select (gboolean enabled)
{
  gint index;
  TShmemEntry *entry;

  if (!t_shmem_open (TRUE))
    {
      g_warning (_("connect to share memory failed."));
      return;
    }

  index = getCDKScrollCurrent (t_screen->scroll);
  entry = g_slist_nth_data (t_screen->entries, index);
  if (!entry)
    {
      t_errbox (_("selected index out of internal list range."));
      return;
    }
  t_admin_set_event_enabled (entry, enabled);
  t_shmem_close ();
}

/** 
 * t_admin_set_event_enabled_all:
 * @enabled 
 */
static void
t_admin_set_event_enabled_all (gboolean enabled)
{
  GSList *node;

  g_return_if_fail (t_screen);

  if (!t_shmem_open (TRUE))
    {
      g_warning (_("connect to share memory failed."));
      return;
    }
  for (node = t_screen->entries; node; node = node->next)
    {
      t_admin_set_event_enabled ((TShmemEntry*) node->data, enabled);
    }
  t_shmem_close ();
}

/** 
 * t_admin_shell_command:
 * @command:
 */
static void
t_admin_shell_command (gchar *command)
{
  gchar *sh, *argv[8];
  GError *error = NULL;

  g_return_if_fail (command);

  sh = g_find_program_in_path ("sh");
  if (!sh)
    {
      t_msgbox (_("shell not found in your PATH"));
      return;
    }
  argv[0] = sh;
  argv[1] = "-c";
  argv[2] = command;
  argv[3] = NULL;

  /* leave the curses mode.
   */
  def_prog_mode();
  endwin ();

  /* FIXME: child process read stdin got errno 11(Resource
   * temporarily unavailable).
   */
  g_spawn_sync (NULL, argv, NULL, G_SPAWN_CHILD_INHERITS_STDIN, 
		NULL, NULL, NULL, NULL, NULL, &error);
  g_free (sh);

  if (error)
    {
      fprintf (stdout, error->message);
      g_error_free (error);
    }

  /* restore the curses mode.
   */
  reset_prog_mode ();
  halfdelay (128);

  fprintf (stdout, _("\npress ENTER or SPACE to continue ..."));
  fflush (stdout);

  while (1)
    {
      gchar ch = getch ();
      if (ch == 10 || ch == 0x20)
	{
	  fputc ('\n', stdout);
	  break;
	}
    }
  refresh ();
}

/** 
 * t_admin_startup_ttylook:
 * @writable: whether writable mode
 */
static void
t_admin_startup_ttylook (gboolean writable)
{
  gchar *path;
  gchar command[256];
  gint index;
  TShmemEntry *entry;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->scroll);

  if (!t_screen->visible)
    return;

  index = getCDKScrollCurrent (t_screen->scroll);
  entry = g_slist_nth_data (t_screen->visible, index);
  if (!entry)
    {
      t_errbox (_("selected index out of internal list range."));
      return;
    }

  path = g_find_program_in_path ("ttylook");
  if (!path)
    {
      t_msgbox (_("ttylook not found in your PATH"));
      return;
    }
  g_snprintf (command, sizeof (command), "%s --pts %s %s",
              path, entry->pts, writable ? "--writable" : "");
  g_free (path);

  t_admin_shell_command (command);
}

/** 
 * t_admin_sysinfo_window:
 */
static void
t_admin_sysinfo_window (void)
{
  WINDOW *curwin;
  CDKSCREEN *screen;
  CDKSWINDOW *window;
  CDKLABEL *label;
  gchar *message[] =
    {
      _("</R> </B>S<!B>: Save, </B>g/G<!B>: Top/Bottom, </B>B/F<!B>: Page Up/Down, </B>Enter<!B>: Close"),
      NULL
    };
  gchar *contents[64];
  gint size, i;
  gint shmid, semid;
  struct shmid_ds shmds;
  struct semid_ds semds;
#ifdef IPC_INFO
  struct shminfo shminfo;
  struct seminfo seminfo;
#endif
  struct tm *tm;
  gchar *atime = NULL;
  gchar *dtime = NULL;
  gchar *htime = NULL;
  gchar *otime = NULL;
  gchar *ctime = NULL;
  gchar *values = NULL;
  struct passwd *pswd;
  struct group *grp;

  if (!t_shmem_open (FALSE))
    {
      t_msgbox (_("connect to share memory failed."));
      return;
    }

  memset (&semds, 0, sizeof (struct semid_ds));
#ifdef IPC_INFO
  memset (&shminfo, 0, sizeof (struct shminfo));
  memset (&seminfo, 0, sizeof (struct seminfo));
#endif

  shmid = t_shmem_get_shmid ();
  if (shmid != -1)
    {
      if (shmctl (shmid, IPC_STAT, &shmds) != 0)
	{
	  contents[0] = g_strdup_printf (_("stat share memory error: %s"),
					 g_strerror (errno));
	  t_msgbox (contents[0]);
	  g_free (contents[0]);
	  t_shmem_close ();
	  return;
	}

#ifdef IPC_INFO
      shmctl (shmid, IPC_INFO, (struct shmid_ds*) &shminfo);
#endif

      tm = localtime (&shmds.shm_atime);
      atime = g_strdup_printf ("%02d/%02d/%02d %02d:%02d:%02d",
			       tm->tm_year - 100, tm->tm_mon + 1, tm->tm_mday,
			       tm->tm_hour, tm->tm_min, tm->tm_sec);
      tm = localtime (&shmds.shm_dtime);
      dtime = g_strdup_printf ("%02d/%02d/%02d %02d:%02d:%02d",
			       tm->tm_year - 100, tm->tm_mon + 1, tm->tm_mday,
			       tm->tm_hour, tm->tm_min, tm->tm_sec);
      tm = localtime (&shmds.shm_ctime);
      htime = g_strdup_printf ("%02d/%02d/%02d %02d:%02d:%02d",
			       tm->tm_year - 100, tm->tm_mon + 1, tm->tm_mday,
			       tm->tm_hour, tm->tm_min, tm->tm_sec);
    }

  semid = t_shmem_get_semid ();
  if (semid != -1)
    {
      union semun arg;
      guint16 *value_arr = NULL;
      GString *string;

      arg.buf = &semds;
      if (semctl (semid, 0, IPC_STAT, arg) != 0)
	{
	  contents[0] = g_strdup_printf (_("stat semphore error: %s"),
					 g_strerror (errno));
	  t_msgbox (contents[0]);
	  g_free (contents[0]);
	  t_shmem_close ();
	  return;
	}

#ifdef IPC_INFO
      arg.__buf = &seminfo;
      semctl (semid, 0, IPC_INFO, arg);
#endif

      value_arr = g_new0 (guint16, semds.sem_nsems);
      arg.array = value_arr;
      if (semctl (semid, 0, GETALL, arg) != 0)
	{
	  contents[0] = g_strdup_printf (_("stat semphore error: %s"),
					 g_strerror (errno));
	  t_msgbox (contents[0]);
	  g_free (contents[0]);
	  g_free (value_arr);
	  t_shmem_close ();
	  return;
	}

      string = g_string_new ("");
      g_string_append_printf (string, "%d", value_arr[0]);
      for (i = 1; i < semds.sem_nsems; i++)
	{
	  g_string_append_printf (string, ",%d", value_arr[i]);
	}
      g_free (value_arr);
      values = g_string_free (string, FALSE);

      tm = localtime (&semds.sem_otime);
      otime = g_strdup_printf ("%02d/%02d/%02d %02d:%02d:%02d",
			       tm->tm_year - 100, tm->tm_mon + 1, tm->tm_mday,
			       tm->tm_hour, tm->tm_min, tm->tm_sec);
      tm = localtime (&semds.sem_ctime);
      ctime = g_strdup_printf ("%02d/%02d/%02d %02d:%02d:%02d",
			       tm->tm_year - 100, tm->tm_mon + 1, tm->tm_mday,
			       tm->tm_hour, tm->tm_min, tm->tm_sec);
    }

  contents[0] = g_strdup (_("</R>Share memory"));
  contents[1] = g_strdup_printf (_("<I=2>id: %d, size: %d bytes, mode: 0x%04X, type: %s"),
				 shmid, shmds.shm_segsz, shmds.shm_perm.mode,
				 t_screen->shmem_type == SHMEM_TYPE_POSIX ? "posix" : "sysv");
  contents[2] = g_strdup_printf (_("<I=2>last attach time: %s"),
				 atime ? atime : _("unknown"));
  if (atime)
    g_free (atime);
  contents[3] = g_strdup_printf (_("<I=2>last detach time: %s"),
				 dtime ? dtime : _("unknown"));
  if (dtime)
    g_free (dtime);
  contents[4] = g_strdup_printf (_("<I=2>last change time: %s"),
				 htime ? htime : _("unknown"));
  if (htime)
    g_free (htime);
  contents[5] = g_strdup_printf (_("<I=2>pid of creator: %d"), shmds.shm_cpid);
  contents[6] = g_strdup_printf (_("<I=2>pid of last shmat()/shmdt(): %d"), shmds.shm_lpid);
  contents[7] = g_strdup_printf (_("<I=2>number of current attaches: %ld"), shmds.shm_nattch);
  pswd = getpwuid (shmds.shm_perm.uid);
  grp = getgrgid (shmds.shm_perm.gid);
  contents[8] = g_strdup_printf (_("<I=2>user and group of owner: %u(%s), %u(%s)"),
				 shmds.shm_perm.uid,
				 pswd ? pswd->pw_name : _("unknown"),
				 shmds.shm_perm.gid,
				 grp ? grp->gr_name : _("unknown"));
  pswd = getpwuid (shmds.shm_perm.cuid);
  grp = getgrgid (shmds.shm_perm.cgid);
  contents[9] = g_strdup_printf (_("<I=2>user and group of creator: %u(%s), %u(%s)"),
				 shmds.shm_perm.cuid,
				 pswd ? pswd->pw_name : _("unknown"),
				 shmds.shm_perm.cgid,
				 grp ? grp->gr_name : _("unknown"));
  contents[10] = g_strdup ("");
  contents[11] = g_strdup (_("</R>Semphores"));
  size = 12;

  if (semid != -1)
    {
      contents[12] = g_strdup_printf (_("<I=2>id: %d, number: %ld[%s], mode: 0x%04X"),
				     t_shmem_get_semid (), semds.sem_nsems,
				     values ? values : _("unknwon"),
				     semds.sem_perm.mode);
      if (values)
	g_free (values);

      pswd = getpwuid (semds.sem_perm.uid);
      grp = getgrgid (semds.sem_perm.gid);
      contents[13] = g_strdup_printf (_("<I=2>user and group of owner: %u(%s), %u(%s)"),
				     semds.sem_perm.uid,
				     pswd ? pswd->pw_name : _("unknown"),
				     semds.sem_perm.gid,
				     grp ? grp->gr_name : _("unknown"));
      pswd = getpwuid (semds.sem_perm.cuid);
      grp = getgrgid (semds.sem_perm.cgid);
      contents[14] = g_strdup_printf (_("<I=2>user and group of creator: %u(%s), %u(%s)"),
				     semds.sem_perm.cuid,
				     pswd ? pswd->pw_name : _("unknown"),
				     semds.sem_perm.cgid,
				     grp ? grp->gr_name : _("unknown"));
      contents[15] = g_strdup_printf (_("<I=2>last operate time: %s"),
				     otime ? otime :  _("unknown"));
      if (otime)
	g_free (otime);
      contents[16] = g_strdup_printf (_("<I=2>last changed time: %s"),
				      ctime ? ctime : _("unknown"));
      if (ctime)
	g_free (ctime);

      size = 17;

#ifdef IPC_INFO
      contents[17] = g_strdup ("");
      contents[18] = g_strdup_printf (_("</R>Limits"));
      contents[19] = g_strdup (_("<I=2>Share memory:"));
      contents[20] = g_strdup_printf (_("<I=4>max segment size: %ld"),
				     shminfo.shmmax);
      contents[21] = g_strdup_printf (_("<I=4>min segment size: %ld"),
				     shminfo.shmmin);
      contents[22] = g_strdup_printf (_("<I=4>max number of segments: %ld"),
				     shminfo.shmmni);
      contents[23] = g_strdup_printf (_("<I=4>max number of segments that a process can attach: %ld"),
				     shminfo.shmseg);
      contents[24] = g_strdup_printf (_("<I=4>max number of pages of shared memory, system-wide: %ld"),
				     shminfo.shmall);
      contents[25] = g_strdup (_("<I=2>Semphore:"));
      contents[26] = g_strdup_printf (_("<I=4>number of entries in semaphore map: %d"),
				     seminfo.semmap);
      contents[27] = g_strdup_printf (_("<I=4>max number of semaphore sets: %d"),
				     seminfo.semmni);
      contents[28] = g_strdup_printf (_("<I=4>max number of semaphores in all semaphore sets: %d"),
				     seminfo.semmns);
      contents[29] = g_strdup_printf (_("<I=4>system-wide max number of undo structures: %d"),
				     seminfo.semmnu);
      contents[30] = g_strdup_printf (_("<I=4>max number of semaphores in a set: %d"),
				     seminfo.semmsl);
      contents[31] = g_strdup_printf (_("<I=4>max number of operations for semop(): %d"),
				     seminfo.semopm);
      contents[32] = g_strdup_printf (_("<I=4>max number of undo entries per process: %d"),
				     seminfo.semume);
      contents[33] = g_strdup_printf (_("<I=4>size of struct sem_undo: %d"),
				     seminfo.semusz);
      contents[34] = g_strdup_printf (_("<I=4>maximum semaphore value: %d"),
				     seminfo.semvmx);
      contents[35] = g_strdup_printf (_("<I=4>max. value that can be recorded for adjustment(SEM_UNDO): %d"),
				     seminfo.semaem);
      size = 36;
#endif
    }
  contents[size] = NULL;

  t_shmem_close ();

  curwin = newwin (t_screen->lines, t_screen->cols, 0, 0);
  screen = initCDKScreen (curwin);

  label = newCDKLabel (screen, 0, t_screen->lines - 1, message, 1, FALSE, FALSE);
  drawCDKLabel (label, ObjOf (label)->box);

  window = newCDKSwindow (screen, 0, 0, t_screen->lines, t_screen->cols,
			  _("</B></R>Ttyutils IPC resources information<!R><!B>"),
			  100, FALSE, FALSE);
  if (window)
    {
      setCDKSwindowBackgroundColor (window, "</8>");
      setCDKSwindowContents (window, contents, size);
      refreshCDKScreen (screen);

      activateCDKSwindow (window, NULL);
      destroyCDKSwindow (window);
    }
  for (i = 0; i < size; i++)
    {
      g_free (contents[i]);
    }
  destroyCDKLabel (label);
  destroyCDKScreen (screen);
  delwin (curwin);
}

/** 
 * t_admin_kill_select:
 *
 * send a SIGTERM to current select instance.
 */
static void
t_admin_kill_select (void)
{
  gint index;
  TShmemEntry *entry;

  index = getCDKScrollCurrent (t_screen->scroll);
  entry = g_slist_nth_data (t_screen->entries, index);
  if (!entry)
    {
      t_errbox (_("selected index out of internal list range."));
      return;
    }
  if (kill (entry->pid, SIGTERM) == 0)
    {
      t_screen->entries = g_slist_remove (t_screen->entries, entry);
      g_free (entry);

      deleteCDKScrollItem (t_screen->scroll, index);
      if (t_screen->scroll->listSize == 0)
	{
	  addCDKScrollItem (t_screen->scroll, "! </2/B>EMPTY");
	}
      drawCDKScroll (t_screen->scroll, ObjOf (t_screen->scroll)->box);
    }
  else
    {
      gchar *msg = g_strdup_printf (_("send SIGTERM to %d error: %s"),
				    entry->pid, g_strerror (errno));
      t_msgbox (msg);
      g_free (msg);
    }
}

/** 
 * t_events_scroll_keypressed:
 * @type 
 * @object 
 * @data 
 * @action 
 * 
 * @Returns: 
 */
static int
t_events_scroll_keypressed (EObjectType type,
			    void       *object,
			    void       *data,
			    chtype      action)
{
  switch (action)
    {
    case 'q':
      exitOKCDKScreen ((CDKSCREEN*) data);
      return FALSE;

    case KEY_RETURN:
      break;
    }

  ungetch (CDK_REFRESH);
  return FALSE;
}

/** 
 * t_admin_events_window:
 * @ptsname: which pts
 * @events: event table array
 * @number: event count
 */
static void
t_admin_events_window (const gchar  *ptsname,
		       TEventEntry  *events,
		       gsize         number)
{
  WINDOW *curwin;
  CDKSCREEN *screen;
  CDKLABEL *head, *foot;
  CDKSCROLL *scroll;
  gchar *message[16];
  gchar buff[256];
  gint i;

  g_return_if_fail (t_screen);

  curwin = newwin (t_screen->lines, t_screen->cols, 0, 0);
  screen = initCDKScreen (curwin);

  /* header */
  message[0] = g_strdup_printf (_("events for %s - %d entry"), ptsname, number);
  message[1] = NULL;

  head = newCDKLabel (screen, 0, 0, message, 1, FALSE, FALSE);
  drawCDKLabel (head, ObjOf (head)->box);
  g_free (message[0]);

  /* footer */
  message[0] = _("</R>  </B>g/G<!B>: Top/Bottom, </B>B/F<!B>: Page Up/Down, </B>Enter<!B>: Detail, </B>q<!B>: Close");
  message[1] = NULL;

  foot = newCDKLabel (screen, 0, t_screen->lines - 1,
		      message, 1, FALSE, FALSE);
  drawCDKLabel (foot, ObjOf (foot)->box);

  /* scroll window */
  message[0] = "</2/B>NONE";
  message[1] = NULL;

  g_snprintf (buff, sizeof (buff), "</R>%-10s %-6s %-34s %s",
	      "NAME", "GROUP", "MATCHER", "PROGRAM");
  for (i = strlen (buff); i < t_screen->cols + 4; i++)
    {
      buff[i] = ' ';
    }
  buff[i] = '\0';
  g_strlcat (buff, "<!R>", sizeof (buff));

  scroll = newCDKScroll (screen, 0, 1, RIGHT,
			 t_screen->lines - 2, t_screen->cols, buff,
			 message, 1, FALSE, 0, FALSE, FALSE);
  setCDKScrollHighlight (scroll, A_REVERSE | A_BOLD);

  if (number > 0)
    {
      deleteCDKScrollItem (scroll, 0);
    }
  for (i = 0; i < number; i++)
    {
      g_snprintf (buff, sizeof (buff), "%-10s %-6s %-34s %s",
		  events[i].name, events[i].group,
		  events[i].matcher, events[i].program);
      addCDKScrollItem (scroll, buff);
    }
  drawCDKScroll (scroll, ObjOf (scroll)->box);

  /* bind keys */
  bindCDKObject (ObjTypeOf (scroll), scroll, 'q',
		 t_events_scroll_keypressed, screen);
  bindCDKObject (ObjTypeOf (scroll), scroll, KEY_RETURN,
		 t_events_scroll_keypressed, screen);
  bindCDKObject (ObjTypeOf (scroll), scroll, KEY_UP,
		 t_events_scroll_keypressed, screen);
  bindCDKObject (ObjTypeOf (scroll), scroll, KEY_DOWN,
		 t_events_scroll_keypressed, screen);

  refreshCDKScreen (screen);
  traverseCDKScreen (screen);

  destroyCDKLabel (head);
  destroyCDKLabel (foot);
  destroyCDKScreen (screen);
  delwin (curwin);
}

/** 
 * t_admin_list_events:
 */
static void
t_admin_list_events (void)
{
  gint index;
  TShmemEntry *entry;
  TRpcTarget target;
  TEventEntry *events = NULL;
  gsize number;

  index = getCDKScrollCurrent (t_screen->scroll);
  entry = g_slist_nth_data (t_screen->entries, index);
  if (!entry)
    {
      t_errbox (_("selected index out of internal list range."));
      return;
    }

  /* contact server by its process id.
   */
  target.rt_timeout = 3;
  target.rt_type = RPC_TARGET_PID;
  target.rt_pid = entry->pid;

  t_rpc_call_set_target (&target);

  /* request events.
   */
  if (!t_rpc_call_get_events (&events, &number))
    {
      t_msgbox (_("get events failed, please see log for detail."));
      return;
    }

  t_admin_events_window (entry->pts, events, number);
  g_free (events);
}

/** 
 * t_admin_command_window:
 */
static void
t_admin_command_window (void)
{
  CDKENTRY *entry;

  g_return_if_fail (t_screen);
  g_return_if_fail (t_screen->screen);

  entry = newCDKEntry (t_screen->screen, 0, 0,
		       _("<C> </B/5>Enter the command\n"),
		       "$ ", A_NORMAL, '_', vMIXED, 40, 1, 100, TRUE, FALSE);
  if (entry)
    {
      gchar *command = NULL;

      moveCDKEntry (entry, CENTER, CENTER, FALSE, FALSE);
      refreshCDKScreen (t_screen->screen);

      command = activateCDKEntry (entry, NULL);
      if (entry->exitType == vNORMAL)
	{
	  t_admin_shell_command (command);
	}
      destroyCDKEntry (entry);
    }
}

/** 
 * t_scroll_keypressed:
 * @type: object type
 * @object: object
 * @data: user data
 * @action: what happen.
 * 
 * @Returns: 0
 */
static int
t_scroll_keypressed (EObjectType type,
		     void       *object,
		     void       *data,
		     chtype      action)
{
  t_screen->update = FALSE;

  switch (action)
    {
    case 'q':		/* quit the app */
      t_quit = TRUE;
      exitOKCDKScreen (t_screen->screen);
      return FALSE;

    case '?':		/* show help window */
    case 'h':
      t_admin_help_window ();
      break;

    case 'a':		/* show about dialog */
      t_admin_about_dialog ();
      break;

    case SPACE:		/* update instance window */
      t_admin_get_info ();
      t_admin_screen_update ();
      break;

    case KEY_RETURN:	/* show instance detail */
      t_admin_detail_window ();
      break;

    case 'c':		/* change background color. */
      t_admin_color_window ();
      break;

    case 'm':		/* sort by time descend */
      t_screen->sorttype = SORT_TIME_DESCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_time_descend);
	  t_admin_screen_update ();
	}
      break;
    case 'M':		/* sort by time ascend */
      t_screen->sorttype = SORT_TIME_ASCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_time_ascend);
	  t_admin_screen_update ();
	}
      break;
    case 'u':		/* sort by user name descend */
      t_screen->sorttype = SORT_USER_DESCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_user_descend);
	  t_admin_screen_update ();
	}
      break;
    case 'U':		/* sort by user name ascend */
      t_screen->sorttype = SORT_USER_ASCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_user_ascend);
	  t_admin_screen_update ();
	}
      break;
    case 'p':		/* sort by pts name descend */
      t_screen->sorttype = SORT_PTS_DESCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_pts_descend);
	  t_admin_screen_update ();
	}
      break;
    case 'P':		/* sort by pts name ascend */
      t_screen->sorttype = SORT_PTS_ASCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_pts_ascend);
	  t_admin_screen_update ();
	}
      break;
    case 't':		/* sort by tty name descend */
      t_screen->sorttype = SORT_TTY_DESCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_tty_descend);
	  t_admin_screen_update ();
	}
      break;
    case 'T':		/* sort by tty name ascend */
      t_screen->sorttype = SORT_TTY_ASCEND;
      if (t_screen->entries)
	{
	  t_admin_save_selection ();
	  t_screen->entries = g_slist_sort (t_screen->entries,
					    t_sort_tty_ascend);
	  t_admin_screen_update ();
	}
      break;

    case CTRL('u'):	/* show only given user */
      t_admin_user_window ();
      t_admin_screen_update ();
      break;

    case CTRL('D'):	/* Disable terminal events report */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_save_selection ();
      t_admin_set_event_enabled_select (FALSE);
      t_admin_screen_update ();
      break;
    case CTRL('E'):	/* Enable terminal events report */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_save_selection ();
      t_admin_set_event_enabled_select (TRUE);
      t_admin_screen_update ();
      break;

    case 'e':		/* List terminal events */
      t_admin_list_events ();
      break;

    case KEY_F(7):	/* Enable all terminal events report */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_save_selection ();
      t_admin_set_event_enabled_all (TRUE);
      t_admin_screen_update ();
      break;
    case KEY_F(8):	/* Disable all terminal events report */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_save_selection ();
      t_admin_set_event_enabled_all (FALSE);
      t_admin_screen_update ();
      break;

    case 'l':		/* startup ttylook with readonly mode */
      t_admin_startup_ttylook (FALSE);
      break;
    case 'L':		/* startup ttylook with writable mode */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_startup_ttylook (TRUE);
      break;

    case 's':		/* show system ipc information */
      t_admin_sysinfo_window ();
      break;

#ifdef T_DEBUG
    case CTRL('K'):	/* send SIGTERM to select entry */
      if (opt_readonly)
	{
	  t_msgbox (_("This command is unusable in readonly mode."));
	  break;
	}
      t_admin_kill_select ();
      break;
#endif

    case ':':		/* execute a shell command */
      t_admin_command_window ();
      break;
    }

  /* refresh screen.
   */
  ungetch (CDK_REFRESH);
  t_screen->update = TRUE;

  return FALSE;
}

/** 
 * t_admin_screen_create:
 * @screen 
 */
static gboolean
t_admin_screen_create (void)
{
  gchar *message[10];
  gint i, j, len;
  gchar buff[1024], *p, *p2;
  struct winsize size;

  g_assert (!t_screen);

  t_screen = g_new0 (TScreen, 1);

  if (ioctl (STDOUT_FILENO, TIOCGWINSZ, &size) == 0)
    {
      t_screen->lines = size.ws_row;
      t_screen->cols = size.ws_col;
    }
  else
    {
      t_screen->lines = LINES;
      t_screen->cols = COLS;
    }
  t_screen->update = FALSE;
  t_screen->color = 7;

  t_screen->screen = initCDKScreen (stdscr);
  if (!t_screen->screen)
    {
      g_critical (_("initCDKScreen error."));
      g_free (t_screen);
      return FALSE;
    }

  /* header label.
   */
  message[0] = "</32>Power by <!32></56>xiaohu417@gmail.com<!56> && "
               "</56>http://www.ttyutils.net<!56> ......";
  message[1] = NULL;

  t_screen->head = newCDKLabel (t_screen->screen, 0, 0,
				message, 1, FALSE, FALSE);
  drawCDKLabel (t_screen->head, ObjOf (t_screen->head)->box);

  /* foot label.
   */
  p = "</R></B></33>Ttyutils Administrator<!33><!B> ";
  p2 = "(</B>?<!B>)Help (</B>^L<!B>)Refresh (</B>a<!B>)About (</B>q<!B>)Quit <!R>";

  len = t_screen->cols - 60;

  g_strlcpy (buff, p, sizeof (buff));

  if (len > strlen (PACKAGE_WEBSITE) + 2)
    {
      g_strlcat (buff, PACKAGE_WEBSITE, sizeof (buff));
      len -= strlen (PACKAGE_WEBSITE);
    }
  for (i = 0, j = strlen (buff); i < len; i++, j++)
    {
      buff[j] = ' ';
    }
  buff[j] = '\0';

  g_strlcat (buff, p2, sizeof (buff));

  message[0] = buff;
  message[1] = NULL;

  t_screen->foot = newCDKLabel (t_screen->screen, 0, t_screen->lines - 1,
				message, 1, FALSE, FALSE);
  drawCDKLabel (t_screen->foot, ObjOf (t_screen->foot)->box);

  /* scroll window.
   */
  g_snprintf (buff, sizeof (buff), "</R>%13s %8s %12s %12s %9s %s",
	      "USER", "PID", "TTY", "PTS", "TIME+", "COMMAND");
  for (i = strlen (buff); i < t_screen->cols + 4; i++)
    {
      buff[i] = ' ';
    }
  buff[i] = '\0';
  g_strlcat (buff, "<!R>", sizeof (buff));

  message[0] = "</2/B>NONE";
  message[1] = NULL;

  t_screen->scroll = newCDKScroll (t_screen->screen, 0, 1, RIGHT,
				   t_screen->lines - 2, t_screen->cols, buff,
				   message, 1, FALSE, 0, FALSE, FALSE);
  setCDKScrollHighlight (t_screen->scroll, A_REVERSE | A_BOLD);
  drawCDKScroll (t_screen->scroll, ObjOf (t_screen->scroll)->box);

  /* bind scroll keys.
   */
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 '?', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'h', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'q', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'a', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 KEY_UP, t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 KEY_DOWN, t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 SPACE, t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 KEY_RETURN, t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'c', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'u', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'U', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'p', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'P', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 't', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'T', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'm', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'M', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 CTRL('u'), t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 CTRL('D'), t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 CTRL('E'), t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 KEY_F(8), t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 KEY_F(7), t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'e', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'l', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 'L', t_scroll_keypressed, NULL);
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 's', t_scroll_keypressed, NULL);
#ifdef T_DEBUG
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 CTRL('K'), t_scroll_keypressed, NULL);
#endif
  bindCDKObject (ObjTypeOf (t_screen->scroll), t_screen->scroll,
		 ':', t_scroll_keypressed, NULL);

  t_screen->update = TRUE;

  return TRUE;
}

/** 
 * t_sigwinch_handler:
 * @signo: signal number
 */
static void
t_sigwinch_handler (gint signo)
{
  if (t_screen && t_screen->screen)
    {
      exitOKCDKScreen (t_screen->screen);
    }
}

/** 
 * t_sigterm_handler:
 * @signo: signal number
 */
static void
t_sigterm_handler (gint signo)
{
  t_admin_screen_destroy ();
  endCDK ();

  exit (0);
}

/** 
 * t_sigint_handler:
 * @signo: signal number
 */
static void
t_sigint_handler (gint signo)
{
  t_sigterm_handler (SIGTERM);
}

/** 
 * t_sigterm_handler:
 * @signo: signal number
 *
 * automatic update scroll list contents
 */
static void
t_sigalrm_handler (gint signo)
{
  if (t_screen && t_screen->scroll && t_screen->update)
    {
      t_admin_get_info ();
      t_admin_screen_update ();
      drawCDKScroll (t_screen->scroll, ObjOf (t_screen->scroll)->box);
    }
  alarm (opt_update);
}

/** 
 * t_admin_print_handler:
 * @string:
 */
static void
t_admin_print_handler (const gchar *string)
{
  if (t_logfile)
    {
      fprintf (t_logfile, "%s", string);
      fflush (t_logfile);
    }
}

/** 
 * t_admin_close_logfile:
 */
static void
t_admin_close_logfile (void)
{
  if (t_logfile)
    {
      fclose (t_logfile);
      t_logfile = NULL;
    }
}

/** 
 * t_admin_window:
 */
static gint
t_admin_window (void)
{
  struct sigaction sigact;

  if (!isatty (STDOUT_FILENO))
    {
      g_warning (_("stdout is not a terminal"));
      return 2;
    }

  sigact.sa_handler = t_sigwinch_handler;
  sigemptyset (&sigact.sa_mask);
  sigact.sa_flags = SA_RESTART;
  sigaction (SIGWINCH, &sigact, NULL);

  signal (SIGQUIT, SIG_IGN);
  signal (SIGINT, t_sigint_handler);
  signal (SIGTERM, t_sigterm_handler);

  if (opt_update > 0)
    {
      sigact.sa_handler = t_sigalrm_handler;
      sigemptyset (&sigact.sa_mask);
      sigact.sa_flags = SA_RESTART;
      sigaction (SIGALRM, &sigact, NULL);

      alarm (opt_update);
    }

  /* redirect all glib message to syslog and
   * redirect all g_print(err) message to file.
   */
  if (opt_logfile)
    {
      t_logfile = fopen (opt_logfile, "a+");
      if (!t_logfile)
	{
	  g_warning (_("open %s error: %s"), opt_logfile, g_strerror (errno));
	  return 1;
	}
      g_free (opt_logfile);
      opt_logfile = NULL;

      g_log_set_default_handler (t_debug_log_handler, t_logfile);
      g_atexit (t_admin_close_logfile);

      g_set_print_handler (t_admin_print_handler);
      g_set_printerr_handler (t_admin_print_handler);
    }
  else
    {
      openlog (g_get_prgname (), LOG_PID, LOG_LOCAL3);
      g_log_set_default_handler (t_debug_log_handler, NULL);

      g_set_print_handler (t_debug_print_handler);
      g_set_printerr_handler (t_debug_print_handler);
    }

  while (!t_quit)
    {
      initscr ();
      initCDKColor ();

      if (!t_admin_screen_create ())
	{
	  endCDK ();
	  return 1;
	}

      t_admin_get_info ();
      t_admin_screen_update ();

      refreshCDKScreen (t_screen->screen);
      traverseCDKScreen (t_screen->screen);

      t_admin_screen_destroy ();
      endCDK ();
    }

  return 0;
}

/** 
 * t_admin_list_entry:
 * @entry: a instance entry
 * @data: user data
 * 
 * @Returns: FALSE to continue, TRUE stop lookup.
 */
static gboolean
t_admin_list_entry (TShmemEntry *entry, gpointer data)
{
  struct passwd *pswd;

  g_return_val_if_fail (entry, FALSE);

  pswd = getpwuid (entry->uid);

  g_print ("%10s %8d %12s %12s %s\n",
	   pswd ? pswd->pw_name : _("Unknown"),
	   entry->pid, entry->tty, entry->pts, entry->command);

  return FALSE;
}

/** 
 * main:
 *
 * @Returns: 0 on success.
 */
gint
main (gint argc, gchar **argv)
{
  GOptionContext *opt_ctx;
  GError *error = NULL;

  t_set_locale ();

  g_set_prgname (argv[0]);
  g_set_application_name ("ttyadmin");

  /* parse command line options
   */
  opt_ctx = g_option_context_new (NULL);
#if GLIB_MINOR_VERSION >= 12
  g_option_context_set_summary (opt_ctx, _("Ttyutils administrator"));
#endif
  g_option_context_add_main_entries (opt_ctx, option_entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse (opt_ctx, &argc, &argv, &error))
    {
      if (error)
	{
	  g_warning (_("parse command line error: %s"), error->message);
	  g_error_free (error);
	}
      else
	{
	  g_warning (_("An unknown error occurred when parse command line."));
	}
      g_option_context_free (opt_ctx);
      return 1;
    }
  g_option_context_free (opt_ctx);
  opt_ctx = NULL;

  /* print version info and exit.
   */
  if (opt_version)
    {
      t_print_version ();
      return 0;
    }

  /* cleanup ipc resources
   */
  if (opt_ipcrm)
    {
      if (!t_shmem_open (TRUE))
	{
	  g_warning (_("connect to share memory failed."));
	  return 1;
	}

      if (t_shmem_get_nentries () > 0)
	{
	  gchar answer[32];

	  fprintf (stdout, _("remain %d entries, are you sure?%s"),
		   t_shmem_get_nentries (), "[yes/no]");
	  fgets (answer, sizeof (answer), stdin);
	  if (strncmp (answer, "yes", 3) != 0)
	    {
	      printf (_("give up!\n"));
	      return 0;
	    }
	}
      if (!t_shmem_remove ())
	{
	  printf (_("cleanup IPC sources failed. detail please see log.\n"));
	  return 1;
	}
      return 0;
    }

  /* cleanup invalid register entry.
   */
  if (opt_cleanup)
    {
      if (!t_shmem_open (TRUE))
	{
	  g_warning (_("connect to share memory failed."));
	  return 1;
	}
      t_shmem_cleanup ();
      t_shmem_close ();

      return 0;
    }

  /* print summary.
   */
  if (opt_summary)
    {
      if (!t_shmem_open (FALSE))
	{
	  g_warning (_("connect to share memory failed."));
	  return 1;
	}
      g_print (_("protocal %d, total %d entry.\n"),
	       t_shmem_get_version (), t_shmem_get_nentries ());

      g_print ("%10s %8s %12s %12s %s\n",
	       "USER", "PID", "TTY", "PTS", "COMMAND");

      t_shmem_lookup_entry (t_admin_list_entry, NULL);
      t_shmem_close ();

      return 0;
    }

  return t_admin_window ();
}
