/* TtyUtils  -- Unix Terminal Utilities
 * Copyright (C) 2008,2009,2010,2011 xiaohu, All rights reserved.
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General
 * Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>

#include "trpc.h"
#include "trpccall.h"
#include "twrap.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

/** 
 * t_rpc_call_snap_getrow:
 * @row:
 * @retstr:
 * 
 * @return 
 */
gboolean
t_rpc_call_snap_getrow (guint16   row,
			gchar   **retstr)
{
  gchar *str;
  guint16 rows, columns;
  gchar **snap = NULL;

  g_return_val_if_fail (retstr, FALSE);

  if (!t_rpc_call_snap (&rows, &columns, &snap))
    {
      g_warning (_("call t_rpc_call_snap() failed."));
      return FALSE;
    }
  g_assert (snap);

  if (row >= rows)
    {
      g_warning (_("row(%d) large than the screen size."), row);
      return FALSE;
    }

  str = g_malloc0 (columns + 1);
  g_strlcpy (str, snap[row], columns);
  g_strfreev (snap);

  *retstr = str;

  return TRUE;
}

/** 
 * t_rpc_call_snap_getxy:
 * @row: x cond
 * @col: y cond
 * @len: expected string length
 * @retstr: string buffer
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_rpc_call_snap_getxy (guint16 row,
		       guint16 col,
		       gsize   len,
		       gchar  *retstr)
{
  guint16 rows, columns, i;
  gchar **snap = NULL;

  g_return_val_if_fail (retstr, FALSE);

  if (!t_rpc_call_snap (&rows, &columns, &snap))
    {
      g_warning (_("call t_rpc_call_snap() failed."));
      return FALSE;
    }
  g_assert (snap);

  if (len == 0)
    {
      len = columns - col;
    }
  if (row >= rows || col + len > columns)
    {
      g_warning (_("row(%d) or column(%d) exceed the screen size."),
		 row, col);
      return FALSE;
    }
  for (i = 0; i < len; i++)
    {
      retstr[i] = snap[row][col + i];
    }
  g_strfreev (snap);

  return TRUE;
}

/** 
 * t_rpc_call_snap_getxy:
 * @row: x cond
 * @col: y cond
 * @len: expected string length
 * @retstr: string buffer
 * 
 * @Returns: TRUE on success.
 */
gboolean
t_getxy_from_snap(guint16    row,
		  guint16    col,
		  gsize      len,
		  gchar    **snap,
		  gchar     *retstr)
{
  guint16	i;

  g_return_val_if_fail (retstr, FALSE);

  g_assert (snap);

  if (len == 0)
    {
      len = strlen(snap[row]) - col;
    }
  for (i = 0; i < len; i++)
    {
      retstr[i] = snap[row][col + i];
    }

  return TRUE;
}
